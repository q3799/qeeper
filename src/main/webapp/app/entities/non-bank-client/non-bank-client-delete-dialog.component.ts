import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { INonBankClient } from 'app/shared/model/non-bank-client.model';
import { NonBankClientService } from './non-bank-client.service';

@Component({
  selector: 'jhi-non-bank-client-delete-dialog',
  templateUrl: './non-bank-client-delete-dialog.component.html'
})
export class NonBankClientDeleteDialogComponent {
  nonBankClient: INonBankClient;

  constructor(
    protected nonBankClientService: NonBankClientService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.nonBankClientService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'nonBankClientListModification',
        content: 'Deleted an nonBankClient'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-non-bank-client-delete-popup',
  template: ''
})
export class NonBankClientDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ nonBankClient }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(NonBankClientDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.nonBankClient = nonBankClient;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/non-bank-client', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/non-bank-client', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
