import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { NonBankClient } from 'app/shared/model/non-bank-client.model';
import { NonBankClientService } from './non-bank-client.service';
import { NonBankClientComponent } from './non-bank-client.component';
import { NonBankClientDetailComponent } from './non-bank-client-detail.component';
import { NonBankClientUpdateComponent } from './non-bank-client-update.component';
import { NonBankClientDeletePopupComponent } from './non-bank-client-delete-dialog.component';
import { INonBankClient } from 'app/shared/model/non-bank-client.model';

@Injectable({ providedIn: 'root' })
export class NonBankClientResolve implements Resolve<INonBankClient> {
  constructor(private service: NonBankClientService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<INonBankClient> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<NonBankClient>) => response.ok),
        map((nonBankClient: HttpResponse<NonBankClient>) => nonBankClient.body)
      );
    }
    return of(new NonBankClient());
  }
}

export const nonBankClientRoute: Routes = [
  {
    path: '',
    component: NonBankClientComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'qeeperApp.nonBankClient.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: NonBankClientDetailComponent,
    resolve: {
      nonBankClient: NonBankClientResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'qeeperApp.nonBankClient.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: NonBankClientUpdateComponent,
    resolve: {
      nonBankClient: NonBankClientResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'qeeperApp.nonBankClient.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: NonBankClientUpdateComponent,
    resolve: {
      nonBankClient: NonBankClientResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'qeeperApp.nonBankClient.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const nonBankClientPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: NonBankClientDeletePopupComponent,
    resolve: {
      nonBankClient: NonBankClientResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'qeeperApp.nonBankClient.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
