import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { INonBankClient, NonBankClient } from 'app/shared/model/non-bank-client.model';
import { NonBankClientService } from './non-bank-client.service';

@Component({
  selector: 'jhi-non-bank-client-update',
  templateUrl: './non-bank-client-update.component.html'
})
export class NonBankClientUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required, Validators.maxLength(100)]],
    url: [
      null,
      [
        Validators.maxLength(400),
        Validators.pattern('https?:\\/\\/(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%_\\+.~#?&\\/\\/=]*)')
      ]
    ]
  });

  constructor(protected nonBankClientService: NonBankClientService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ nonBankClient }) => {
      this.updateForm(nonBankClient);
    });
  }

  updateForm(nonBankClient: INonBankClient) {
    this.editForm.patchValue({
      id: nonBankClient.id,
      name: nonBankClient.name,
      url: nonBankClient.url
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const nonBankClient = this.createFromForm();
    if (nonBankClient.id !== undefined) {
      this.subscribeToSaveResponse(this.nonBankClientService.update(nonBankClient));
    } else {
      this.subscribeToSaveResponse(this.nonBankClientService.create(nonBankClient));
    }
  }

  private createFromForm(): INonBankClient {
    return {
      ...new NonBankClient(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      url: this.editForm.get(['url']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<INonBankClient>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
