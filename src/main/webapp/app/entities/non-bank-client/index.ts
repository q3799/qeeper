export * from './non-bank-client.service';
export * from './non-bank-client-update.component';
export * from './non-bank-client-delete-dialog.component';
export * from './non-bank-client-detail.component';
export * from './non-bank-client.component';
export * from './non-bank-client.route';
