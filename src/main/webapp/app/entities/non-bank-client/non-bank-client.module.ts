import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { QeeperSharedModule } from 'app/shared';
import {
  NonBankClientComponent,
  NonBankClientDetailComponent,
  NonBankClientUpdateComponent,
  NonBankClientDeletePopupComponent,
  NonBankClientDeleteDialogComponent,
  nonBankClientRoute,
  nonBankClientPopupRoute
} from './';

const ENTITY_STATES = [...nonBankClientRoute, ...nonBankClientPopupRoute];

@NgModule({
  imports: [QeeperSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    NonBankClientComponent,
    NonBankClientDetailComponent,
    NonBankClientUpdateComponent,
    NonBankClientDeleteDialogComponent,
    NonBankClientDeletePopupComponent
  ],
  entryComponents: [
    NonBankClientComponent,
    NonBankClientUpdateComponent,
    NonBankClientDeleteDialogComponent,
    NonBankClientDeletePopupComponent
  ],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class QeeperNonBankClientModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
