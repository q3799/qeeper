import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { INonBankClient } from 'app/shared/model/non-bank-client.model';

type EntityResponseType = HttpResponse<INonBankClient>;
type EntityArrayResponseType = HttpResponse<INonBankClient[]>;

@Injectable({ providedIn: 'root' })
export class NonBankClientService {
  public resourceUrl = SERVER_API_URL + 'api/non-bank-clients';

  constructor(protected http: HttpClient) {}

  create(nonBankClient: INonBankClient): Observable<EntityResponseType> {
    return this.http.post<INonBankClient>(this.resourceUrl, nonBankClient, { observe: 'response' });
  }

  update(nonBankClient: INonBankClient): Observable<EntityResponseType> {
    return this.http.put<INonBankClient>(this.resourceUrl, nonBankClient, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<INonBankClient>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<INonBankClient[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
