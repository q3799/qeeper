import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { INonBankClient } from 'app/shared/model/non-bank-client.model';

@Component({
  selector: 'jhi-non-bank-client-detail',
  templateUrl: './non-bank-client-detail.component.html'
})
export class NonBankClientDetailComponent implements OnInit {
  nonBankClient: INonBankClient;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ nonBankClient }) => {
      this.nonBankClient = nonBankClient;
    });
  }

  previousState() {
    window.history.back();
  }
}
