import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { INonBankAccount } from 'app/shared/model/non-bank-account.model';

@Component({
  selector: 'jhi-non-bank-account-detail',
  templateUrl: './non-bank-account-detail.component.html'
})
export class NonBankAccountDetailComponent implements OnInit {
  nonBankAccount: INonBankAccount;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ nonBankAccount }) => {
      this.nonBankAccount = nonBankAccount;
    });
  }

  previousState() {
    window.history.back();
  }
}
