import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { INonBankAccount } from 'app/shared/model/non-bank-account.model';
import { NonBankAccountService } from './non-bank-account.service';

@Component({
  selector: 'jhi-non-bank-account-delete-dialog',
  templateUrl: './non-bank-account-delete-dialog.component.html'
})
export class NonBankAccountDeleteDialogComponent {
  nonBankAccount: INonBankAccount;

  constructor(
    protected nonBankAccountService: NonBankAccountService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.nonBankAccountService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'nonBankAccountListModification',
        content: 'Deleted an nonBankAccount'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-non-bank-account-delete-popup',
  template: ''
})
export class NonBankAccountDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ nonBankAccount }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(NonBankAccountDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.nonBankAccount = nonBankAccount;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/non-bank-account', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/non-bank-account', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
