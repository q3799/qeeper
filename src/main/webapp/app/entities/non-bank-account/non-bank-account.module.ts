import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { QeeperSharedModule } from 'app/shared';
import {
  NonBankAccountComponent,
  NonBankAccountDetailComponent,
  NonBankAccountUpdateComponent,
  NonBankAccountDeletePopupComponent,
  NonBankAccountDeleteDialogComponent,
  nonBankAccountRoute,
  nonBankAccountPopupRoute
} from './';

const ENTITY_STATES = [...nonBankAccountRoute, ...nonBankAccountPopupRoute];

@NgModule({
  imports: [QeeperSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    NonBankAccountComponent,
    NonBankAccountDetailComponent,
    NonBankAccountUpdateComponent,
    NonBankAccountDeleteDialogComponent,
    NonBankAccountDeletePopupComponent
  ],
  entryComponents: [
    NonBankAccountComponent,
    NonBankAccountUpdateComponent,
    NonBankAccountDeleteDialogComponent,
    NonBankAccountDeletePopupComponent
  ],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class QeeperNonBankAccountModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
