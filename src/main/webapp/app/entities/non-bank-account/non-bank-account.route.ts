import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { NonBankAccount } from 'app/shared/model/non-bank-account.model';
import { NonBankAccountService } from './non-bank-account.service';
import { NonBankAccountComponent } from './non-bank-account.component';
import { NonBankAccountDetailComponent } from './non-bank-account-detail.component';
import { NonBankAccountUpdateComponent } from './non-bank-account-update.component';
import { NonBankAccountDeletePopupComponent } from './non-bank-account-delete-dialog.component';
import { INonBankAccount } from 'app/shared/model/non-bank-account.model';

@Injectable({ providedIn: 'root' })
export class NonBankAccountResolve implements Resolve<INonBankAccount> {
  constructor(private service: NonBankAccountService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<INonBankAccount> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<NonBankAccount>) => response.ok),
        map((nonBankAccount: HttpResponse<NonBankAccount>) => nonBankAccount.body)
      );
    }
    return of(new NonBankAccount());
  }
}

export const nonBankAccountRoute: Routes = [
  {
    path: '',
    component: NonBankAccountComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'qeeperApp.nonBankAccount.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: NonBankAccountDetailComponent,
    resolve: {
      nonBankAccount: NonBankAccountResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'qeeperApp.nonBankAccount.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: NonBankAccountUpdateComponent,
    resolve: {
      nonBankAccount: NonBankAccountResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'qeeperApp.nonBankAccount.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: NonBankAccountUpdateComponent,
    resolve: {
      nonBankAccount: NonBankAccountResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'qeeperApp.nonBankAccount.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const nonBankAccountPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: NonBankAccountDeletePopupComponent,
    resolve: {
      nonBankAccount: NonBankAccountResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'qeeperApp.nonBankAccount.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
