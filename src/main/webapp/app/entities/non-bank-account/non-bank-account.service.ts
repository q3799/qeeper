import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { INonBankAccount } from 'app/shared/model/non-bank-account.model';

type EntityResponseType = HttpResponse<INonBankAccount>;
type EntityArrayResponseType = HttpResponse<INonBankAccount[]>;

@Injectable({ providedIn: 'root' })
export class NonBankAccountService {
  public resourceUrl = SERVER_API_URL + 'api/non-bank-accounts';

  constructor(protected http: HttpClient) {}

  create(nonBankAccount: INonBankAccount): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(nonBankAccount);
    return this.http
      .post<INonBankAccount>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(nonBankAccount: INonBankAccount): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(nonBankAccount);
    return this.http
      .put<INonBankAccount>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<INonBankAccount>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<INonBankAccount[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(nonBankAccount: INonBankAccount): INonBankAccount {
    const copy: INonBankAccount = Object.assign({}, nonBankAccount, {
      passwordUpdatedOn:
        nonBankAccount.passwordUpdatedOn != null && nonBankAccount.passwordUpdatedOn.isValid()
          ? nonBankAccount.passwordUpdatedOn.toJSON()
          : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.passwordUpdatedOn = res.body.passwordUpdatedOn != null ? moment(res.body.passwordUpdatedOn) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((nonBankAccount: INonBankAccount) => {
        nonBankAccount.passwordUpdatedOn = nonBankAccount.passwordUpdatedOn != null ? moment(nonBankAccount.passwordUpdatedOn) : null;
      });
    }
    return res;
  }
}
