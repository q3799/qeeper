import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { INonBankAccount, NonBankAccount } from 'app/shared/model/non-bank-account.model';
import { NonBankAccountService } from './non-bank-account.service';
import { INonBankClient } from 'app/shared/model/non-bank-client.model';
import { NonBankClientService } from 'app/entities/non-bank-client';
import { INonBankAccountGroup } from 'app/shared/model/non-bank-account-group.model';
import { NonBankAccountGroupService } from 'app/entities/non-bank-account-group';

@Component({
  selector: 'jhi-non-bank-account-update',
  templateUrl: './non-bank-account-update.component.html'
})
export class NonBankAccountUpdateComponent implements OnInit {
  isSaving: boolean;

  nonbankclients: INonBankClient[];

  nonbankaccountgroups: INonBankAccountGroup[];

  editForm = this.fb.group({
    id: [],
    loginId: [null, [Validators.required, Validators.maxLength(50)]],
    loginPassword: [null, [Validators.required, Validators.maxLength(50)]],
    name: [null, [Validators.maxLength(100)]],
    email: [
      null,
      [
        Validators.required,
        Validators.maxLength(200),
        Validators.pattern(
          '^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$'
        )
      ]
    ],
    phone: [null, [Validators.pattern('^\\+?[1-9]\\d{1,14}$')]],
    passwordGetsExpired: [null, [Validators.required]],
    passwordUpdatedOn: [],
    expirationPeriodDays: [null, [Validators.min(0)]],
    accountStatus: [null, [Validators.required]],
    remarks: [null, [Validators.maxLength(1000)]],
    multipleSessionAllowed: [null, [Validators.required]],
    accountId: [null, [Validators.maxLength(100)]],
    nonBankClientId: [null, Validators.required],
    nonBankAccountGroupId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected nonBankAccountService: NonBankAccountService,
    protected nonBankClientService: NonBankClientService,
    protected nonBankAccountGroupService: NonBankAccountGroupService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ nonBankAccount }) => {
      this.updateForm(nonBankAccount);
    });
    this.nonBankClientService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<INonBankClient[]>) => mayBeOk.ok),
        map((response: HttpResponse<INonBankClient[]>) => response.body)
      )
      .subscribe((res: INonBankClient[]) => (this.nonbankclients = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.nonBankAccountGroupService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<INonBankAccountGroup[]>) => mayBeOk.ok),
        map((response: HttpResponse<INonBankAccountGroup[]>) => response.body)
      )
      .subscribe((res: INonBankAccountGroup[]) => (this.nonbankaccountgroups = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(nonBankAccount: INonBankAccount) {
    this.editForm.patchValue({
      id: nonBankAccount.id,
      loginId: nonBankAccount.loginId,
      loginPassword: nonBankAccount.loginPassword,
      name: nonBankAccount.name,
      email: nonBankAccount.email,
      phone: nonBankAccount.phone,
      passwordGetsExpired: nonBankAccount.passwordGetsExpired,
      passwordUpdatedOn: nonBankAccount.passwordUpdatedOn != null ? nonBankAccount.passwordUpdatedOn.format(DATE_TIME_FORMAT) : null,
      expirationPeriodDays: nonBankAccount.expirationPeriodDays,
      accountStatus: nonBankAccount.accountStatus,
      remarks: nonBankAccount.remarks,
      multipleSessionAllowed: nonBankAccount.multipleSessionAllowed,
      accountId: nonBankAccount.accountId,
      nonBankClientId: nonBankAccount.nonBankClientId,
      nonBankAccountGroupId: nonBankAccount.nonBankAccountGroupId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const nonBankAccount = this.createFromForm();
    if (nonBankAccount.id !== undefined) {
      this.subscribeToSaveResponse(this.nonBankAccountService.update(nonBankAccount));
    } else {
      this.subscribeToSaveResponse(this.nonBankAccountService.create(nonBankAccount));
    }
  }

  private createFromForm(): INonBankAccount {
    return {
      ...new NonBankAccount(),
      id: this.editForm.get(['id']).value,
      loginId: this.editForm.get(['loginId']).value,
      loginPassword: this.editForm.get(['loginPassword']).value,
      name: this.editForm.get(['name']).value,
      email: this.editForm.get(['email']).value,
      phone: this.editForm.get(['phone']).value,
      passwordGetsExpired: this.editForm.get(['passwordGetsExpired']).value,
      passwordUpdatedOn:
        this.editForm.get(['passwordUpdatedOn']).value != null
          ? moment(this.editForm.get(['passwordUpdatedOn']).value, DATE_TIME_FORMAT)
          : undefined,
      expirationPeriodDays: this.editForm.get(['expirationPeriodDays']).value,
      accountStatus: this.editForm.get(['accountStatus']).value,
      remarks: this.editForm.get(['remarks']).value,
      multipleSessionAllowed: this.editForm.get(['multipleSessionAllowed']).value,
      accountId: this.editForm.get(['accountId']).value,
      nonBankClientId: this.editForm.get(['nonBankClientId']).value,
      nonBankAccountGroupId: this.editForm.get(['nonBankAccountGroupId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<INonBankAccount>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackNonBankClientById(index: number, item: INonBankClient) {
    return item.id;
  }

  trackNonBankAccountGroupById(index: number, item: INonBankAccountGroup) {
    return item.id;
  }
}
