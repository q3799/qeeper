import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IBankBranch, BankBranch } from 'app/shared/model/bank-branch.model';
import { BankBranchService } from './bank-branch.service';
import { IBank } from 'app/shared/model/bank.model';
import { BankService } from 'app/entities/bank';

@Component({
  selector: 'jhi-bank-branch-update',
  templateUrl: './bank-branch-update.component.html'
})
export class BankBranchUpdateComponent implements OnInit {
  isSaving: boolean;

  banks: IBank[];

  editForm = this.fb.group({
    id: [],
    location: [null, [Validators.required, Validators.maxLength(100)]],
    address: [null, [Validators.maxLength(1000)]],
    ifscCode: [null, [Validators.maxLength(50), Validators.pattern('^[A-Za-z]{4}[a-zA-Z0-9]{7}$')]],
    micrCode: [null, [Validators.maxLength(50)]],
    remarks: [null, [Validators.maxLength(1000)]],
    bankId: [null, Validators.required]
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected bankBranchService: BankBranchService,
    protected bankService: BankService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ bankBranch }) => {
      this.updateForm(bankBranch);
    });
    this.bankService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IBank[]>) => mayBeOk.ok),
        map((response: HttpResponse<IBank[]>) => response.body)
      )
      .subscribe((res: IBank[]) => (this.banks = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(bankBranch: IBankBranch) {
    this.editForm.patchValue({
      id: bankBranch.id,
      location: bankBranch.location,
      address: bankBranch.address,
      ifscCode: bankBranch.ifscCode,
      micrCode: bankBranch.micrCode,
      remarks: bankBranch.remarks,
      bankId: bankBranch.bankId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const bankBranch = this.createFromForm();
    if (bankBranch.id !== undefined) {
      this.subscribeToSaveResponse(this.bankBranchService.update(bankBranch));
    } else {
      this.subscribeToSaveResponse(this.bankBranchService.create(bankBranch));
    }
  }

  private createFromForm(): IBankBranch {
    return {
      ...new BankBranch(),
      id: this.editForm.get(['id']).value,
      location: this.editForm.get(['location']).value,
      address: this.editForm.get(['address']).value,
      ifscCode: this.editForm.get(['ifscCode']).value,
      micrCode: this.editForm.get(['micrCode']).value,
      remarks: this.editForm.get(['remarks']).value,
      bankId: this.editForm.get(['bankId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBankBranch>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackBankById(index: number, item: IBank) {
    return item.id;
  }
}
