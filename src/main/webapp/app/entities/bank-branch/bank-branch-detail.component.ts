import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IBankBranch } from 'app/shared/model/bank-branch.model';

@Component({
  selector: 'jhi-bank-branch-detail',
  templateUrl: './bank-branch-detail.component.html'
})
export class BankBranchDetailComponent implements OnInit {
  bankBranch: IBankBranch;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ bankBranch }) => {
      this.bankBranch = bankBranch;
    });
  }

  previousState() {
    window.history.back();
  }
}
