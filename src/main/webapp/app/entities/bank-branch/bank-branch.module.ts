import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { QeeperSharedModule } from 'app/shared';
import {
  BankBranchComponent,
  BankBranchDetailComponent,
  BankBranchUpdateComponent,
  BankBranchDeletePopupComponent,
  BankBranchDeleteDialogComponent,
  bankBranchRoute,
  bankBranchPopupRoute
} from './';

const ENTITY_STATES = [...bankBranchRoute, ...bankBranchPopupRoute];

@NgModule({
  imports: [QeeperSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    BankBranchComponent,
    BankBranchDetailComponent,
    BankBranchUpdateComponent,
    BankBranchDeleteDialogComponent,
    BankBranchDeletePopupComponent
  ],
  entryComponents: [BankBranchComponent, BankBranchUpdateComponent, BankBranchDeleteDialogComponent, BankBranchDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class QeeperBankBranchModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
