import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { BankBranch } from 'app/shared/model/bank-branch.model';
import { BankBranchService } from './bank-branch.service';
import { BankBranchComponent } from './bank-branch.component';
import { BankBranchDetailComponent } from './bank-branch-detail.component';
import { BankBranchUpdateComponent } from './bank-branch-update.component';
import { BankBranchDeletePopupComponent } from './bank-branch-delete-dialog.component';
import { IBankBranch } from 'app/shared/model/bank-branch.model';

@Injectable({ providedIn: 'root' })
export class BankBranchResolve implements Resolve<IBankBranch> {
  constructor(private service: BankBranchService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IBankBranch> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<BankBranch>) => response.ok),
        map((bankBranch: HttpResponse<BankBranch>) => bankBranch.body)
      );
    }
    return of(new BankBranch());
  }
}

export const bankBranchRoute: Routes = [
  {
    path: '',
    component: BankBranchComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'qeeperApp.bankBranch.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: BankBranchDetailComponent,
    resolve: {
      bankBranch: BankBranchResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'qeeperApp.bankBranch.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: BankBranchUpdateComponent,
    resolve: {
      bankBranch: BankBranchResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'qeeperApp.bankBranch.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: BankBranchUpdateComponent,
    resolve: {
      bankBranch: BankBranchResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'qeeperApp.bankBranch.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const bankBranchPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: BankBranchDeletePopupComponent,
    resolve: {
      bankBranch: BankBranchResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'qeeperApp.bankBranch.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
