import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IBankBranch } from 'app/shared/model/bank-branch.model';
import { BankBranchService } from './bank-branch.service';

@Component({
  selector: 'jhi-bank-branch-delete-dialog',
  templateUrl: './bank-branch-delete-dialog.component.html'
})
export class BankBranchDeleteDialogComponent {
  bankBranch: IBankBranch;

  constructor(
    protected bankBranchService: BankBranchService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.bankBranchService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'bankBranchListModification',
        content: 'Deleted an bankBranch'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-bank-branch-delete-popup',
  template: ''
})
export class BankBranchDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ bankBranch }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(BankBranchDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.bankBranch = bankBranch;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/bank-branch', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/bank-branch', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
