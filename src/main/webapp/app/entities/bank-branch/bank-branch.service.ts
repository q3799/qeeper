import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IBankBranch } from 'app/shared/model/bank-branch.model';

type EntityResponseType = HttpResponse<IBankBranch>;
type EntityArrayResponseType = HttpResponse<IBankBranch[]>;

@Injectable({ providedIn: 'root' })
export class BankBranchService {
  public resourceUrl = SERVER_API_URL + 'api/bank-branches';

  constructor(protected http: HttpClient) {}

  create(bankBranch: IBankBranch): Observable<EntityResponseType> {
    return this.http.post<IBankBranch>(this.resourceUrl, bankBranch, { observe: 'response' });
  }

  update(bankBranch: IBankBranch): Observable<EntityResponseType> {
    return this.http.put<IBankBranch>(this.resourceUrl, bankBranch, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IBankBranch>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IBankBranch[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
