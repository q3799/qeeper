import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICardGroup } from 'app/shared/model/card-group.model';

@Component({
  selector: 'jhi-card-group-detail',
  templateUrl: './card-group-detail.component.html'
})
export class CardGroupDetailComponent implements OnInit {
  cardGroup: ICardGroup;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ cardGroup }) => {
      this.cardGroup = cardGroup;
    });
  }

  previousState() {
    window.history.back();
  }
}
