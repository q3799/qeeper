import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { CardGroup } from 'app/shared/model/card-group.model';
import { CardGroupService } from './card-group.service';
import { CardGroupComponent } from './card-group.component';
import { CardGroupDetailComponent } from './card-group-detail.component';
import { CardGroupUpdateComponent } from './card-group-update.component';
import { CardGroupDeletePopupComponent } from './card-group-delete-dialog.component';
import { ICardGroup } from 'app/shared/model/card-group.model';

@Injectable({ providedIn: 'root' })
export class CardGroupResolve implements Resolve<ICardGroup> {
  constructor(private service: CardGroupService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ICardGroup> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<CardGroup>) => response.ok),
        map((cardGroup: HttpResponse<CardGroup>) => cardGroup.body)
      );
    }
    return of(new CardGroup());
  }
}

export const cardGroupRoute: Routes = [
  {
    path: '',
    component: CardGroupComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'qeeperApp.cardGroup.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: CardGroupDetailComponent,
    resolve: {
      cardGroup: CardGroupResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'qeeperApp.cardGroup.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: CardGroupUpdateComponent,
    resolve: {
      cardGroup: CardGroupResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'qeeperApp.cardGroup.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: CardGroupUpdateComponent,
    resolve: {
      cardGroup: CardGroupResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'qeeperApp.cardGroup.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const cardGroupPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: CardGroupDeletePopupComponent,
    resolve: {
      cardGroup: CardGroupResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'qeeperApp.cardGroup.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
