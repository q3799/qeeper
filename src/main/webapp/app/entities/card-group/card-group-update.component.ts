import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { ICardGroup, CardGroup } from 'app/shared/model/card-group.model';
import { CardGroupService } from './card-group.service';

@Component({
  selector: 'jhi-card-group-update',
  templateUrl: './card-group-update.component.html'
})
export class CardGroupUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    code: [null, [Validators.required, Validators.maxLength(50)]],
    name: [null, [Validators.required, Validators.maxLength(100)]]
  });

  constructor(protected cardGroupService: CardGroupService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ cardGroup }) => {
      this.updateForm(cardGroup);
    });
  }

  updateForm(cardGroup: ICardGroup) {
    this.editForm.patchValue({
      id: cardGroup.id,
      code: cardGroup.code,
      name: cardGroup.name
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const cardGroup = this.createFromForm();
    if (cardGroup.id !== undefined) {
      this.subscribeToSaveResponse(this.cardGroupService.update(cardGroup));
    } else {
      this.subscribeToSaveResponse(this.cardGroupService.create(cardGroup));
    }
  }

  private createFromForm(): ICardGroup {
    return {
      ...new CardGroup(),
      id: this.editForm.get(['id']).value,
      code: this.editForm.get(['code']).value,
      name: this.editForm.get(['name']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICardGroup>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
