import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { QeeperSharedModule } from 'app/shared';
import {
  CardGroupComponent,
  CardGroupDetailComponent,
  CardGroupUpdateComponent,
  CardGroupDeletePopupComponent,
  CardGroupDeleteDialogComponent,
  cardGroupRoute,
  cardGroupPopupRoute
} from './';

const ENTITY_STATES = [...cardGroupRoute, ...cardGroupPopupRoute];

@NgModule({
  imports: [QeeperSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    CardGroupComponent,
    CardGroupDetailComponent,
    CardGroupUpdateComponent,
    CardGroupDeleteDialogComponent,
    CardGroupDeletePopupComponent
  ],
  entryComponents: [CardGroupComponent, CardGroupUpdateComponent, CardGroupDeleteDialogComponent, CardGroupDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class QeeperCardGroupModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
