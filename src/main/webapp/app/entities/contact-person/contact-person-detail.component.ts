import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IContactPerson } from 'app/shared/model/contact-person.model';

@Component({
  selector: 'jhi-contact-person-detail',
  templateUrl: './contact-person-detail.component.html'
})
export class ContactPersonDetailComponent implements OnInit {
  contactPerson: IContactPerson;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ contactPerson }) => {
      this.contactPerson = contactPerson;
    });
  }

  previousState() {
    window.history.back();
  }
}
