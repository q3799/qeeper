export * from './contact-person.service';
export * from './contact-person-update.component';
export * from './contact-person-delete-dialog.component';
export * from './contact-person-detail.component';
export * from './contact-person.component';
export * from './contact-person.route';
