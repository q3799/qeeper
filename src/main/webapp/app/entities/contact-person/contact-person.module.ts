import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { QeeperSharedModule } from 'app/shared';
import {
  ContactPersonComponent,
  ContactPersonDetailComponent,
  ContactPersonUpdateComponent,
  ContactPersonDeletePopupComponent,
  ContactPersonDeleteDialogComponent,
  contactPersonRoute,
  contactPersonPopupRoute
} from './';

const ENTITY_STATES = [...contactPersonRoute, ...contactPersonPopupRoute];

@NgModule({
  imports: [QeeperSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    ContactPersonComponent,
    ContactPersonDetailComponent,
    ContactPersonUpdateComponent,
    ContactPersonDeleteDialogComponent,
    ContactPersonDeletePopupComponent
  ],
  entryComponents: [
    ContactPersonComponent,
    ContactPersonUpdateComponent,
    ContactPersonDeleteDialogComponent,
    ContactPersonDeletePopupComponent
  ],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class QeeperContactPersonModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
