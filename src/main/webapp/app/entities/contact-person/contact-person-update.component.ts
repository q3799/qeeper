import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IContactPerson, ContactPerson } from 'app/shared/model/contact-person.model';
import { ContactPersonService } from './contact-person.service';

@Component({
  selector: 'jhi-contact-person-update',
  templateUrl: './contact-person-update.component.html'
})
export class ContactPersonUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required, Validators.maxLength(100)]],
    primaryEmail: [
      null,
      [
        Validators.required,
        Validators.maxLength(400),
        Validators.pattern(
          '^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$'
        )
      ]
    ],
    secondaryEmail: [
      null,
      [
        Validators.maxLength(400),
        Validators.pattern(
          '^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$'
        )
      ]
    ],
    primaryPhone: [null, [Validators.required, Validators.maxLength(20), Validators.pattern('^\\+?[1-9]\\d{1,14}$')]],
    secondaryPhone: [null, [Validators.maxLength(20), Validators.pattern('^\\+?[1-9]\\d{1,14}$')]],
    active: [null, [Validators.required]]
  });

  constructor(protected contactPersonService: ContactPersonService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ contactPerson }) => {
      this.updateForm(contactPerson);
    });
  }

  updateForm(contactPerson: IContactPerson) {
    this.editForm.patchValue({
      id: contactPerson.id,
      name: contactPerson.name,
      primaryEmail: contactPerson.primaryEmail,
      secondaryEmail: contactPerson.secondaryEmail,
      primaryPhone: contactPerson.primaryPhone,
      secondaryPhone: contactPerson.secondaryPhone,
      active: contactPerson.active
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const contactPerson = this.createFromForm();
    if (contactPerson.id !== undefined) {
      this.subscribeToSaveResponse(this.contactPersonService.update(contactPerson));
    } else {
      this.subscribeToSaveResponse(this.contactPersonService.create(contactPerson));
    }
  }

  private createFromForm(): IContactPerson {
    return {
      ...new ContactPerson(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      primaryEmail: this.editForm.get(['primaryEmail']).value,
      secondaryEmail: this.editForm.get(['secondaryEmail']).value,
      primaryPhone: this.editForm.get(['primaryPhone']).value,
      secondaryPhone: this.editForm.get(['secondaryPhone']).value,
      active: this.editForm.get(['active']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IContactPerson>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
