import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ContactPerson } from 'app/shared/model/contact-person.model';
import { ContactPersonService } from './contact-person.service';
import { ContactPersonComponent } from './contact-person.component';
import { ContactPersonDetailComponent } from './contact-person-detail.component';
import { ContactPersonUpdateComponent } from './contact-person-update.component';
import { ContactPersonDeletePopupComponent } from './contact-person-delete-dialog.component';
import { IContactPerson } from 'app/shared/model/contact-person.model';

@Injectable({ providedIn: 'root' })
export class ContactPersonResolve implements Resolve<IContactPerson> {
  constructor(private service: ContactPersonService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IContactPerson> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<ContactPerson>) => response.ok),
        map((contactPerson: HttpResponse<ContactPerson>) => contactPerson.body)
      );
    }
    return of(new ContactPerson());
  }
}

export const contactPersonRoute: Routes = [
  {
    path: '',
    component: ContactPersonComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'qeeperApp.contactPerson.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ContactPersonDetailComponent,
    resolve: {
      contactPerson: ContactPersonResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'qeeperApp.contactPerson.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ContactPersonUpdateComponent,
    resolve: {
      contactPerson: ContactPersonResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'qeeperApp.contactPerson.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ContactPersonUpdateComponent,
    resolve: {
      contactPerson: ContactPersonResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'qeeperApp.contactPerson.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const contactPersonPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: ContactPersonDeletePopupComponent,
    resolve: {
      contactPerson: ContactPersonResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'qeeperApp.contactPerson.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
