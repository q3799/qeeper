import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IContactPerson } from 'app/shared/model/contact-person.model';
import { ContactPersonService } from './contact-person.service';

@Component({
  selector: 'jhi-contact-person-delete-dialog',
  templateUrl: './contact-person-delete-dialog.component.html'
})
export class ContactPersonDeleteDialogComponent {
  contactPerson: IContactPerson;

  constructor(
    protected contactPersonService: ContactPersonService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.contactPersonService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'contactPersonListModification',
        content: 'Deleted an contactPerson'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-contact-person-delete-popup',
  template: ''
})
export class ContactPersonDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ contactPerson }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(ContactPersonDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.contactPerson = contactPerson;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/contact-person', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/contact-person', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
