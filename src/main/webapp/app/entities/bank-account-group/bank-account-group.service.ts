import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IBankAccountGroup } from 'app/shared/model/bank-account-group.model';

type EntityResponseType = HttpResponse<IBankAccountGroup>;
type EntityArrayResponseType = HttpResponse<IBankAccountGroup[]>;

@Injectable({ providedIn: 'root' })
export class BankAccountGroupService {
  public resourceUrl = SERVER_API_URL + 'api/bank-account-groups';

  constructor(protected http: HttpClient) {}

  create(bankAccountGroup: IBankAccountGroup): Observable<EntityResponseType> {
    return this.http.post<IBankAccountGroup>(this.resourceUrl, bankAccountGroup, { observe: 'response' });
  }

  update(bankAccountGroup: IBankAccountGroup): Observable<EntityResponseType> {
    return this.http.put<IBankAccountGroup>(this.resourceUrl, bankAccountGroup, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IBankAccountGroup>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IBankAccountGroup[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
