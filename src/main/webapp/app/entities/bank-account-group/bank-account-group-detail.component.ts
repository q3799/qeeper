import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IBankAccountGroup } from 'app/shared/model/bank-account-group.model';

@Component({
  selector: 'jhi-bank-account-group-detail',
  templateUrl: './bank-account-group-detail.component.html'
})
export class BankAccountGroupDetailComponent implements OnInit {
  bankAccountGroup: IBankAccountGroup;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ bankAccountGroup }) => {
      this.bankAccountGroup = bankAccountGroup;
    });
  }

  previousState() {
    window.history.back();
  }
}
