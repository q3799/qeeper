import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IBankAccountGroup } from 'app/shared/model/bank-account-group.model';
import { BankAccountGroupService } from './bank-account-group.service';

@Component({
  selector: 'jhi-bank-account-group-delete-dialog',
  templateUrl: './bank-account-group-delete-dialog.component.html'
})
export class BankAccountGroupDeleteDialogComponent {
  bankAccountGroup: IBankAccountGroup;

  constructor(
    protected bankAccountGroupService: BankAccountGroupService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.bankAccountGroupService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'bankAccountGroupListModification',
        content: 'Deleted an bankAccountGroup'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-bank-account-group-delete-popup',
  template: ''
})
export class BankAccountGroupDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ bankAccountGroup }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(BankAccountGroupDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.bankAccountGroup = bankAccountGroup;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/bank-account-group', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/bank-account-group', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
