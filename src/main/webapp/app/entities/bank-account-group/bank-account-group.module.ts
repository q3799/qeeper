import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { QeeperSharedModule } from 'app/shared';
import {
  BankAccountGroupComponent,
  BankAccountGroupDetailComponent,
  BankAccountGroupUpdateComponent,
  BankAccountGroupDeletePopupComponent,
  BankAccountGroupDeleteDialogComponent,
  bankAccountGroupRoute,
  bankAccountGroupPopupRoute
} from './';

const ENTITY_STATES = [...bankAccountGroupRoute, ...bankAccountGroupPopupRoute];

@NgModule({
  imports: [QeeperSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    BankAccountGroupComponent,
    BankAccountGroupDetailComponent,
    BankAccountGroupUpdateComponent,
    BankAccountGroupDeleteDialogComponent,
    BankAccountGroupDeletePopupComponent
  ],
  entryComponents: [
    BankAccountGroupComponent,
    BankAccountGroupUpdateComponent,
    BankAccountGroupDeleteDialogComponent,
    BankAccountGroupDeletePopupComponent
  ],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class QeeperBankAccountGroupModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
