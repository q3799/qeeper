import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { BankAccountGroup } from 'app/shared/model/bank-account-group.model';
import { BankAccountGroupService } from './bank-account-group.service';
import { BankAccountGroupComponent } from './bank-account-group.component';
import { BankAccountGroupDetailComponent } from './bank-account-group-detail.component';
import { BankAccountGroupUpdateComponent } from './bank-account-group-update.component';
import { BankAccountGroupDeletePopupComponent } from './bank-account-group-delete-dialog.component';
import { IBankAccountGroup } from 'app/shared/model/bank-account-group.model';

@Injectable({ providedIn: 'root' })
export class BankAccountGroupResolve implements Resolve<IBankAccountGroup> {
  constructor(private service: BankAccountGroupService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IBankAccountGroup> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<BankAccountGroup>) => response.ok),
        map((bankAccountGroup: HttpResponse<BankAccountGroup>) => bankAccountGroup.body)
      );
    }
    return of(new BankAccountGroup());
  }
}

export const bankAccountGroupRoute: Routes = [
  {
    path: '',
    component: BankAccountGroupComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'qeeperApp.bankAccountGroup.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: BankAccountGroupDetailComponent,
    resolve: {
      bankAccountGroup: BankAccountGroupResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'qeeperApp.bankAccountGroup.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: BankAccountGroupUpdateComponent,
    resolve: {
      bankAccountGroup: BankAccountGroupResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'qeeperApp.bankAccountGroup.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: BankAccountGroupUpdateComponent,
    resolve: {
      bankAccountGroup: BankAccountGroupResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'qeeperApp.bankAccountGroup.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const bankAccountGroupPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: BankAccountGroupDeletePopupComponent,
    resolve: {
      bankAccountGroup: BankAccountGroupResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'qeeperApp.bankAccountGroup.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
