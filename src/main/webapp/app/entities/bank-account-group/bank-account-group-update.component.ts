import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IBankAccountGroup, BankAccountGroup } from 'app/shared/model/bank-account-group.model';
import { BankAccountGroupService } from './bank-account-group.service';

@Component({
  selector: 'jhi-bank-account-group-update',
  templateUrl: './bank-account-group-update.component.html'
})
export class BankAccountGroupUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    code: [null, [Validators.required, Validators.maxLength(50)]],
    name: [null, [Validators.required, Validators.maxLength(100)]]
  });

  constructor(
    protected bankAccountGroupService: BankAccountGroupService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ bankAccountGroup }) => {
      this.updateForm(bankAccountGroup);
    });
  }

  updateForm(bankAccountGroup: IBankAccountGroup) {
    this.editForm.patchValue({
      id: bankAccountGroup.id,
      code: bankAccountGroup.code,
      name: bankAccountGroup.name
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const bankAccountGroup = this.createFromForm();
    if (bankAccountGroup.id !== undefined) {
      this.subscribeToSaveResponse(this.bankAccountGroupService.update(bankAccountGroup));
    } else {
      this.subscribeToSaveResponse(this.bankAccountGroupService.create(bankAccountGroup));
    }
  }

  private createFromForm(): IBankAccountGroup {
    return {
      ...new BankAccountGroup(),
      id: this.editForm.get(['id']).value,
      code: this.editForm.get(['code']).value,
      name: this.editForm.get(['name']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBankAccountGroup>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
