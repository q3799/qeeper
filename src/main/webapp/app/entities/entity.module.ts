import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'bank',
        loadChildren: './bank/bank.module#QeeperBankModule'
      },
      {
        path: 'contact-person',
        loadChildren: './contact-person/contact-person.module#QeeperContactPersonModule'
      },
      {
        path: 'escalation-matrix',
        loadChildren: './escalation-matrix/escalation-matrix.module#QeeperEscalationMatrixModule'
      },
      {
        path: 'bank-branch',
        loadChildren: './bank-branch/bank-branch.module#QeeperBankBranchModule'
      },
      {
        path: 'mobile-number',
        loadChildren: './mobile-number/mobile-number.module#QeeperMobileNumberModule'
      },
      {
        path: 'employee',
        loadChildren: './employee/employee.module#QeeperEmployeeModule'
      },
      {
        path: 'bank-account',
        loadChildren: './bank-account/bank-account.module#QeeperBankAccountModule'
      },
      {
        path: 'card',
        loadChildren: './card/card.module#QeeperCardModule'
      },
      {
        path: 'mail-log',
        loadChildren: './mail-log/mail-log.module#QeeperMailLogModule'
      },
      {
        path: 'non-bank-client',
        loadChildren: './non-bank-client/non-bank-client.module#QeeperNonBankClientModule'
      },
      {
        path: 'non-bank-account',
        loadChildren: './non-bank-account/non-bank-account.module#QeeperNonBankAccountModule'
      },
      {
        path: 'card-group',
        loadChildren: './card-group/card-group.module#QeeperCardGroupModule'
      },
      {
        path: 'bank-account-group',
        loadChildren: './bank-account-group/bank-account-group.module#QeeperBankAccountGroupModule'
      },
      {
        path: 'non-bank-account-group',
        loadChildren: './non-bank-account-group/non-bank-account-group.module#QeeperNonBankAccountGroupModule'
      },
      {
        path: 'secret-question',
        loadChildren: './secret-question/secret-question.module#QeeperSecretQuestionModule'
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ],
  declarations: [],
  entryComponents: [],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class QeeperEntityModule {}
