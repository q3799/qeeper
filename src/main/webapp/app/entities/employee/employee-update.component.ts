import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IEmployee, Employee } from 'app/shared/model/employee.model';
import { EmployeeService } from './employee.service';

@Component({
  selector: 'jhi-employee-update',
  templateUrl: './employee-update.component.html'
})
export class EmployeeUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required, Validators.maxLength(100)]],
    pan: [null, [Validators.pattern('^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$')]],
    dateOfBirth: [
      null,
      [
        Validators.pattern(
          '(((0[1-9]|[12][0-9]|3[01])([/])(0[13578]|10|12)([/])(\\d{4}))|(([0][1-9]|[12][0-9]|30)([/])(0[469]|11)([/])(\\d{4}))|((0[1-9]|1[0-9]|2[0-8])([/])(02)([/])(\\d{4}))|((29)(\\.|-|\\/)(02)([/])([02468][048]00))|((29)([/])(02)([/])([13579][26]00))|((29)([/])(02)([/])([0-9][0-9][0][48]))|((29)([/])(02)([/])([0-9][0-9][2468][048]))|((29)([/])(02)([/])([0-9][0-9][13579][26])))'
        )
      ]
    ],
    aadhaar: [null, [Validators.pattern('^\\d{4}\\s?\\d{4}\\s?\\d{4}$')]],
    email: [
      null,
      [
        Validators.maxLength(400),
        Validators.pattern(
          '^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$'
        )
      ]
    ],
    phone: [null, [Validators.pattern('^\\+?[1-9]\\d{1,14}$')]],
    communicationAddress: [null, [Validators.maxLength(1000)]],
    permanentAddress: [null, [Validators.maxLength(1000)]],
    remarks: [null, [Validators.maxLength(2000)]],
    active: [null, [Validators.required]]
  });

  constructor(protected employeeService: EmployeeService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ employee }) => {
      this.updateForm(employee);
    });
  }

  updateForm(employee: IEmployee) {
    this.editForm.patchValue({
      id: employee.id,
      name: employee.name,
      pan: employee.pan,
      dateOfBirth: employee.dateOfBirth,
      aadhaar: employee.aadhaar,
      email: employee.email,
      phone: employee.phone,
      communicationAddress: employee.communicationAddress,
      permanentAddress: employee.permanentAddress,
      remarks: employee.remarks,
      active: employee.active
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const employee = this.createFromForm();
    if (employee.id !== undefined) {
      this.subscribeToSaveResponse(this.employeeService.update(employee));
    } else {
      this.subscribeToSaveResponse(this.employeeService.create(employee));
    }
  }

  private createFromForm(): IEmployee {
    return {
      ...new Employee(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      pan: this.editForm.get(['pan']).value,
      dateOfBirth: this.editForm.get(['dateOfBirth']).value,
      aadhaar: this.editForm.get(['aadhaar']).value,
      email: this.editForm.get(['email']).value,
      phone: this.editForm.get(['phone']).value,
      communicationAddress: this.editForm.get(['communicationAddress']).value,
      permanentAddress: this.editForm.get(['permanentAddress']).value,
      remarks: this.editForm.get(['remarks']).value,
      active: this.editForm.get(['active']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEmployee>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
