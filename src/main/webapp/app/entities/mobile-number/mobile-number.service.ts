import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IMobileNumber } from 'app/shared/model/mobile-number.model';

type EntityResponseType = HttpResponse<IMobileNumber>;
type EntityArrayResponseType = HttpResponse<IMobileNumber[]>;

@Injectable({ providedIn: 'root' })
export class MobileNumberService {
  public resourceUrl = SERVER_API_URL + 'api/mobile-numbers';

  constructor(protected http: HttpClient) {}

  create(mobileNumber: IMobileNumber): Observable<EntityResponseType> {
    return this.http.post<IMobileNumber>(this.resourceUrl, mobileNumber, { observe: 'response' });
  }

  update(mobileNumber: IMobileNumber): Observable<EntityResponseType> {
    return this.http.put<IMobileNumber>(this.resourceUrl, mobileNumber, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IMobileNumber>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IMobileNumber[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
