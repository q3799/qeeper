import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IMobileNumber, MobileNumber } from 'app/shared/model/mobile-number.model';
import { MobileNumberService } from './mobile-number.service';
import { IEmployee } from 'app/shared/model/employee.model';
import { EmployeeService } from 'app/entities/employee';

@Component({
  selector: 'jhi-mobile-number-update',
  templateUrl: './mobile-number-update.component.html'
})
export class MobileNumberUpdateComponent implements OnInit {
  isSaving: boolean;

  employees: IEmployee[];

  editForm = this.fb.group({
    id: [],
    number: [null, [Validators.maxLength(20), Validators.pattern('^\\+?[1-9]\\d{1,14}$')]],
    assetTag: [null, [Validators.maxLength(50)]],
    operator: [null, [Validators.required]],
    mobileConnectionPlan: [],
    remarks: [null, [Validators.maxLength(2000)]],
    active: [null, [Validators.required]],
    registeredToId: [null, Validators.required]
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected mobileNumberService: MobileNumberService,
    protected employeeService: EmployeeService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ mobileNumber }) => {
      this.updateForm(mobileNumber);
    });
    this.employeeService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IEmployee[]>) => mayBeOk.ok),
        map((response: HttpResponse<IEmployee[]>) => response.body)
      )
      .subscribe((res: IEmployee[]) => (this.employees = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(mobileNumber: IMobileNumber) {
    this.editForm.patchValue({
      id: mobileNumber.id,
      number: mobileNumber.number,
      assetTag: mobileNumber.assetTag,
      operator: mobileNumber.operator,
      mobileConnectionPlan: mobileNumber.mobileConnectionPlan,
      remarks: mobileNumber.remarks,
      active: mobileNumber.active,
      registeredToId: mobileNumber.registeredToId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const mobileNumber = this.createFromForm();
    if (mobileNumber.id !== undefined) {
      this.subscribeToSaveResponse(this.mobileNumberService.update(mobileNumber));
    } else {
      this.subscribeToSaveResponse(this.mobileNumberService.create(mobileNumber));
    }
  }

  private createFromForm(): IMobileNumber {
    return {
      ...new MobileNumber(),
      id: this.editForm.get(['id']).value,
      number: this.editForm.get(['number']).value,
      assetTag: this.editForm.get(['assetTag']).value,
      operator: this.editForm.get(['operator']).value,
      mobileConnectionPlan: this.editForm.get(['mobileConnectionPlan']).value,
      remarks: this.editForm.get(['remarks']).value,
      active: this.editForm.get(['active']).value,
      registeredToId: this.editForm.get(['registeredToId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMobileNumber>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackEmployeeById(index: number, item: IEmployee) {
    return item.id;
  }
}
