import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IMobileNumber } from 'app/shared/model/mobile-number.model';
import { MobileNumberService } from './mobile-number.service';

@Component({
  selector: 'jhi-mobile-number-delete-dialog',
  templateUrl: './mobile-number-delete-dialog.component.html'
})
export class MobileNumberDeleteDialogComponent {
  mobileNumber: IMobileNumber;

  constructor(
    protected mobileNumberService: MobileNumberService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.mobileNumberService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'mobileNumberListModification',
        content: 'Deleted an mobileNumber'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-mobile-number-delete-popup',
  template: ''
})
export class MobileNumberDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ mobileNumber }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(MobileNumberDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.mobileNumber = mobileNumber;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/mobile-number', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/mobile-number', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
