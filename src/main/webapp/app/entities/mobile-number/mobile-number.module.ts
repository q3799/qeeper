import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { QeeperSharedModule } from 'app/shared';
import {
  MobileNumberComponent,
  MobileNumberDetailComponent,
  MobileNumberUpdateComponent,
  MobileNumberDeletePopupComponent,
  MobileNumberDeleteDialogComponent,
  mobileNumberRoute,
  mobileNumberPopupRoute
} from './';

const ENTITY_STATES = [...mobileNumberRoute, ...mobileNumberPopupRoute];

@NgModule({
  imports: [QeeperSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    MobileNumberComponent,
    MobileNumberDetailComponent,
    MobileNumberUpdateComponent,
    MobileNumberDeleteDialogComponent,
    MobileNumberDeletePopupComponent
  ],
  entryComponents: [
    MobileNumberComponent,
    MobileNumberUpdateComponent,
    MobileNumberDeleteDialogComponent,
    MobileNumberDeletePopupComponent
  ],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class QeeperMobileNumberModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
