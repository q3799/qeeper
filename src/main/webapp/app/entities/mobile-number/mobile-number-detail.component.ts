import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMobileNumber } from 'app/shared/model/mobile-number.model';

@Component({
  selector: 'jhi-mobile-number-detail',
  templateUrl: './mobile-number-detail.component.html'
})
export class MobileNumberDetailComponent implements OnInit {
  mobileNumber: IMobileNumber;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ mobileNumber }) => {
      this.mobileNumber = mobileNumber;
    });
  }

  previousState() {
    window.history.back();
  }
}
