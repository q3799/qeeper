import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { MobileNumber } from 'app/shared/model/mobile-number.model';
import { MobileNumberService } from './mobile-number.service';
import { MobileNumberComponent } from './mobile-number.component';
import { MobileNumberDetailComponent } from './mobile-number-detail.component';
import { MobileNumberUpdateComponent } from './mobile-number-update.component';
import { MobileNumberDeletePopupComponent } from './mobile-number-delete-dialog.component';
import { IMobileNumber } from 'app/shared/model/mobile-number.model';

@Injectable({ providedIn: 'root' })
export class MobileNumberResolve implements Resolve<IMobileNumber> {
  constructor(private service: MobileNumberService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IMobileNumber> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<MobileNumber>) => response.ok),
        map((mobileNumber: HttpResponse<MobileNumber>) => mobileNumber.body)
      );
    }
    return of(new MobileNumber());
  }
}

export const mobileNumberRoute: Routes = [
  {
    path: '',
    component: MobileNumberComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'qeeperApp.mobileNumber.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: MobileNumberDetailComponent,
    resolve: {
      mobileNumber: MobileNumberResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'qeeperApp.mobileNumber.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: MobileNumberUpdateComponent,
    resolve: {
      mobileNumber: MobileNumberResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'qeeperApp.mobileNumber.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: MobileNumberUpdateComponent,
    resolve: {
      mobileNumber: MobileNumberResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'qeeperApp.mobileNumber.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const mobileNumberPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: MobileNumberDeletePopupComponent,
    resolve: {
      mobileNumber: MobileNumberResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'qeeperApp.mobileNumber.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
