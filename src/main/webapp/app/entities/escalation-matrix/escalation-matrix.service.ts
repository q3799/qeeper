import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IEscalationMatrix } from 'app/shared/model/escalation-matrix.model';

type EntityResponseType = HttpResponse<IEscalationMatrix>;
type EntityArrayResponseType = HttpResponse<IEscalationMatrix[]>;

@Injectable({ providedIn: 'root' })
export class EscalationMatrixService {
  public resourceUrl = SERVER_API_URL + 'api/escalation-matrices';

  constructor(protected http: HttpClient) {}

  create(escalationMatrix: IEscalationMatrix): Observable<EntityResponseType> {
    return this.http.post<IEscalationMatrix>(this.resourceUrl, escalationMatrix, { observe: 'response' });
  }

  update(escalationMatrix: IEscalationMatrix): Observable<EntityResponseType> {
    return this.http.put<IEscalationMatrix>(this.resourceUrl, escalationMatrix, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IEscalationMatrix>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IEscalationMatrix[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
