import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { EscalationMatrix } from 'app/shared/model/escalation-matrix.model';
import { EscalationMatrixService } from './escalation-matrix.service';
import { EscalationMatrixComponent } from './escalation-matrix.component';
import { EscalationMatrixDetailComponent } from './escalation-matrix-detail.component';
import { EscalationMatrixUpdateComponent } from './escalation-matrix-update.component';
import { EscalationMatrixDeletePopupComponent } from './escalation-matrix-delete-dialog.component';
import { IEscalationMatrix } from 'app/shared/model/escalation-matrix.model';

@Injectable({ providedIn: 'root' })
export class EscalationMatrixResolve implements Resolve<IEscalationMatrix> {
  constructor(private service: EscalationMatrixService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IEscalationMatrix> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<EscalationMatrix>) => response.ok),
        map((escalationMatrix: HttpResponse<EscalationMatrix>) => escalationMatrix.body)
      );
    }
    return of(new EscalationMatrix());
  }
}

export const escalationMatrixRoute: Routes = [
  {
    path: '',
    component: EscalationMatrixComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'qeeperApp.escalationMatrix.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: EscalationMatrixDetailComponent,
    resolve: {
      escalationMatrix: EscalationMatrixResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'qeeperApp.escalationMatrix.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: EscalationMatrixUpdateComponent,
    resolve: {
      escalationMatrix: EscalationMatrixResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'qeeperApp.escalationMatrix.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: EscalationMatrixUpdateComponent,
    resolve: {
      escalationMatrix: EscalationMatrixResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'qeeperApp.escalationMatrix.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const escalationMatrixPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: EscalationMatrixDeletePopupComponent,
    resolve: {
      escalationMatrix: EscalationMatrixResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'qeeperApp.escalationMatrix.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
