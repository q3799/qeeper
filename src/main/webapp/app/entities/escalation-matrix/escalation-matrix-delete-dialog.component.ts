import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IEscalationMatrix } from 'app/shared/model/escalation-matrix.model';
import { EscalationMatrixService } from './escalation-matrix.service';

@Component({
  selector: 'jhi-escalation-matrix-delete-dialog',
  templateUrl: './escalation-matrix-delete-dialog.component.html'
})
export class EscalationMatrixDeleteDialogComponent {
  escalationMatrix: IEscalationMatrix;

  constructor(
    protected escalationMatrixService: EscalationMatrixService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.escalationMatrixService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'escalationMatrixListModification',
        content: 'Deleted an escalationMatrix'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-escalation-matrix-delete-popup',
  template: ''
})
export class EscalationMatrixDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ escalationMatrix }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(EscalationMatrixDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.escalationMatrix = escalationMatrix;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/escalation-matrix', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/escalation-matrix', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
