import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IEscalationMatrix } from 'app/shared/model/escalation-matrix.model';

@Component({
  selector: 'jhi-escalation-matrix-detail',
  templateUrl: './escalation-matrix-detail.component.html'
})
export class EscalationMatrixDetailComponent implements OnInit {
  escalationMatrix: IEscalationMatrix;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ escalationMatrix }) => {
      this.escalationMatrix = escalationMatrix;
    });
  }

  previousState() {
    window.history.back();
  }
}
