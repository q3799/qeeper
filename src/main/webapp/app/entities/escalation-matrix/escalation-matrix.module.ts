import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { QeeperSharedModule } from 'app/shared';
import {
  EscalationMatrixComponent,
  EscalationMatrixDetailComponent,
  EscalationMatrixUpdateComponent,
  EscalationMatrixDeletePopupComponent,
  EscalationMatrixDeleteDialogComponent,
  escalationMatrixRoute,
  escalationMatrixPopupRoute
} from './';

const ENTITY_STATES = [...escalationMatrixRoute, ...escalationMatrixPopupRoute];

@NgModule({
  imports: [QeeperSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    EscalationMatrixComponent,
    EscalationMatrixDetailComponent,
    EscalationMatrixUpdateComponent,
    EscalationMatrixDeleteDialogComponent,
    EscalationMatrixDeletePopupComponent
  ],
  entryComponents: [
    EscalationMatrixComponent,
    EscalationMatrixUpdateComponent,
    EscalationMatrixDeleteDialogComponent,
    EscalationMatrixDeletePopupComponent
  ],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class QeeperEscalationMatrixModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
