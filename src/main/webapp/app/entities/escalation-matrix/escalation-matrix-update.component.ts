import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IEscalationMatrix, EscalationMatrix } from 'app/shared/model/escalation-matrix.model';
import { EscalationMatrixService } from './escalation-matrix.service';
import { IContactPerson } from 'app/shared/model/contact-person.model';
import { ContactPersonService } from 'app/entities/contact-person';

@Component({
  selector: 'jhi-escalation-matrix-update',
  templateUrl: './escalation-matrix-update.component.html'
})
export class EscalationMatrixUpdateComponent implements OnInit {
  isSaving: boolean;

  contactpeople: IContactPerson[];

  editForm = this.fb.group({
    id: [],
    escalationType: [null, [Validators.required]],
    level: [null, [Validators.required, Validators.min(1), Validators.max(100)]],
    receiveSms: [null, [Validators.required]],
    receiveEmail: [null, [Validators.required]],
    contactPersonId: [null, Validators.required]
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected escalationMatrixService: EscalationMatrixService,
    protected contactPersonService: ContactPersonService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ escalationMatrix }) => {
      this.updateForm(escalationMatrix);
    });
    this.contactPersonService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IContactPerson[]>) => mayBeOk.ok),
        map((response: HttpResponse<IContactPerson[]>) => response.body)
      )
      .subscribe((res: IContactPerson[]) => (this.contactpeople = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(escalationMatrix: IEscalationMatrix) {
    this.editForm.patchValue({
      id: escalationMatrix.id,
      escalationType: escalationMatrix.escalationType,
      level: escalationMatrix.level,
      receiveSms: escalationMatrix.receiveSms,
      receiveEmail: escalationMatrix.receiveEmail,
      contactPersonId: escalationMatrix.contactPersonId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const escalationMatrix = this.createFromForm();
    if (escalationMatrix.id !== undefined) {
      this.subscribeToSaveResponse(this.escalationMatrixService.update(escalationMatrix));
    } else {
      this.subscribeToSaveResponse(this.escalationMatrixService.create(escalationMatrix));
    }
  }

  private createFromForm(): IEscalationMatrix {
    return {
      ...new EscalationMatrix(),
      id: this.editForm.get(['id']).value,
      escalationType: this.editForm.get(['escalationType']).value,
      level: this.editForm.get(['level']).value,
      receiveSms: this.editForm.get(['receiveSms']).value,
      receiveEmail: this.editForm.get(['receiveEmail']).value,
      contactPersonId: this.editForm.get(['contactPersonId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEscalationMatrix>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackContactPersonById(index: number, item: IContactPerson) {
    return item.id;
  }
}
