import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { ICard, Card } from 'app/shared/model/card.model';
import { CardService } from './card.service';
import { IMobileNumber } from 'app/shared/model/mobile-number.model';
import { MobileNumberService } from 'app/entities/mobile-number';
import { IEmployee } from 'app/shared/model/employee.model';
import { EmployeeService } from 'app/entities/employee';
import { IBank } from 'app/shared/model/bank.model';
import { BankService } from 'app/entities/bank';
import { IBankAccount } from 'app/shared/model/bank-account.model';
import { BankAccountService } from 'app/entities/bank-account';
import { ICardGroup } from 'app/shared/model/card-group.model';
import { CardGroupService } from 'app/entities/card-group';

@Component({
  selector: 'jhi-card-update',
  templateUrl: './card-update.component.html'
})
export class CardUpdateComponent implements OnInit {
  isSaving: boolean;

  mobilenumbers: IMobileNumber[];

  employees: IEmployee[];

  banks: IBank[];

  bankaccounts: IBankAccount[];

  cardgroups: ICardGroup[];

  editForm = this.fb.group({
    id: [],
    cardType: [null, [Validators.required]],
    paymentNetwork: [null, [Validators.required]],
    cardNumber: [
      null,
      [
        Validators.required,
        Validators.maxLength(30),
        Validators.pattern(
          '^(?:4[0-9]{12}(?:[0-9]{3})?|[25][1-7][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\\d{3})\\d{11})$'
        )
      ]
    ],
    validFrom: [null, [Validators.required, Validators.maxLength(30), Validators.pattern('^(0[1-9]|1[0-2])\\/([0-9]{4}|[0-9]{2})$')]],
    validTill: [null, [Validators.required, Validators.maxLength(30), Validators.pattern('^(0[1-9]|1[0-2])\\/([0-9]{4}|[0-9]{2})$')]],
    paymentNetworkPin: [null, [Validators.minLength(1), Validators.maxLength(10), Validators.pattern('^\\d{1,8}$')]],
    cvv: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(5), Validators.pattern('^\\d{3,5}$')]],
    nameOnCard: [null, [Validators.required, Validators.maxLength(100)]],
    remarks: [null, [Validators.maxLength(1000)]],
    pin: [null, [Validators.minLength(3), Validators.maxLength(6), Validators.pattern('^\\d{3,6}$')]],
    tpin: [null, [Validators.minLength(3), Validators.maxLength(6), Validators.pattern('^\\d{3,6}$')]],
    mobileNumberMappedId: [null, Validators.required],
    registeredToId: [null, Validators.required],
    bankId: [null, Validators.required],
    bankAccountId: [],
    cardGroupId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected cardService: CardService,
    protected mobileNumberService: MobileNumberService,
    protected employeeService: EmployeeService,
    protected bankService: BankService,
    protected bankAccountService: BankAccountService,
    protected cardGroupService: CardGroupService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ card }) => {
      this.updateForm(card);
    });
    this.mobileNumberService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IMobileNumber[]>) => mayBeOk.ok),
        map((response: HttpResponse<IMobileNumber[]>) => response.body)
      )
      .subscribe((res: IMobileNumber[]) => (this.mobilenumbers = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.employeeService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IEmployee[]>) => mayBeOk.ok),
        map((response: HttpResponse<IEmployee[]>) => response.body)
      )
      .subscribe((res: IEmployee[]) => (this.employees = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.bankService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IBank[]>) => mayBeOk.ok),
        map((response: HttpResponse<IBank[]>) => response.body)
      )
      .subscribe((res: IBank[]) => (this.banks = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.bankAccountService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IBankAccount[]>) => mayBeOk.ok),
        map((response: HttpResponse<IBankAccount[]>) => response.body)
      )
      .subscribe((res: IBankAccount[]) => (this.bankaccounts = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.cardGroupService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ICardGroup[]>) => mayBeOk.ok),
        map((response: HttpResponse<ICardGroup[]>) => response.body)
      )
      .subscribe((res: ICardGroup[]) => (this.cardgroups = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(card: ICard) {
    this.editForm.patchValue({
      id: card.id,
      cardType: card.cardType,
      paymentNetwork: card.paymentNetwork,
      cardNumber: card.cardNumber,
      validFrom: card.validFrom,
      validTill: card.validTill,
      paymentNetworkPin: card.paymentNetworkPin,
      cvv: card.cvv,
      nameOnCard: card.nameOnCard,
      remarks: card.remarks,
      pin: card.pin,
      tpin: card.tpin,
      mobileNumberMappedId: card.mobileNumberMappedId,
      registeredToId: card.registeredToId,
      bankId: card.bankId,
      bankAccountId: card.bankAccountId,
      cardGroupId: card.cardGroupId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const card = this.createFromForm();
    if (card.id !== undefined) {
      this.subscribeToSaveResponse(this.cardService.update(card));
    } else {
      this.subscribeToSaveResponse(this.cardService.create(card));
    }
  }

  private createFromForm(): ICard {
    return {
      ...new Card(),
      id: this.editForm.get(['id']).value,
      cardType: this.editForm.get(['cardType']).value,
      paymentNetwork: this.editForm.get(['paymentNetwork']).value,
      cardNumber: this.editForm.get(['cardNumber']).value,
      validFrom: this.editForm.get(['validFrom']).value,
      validTill: this.editForm.get(['validTill']).value,
      paymentNetworkPin: this.editForm.get(['paymentNetworkPin']).value,
      cvv: this.editForm.get(['cvv']).value,
      nameOnCard: this.editForm.get(['nameOnCard']).value,
      remarks: this.editForm.get(['remarks']).value,
      pin: this.editForm.get(['pin']).value,
      tpin: this.editForm.get(['tpin']).value,
      mobileNumberMappedId: this.editForm.get(['mobileNumberMappedId']).value,
      registeredToId: this.editForm.get(['registeredToId']).value,
      bankId: this.editForm.get(['bankId']).value,
      bankAccountId: this.editForm.get(['bankAccountId']).value,
      cardGroupId: this.editForm.get(['cardGroupId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICard>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackMobileNumberById(index: number, item: IMobileNumber) {
    return item.id;
  }

  trackEmployeeById(index: number, item: IEmployee) {
    return item.id;
  }

  trackBankById(index: number, item: IBank) {
    return item.id;
  }

  trackBankAccountById(index: number, item: IBankAccount) {
    return item.id;
  }

  trackCardGroupById(index: number, item: ICardGroup) {
    return item.id;
  }
}
