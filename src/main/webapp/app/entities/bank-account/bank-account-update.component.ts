import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { IBankAccount, BankAccount } from 'app/shared/model/bank-account.model';
import { BankAccountService } from './bank-account.service';
import { IEmployee } from 'app/shared/model/employee.model';
import { EmployeeService } from 'app/entities/employee';
import { IBankBranch } from 'app/shared/model/bank-branch.model';
import { BankBranchService } from 'app/entities/bank-branch';
import { IMobileNumber } from 'app/shared/model/mobile-number.model';
import { MobileNumberService } from 'app/entities/mobile-number';
import { IBankAccountGroup } from 'app/shared/model/bank-account-group.model';
import { BankAccountGroupService } from 'app/entities/bank-account-group';

@Component({
  selector: 'jhi-bank-account-update',
  templateUrl: './bank-account-update.component.html'
})
export class BankAccountUpdateComponent implements OnInit {
  isSaving: boolean;

  employees: IEmployee[];

  bankbranches: IBankBranch[];

  mobilenumbers: IMobileNumber[];

  bankaccounts: IBankAccount[];

  bankaccountgroups: IBankAccountGroup[];

  editForm = this.fb.group({
    id: [],
    dateOfOpening: [],
    openedBy: [null, [Validators.maxLength(100)]],
    customerId: [null, [Validators.required, Validators.maxLength(50)]],
    accountNumber: [null, [Validators.maxLength(100)]],
    loginId: [null, [Validators.required, Validators.maxLength(50)]],
    loginPassword: [null, [Validators.required, Validators.maxLength(50)]],
    email: [
      null,
      [
        Validators.required,
        Validators.maxLength(200),
        Validators.pattern(
          '^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$'
        )
      ]
    ],
    phone: [null, [Validators.pattern('^\\+?[1-9]\\d{1,14}$')]],
    bankAccountType: [null, [Validators.required]],
    chequeBookIssued: [null, [Validators.required]],
    accountBalance: [null, [Validators.min(0)]],
    balanceCheckedOn: [],
    minimumBalance: [],
    passwordGetsExpired: [null, [Validators.required]],
    expirationPeriodDays: [null, [Validators.min(0)]],
    passwordUpdatedOn: [],
    remarks: [null, [Validators.maxLength(1000)]],
    accountStatus: [null, [Validators.required]],
    multipleSessionAllowed: [null, [Validators.required]],
    projectName: [null, [Validators.maxLength(100)]],
    projectCode: [null, [Validators.maxLength(50)]],
    requestedOn: [],
    requestedBy: [null, [Validators.maxLength(100)]],
    accountHolderId: [null, Validators.required],
    bankBranchId: [null, Validators.required],
    mobileNumberMappedId: [null, Validators.required],
    parentAccountId: [],
    bankAccountGroupId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected bankAccountService: BankAccountService,
    protected employeeService: EmployeeService,
    protected bankBranchService: BankBranchService,
    protected mobileNumberService: MobileNumberService,
    protected bankAccountGroupService: BankAccountGroupService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ bankAccount }) => {
      this.updateForm(bankAccount);
    });
    this.employeeService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IEmployee[]>) => mayBeOk.ok),
        map((response: HttpResponse<IEmployee[]>) => response.body)
      )
      .subscribe((res: IEmployee[]) => (this.employees = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.bankBranchService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IBankBranch[]>) => mayBeOk.ok),
        map((response: HttpResponse<IBankBranch[]>) => response.body)
      )
      .subscribe((res: IBankBranch[]) => (this.bankbranches = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.mobileNumberService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IMobileNumber[]>) => mayBeOk.ok),
        map((response: HttpResponse<IMobileNumber[]>) => response.body)
      )
      .subscribe((res: IMobileNumber[]) => (this.mobilenumbers = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.bankAccountService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IBankAccount[]>) => mayBeOk.ok),
        map((response: HttpResponse<IBankAccount[]>) => response.body)
      )
      .subscribe((res: IBankAccount[]) => (this.bankaccounts = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.bankAccountGroupService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IBankAccountGroup[]>) => mayBeOk.ok),
        map((response: HttpResponse<IBankAccountGroup[]>) => response.body)
      )
      .subscribe((res: IBankAccountGroup[]) => (this.bankaccountgroups = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(bankAccount: IBankAccount) {
    this.editForm.patchValue({
      id: bankAccount.id,
      dateOfOpening: bankAccount.dateOfOpening != null ? bankAccount.dateOfOpening.format(DATE_TIME_FORMAT) : null,
      openedBy: bankAccount.openedBy,
      customerId: bankAccount.customerId,
      accountNumber: bankAccount.accountNumber,
      loginId: bankAccount.loginId,
      loginPassword: bankAccount.loginPassword,
      email: bankAccount.email,
      phone: bankAccount.phone,
      bankAccountType: bankAccount.bankAccountType,
      chequeBookIssued: bankAccount.chequeBookIssued,
      accountBalance: bankAccount.accountBalance,
      balanceCheckedOn: bankAccount.balanceCheckedOn != null ? bankAccount.balanceCheckedOn.format(DATE_TIME_FORMAT) : null,
      minimumBalance: bankAccount.minimumBalance,
      passwordGetsExpired: bankAccount.passwordGetsExpired,
      expirationPeriodDays: bankAccount.expirationPeriodDays,
      passwordUpdatedOn: bankAccount.passwordUpdatedOn != null ? bankAccount.passwordUpdatedOn.format(DATE_TIME_FORMAT) : null,
      remarks: bankAccount.remarks,
      accountStatus: bankAccount.accountStatus,
      multipleSessionAllowed: bankAccount.multipleSessionAllowed,
      projectName: bankAccount.projectName,
      projectCode: bankAccount.projectCode,
      requestedOn: bankAccount.requestedOn != null ? bankAccount.requestedOn.format(DATE_TIME_FORMAT) : null,
      requestedBy: bankAccount.requestedBy,
      accountHolderId: bankAccount.accountHolderId,
      bankBranchId: bankAccount.bankBranchId,
      mobileNumberMappedId: bankAccount.mobileNumberMappedId,
      parentAccountId: bankAccount.parentAccountId,
      bankAccountGroupId: bankAccount.bankAccountGroupId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const bankAccount = this.createFromForm();
    if (bankAccount.id !== undefined) {
      this.subscribeToSaveResponse(this.bankAccountService.update(bankAccount));
    } else {
      this.subscribeToSaveResponse(this.bankAccountService.create(bankAccount));
    }
  }

  private createFromForm(): IBankAccount {
    return {
      ...new BankAccount(),
      id: this.editForm.get(['id']).value,
      dateOfOpening:
        this.editForm.get(['dateOfOpening']).value != null
          ? moment(this.editForm.get(['dateOfOpening']).value, DATE_TIME_FORMAT)
          : undefined,
      openedBy: this.editForm.get(['openedBy']).value,
      customerId: this.editForm.get(['customerId']).value,
      accountNumber: this.editForm.get(['accountNumber']).value,
      loginId: this.editForm.get(['loginId']).value,
      loginPassword: this.editForm.get(['loginPassword']).value,
      email: this.editForm.get(['email']).value,
      phone: this.editForm.get(['phone']).value,
      bankAccountType: this.editForm.get(['bankAccountType']).value,
      chequeBookIssued: this.editForm.get(['chequeBookIssued']).value,
      accountBalance: this.editForm.get(['accountBalance']).value,
      balanceCheckedOn:
        this.editForm.get(['balanceCheckedOn']).value != null
          ? moment(this.editForm.get(['balanceCheckedOn']).value, DATE_TIME_FORMAT)
          : undefined,
      minimumBalance: this.editForm.get(['minimumBalance']).value,
      passwordGetsExpired: this.editForm.get(['passwordGetsExpired']).value,
      expirationPeriodDays: this.editForm.get(['expirationPeriodDays']).value,
      passwordUpdatedOn:
        this.editForm.get(['passwordUpdatedOn']).value != null
          ? moment(this.editForm.get(['passwordUpdatedOn']).value, DATE_TIME_FORMAT)
          : undefined,
      remarks: this.editForm.get(['remarks']).value,
      accountStatus: this.editForm.get(['accountStatus']).value,
      multipleSessionAllowed: this.editForm.get(['multipleSessionAllowed']).value,
      projectName: this.editForm.get(['projectName']).value,
      projectCode: this.editForm.get(['projectCode']).value,
      requestedOn:
        this.editForm.get(['requestedOn']).value != null ? moment(this.editForm.get(['requestedOn']).value, DATE_TIME_FORMAT) : undefined,
      requestedBy: this.editForm.get(['requestedBy']).value,
      accountHolderId: this.editForm.get(['accountHolderId']).value,
      bankBranchId: this.editForm.get(['bankBranchId']).value,
      mobileNumberMappedId: this.editForm.get(['mobileNumberMappedId']).value,
      parentAccountId: this.editForm.get(['parentAccountId']).value,
      bankAccountGroupId: this.editForm.get(['bankAccountGroupId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBankAccount>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackEmployeeById(index: number, item: IEmployee) {
    return item.id;
  }

  trackBankBranchById(index: number, item: IBankBranch) {
    return item.id;
  }

  trackMobileNumberById(index: number, item: IMobileNumber) {
    return item.id;
  }

  trackBankAccountById(index: number, item: IBankAccount) {
    return item.id;
  }

  trackBankAccountGroupById(index: number, item: IBankAccountGroup) {
    return item.id;
  }
}
