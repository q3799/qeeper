import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IBankAccount } from 'app/shared/model/bank-account.model';

type EntityResponseType = HttpResponse<IBankAccount>;
type EntityArrayResponseType = HttpResponse<IBankAccount[]>;

@Injectable({ providedIn: 'root' })
export class BankAccountService {
  public resourceUrl = SERVER_API_URL + 'api/bank-accounts';

  constructor(protected http: HttpClient) {}

  create(bankAccount: IBankAccount): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(bankAccount);
    return this.http
      .post<IBankAccount>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(bankAccount: IBankAccount): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(bankAccount);
    return this.http
      .put<IBankAccount>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IBankAccount>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IBankAccount[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(bankAccount: IBankAccount): IBankAccount {
    const copy: IBankAccount = Object.assign({}, bankAccount, {
      dateOfOpening: bankAccount.dateOfOpening != null && bankAccount.dateOfOpening.isValid() ? bankAccount.dateOfOpening.toJSON() : null,
      balanceCheckedOn:
        bankAccount.balanceCheckedOn != null && bankAccount.balanceCheckedOn.isValid() ? bankAccount.balanceCheckedOn.toJSON() : null,
      passwordUpdatedOn:
        bankAccount.passwordUpdatedOn != null && bankAccount.passwordUpdatedOn.isValid() ? bankAccount.passwordUpdatedOn.toJSON() : null,
      requestedOn: bankAccount.requestedOn != null && bankAccount.requestedOn.isValid() ? bankAccount.requestedOn.toJSON() : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dateOfOpening = res.body.dateOfOpening != null ? moment(res.body.dateOfOpening) : null;
      res.body.balanceCheckedOn = res.body.balanceCheckedOn != null ? moment(res.body.balanceCheckedOn) : null;
      res.body.passwordUpdatedOn = res.body.passwordUpdatedOn != null ? moment(res.body.passwordUpdatedOn) : null;
      res.body.requestedOn = res.body.requestedOn != null ? moment(res.body.requestedOn) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((bankAccount: IBankAccount) => {
        bankAccount.dateOfOpening = bankAccount.dateOfOpening != null ? moment(bankAccount.dateOfOpening) : null;
        bankAccount.balanceCheckedOn = bankAccount.balanceCheckedOn != null ? moment(bankAccount.balanceCheckedOn) : null;
        bankAccount.passwordUpdatedOn = bankAccount.passwordUpdatedOn != null ? moment(bankAccount.passwordUpdatedOn) : null;
        bankAccount.requestedOn = bankAccount.requestedOn != null ? moment(bankAccount.requestedOn) : null;
      });
    }
    return res;
  }
}
