import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { ISecretQuestion, SecretQuestion } from 'app/shared/model/secret-question.model';
import { SecretQuestionService } from './secret-question.service';
import { IBankAccount } from 'app/shared/model/bank-account.model';
import { BankAccountService } from 'app/entities/bank-account';

@Component({
  selector: 'jhi-secret-question-update',
  templateUrl: './secret-question-update.component.html'
})
export class SecretQuestionUpdateComponent implements OnInit {
  isSaving: boolean;

  bankaccounts: IBankAccount[];

  editForm = this.fb.group({
    id: [],
    question: [null, [Validators.required, Validators.maxLength(200)]],
    answer: [null, [Validators.required, Validators.maxLength(200)]],
    bankAccountId: [null, Validators.required]
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected secretQuestionService: SecretQuestionService,
    protected bankAccountService: BankAccountService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ secretQuestion }) => {
      this.updateForm(secretQuestion);
    });
    this.bankAccountService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IBankAccount[]>) => mayBeOk.ok),
        map((response: HttpResponse<IBankAccount[]>) => response.body)
      )
      .subscribe((res: IBankAccount[]) => (this.bankaccounts = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(secretQuestion: ISecretQuestion) {
    this.editForm.patchValue({
      id: secretQuestion.id,
      question: secretQuestion.question,
      answer: secretQuestion.answer,
      bankAccountId: secretQuestion.bankAccountId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const secretQuestion = this.createFromForm();
    if (secretQuestion.id !== undefined) {
      this.subscribeToSaveResponse(this.secretQuestionService.update(secretQuestion));
    } else {
      this.subscribeToSaveResponse(this.secretQuestionService.create(secretQuestion));
    }
  }

  private createFromForm(): ISecretQuestion {
    return {
      ...new SecretQuestion(),
      id: this.editForm.get(['id']).value,
      question: this.editForm.get(['question']).value,
      answer: this.editForm.get(['answer']).value,
      bankAccountId: this.editForm.get(['bankAccountId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISecretQuestion>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackBankAccountById(index: number, item: IBankAccount) {
    return item.id;
  }
}
