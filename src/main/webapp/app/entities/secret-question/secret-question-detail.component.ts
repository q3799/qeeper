import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISecretQuestion } from 'app/shared/model/secret-question.model';

@Component({
  selector: 'jhi-secret-question-detail',
  templateUrl: './secret-question-detail.component.html'
})
export class SecretQuestionDetailComponent implements OnInit {
  secretQuestion: ISecretQuestion;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ secretQuestion }) => {
      this.secretQuestion = secretQuestion;
    });
  }

  previousState() {
    window.history.back();
  }
}
