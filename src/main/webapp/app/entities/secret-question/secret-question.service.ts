import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ISecretQuestion } from 'app/shared/model/secret-question.model';

type EntityResponseType = HttpResponse<ISecretQuestion>;
type EntityArrayResponseType = HttpResponse<ISecretQuestion[]>;

@Injectable({ providedIn: 'root' })
export class SecretQuestionService {
  public resourceUrl = SERVER_API_URL + 'api/secret-questions';

  constructor(protected http: HttpClient) {}

  create(secretQuestion: ISecretQuestion): Observable<EntityResponseType> {
    return this.http.post<ISecretQuestion>(this.resourceUrl, secretQuestion, { observe: 'response' });
  }

  update(secretQuestion: ISecretQuestion): Observable<EntityResponseType> {
    return this.http.put<ISecretQuestion>(this.resourceUrl, secretQuestion, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ISecretQuestion>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ISecretQuestion[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
