import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ISecretQuestion } from 'app/shared/model/secret-question.model';
import { SecretQuestionService } from './secret-question.service';

@Component({
  selector: 'jhi-secret-question-delete-dialog',
  templateUrl: './secret-question-delete-dialog.component.html'
})
export class SecretQuestionDeleteDialogComponent {
  secretQuestion: ISecretQuestion;

  constructor(
    protected secretQuestionService: SecretQuestionService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.secretQuestionService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'secretQuestionListModification',
        content: 'Deleted an secretQuestion'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-secret-question-delete-popup',
  template: ''
})
export class SecretQuestionDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ secretQuestion }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(SecretQuestionDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.secretQuestion = secretQuestion;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/secret-question', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/secret-question', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
