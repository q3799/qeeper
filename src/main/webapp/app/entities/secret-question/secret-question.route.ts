import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { SecretQuestion } from 'app/shared/model/secret-question.model';
import { SecretQuestionService } from './secret-question.service';
import { SecretQuestionComponent } from './secret-question.component';
import { SecretQuestionDetailComponent } from './secret-question-detail.component';
import { SecretQuestionUpdateComponent } from './secret-question-update.component';
import { SecretQuestionDeletePopupComponent } from './secret-question-delete-dialog.component';
import { ISecretQuestion } from 'app/shared/model/secret-question.model';

@Injectable({ providedIn: 'root' })
export class SecretQuestionResolve implements Resolve<ISecretQuestion> {
  constructor(private service: SecretQuestionService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ISecretQuestion> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<SecretQuestion>) => response.ok),
        map((secretQuestion: HttpResponse<SecretQuestion>) => secretQuestion.body)
      );
    }
    return of(new SecretQuestion());
  }
}

export const secretQuestionRoute: Routes = [
  {
    path: '',
    component: SecretQuestionComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'qeeperApp.secretQuestion.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: SecretQuestionDetailComponent,
    resolve: {
      secretQuestion: SecretQuestionResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'qeeperApp.secretQuestion.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: SecretQuestionUpdateComponent,
    resolve: {
      secretQuestion: SecretQuestionResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'qeeperApp.secretQuestion.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: SecretQuestionUpdateComponent,
    resolve: {
      secretQuestion: SecretQuestionResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'qeeperApp.secretQuestion.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const secretQuestionPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: SecretQuestionDeletePopupComponent,
    resolve: {
      secretQuestion: SecretQuestionResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'qeeperApp.secretQuestion.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
