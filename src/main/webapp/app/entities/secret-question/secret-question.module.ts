import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { QeeperSharedModule } from 'app/shared';
import {
  SecretQuestionComponent,
  SecretQuestionDetailComponent,
  SecretQuestionUpdateComponent,
  SecretQuestionDeletePopupComponent,
  SecretQuestionDeleteDialogComponent,
  secretQuestionRoute,
  secretQuestionPopupRoute
} from './';

const ENTITY_STATES = [...secretQuestionRoute, ...secretQuestionPopupRoute];

@NgModule({
  imports: [QeeperSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    SecretQuestionComponent,
    SecretQuestionDetailComponent,
    SecretQuestionUpdateComponent,
    SecretQuestionDeleteDialogComponent,
    SecretQuestionDeletePopupComponent
  ],
  entryComponents: [
    SecretQuestionComponent,
    SecretQuestionUpdateComponent,
    SecretQuestionDeleteDialogComponent,
    SecretQuestionDeletePopupComponent
  ],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class QeeperSecretQuestionModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
