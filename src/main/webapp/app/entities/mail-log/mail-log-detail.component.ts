import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMailLog } from 'app/shared/model/mail-log.model';

@Component({
  selector: 'jhi-mail-log-detail',
  templateUrl: './mail-log-detail.component.html'
})
export class MailLogDetailComponent implements OnInit {
  mailLog: IMailLog;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ mailLog }) => {
      this.mailLog = mailLog;
    });
  }

  previousState() {
    window.history.back();
  }
}
