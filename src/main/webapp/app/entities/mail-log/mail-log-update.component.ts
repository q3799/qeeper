import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { IMailLog, MailLog } from 'app/shared/model/mail-log.model';
import { MailLogService } from './mail-log.service';

@Component({
  selector: 'jhi-mail-log-update',
  templateUrl: './mail-log-update.component.html'
})
export class MailLogUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    subject: [null, [Validators.required, Validators.maxLength(400)]],
    content: [null, [Validators.maxLength(4000)]],
    mailDate: [],
    mailServer: [],
    responseReceived: [null, [Validators.maxLength(1000)]],
    remarks: [null, [Validators.maxLength(2000)]],
    toList: [null, [Validators.maxLength(4000)]],
    ccList: [null, [Validators.maxLength(4000)]],
    processingTime: [null, [Validators.min(0)]]
  });

  constructor(protected mailLogService: MailLogService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ mailLog }) => {
      this.updateForm(mailLog);
    });
  }

  updateForm(mailLog: IMailLog) {
    this.editForm.patchValue({
      id: mailLog.id,
      subject: mailLog.subject,
      content: mailLog.content,
      mailDate: mailLog.mailDate != null ? mailLog.mailDate.format(DATE_TIME_FORMAT) : null,
      mailServer: mailLog.mailServer,
      responseReceived: mailLog.responseReceived,
      remarks: mailLog.remarks,
      toList: mailLog.toList,
      ccList: mailLog.ccList,
      processingTime: mailLog.processingTime
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const mailLog = this.createFromForm();
    if (mailLog.id !== undefined) {
      this.subscribeToSaveResponse(this.mailLogService.update(mailLog));
    } else {
      this.subscribeToSaveResponse(this.mailLogService.create(mailLog));
    }
  }

  private createFromForm(): IMailLog {
    return {
      ...new MailLog(),
      id: this.editForm.get(['id']).value,
      subject: this.editForm.get(['subject']).value,
      content: this.editForm.get(['content']).value,
      mailDate: this.editForm.get(['mailDate']).value != null ? moment(this.editForm.get(['mailDate']).value, DATE_TIME_FORMAT) : undefined,
      mailServer: this.editForm.get(['mailServer']).value,
      responseReceived: this.editForm.get(['responseReceived']).value,
      remarks: this.editForm.get(['remarks']).value,
      toList: this.editForm.get(['toList']).value,
      ccList: this.editForm.get(['ccList']).value,
      processingTime: this.editForm.get(['processingTime']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMailLog>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
