import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IMailLog } from 'app/shared/model/mail-log.model';
import { MailLogService } from './mail-log.service';

@Component({
  selector: 'jhi-mail-log-delete-dialog',
  templateUrl: './mail-log-delete-dialog.component.html'
})
export class MailLogDeleteDialogComponent {
  mailLog: IMailLog;

  constructor(protected mailLogService: MailLogService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.mailLogService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'mailLogListModification',
        content: 'Deleted an mailLog'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-mail-log-delete-popup',
  template: ''
})
export class MailLogDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ mailLog }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(MailLogDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.mailLog = mailLog;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/mail-log', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/mail-log', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
