import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IMailLog } from 'app/shared/model/mail-log.model';

type EntityResponseType = HttpResponse<IMailLog>;
type EntityArrayResponseType = HttpResponse<IMailLog[]>;

@Injectable({ providedIn: 'root' })
export class MailLogService {
  public resourceUrl = SERVER_API_URL + 'api/mail-logs';

  constructor(protected http: HttpClient) {}

  create(mailLog: IMailLog): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(mailLog);
    return this.http
      .post<IMailLog>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(mailLog: IMailLog): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(mailLog);
    return this.http
      .put<IMailLog>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IMailLog>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IMailLog[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(mailLog: IMailLog): IMailLog {
    const copy: IMailLog = Object.assign({}, mailLog, {
      mailDate: mailLog.mailDate != null && mailLog.mailDate.isValid() ? mailLog.mailDate.toJSON() : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.mailDate = res.body.mailDate != null ? moment(res.body.mailDate) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((mailLog: IMailLog) => {
        mailLog.mailDate = mailLog.mailDate != null ? moment(mailLog.mailDate) : null;
      });
    }
    return res;
  }
}
