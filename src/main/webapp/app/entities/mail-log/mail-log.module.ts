import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { QeeperSharedModule } from 'app/shared';
import {
  MailLogComponent,
  MailLogDetailComponent,
  MailLogUpdateComponent,
  MailLogDeletePopupComponent,
  MailLogDeleteDialogComponent,
  mailLogRoute,
  mailLogPopupRoute
} from './';

const ENTITY_STATES = [...mailLogRoute, ...mailLogPopupRoute];

@NgModule({
  imports: [QeeperSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    MailLogComponent,
    MailLogDetailComponent,
    MailLogUpdateComponent,
    MailLogDeleteDialogComponent,
    MailLogDeletePopupComponent
  ],
  entryComponents: [MailLogComponent, MailLogUpdateComponent, MailLogDeleteDialogComponent, MailLogDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class QeeperMailLogModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
