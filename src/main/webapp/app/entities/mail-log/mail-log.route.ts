import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { MailLog } from 'app/shared/model/mail-log.model';
import { MailLogService } from './mail-log.service';
import { MailLogComponent } from './mail-log.component';
import { MailLogDetailComponent } from './mail-log-detail.component';
import { MailLogUpdateComponent } from './mail-log-update.component';
import { MailLogDeletePopupComponent } from './mail-log-delete-dialog.component';
import { IMailLog } from 'app/shared/model/mail-log.model';

@Injectable({ providedIn: 'root' })
export class MailLogResolve implements Resolve<IMailLog> {
  constructor(private service: MailLogService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IMailLog> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<MailLog>) => response.ok),
        map((mailLog: HttpResponse<MailLog>) => mailLog.body)
      );
    }
    return of(new MailLog());
  }
}

export const mailLogRoute: Routes = [
  {
    path: '',
    component: MailLogComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'qeeperApp.mailLog.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: MailLogDetailComponent,
    resolve: {
      mailLog: MailLogResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'qeeperApp.mailLog.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: MailLogUpdateComponent,
    resolve: {
      mailLog: MailLogResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'qeeperApp.mailLog.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: MailLogUpdateComponent,
    resolve: {
      mailLog: MailLogResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'qeeperApp.mailLog.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const mailLogPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: MailLogDeletePopupComponent,
    resolve: {
      mailLog: MailLogResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'qeeperApp.mailLog.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
