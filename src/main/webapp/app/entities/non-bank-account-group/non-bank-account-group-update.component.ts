import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { INonBankAccountGroup, NonBankAccountGroup } from 'app/shared/model/non-bank-account-group.model';
import { NonBankAccountGroupService } from './non-bank-account-group.service';

@Component({
  selector: 'jhi-non-bank-account-group-update',
  templateUrl: './non-bank-account-group-update.component.html'
})
export class NonBankAccountGroupUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    code: [null, [Validators.required, Validators.maxLength(50)]],
    name: [null, [Validators.required, Validators.maxLength(100)]]
  });

  constructor(
    protected nonBankAccountGroupService: NonBankAccountGroupService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ nonBankAccountGroup }) => {
      this.updateForm(nonBankAccountGroup);
    });
  }

  updateForm(nonBankAccountGroup: INonBankAccountGroup) {
    this.editForm.patchValue({
      id: nonBankAccountGroup.id,
      code: nonBankAccountGroup.code,
      name: nonBankAccountGroup.name
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const nonBankAccountGroup = this.createFromForm();
    if (nonBankAccountGroup.id !== undefined) {
      this.subscribeToSaveResponse(this.nonBankAccountGroupService.update(nonBankAccountGroup));
    } else {
      this.subscribeToSaveResponse(this.nonBankAccountGroupService.create(nonBankAccountGroup));
    }
  }

  private createFromForm(): INonBankAccountGroup {
    return {
      ...new NonBankAccountGroup(),
      id: this.editForm.get(['id']).value,
      code: this.editForm.get(['code']).value,
      name: this.editForm.get(['name']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<INonBankAccountGroup>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
