import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { INonBankAccountGroup } from 'app/shared/model/non-bank-account-group.model';

type EntityResponseType = HttpResponse<INonBankAccountGroup>;
type EntityArrayResponseType = HttpResponse<INonBankAccountGroup[]>;

@Injectable({ providedIn: 'root' })
export class NonBankAccountGroupService {
  public resourceUrl = SERVER_API_URL + 'api/non-bank-account-groups';

  constructor(protected http: HttpClient) {}

  create(nonBankAccountGroup: INonBankAccountGroup): Observable<EntityResponseType> {
    return this.http.post<INonBankAccountGroup>(this.resourceUrl, nonBankAccountGroup, { observe: 'response' });
  }

  update(nonBankAccountGroup: INonBankAccountGroup): Observable<EntityResponseType> {
    return this.http.put<INonBankAccountGroup>(this.resourceUrl, nonBankAccountGroup, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<INonBankAccountGroup>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<INonBankAccountGroup[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
