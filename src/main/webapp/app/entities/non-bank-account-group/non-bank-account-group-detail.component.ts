import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { INonBankAccountGroup } from 'app/shared/model/non-bank-account-group.model';

@Component({
  selector: 'jhi-non-bank-account-group-detail',
  templateUrl: './non-bank-account-group-detail.component.html'
})
export class NonBankAccountGroupDetailComponent implements OnInit {
  nonBankAccountGroup: INonBankAccountGroup;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ nonBankAccountGroup }) => {
      this.nonBankAccountGroup = nonBankAccountGroup;
    });
  }

  previousState() {
    window.history.back();
  }
}
