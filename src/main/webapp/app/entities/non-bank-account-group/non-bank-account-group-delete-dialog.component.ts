import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { INonBankAccountGroup } from 'app/shared/model/non-bank-account-group.model';
import { NonBankAccountGroupService } from './non-bank-account-group.service';

@Component({
  selector: 'jhi-non-bank-account-group-delete-dialog',
  templateUrl: './non-bank-account-group-delete-dialog.component.html'
})
export class NonBankAccountGroupDeleteDialogComponent {
  nonBankAccountGroup: INonBankAccountGroup;

  constructor(
    protected nonBankAccountGroupService: NonBankAccountGroupService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.nonBankAccountGroupService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'nonBankAccountGroupListModification',
        content: 'Deleted an nonBankAccountGroup'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-non-bank-account-group-delete-popup',
  template: ''
})
export class NonBankAccountGroupDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ nonBankAccountGroup }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(NonBankAccountGroupDeleteDialogComponent as Component, {
          size: 'lg',
          backdrop: 'static'
        });
        this.ngbModalRef.componentInstance.nonBankAccountGroup = nonBankAccountGroup;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/non-bank-account-group', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/non-bank-account-group', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
