import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { QeeperSharedModule } from 'app/shared';
import {
  NonBankAccountGroupComponent,
  NonBankAccountGroupDetailComponent,
  NonBankAccountGroupUpdateComponent,
  NonBankAccountGroupDeletePopupComponent,
  NonBankAccountGroupDeleteDialogComponent,
  nonBankAccountGroupRoute,
  nonBankAccountGroupPopupRoute
} from './';

const ENTITY_STATES = [...nonBankAccountGroupRoute, ...nonBankAccountGroupPopupRoute];

@NgModule({
  imports: [QeeperSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    NonBankAccountGroupComponent,
    NonBankAccountGroupDetailComponent,
    NonBankAccountGroupUpdateComponent,
    NonBankAccountGroupDeleteDialogComponent,
    NonBankAccountGroupDeletePopupComponent
  ],
  entryComponents: [
    NonBankAccountGroupComponent,
    NonBankAccountGroupUpdateComponent,
    NonBankAccountGroupDeleteDialogComponent,
    NonBankAccountGroupDeletePopupComponent
  ],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class QeeperNonBankAccountGroupModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
