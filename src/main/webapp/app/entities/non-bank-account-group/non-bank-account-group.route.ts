import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { NonBankAccountGroup } from 'app/shared/model/non-bank-account-group.model';
import { NonBankAccountGroupService } from './non-bank-account-group.service';
import { NonBankAccountGroupComponent } from './non-bank-account-group.component';
import { NonBankAccountGroupDetailComponent } from './non-bank-account-group-detail.component';
import { NonBankAccountGroupUpdateComponent } from './non-bank-account-group-update.component';
import { NonBankAccountGroupDeletePopupComponent } from './non-bank-account-group-delete-dialog.component';
import { INonBankAccountGroup } from 'app/shared/model/non-bank-account-group.model';

@Injectable({ providedIn: 'root' })
export class NonBankAccountGroupResolve implements Resolve<INonBankAccountGroup> {
  constructor(private service: NonBankAccountGroupService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<INonBankAccountGroup> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<NonBankAccountGroup>) => response.ok),
        map((nonBankAccountGroup: HttpResponse<NonBankAccountGroup>) => nonBankAccountGroup.body)
      );
    }
    return of(new NonBankAccountGroup());
  }
}

export const nonBankAccountGroupRoute: Routes = [
  {
    path: '',
    component: NonBankAccountGroupComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'qeeperApp.nonBankAccountGroup.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: NonBankAccountGroupDetailComponent,
    resolve: {
      nonBankAccountGroup: NonBankAccountGroupResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'qeeperApp.nonBankAccountGroup.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: NonBankAccountGroupUpdateComponent,
    resolve: {
      nonBankAccountGroup: NonBankAccountGroupResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'qeeperApp.nonBankAccountGroup.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: NonBankAccountGroupUpdateComponent,
    resolve: {
      nonBankAccountGroup: NonBankAccountGroupResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'qeeperApp.nonBankAccountGroup.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const nonBankAccountGroupPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: NonBankAccountGroupDeletePopupComponent,
    resolve: {
      nonBankAccountGroup: NonBankAccountGroupResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'qeeperApp.nonBankAccountGroup.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
