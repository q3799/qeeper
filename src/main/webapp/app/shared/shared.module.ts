import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { QeeperSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [QeeperSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [QeeperSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class QeeperSharedModule {
  static forRoot() {
    return {
      ngModule: QeeperSharedModule
    };
  }
}
