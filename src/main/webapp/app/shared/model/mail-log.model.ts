import { Moment } from 'moment';

export const enum MailServer {
  PRIMARY = 'PRIMARY',
  SECONDARY = 'SECONDARY'
}

export interface IMailLog {
  id?: number;
  subject?: string;
  content?: string;
  mailDate?: Moment;
  mailServer?: MailServer;
  responseReceived?: string;
  remarks?: string;
  toList?: string;
  ccList?: string;
  processingTime?: number;
}

export class MailLog implements IMailLog {
  constructor(
    public id?: number,
    public subject?: string,
    public content?: string,
    public mailDate?: Moment,
    public mailServer?: MailServer,
    public responseReceived?: string,
    public remarks?: string,
    public toList?: string,
    public ccList?: string,
    public processingTime?: number
  ) {}
}
