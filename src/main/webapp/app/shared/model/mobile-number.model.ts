export const enum Operator {
  AIRTEL = 'AIRTEL',
  VODAFONE = 'VODAFONE',
  JIO = 'JIO',
  IDEA = 'IDEA',
  UNINOR = 'UNINOR',
  OTHER = 'OTHER'
}

export const enum MobileConnectionPlan {
  PREPAID = 'PREPAID',
  POSTPAID = 'POSTPAID'
}

export interface IMobileNumber {
  id?: number;
  number?: string;
  assetTag?: string;
  operator?: Operator;
  mobileConnectionPlan?: MobileConnectionPlan;
  remarks?: string;
  active?: boolean;
  registeredToName?: string;
  registeredToId?: number;
}

export class MobileNumber implements IMobileNumber {
  constructor(
    public id?: number,
    public number?: string,
    public assetTag?: string,
    public operator?: Operator,
    public mobileConnectionPlan?: MobileConnectionPlan,
    public remarks?: string,
    public active?: boolean,
    public registeredToName?: string,
    public registeredToId?: number
  ) {
    this.active = this.active || false;
  }
}
