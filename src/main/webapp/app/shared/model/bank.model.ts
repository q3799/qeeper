export interface IBank {
  id?: number;
  name?: string;
  url?: string;
}

export class Bank implements IBank {
  constructor(public id?: number, public name?: string, public url?: string) {}
}
