export interface IBankBranch {
  id?: number;
  location?: string;
  address?: string;
  ifscCode?: string;
  micrCode?: string;
  remarks?: string;
  bankName?: string;
  bankId?: number;
}

export class BankBranch implements IBankBranch {
  constructor(
    public id?: number,
    public location?: string,
    public address?: string,
    public ifscCode?: string,
    public micrCode?: string,
    public remarks?: string,
    public bankName?: string,
    public bankId?: number
  ) {}
}
