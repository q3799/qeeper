import { IBankAccount } from 'app/shared/model/bank-account.model';

export interface IBankAccountGroup {
  id?: number;
  code?: string;
  name?: string;
  bankAccounts?: IBankAccount[];
}

export class BankAccountGroup implements IBankAccountGroup {
  constructor(public id?: number, public code?: string, public name?: string, public bankAccounts?: IBankAccount[]) {}
}
