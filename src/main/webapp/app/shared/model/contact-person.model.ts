export interface IContactPerson {
  id?: number;
  name?: string;
  primaryEmail?: string;
  secondaryEmail?: string;
  primaryPhone?: string;
  secondaryPhone?: string;
  active?: boolean;
}

export class ContactPerson implements IContactPerson {
  constructor(
    public id?: number,
    public name?: string,
    public primaryEmail?: string,
    public secondaryEmail?: string,
    public primaryPhone?: string,
    public secondaryPhone?: string,
    public active?: boolean
  ) {
    this.active = this.active || false;
  }
}
