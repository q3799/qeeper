export interface ISecretQuestion {
  id?: number;
  question?: string;
  answer?: string;
  bankAccountId?: number;
}

export class SecretQuestion implements ISecretQuestion {
  constructor(public id?: number, public question?: string, public answer?: string, public bankAccountId?: number) {}
}
