import { Moment } from 'moment';
import { ICard } from 'app/shared/model/card.model';
import { ISecretQuestion } from 'app/shared/model/secret-question.model';

export const enum BankAccountType {
  SAVING = 'SAVING',
  CURRENT = 'CURRENT',
  SECURITIES = 'SECURITIES',
  INSURANCE = 'INSURANCE',
  MUTUAL_FUND = 'MUTUAL_FUND'
}

export const enum AccountStatus {
  ACTIVE = 'ACTIVE',
  IN_USE = 'IN_USE',
  IN_ACTIVE = 'IN_ACTIVE',
  SUSPENDED = 'SUSPENDED'
}

export interface IBankAccount {
  id?: number;
  dateOfOpening?: Moment;
  openedBy?: string;
  customerId?: string;
  accountNumber?: string;
  loginId?: string;
  loginPassword?: string;
  email?: string;
  phone?: string;
  bankAccountType?: BankAccountType;
  chequeBookIssued?: boolean;
  accountBalance?: number;
  balanceCheckedOn?: Moment;
  minimumBalance?: number;
  passwordGetsExpired?: boolean;
  expirationPeriodDays?: number;
  passwordUpdatedOn?: Moment;
  remarks?: string;
  accountStatus?: AccountStatus;
  multipleSessionAllowed?: boolean;
  projectName?: string;
  projectCode?: string;
  requestedOn?: Moment;
  requestedBy?: string;
  accountHolderName?: string;
  accountHolderId?: number;
  bankBranchLocation?: string;
  bankBranchId?: number;
  mobileNumberMappedNumber?: string;
  mobileNumberMappedId?: number;
  parentAccountId?: number;
  cards?: ICard[];
  bankAccountGroupName?: string;
  bankAccountGroupId?: number;
  secretQuestions?: ISecretQuestion[];
}

export class BankAccount implements IBankAccount {
  constructor(
    public id?: number,
    public dateOfOpening?: Moment,
    public openedBy?: string,
    public customerId?: string,
    public accountNumber?: string,
    public loginId?: string,
    public loginPassword?: string,
    public email?: string,
    public phone?: string,
    public bankAccountType?: BankAccountType,
    public chequeBookIssued?: boolean,
    public accountBalance?: number,
    public balanceCheckedOn?: Moment,
    public minimumBalance?: number,
    public passwordGetsExpired?: boolean,
    public expirationPeriodDays?: number,
    public passwordUpdatedOn?: Moment,
    public remarks?: string,
    public accountStatus?: AccountStatus,
    public multipleSessionAllowed?: boolean,
    public projectName?: string,
    public projectCode?: string,
    public requestedOn?: Moment,
    public requestedBy?: string,
    public accountHolderName?: string,
    public accountHolderId?: number,
    public bankBranchLocation?: string,
    public bankBranchId?: number,
    public mobileNumberMappedNumber?: string,
    public mobileNumberMappedId?: number,
    public parentAccountId?: number,
    public cards?: ICard[],
    public bankAccountGroupName?: string,
    public bankAccountGroupId?: number,
    public secretQuestions?: ISecretQuestion[]
  ) {
    this.chequeBookIssued = this.chequeBookIssued || false;
    this.passwordGetsExpired = this.passwordGetsExpired || false;
    this.multipleSessionAllowed = this.multipleSessionAllowed || false;
  }
}
