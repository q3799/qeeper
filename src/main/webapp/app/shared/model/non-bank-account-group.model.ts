import { INonBankAccount } from 'app/shared/model/non-bank-account.model';

export interface INonBankAccountGroup {
  id?: number;
  code?: string;
  name?: string;
  nonBankAccounts?: INonBankAccount[];
}

export class NonBankAccountGroup implements INonBankAccountGroup {
  constructor(public id?: number, public code?: string, public name?: string, public nonBankAccounts?: INonBankAccount[]) {}
}
