import { Moment } from 'moment';

export const enum AccountStatus {
  ACTIVE = 'ACTIVE',
  IN_USE = 'IN_USE',
  IN_ACTIVE = 'IN_ACTIVE',
  SUSPENDED = 'SUSPENDED'
}

export interface INonBankAccount {
  id?: number;
  loginId?: string;
  loginPassword?: string;
  name?: string;
  email?: string;
  phone?: string;
  passwordGetsExpired?: boolean;
  passwordUpdatedOn?: Moment;
  expirationPeriodDays?: number;
  accountStatus?: AccountStatus;
  remarks?: string;
  multipleSessionAllowed?: boolean;
  accountId?: string;
  nonBankClientName?: string;
  nonBankClientId?: number;
  nonBankAccountGroupName?: string;
  nonBankAccountGroupId?: number;
}

export class NonBankAccount implements INonBankAccount {
  constructor(
    public id?: number,
    public loginId?: string,
    public loginPassword?: string,
    public name?: string,
    public email?: string,
    public phone?: string,
    public passwordGetsExpired?: boolean,
    public passwordUpdatedOn?: Moment,
    public expirationPeriodDays?: number,
    public accountStatus?: AccountStatus,
    public remarks?: string,
    public multipleSessionAllowed?: boolean,
    public accountId?: string,
    public nonBankClientName?: string,
    public nonBankClientId?: number,
    public nonBankAccountGroupName?: string,
    public nonBankAccountGroupId?: number
  ) {
    this.passwordGetsExpired = this.passwordGetsExpired || false;
    this.multipleSessionAllowed = this.multipleSessionAllowed || false;
  }
}
