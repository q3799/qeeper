export interface IEmployee {
  id?: number;
  name?: string;
  pan?: string;
  dateOfBirth?: string;
  aadhaar?: string;
  email?: string;
  phone?: string;
  communicationAddress?: string;
  permanentAddress?: string;
  remarks?: string;
  active?: boolean;
}

export class Employee implements IEmployee {
  constructor(
    public id?: number,
    public name?: string,
    public pan?: string,
    public dateOfBirth?: string,
    public aadhaar?: string,
    public email?: string,
    public phone?: string,
    public communicationAddress?: string,
    public permanentAddress?: string,
    public remarks?: string,
    public active?: boolean
  ) {
    this.active = this.active || false;
  }
}
