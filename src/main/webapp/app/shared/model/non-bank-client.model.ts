export interface INonBankClient {
  id?: number;
  name?: string;
  url?: string;
}

export class NonBankClient implements INonBankClient {
  constructor(public id?: number, public name?: string, public url?: string) {}
}
