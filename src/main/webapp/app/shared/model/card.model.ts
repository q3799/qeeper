export const enum CardType {
  DEBIT = 'DEBIT',
  CREDIT = 'CREDIT',
  PREPAID = 'PREPAID',
  FOREX = 'FOREX'
}

export const enum PaymentNetwork {
  VISA = 'VISA',
  MASTERCARD = 'MASTERCARD',
  DISCOVER = 'DISCOVER',
  MAESTRO = 'MAESTRO',
  RUPAY = 'RUPAY'
}

export interface ICard {
  id?: number;
  cardType?: CardType;
  paymentNetwork?: PaymentNetwork;
  cardNumber?: string;
  validFrom?: string;
  validTill?: string;
  paymentNetworkPin?: string;
  cvv?: string;
  nameOnCard?: string;
  remarks?: string;
  pin?: string;
  tpin?: string;
  mobileNumberMappedNumber?: string;
  mobileNumberMappedId?: number;
  registeredToName?: string;
  registeredToId?: number;
  bankName?: string;
  bankId?: number;
  bankAccountId?: number;
  cardGroupName?: string;
  cardGroupId?: number;
}

export class Card implements ICard {
  constructor(
    public id?: number,
    public cardType?: CardType,
    public paymentNetwork?: PaymentNetwork,
    public cardNumber?: string,
    public validFrom?: string,
    public validTill?: string,
    public paymentNetworkPin?: string,
    public cvv?: string,
    public nameOnCard?: string,
    public remarks?: string,
    public pin?: string,
    public tpin?: string,
    public mobileNumberMappedNumber?: string,
    public mobileNumberMappedId?: number,
    public registeredToName?: string,
    public registeredToId?: number,
    public bankName?: string,
    public bankId?: number,
    public bankAccountId?: number,
    public cardGroupName?: string,
    public cardGroupId?: number
  ) {}
}
