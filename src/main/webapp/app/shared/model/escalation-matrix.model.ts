export const enum EscalationType {
  PASSWORD_EXPIRE = 'PASSWORD_EXPIRE',
  ACCOUNT_NOT_WORKING = 'ACCOUNT_NOT_WORKING',
  OTHER = 'OTHER'
}

export interface IEscalationMatrix {
  id?: number;
  escalationType?: EscalationType;
  level?: number;
  receiveSms?: boolean;
  receiveEmail?: boolean;
  contactPersonName?: string;
  contactPersonId?: number;
}

export class EscalationMatrix implements IEscalationMatrix {
  constructor(
    public id?: number,
    public escalationType?: EscalationType,
    public level?: number,
    public receiveSms?: boolean,
    public receiveEmail?: boolean,
    public contactPersonName?: string,
    public contactPersonId?: number
  ) {
    this.receiveSms = this.receiveSms || false;
    this.receiveEmail = this.receiveEmail || false;
  }
}
