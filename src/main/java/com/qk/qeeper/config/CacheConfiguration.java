package com.qk.qeeper.config;

import java.time.Duration;

import org.ehcache.config.builders.*;
import org.ehcache.jsr107.Eh107Configuration;

import org.hibernate.cache.jcache.ConfigSettings;
import io.github.jhipster.config.JHipsterProperties;

import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.boot.autoconfigure.orm.jpa.HibernatePropertiesCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcache.getTimeToLiveSeconds())))
                .build());
    }

    @Bean
    public HibernatePropertiesCustomizer hibernatePropertiesCustomizer(javax.cache.CacheManager cacheManager) {
        return hibernateProperties -> hibernateProperties.put(ConfigSettings.CACHE_MANAGER, cacheManager);
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            createCache(cm, com.qk.qeeper.repository.UserRepository.USERS_BY_LOGIN_CACHE);
            createCache(cm, com.qk.qeeper.repository.UserRepository.USERS_BY_EMAIL_CACHE);
            createCache(cm, com.qk.qeeper.domain.User.class.getName());
            createCache(cm, com.qk.qeeper.domain.Authority.class.getName());
            createCache(cm, com.qk.qeeper.domain.User.class.getName() + ".authorities");
            createCache(cm, com.qk.qeeper.domain.Bank.class.getName());
            createCache(cm, com.qk.qeeper.domain.ContactPerson.class.getName());
            createCache(cm, com.qk.qeeper.domain.EscalationMatrix.class.getName());
            createCache(cm, com.qk.qeeper.domain.BankBranch.class.getName());
            createCache(cm, com.qk.qeeper.domain.MobileNumber.class.getName());
            createCache(cm, com.qk.qeeper.domain.Employee.class.getName());
            createCache(cm, com.qk.qeeper.domain.BankAccount.class.getName());
            createCache(cm, com.qk.qeeper.domain.Card.class.getName());
            createCache(cm, com.qk.qeeper.domain.BankAccount.class.getName() + ".cards");
            createCache(cm, com.qk.qeeper.domain.MailLog.class.getName());
            createCache(cm, com.qk.qeeper.domain.NonBankClient.class.getName());
            createCache(cm, com.qk.qeeper.domain.NonBankAccount.class.getName());
            createCache(cm, com.qk.qeeper.domain.CardGroup.class.getName());
            createCache(cm, com.qk.qeeper.domain.CardGroup.class.getName() + ".cards");
            createCache(cm, com.qk.qeeper.domain.BankAccountGroup.class.getName());
            createCache(cm, com.qk.qeeper.domain.BankAccountGroup.class.getName() + ".bankAccounts");
            createCache(cm, com.qk.qeeper.domain.NonBankAccountGroup.class.getName());
            createCache(cm, com.qk.qeeper.domain.NonBankAccountGroup.class.getName() + ".nonBankAccounts");
            createCache(cm, com.qk.qeeper.domain.SecretQuestion.class.getName());
            createCache(cm, com.qk.qeeper.domain.BankAccount.class.getName() + ".secretQuestions");
            // jhipster-needle-ehcache-add-entry
        };
    }

    private void createCache(javax.cache.CacheManager cm, String cacheName) {
        javax.cache.Cache<Object, Object> cache = cm.getCache(cacheName);
        if (cache != null) {
            cm.destroyCache(cacheName);
        }
        cm.createCache(cacheName, jcacheConfiguration);
    }
}
