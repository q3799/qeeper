package com.qk.qeeper.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A NonBankAccountGroup.
 */
@Entity
@Table(name = "non_bank_account_group")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class NonBankAccountGroup implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 50)
    @Column(name = "code", length = 50, nullable = false)
    private String code;

    @NotNull
    @Size(max = 100)
    @Column(name = "name", length = 100, nullable = false)
    private String name;

    @OneToMany(mappedBy = "nonBankAccountGroup")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<NonBankAccount> nonBankAccounts = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public NonBankAccountGroup code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public NonBankAccountGroup name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<NonBankAccount> getNonBankAccounts() {
        return nonBankAccounts;
    }

    public NonBankAccountGroup nonBankAccounts(Set<NonBankAccount> nonBankAccounts) {
        this.nonBankAccounts = nonBankAccounts;
        return this;
    }

    public NonBankAccountGroup addNonBankAccounts(NonBankAccount nonBankAccount) {
        this.nonBankAccounts.add(nonBankAccount);
        nonBankAccount.setNonBankAccountGroup(this);
        return this;
    }

    public NonBankAccountGroup removeNonBankAccounts(NonBankAccount nonBankAccount) {
        this.nonBankAccounts.remove(nonBankAccount);
        nonBankAccount.setNonBankAccountGroup(null);
        return this;
    }

    public void setNonBankAccounts(Set<NonBankAccount> nonBankAccounts) {
        this.nonBankAccounts = nonBankAccounts;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof NonBankAccountGroup)) {
            return false;
        }
        return id != null && id.equals(((NonBankAccountGroup) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "NonBankAccountGroup{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", name='" + getName() + "'" +
            "}";
    }
}
