package com.qk.qeeper.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

import com.qk.qeeper.domain.enumeration.CardType;

import com.qk.qeeper.domain.enumeration.PaymentNetwork;

/**
 * A Card.
 */
@Entity
@Table(name = "card")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Card implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "card_type", nullable = false)
    private CardType cardType;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "payment_network", nullable = false)
    private PaymentNetwork paymentNetwork;

    @NotNull
    @Size(max = 30)
    @Pattern(regexp = "^(?:4[0-9]{12}(?:[0-9]{3})?|[25][1-7][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\\d{3})\\d{11})$")
    @Column(name = "card_number", length = 30, nullable = false)
    private String cardNumber;

    @NotNull
    @Size(max = 30)
    @Pattern(regexp = "^(0[1-9]|1[0-2])\\/([0-9]{4}|[0-9]{2})$")
    @Column(name = "valid_from", length = 30, nullable = false)
    private String validFrom;

    @NotNull
    @Size(max = 30)
    @Pattern(regexp = "^(0[1-9]|1[0-2])\\/([0-9]{4}|[0-9]{2})$")
    @Column(name = "valid_till", length = 30, nullable = false)
    private String validTill;

    @Size(min = 1, max = 10)
    @Pattern(regexp = "^\\d{1,8}$")
    @Column(name = "payment_network_pin", length = 10)
    private String paymentNetworkPin;

    @NotNull
    @Size(min = 3, max = 5)
    @Pattern(regexp = "^\\d{3,5}$")
    @Column(name = "cvv", length = 5, nullable = false)
    private String cvv;

    @NotNull
    @Size(max = 100)
    @Column(name = "name_on_card", length = 100, nullable = false)
    private String nameOnCard;

    @Size(max = 1000)
    @Column(name = "remarks", length = 1000)
    private String remarks;

    @Size(min = 3, max = 6)
    @Pattern(regexp = "^\\d{3,6}$")
    @Column(name = "pin", length = 6)
    private String pin;

    @Size(min = 3, max = 6)
    @Pattern(regexp = "^\\d{3,6}$")
    @Column(name = "tpin", length = 6)
    private String tpin;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("cards")
    private MobileNumber mobileNumberMapped;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("cards")
    private Employee registeredTo;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("cards")
    private Bank bank;

    @ManyToOne
    @JsonIgnoreProperties("cards")
    private BankAccount bankAccount;

    @ManyToOne
    @JsonIgnoreProperties("cards")
    private CardGroup cardGroup;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CardType getCardType() {
        return cardType;
    }

    public Card cardType(CardType cardType) {
        this.cardType = cardType;
        return this;
    }

    public void setCardType(CardType cardType) {
        this.cardType = cardType;
    }

    public PaymentNetwork getPaymentNetwork() {
        return paymentNetwork;
    }

    public Card paymentNetwork(PaymentNetwork paymentNetwork) {
        this.paymentNetwork = paymentNetwork;
        return this;
    }

    public void setPaymentNetwork(PaymentNetwork paymentNetwork) {
        this.paymentNetwork = paymentNetwork;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public Card cardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
        return this;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getValidFrom() {
        return validFrom;
    }

    public Card validFrom(String validFrom) {
        this.validFrom = validFrom;
        return this;
    }

    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }

    public String getValidTill() {
        return validTill;
    }

    public Card validTill(String validTill) {
        this.validTill = validTill;
        return this;
    }

    public void setValidTill(String validTill) {
        this.validTill = validTill;
    }

    public String getPaymentNetworkPin() {
        return paymentNetworkPin;
    }

    public Card paymentNetworkPin(String paymentNetworkPin) {
        this.paymentNetworkPin = paymentNetworkPin;
        return this;
    }

    public void setPaymentNetworkPin(String paymentNetworkPin) {
        this.paymentNetworkPin = paymentNetworkPin;
    }

    public String getCvv() {
        return cvv;
    }

    public Card cvv(String cvv) {
        this.cvv = cvv;
        return this;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public String getNameOnCard() {
        return nameOnCard;
    }

    public Card nameOnCard(String nameOnCard) {
        this.nameOnCard = nameOnCard;
        return this;
    }

    public void setNameOnCard(String nameOnCard) {
        this.nameOnCard = nameOnCard;
    }

    public String getRemarks() {
        return remarks;
    }

    public Card remarks(String remarks) {
        this.remarks = remarks;
        return this;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getPin() {
        return pin;
    }

    public Card pin(String pin) {
        this.pin = pin;
        return this;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getTpin() {
        return tpin;
    }

    public Card tpin(String tpin) {
        this.tpin = tpin;
        return this;
    }

    public void setTpin(String tpin) {
        this.tpin = tpin;
    }

    public MobileNumber getMobileNumberMapped() {
        return mobileNumberMapped;
    }

    public Card mobileNumberMapped(MobileNumber mobileNumber) {
        this.mobileNumberMapped = mobileNumber;
        return this;
    }

    public void setMobileNumberMapped(MobileNumber mobileNumber) {
        this.mobileNumberMapped = mobileNumber;
    }

    public Employee getRegisteredTo() {
        return registeredTo;
    }

    public Card registeredTo(Employee employee) {
        this.registeredTo = employee;
        return this;
    }

    public void setRegisteredTo(Employee employee) {
        this.registeredTo = employee;
    }

    public Bank getBank() {
        return bank;
    }

    public Card bank(Bank bank) {
        this.bank = bank;
        return this;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public Card bankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
        return this;
    }

    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    public CardGroup getCardGroup() {
        return cardGroup;
    }

    public Card cardGroup(CardGroup cardGroup) {
        this.cardGroup = cardGroup;
        return this;
    }

    public void setCardGroup(CardGroup cardGroup) {
        this.cardGroup = cardGroup;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Card)) {
            return false;
        }
        return id != null && id.equals(((Card) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Card{" +
            "id=" + getId() +
            ", cardType='" + getCardType() + "'" +
            ", paymentNetwork='" + getPaymentNetwork() + "'" +
            ", cardNumber='" + getCardNumber() + "'" +
            ", validFrom='" + getValidFrom() + "'" +
            ", validTill='" + getValidTill() + "'" +
            ", paymentNetworkPin='" + getPaymentNetworkPin() + "'" +
            ", cvv='" + getCvv() + "'" +
            ", nameOnCard='" + getNameOnCard() + "'" +
            ", remarks='" + getRemarks() + "'" +
            ", pin='" + getPin() + "'" +
            ", tpin='" + getTpin() + "'" +
            "}";
    }
}
