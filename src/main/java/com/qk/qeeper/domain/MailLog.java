package com.qk.qeeper.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;

import com.qk.qeeper.domain.enumeration.MailServer;

/**
 * A MailLog.
 */
@Entity
@Table(name = "mail_log")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class MailLog implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 400)
    @Column(name = "subject", length = 400, nullable = false)
    private String subject;

    @Size(max = 4000)
    @Column(name = "content", length = 4000)
    private String content;

    @Column(name = "mail_date")
    private Instant mailDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "mail_server")
    private MailServer mailServer;

    @Size(max = 1000)
    @Column(name = "response_received", length = 1000)
    private String responseReceived;

    @Size(max = 2000)
    @Column(name = "remarks", length = 2000)
    private String remarks;

    @Size(max = 4000)
    @Column(name = "to_list", length = 4000)
    private String toList;

    @Size(max = 4000)
    @Column(name = "cc_list", length = 4000)
    private String ccList;

    @Min(value = 0L)
    @Column(name = "processing_time")
    private Long processingTime;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public MailLog subject(String subject) {
        this.subject = subject;
        return this;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public MailLog content(String content) {
        this.content = content;
        return this;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Instant getMailDate() {
        return mailDate;
    }

    public MailLog mailDate(Instant mailDate) {
        this.mailDate = mailDate;
        return this;
    }

    public void setMailDate(Instant mailDate) {
        this.mailDate = mailDate;
    }

    public MailServer getMailServer() {
        return mailServer;
    }

    public MailLog mailServer(MailServer mailServer) {
        this.mailServer = mailServer;
        return this;
    }

    public void setMailServer(MailServer mailServer) {
        this.mailServer = mailServer;
    }

    public String getResponseReceived() {
        return responseReceived;
    }

    public MailLog responseReceived(String responseReceived) {
        this.responseReceived = responseReceived;
        return this;
    }

    public void setResponseReceived(String responseReceived) {
        this.responseReceived = responseReceived;
    }

    public String getRemarks() {
        return remarks;
    }

    public MailLog remarks(String remarks) {
        this.remarks = remarks;
        return this;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getToList() {
        return toList;
    }

    public MailLog toList(String toList) {
        this.toList = toList;
        return this;
    }

    public void setToList(String toList) {
        this.toList = toList;
    }

    public String getCcList() {
        return ccList;
    }

    public MailLog ccList(String ccList) {
        this.ccList = ccList;
        return this;
    }

    public void setCcList(String ccList) {
        this.ccList = ccList;
    }

    public Long getProcessingTime() {
        return processingTime;
    }

    public MailLog processingTime(Long processingTime) {
        this.processingTime = processingTime;
        return this;
    }

    public void setProcessingTime(Long processingTime) {
        this.processingTime = processingTime;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MailLog)) {
            return false;
        }
        return id != null && id.equals(((MailLog) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "MailLog{" +
            "id=" + getId() +
            ", subject='" + getSubject() + "'" +
            ", content='" + getContent() + "'" +
            ", mailDate='" + getMailDate() + "'" +
            ", mailServer='" + getMailServer() + "'" +
            ", responseReceived='" + getResponseReceived() + "'" +
            ", remarks='" + getRemarks() + "'" +
            ", toList='" + getToList() + "'" +
            ", ccList='" + getCcList() + "'" +
            ", processingTime=" + getProcessingTime() +
            "}";
    }
}
