package com.qk.qeeper.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

import com.qk.qeeper.domain.enumeration.Operator;

import com.qk.qeeper.domain.enumeration.MobileConnectionPlan;

/**
 * A MobileNumber.
 */
@Entity
@Table(name = "mobile_number")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class MobileNumber implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 20)
    @Pattern(regexp = "^\\+?[1-9]\\d{1,14}$")
    @Column(name = "number", length = 20)
    private String number;

    @Size(max = 50)
    @Column(name = "asset_tag", length = 50)
    private String assetTag;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "operator", nullable = false)
    private Operator operator;

    @Enumerated(EnumType.STRING)
    @Column(name = "mobile_connection_plan")
    private MobileConnectionPlan mobileConnectionPlan;

    @Size(max = 2000)
    @Column(name = "remarks", length = 2000)
    private String remarks;

    @NotNull
    @Column(name = "active", nullable = false)
    private Boolean active;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("mobileNumbers")
    private Employee registeredTo;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public MobileNumber number(String number) {
        this.number = number;
        return this;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getAssetTag() {
        return assetTag;
    }

    public MobileNumber assetTag(String assetTag) {
        this.assetTag = assetTag;
        return this;
    }

    public void setAssetTag(String assetTag) {
        this.assetTag = assetTag;
    }

    public Operator getOperator() {
        return operator;
    }

    public MobileNumber operator(Operator operator) {
        this.operator = operator;
        return this;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    public MobileConnectionPlan getMobileConnectionPlan() {
        return mobileConnectionPlan;
    }

    public MobileNumber mobileConnectionPlan(MobileConnectionPlan mobileConnectionPlan) {
        this.mobileConnectionPlan = mobileConnectionPlan;
        return this;
    }

    public void setMobileConnectionPlan(MobileConnectionPlan mobileConnectionPlan) {
        this.mobileConnectionPlan = mobileConnectionPlan;
    }

    public String getRemarks() {
        return remarks;
    }

    public MobileNumber remarks(String remarks) {
        this.remarks = remarks;
        return this;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Boolean isActive() {
        return active;
    }

    public MobileNumber active(Boolean active) {
        this.active = active;
        return this;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Employee getRegisteredTo() {
        return registeredTo;
    }

    public MobileNumber registeredTo(Employee employee) {
        this.registeredTo = employee;
        return this;
    }

    public void setRegisteredTo(Employee employee) {
        this.registeredTo = employee;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MobileNumber)) {
            return false;
        }
        return id != null && id.equals(((MobileNumber) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "MobileNumber{" +
            "id=" + getId() +
            ", number='" + getNumber() + "'" +
            ", assetTag='" + getAssetTag() + "'" +
            ", operator='" + getOperator() + "'" +
            ", mobileConnectionPlan='" + getMobileConnectionPlan() + "'" +
            ", remarks='" + getRemarks() + "'" +
            ", active='" + isActive() + "'" +
            "}";
    }
}
