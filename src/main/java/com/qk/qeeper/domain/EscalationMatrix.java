package com.qk.qeeper.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

import com.qk.qeeper.domain.enumeration.EscalationType;

/**
 * A EscalationMatrix.
 */
@Entity
@Table(name = "escalation_matrix")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class EscalationMatrix implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "escalation_type", nullable = false)
    private EscalationType escalationType;

    @NotNull
    @Min(value = 1)
    @Max(value = 100)
    @Column(name = "level", nullable = false)
    private Integer level;

    @NotNull
    @Column(name = "receive_sms", nullable = false)
    private Boolean receiveSms;

    @NotNull
    @Column(name = "receive_email", nullable = false)
    private Boolean receiveEmail;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("escalationMatrices")
    private ContactPerson contactPerson;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EscalationType getEscalationType() {
        return escalationType;
    }

    public EscalationMatrix escalationType(EscalationType escalationType) {
        this.escalationType = escalationType;
        return this;
    }

    public void setEscalationType(EscalationType escalationType) {
        this.escalationType = escalationType;
    }

    public Integer getLevel() {
        return level;
    }

    public EscalationMatrix level(Integer level) {
        this.level = level;
        return this;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Boolean isReceiveSms() {
        return receiveSms;
    }

    public EscalationMatrix receiveSms(Boolean receiveSms) {
        this.receiveSms = receiveSms;
        return this;
    }

    public void setReceiveSms(Boolean receiveSms) {
        this.receiveSms = receiveSms;
    }

    public Boolean isReceiveEmail() {
        return receiveEmail;
    }

    public EscalationMatrix receiveEmail(Boolean receiveEmail) {
        this.receiveEmail = receiveEmail;
        return this;
    }

    public void setReceiveEmail(Boolean receiveEmail) {
        this.receiveEmail = receiveEmail;
    }

    public ContactPerson getContactPerson() {
        return contactPerson;
    }

    public EscalationMatrix contactPerson(ContactPerson contactPerson) {
        this.contactPerson = contactPerson;
        return this;
    }

    public void setContactPerson(ContactPerson contactPerson) {
        this.contactPerson = contactPerson;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EscalationMatrix)) {
            return false;
        }
        return id != null && id.equals(((EscalationMatrix) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "EscalationMatrix{" +
            "id=" + getId() +
            ", escalationType='" + getEscalationType() + "'" +
            ", level=" + getLevel() +
            ", receiveSms='" + isReceiveSms() + "'" +
            ", receiveEmail='" + isReceiveEmail() + "'" +
            "}";
    }
}
