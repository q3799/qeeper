package com.qk.qeeper.domain.enumeration;

/**
 * The MobileConnectionPlan enumeration.
 */
public enum MobileConnectionPlan {
    PREPAID, POSTPAID
}
