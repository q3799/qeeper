package com.qk.qeeper.domain.enumeration;

/**
 * The CardType enumeration.
 */
public enum CardType {
    DEBIT, CREDIT, PREPAID, FOREX
}
