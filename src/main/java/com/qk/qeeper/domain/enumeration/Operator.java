package com.qk.qeeper.domain.enumeration;

/**
 * The Operator enumeration.
 */
public enum Operator {
    AIRTEL, VODAFONE, JIO, IDEA, UNINOR, OTHER
}
