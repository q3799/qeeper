package com.qk.qeeper.domain.enumeration;

/**
 * The EscalationType enumeration.
 */
public enum EscalationType {
    PASSWORD_EXPIRE, ACCOUNT_NOT_WORKING, OTHER
}
