package com.qk.qeeper.domain.enumeration;

/**
 * The ResourceType enumeration
 */
public enum ResourceType {
    BANK_ACCOUNT, NON_BANK_ACCOUNT, CARD
}
