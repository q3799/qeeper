package com.qk.qeeper.domain.enumeration;

/**
 * The AccountStatus enumeration.
 */
public enum AccountStatus {
    ACTIVE, IN_USE, IN_ACTIVE, SUSPENDED
}
