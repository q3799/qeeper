package com.qk.qeeper.domain.enumeration;

/**
 * The PaymentNetwork enumeration.
 */
public enum PaymentNetwork {
    VISA, MASTERCARD, DISCOVER, MAESTRO, RUPAY
}
