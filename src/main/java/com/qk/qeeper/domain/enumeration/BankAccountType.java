package com.qk.qeeper.domain.enumeration;

/**
 * The BankAccountType enumeration.
 */
public enum BankAccountType {
    SAVING, CURRENT, SECURITIES, INSURANCE, MUTUAL_FUND
}
