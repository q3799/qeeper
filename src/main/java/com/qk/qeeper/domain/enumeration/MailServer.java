package com.qk.qeeper.domain.enumeration;

/**
 * The MailServer enumeration.
 */
public enum MailServer {
    PRIMARY, SECONDARY
}
