package com.qk.qeeper.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;

import com.qk.qeeper.domain.enumeration.AccountStatus;

/**
 * A NonBankAccount.
 */
@Entity
@Table(name = "non_bank_account")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class NonBankAccount implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 50)
    @Column(name = "login_id", length = 50, nullable = false)
    private String loginId;

    @NotNull
    @Size(max = 50)
    @Column(name = "login_password", length = 50, nullable = false)
    private String loginPassword;

    @Size(max = 100)
    @Column(name = "name", length = 100)
    private String name;

    @NotNull
    @Size(max = 200)
    @Pattern(regexp = "^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$")
    @Column(name = "email", length = 200, nullable = false)
    private String email;

    @Pattern(regexp = "^\\+?[1-9]\\d{1,14}$")
    @Column(name = "phone")
    private String phone;

    @NotNull
    @Column(name = "password_gets_expired", nullable = false)
    private Boolean passwordGetsExpired;

    @Column(name = "password_updated_on")
    private Instant passwordUpdatedOn;

    @Min(value = 0)
    @Column(name = "expiration_period_days")
    private Integer expirationPeriodDays;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "account_status", nullable = false)
    private AccountStatus accountStatus;

    @Size(max = 1000)
    @Column(name = "remarks", length = 1000)
    private String remarks;

    @NotNull
    @Column(name = "multiple_session_allowed", nullable = false)
    private Boolean multipleSessionAllowed;

    @Size(max = 100)
    @Column(name = "account_id", length = 100)
    private String accountId;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("nonBankAccounts")
    private NonBankClient nonBankClient;

    @ManyToOne
    @JsonIgnoreProperties("nonBankAccounts")
    private NonBankAccountGroup nonBankAccountGroup;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLoginId() {
        return loginId;
    }

    public NonBankAccount loginId(String loginId) {
        this.loginId = loginId;
        return this;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getLoginPassword() {
        return loginPassword;
    }

    public NonBankAccount loginPassword(String loginPassword) {
        this.loginPassword = loginPassword;
        return this;
    }

    public void setLoginPassword(String loginPassword) {
        this.loginPassword = loginPassword;
    }

    public String getName() {
        return name;
    }

    public NonBankAccount name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public NonBankAccount email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public NonBankAccount phone(String phone) {
        this.phone = phone;
        return this;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Boolean isPasswordGetsExpired() {
        return passwordGetsExpired;
    }

    public NonBankAccount passwordGetsExpired(Boolean passwordGetsExpired) {
        this.passwordGetsExpired = passwordGetsExpired;
        return this;
    }

    public void setPasswordGetsExpired(Boolean passwordGetsExpired) {
        this.passwordGetsExpired = passwordGetsExpired;
    }

    public Instant getPasswordUpdatedOn() {
        return passwordUpdatedOn;
    }

    public NonBankAccount passwordUpdatedOn(Instant passwordUpdatedOn) {
        this.passwordUpdatedOn = passwordUpdatedOn;
        return this;
    }

    public void setPasswordUpdatedOn(Instant passwordUpdatedOn) {
        this.passwordUpdatedOn = passwordUpdatedOn;
    }

    public Integer getExpirationPeriodDays() {
        return expirationPeriodDays;
    }

    public NonBankAccount expirationPeriodDays(Integer expirationPeriodDays) {
        this.expirationPeriodDays = expirationPeriodDays;
        return this;
    }

    public void setExpirationPeriodDays(Integer expirationPeriodDays) {
        this.expirationPeriodDays = expirationPeriodDays;
    }

    public AccountStatus getAccountStatus() {
        return accountStatus;
    }

    public NonBankAccount accountStatus(AccountStatus accountStatus) {
        this.accountStatus = accountStatus;
        return this;
    }

    public void setAccountStatus(AccountStatus accountStatus) {
        this.accountStatus = accountStatus;
    }

    public String getRemarks() {
        return remarks;
    }

    public NonBankAccount remarks(String remarks) {
        this.remarks = remarks;
        return this;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Boolean isMultipleSessionAllowed() {
        return multipleSessionAllowed;
    }

    public NonBankAccount multipleSessionAllowed(Boolean multipleSessionAllowed) {
        this.multipleSessionAllowed = multipleSessionAllowed;
        return this;
    }

    public void setMultipleSessionAllowed(Boolean multipleSessionAllowed) {
        this.multipleSessionAllowed = multipleSessionAllowed;
    }

    public String getAccountId() {
        return accountId;
    }

    public NonBankAccount accountId(String accountId) {
        this.accountId = accountId;
        return this;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public NonBankClient getNonBankClient() {
        return nonBankClient;
    }

    public NonBankAccount nonBankClient(NonBankClient nonBankClient) {
        this.nonBankClient = nonBankClient;
        return this;
    }

    public void setNonBankClient(NonBankClient nonBankClient) {
        this.nonBankClient = nonBankClient;
    }

    public NonBankAccountGroup getNonBankAccountGroup() {
        return nonBankAccountGroup;
    }

    public NonBankAccount nonBankAccountGroup(NonBankAccountGroup nonBankAccountGroup) {
        this.nonBankAccountGroup = nonBankAccountGroup;
        return this;
    }

    public void setNonBankAccountGroup(NonBankAccountGroup nonBankAccountGroup) {
        this.nonBankAccountGroup = nonBankAccountGroup;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof NonBankAccount)) {
            return false;
        }
        return id != null && id.equals(((NonBankAccount) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "NonBankAccount{" +
            "id=" + getId() +
            ", loginId='" + getLoginId() + "'" +
            ", loginPassword='" + getLoginPassword() + "'" +
            ", name='" + getName() + "'" +
            ", email='" + getEmail() + "'" +
            ", phone='" + getPhone() + "'" +
            ", passwordGetsExpired='" + isPasswordGetsExpired() + "'" +
            ", passwordUpdatedOn='" + getPasswordUpdatedOn() + "'" +
            ", expirationPeriodDays=" + getExpirationPeriodDays() +
            ", accountStatus='" + getAccountStatus() + "'" +
            ", remarks='" + getRemarks() + "'" +
            ", multipleSessionAllowed='" + isMultipleSessionAllowed() + "'" +
            ", accountId='" + getAccountId() + "'" +
            "}";
    }
}
