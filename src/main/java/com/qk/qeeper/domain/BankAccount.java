package com.qk.qeeper.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

import com.qk.qeeper.domain.enumeration.BankAccountType;

import com.qk.qeeper.domain.enumeration.AccountStatus;

/**
 * A BankAccount.
 */
@Entity
@Table(name = "bank_account")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class BankAccount implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "date_of_opening")
    private Instant dateOfOpening;

    @Size(max = 100)
    @Column(name = "opened_by", length = 100)
    private String openedBy;

    @NotNull
    @Size(max = 50)
    @Column(name = "customer_id", length = 50, nullable = false)
    private String customerId;

    @Size(max = 100)
    @Column(name = "account_number", length = 100)
    private String accountNumber;

    @NotNull
    @Size(max = 50)
    @Column(name = "login_id", length = 50, nullable = false)
    private String loginId;

    @NotNull
    @Size(max = 50)
    @Column(name = "login_password", length = 50, nullable = false)
    private String loginPassword;

    @NotNull
    @Size(max = 200)
    @Pattern(regexp = "^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$")
    @Column(name = "email", length = 200, nullable = false)
    private String email;

    @Pattern(regexp = "^\\+?[1-9]\\d{1,14}$")
    @Column(name = "phone")
    private String phone;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "bank_account_type", nullable = false)
    private BankAccountType bankAccountType;

    @NotNull
    @Column(name = "cheque_book_issued", nullable = false)
    private Boolean chequeBookIssued;

    @DecimalMin(value = "0")
    @Column(name = "account_balance")
    private Double accountBalance;

    @Column(name = "balance_checked_on")
    private Instant balanceCheckedOn;

    @Column(name = "minimum_balance")
    private Double minimumBalance;

    @NotNull
    @Column(name = "password_gets_expired", nullable = false)
    private Boolean passwordGetsExpired;

    @Min(value = 0)
    @Column(name = "expiration_period_days")
    private Integer expirationPeriodDays;

    @Column(name = "password_updated_on")
    private Instant passwordUpdatedOn;

    @Size(max = 1000)
    @Column(name = "remarks", length = 1000)
    private String remarks;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "account_status", nullable = false)
    private AccountStatus accountStatus;

    @NotNull
    @Column(name = "multiple_session_allowed", nullable = false)
    private Boolean multipleSessionAllowed;

    @Size(max = 100)
    @Column(name = "project_name", length = 100)
    private String projectName;

    @Size(max = 50)
    @Column(name = "project_code", length = 50)
    private String projectCode;

    @Column(name = "requested_on")
    private Instant requestedOn;

    @Size(max = 100)
    @Column(name = "requested_by", length = 100)
    private String requestedBy;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("bankAccounts")
    private Employee accountHolder;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("bankAccounts")
    private BankBranch bankBranch;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("bankAccounts")
    private MobileNumber mobileNumberMapped;

    @ManyToOne
    @JsonIgnoreProperties("bankAccounts")
    private BankAccount parentAccount;

    @OneToMany(mappedBy = "bankAccount")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Card> cards = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("bankAccounts")
    private BankAccountGroup bankAccountGroup;

    @OneToMany(mappedBy = "bankAccount")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<SecretQuestion> secretQuestions = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getDateOfOpening() {
        return dateOfOpening;
    }

    public BankAccount dateOfOpening(Instant dateOfOpening) {
        this.dateOfOpening = dateOfOpening;
        return this;
    }

    public void setDateOfOpening(Instant dateOfOpening) {
        this.dateOfOpening = dateOfOpening;
    }

    public String getOpenedBy() {
        return openedBy;
    }

    public BankAccount openedBy(String openedBy) {
        this.openedBy = openedBy;
        return this;
    }

    public void setOpenedBy(String openedBy) {
        this.openedBy = openedBy;
    }

    public String getCustomerId() {
        return customerId;
    }

    public BankAccount customerId(String customerId) {
        this.customerId = customerId;
        return this;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public BankAccount accountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
        return this;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getLoginId() {
        return loginId;
    }

    public BankAccount loginId(String loginId) {
        this.loginId = loginId;
        return this;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getLoginPassword() {
        return loginPassword;
    }

    public BankAccount loginPassword(String loginPassword) {
        this.loginPassword = loginPassword;
        return this;
    }

    public void setLoginPassword(String loginPassword) {
        this.loginPassword = loginPassword;
    }

    public String getEmail() {
        return email;
    }

    public BankAccount email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public BankAccount phone(String phone) {
        this.phone = phone;
        return this;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public BankAccountType getBankAccountType() {
        return bankAccountType;
    }

    public BankAccount bankAccountType(BankAccountType bankAccountType) {
        this.bankAccountType = bankAccountType;
        return this;
    }

    public void setBankAccountType(BankAccountType bankAccountType) {
        this.bankAccountType = bankAccountType;
    }

    public Boolean isChequeBookIssued() {
        return chequeBookIssued;
    }

    public BankAccount chequeBookIssued(Boolean chequeBookIssued) {
        this.chequeBookIssued = chequeBookIssued;
        return this;
    }

    public void setChequeBookIssued(Boolean chequeBookIssued) {
        this.chequeBookIssued = chequeBookIssued;
    }

    public Double getAccountBalance() {
        return accountBalance;
    }

    public BankAccount accountBalance(Double accountBalance) {
        this.accountBalance = accountBalance;
        return this;
    }

    public void setAccountBalance(Double accountBalance) {
        this.accountBalance = accountBalance;
    }

    public Instant getBalanceCheckedOn() {
        return balanceCheckedOn;
    }

    public BankAccount balanceCheckedOn(Instant balanceCheckedOn) {
        this.balanceCheckedOn = balanceCheckedOn;
        return this;
    }

    public void setBalanceCheckedOn(Instant balanceCheckedOn) {
        this.balanceCheckedOn = balanceCheckedOn;
    }

    public Double getMinimumBalance() {
        return minimumBalance;
    }

    public BankAccount minimumBalance(Double minimumBalance) {
        this.minimumBalance = minimumBalance;
        return this;
    }

    public void setMinimumBalance(Double minimumBalance) {
        this.minimumBalance = minimumBalance;
    }

    public Boolean isPasswordGetsExpired() {
        return passwordGetsExpired;
    }

    public BankAccount passwordGetsExpired(Boolean passwordGetsExpired) {
        this.passwordGetsExpired = passwordGetsExpired;
        return this;
    }

    public void setPasswordGetsExpired(Boolean passwordGetsExpired) {
        this.passwordGetsExpired = passwordGetsExpired;
    }

    public Integer getExpirationPeriodDays() {
        return expirationPeriodDays;
    }

    public BankAccount expirationPeriodDays(Integer expirationPeriodDays) {
        this.expirationPeriodDays = expirationPeriodDays;
        return this;
    }

    public void setExpirationPeriodDays(Integer expirationPeriodDays) {
        this.expirationPeriodDays = expirationPeriodDays;
    }

    public Instant getPasswordUpdatedOn() {
        return passwordUpdatedOn;
    }

    public BankAccount passwordUpdatedOn(Instant passwordUpdatedOn) {
        this.passwordUpdatedOn = passwordUpdatedOn;
        return this;
    }

    public void setPasswordUpdatedOn(Instant passwordUpdatedOn) {
        this.passwordUpdatedOn = passwordUpdatedOn;
    }

    public String getRemarks() {
        return remarks;
    }

    public BankAccount remarks(String remarks) {
        this.remarks = remarks;
        return this;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public AccountStatus getAccountStatus() {
        return accountStatus;
    }

    public BankAccount accountStatus(AccountStatus accountStatus) {
        this.accountStatus = accountStatus;
        return this;
    }

    public void setAccountStatus(AccountStatus accountStatus) {
        this.accountStatus = accountStatus;
    }

    public Boolean isMultipleSessionAllowed() {
        return multipleSessionAllowed;
    }

    public BankAccount multipleSessionAllowed(Boolean multipleSessionAllowed) {
        this.multipleSessionAllowed = multipleSessionAllowed;
        return this;
    }

    public void setMultipleSessionAllowed(Boolean multipleSessionAllowed) {
        this.multipleSessionAllowed = multipleSessionAllowed;
    }

    public String getProjectName() {
        return projectName;
    }

    public BankAccount projectName(String projectName) {
        this.projectName = projectName;
        return this;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectCode() {
        return projectCode;
    }

    public BankAccount projectCode(String projectCode) {
        this.projectCode = projectCode;
        return this;
    }

    public void setProjectCode(String projectCode) {
        this.projectCode = projectCode;
    }

    public Instant getRequestedOn() {
        return requestedOn;
    }

    public BankAccount requestedOn(Instant requestedOn) {
        this.requestedOn = requestedOn;
        return this;
    }

    public void setRequestedOn(Instant requestedOn) {
        this.requestedOn = requestedOn;
    }

    public String getRequestedBy() {
        return requestedBy;
    }

    public BankAccount requestedBy(String requestedBy) {
        this.requestedBy = requestedBy;
        return this;
    }

    public void setRequestedBy(String requestedBy) {
        this.requestedBy = requestedBy;
    }

    public Employee getAccountHolder() {
        return accountHolder;
    }

    public BankAccount accountHolder(Employee employee) {
        this.accountHolder = employee;
        return this;
    }

    public void setAccountHolder(Employee employee) {
        this.accountHolder = employee;
    }

    public BankBranch getBankBranch() {
        return bankBranch;
    }

    public BankAccount bankBranch(BankBranch bankBranch) {
        this.bankBranch = bankBranch;
        return this;
    }

    public void setBankBranch(BankBranch bankBranch) {
        this.bankBranch = bankBranch;
    }

    public MobileNumber getMobileNumberMapped() {
        return mobileNumberMapped;
    }

    public BankAccount mobileNumberMapped(MobileNumber mobileNumber) {
        this.mobileNumberMapped = mobileNumber;
        return this;
    }

    public void setMobileNumberMapped(MobileNumber mobileNumber) {
        this.mobileNumberMapped = mobileNumber;
    }

    public BankAccount getParentAccount() {
        return parentAccount;
    }

    public BankAccount parentAccount(BankAccount bankAccount) {
        this.parentAccount = bankAccount;
        return this;
    }

    public void setParentAccount(BankAccount bankAccount) {
        this.parentAccount = bankAccount;
    }

    public Set<Card> getCards() {
        return cards;
    }

    public BankAccount cards(Set<Card> cards) {
        this.cards = cards;
        return this;
    }

    public BankAccount addCards(Card card) {
        this.cards.add(card);
        card.setBankAccount(this);
        return this;
    }

    public BankAccount removeCards(Card card) {
        this.cards.remove(card);
        card.setBankAccount(null);
        return this;
    }

    public void setCards(Set<Card> cards) {
        this.cards = cards;
    }

    public BankAccountGroup getBankAccountGroup() {
        return bankAccountGroup;
    }

    public BankAccount bankAccountGroup(BankAccountGroup bankAccountGroup) {
        this.bankAccountGroup = bankAccountGroup;
        return this;
    }

    public void setBankAccountGroup(BankAccountGroup bankAccountGroup) {
        this.bankAccountGroup = bankAccountGroup;
    }

    public Set<SecretQuestion> getSecretQuestions() {
        return secretQuestions;
    }

    public BankAccount secretQuestions(Set<SecretQuestion> secretQuestions) {
        this.secretQuestions = secretQuestions;
        return this;
    }

    public BankAccount addSecretQuestions(SecretQuestion secretQuestion) {
        this.secretQuestions.add(secretQuestion);
        secretQuestion.setBankAccount(this);
        return this;
    }

    public BankAccount removeSecretQuestions(SecretQuestion secretQuestion) {
        this.secretQuestions.remove(secretQuestion);
        secretQuestion.setBankAccount(null);
        return this;
    }

    public void setSecretQuestions(Set<SecretQuestion> secretQuestions) {
        this.secretQuestions = secretQuestions;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BankAccount)) {
            return false;
        }
        return id != null && id.equals(((BankAccount) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "BankAccount{" +
            "id=" + getId() +
            ", dateOfOpening='" + getDateOfOpening() + "'" +
            ", openedBy='" + getOpenedBy() + "'" +
            ", customerId='" + getCustomerId() + "'" +
            ", accountNumber='" + getAccountNumber() + "'" +
            ", loginId='" + getLoginId() + "'" +
            ", loginPassword='" + getLoginPassword() + "'" +
            ", email='" + getEmail() + "'" +
            ", phone='" + getPhone() + "'" +
            ", bankAccountType='" + getBankAccountType() + "'" +
            ", chequeBookIssued='" + isChequeBookIssued() + "'" +
            ", accountBalance=" + getAccountBalance() +
            ", balanceCheckedOn='" + getBalanceCheckedOn() + "'" +
            ", minimumBalance=" + getMinimumBalance() +
            ", passwordGetsExpired='" + isPasswordGetsExpired() + "'" +
            ", expirationPeriodDays=" + getExpirationPeriodDays() +
            ", passwordUpdatedOn='" + getPasswordUpdatedOn() + "'" +
            ", remarks='" + getRemarks() + "'" +
            ", accountStatus='" + getAccountStatus() + "'" +
            ", multipleSessionAllowed='" + isMultipleSessionAllowed() + "'" +
            ", projectName='" + getProjectName() + "'" +
            ", projectCode='" + getProjectCode() + "'" +
            ", requestedOn='" + getRequestedOn() + "'" +
            ", requestedBy='" + getRequestedBy() + "'" +
            "}";
    }
}
