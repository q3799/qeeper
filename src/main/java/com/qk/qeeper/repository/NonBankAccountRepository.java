package com.qk.qeeper.repository;

import com.qk.qeeper.domain.NonBankAccount;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the NonBankAccount entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NonBankAccountRepository extends JpaRepository<NonBankAccount, Long>, JpaSpecificationExecutor<NonBankAccount> {

}
