package com.qk.qeeper.repository;

import com.qk.qeeper.domain.SecretQuestion;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the SecretQuestion entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SecretQuestionRepository extends JpaRepository<SecretQuestion, Long>, JpaSpecificationExecutor<SecretQuestion> {

}
