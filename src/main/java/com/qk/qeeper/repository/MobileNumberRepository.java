package com.qk.qeeper.repository;

import com.qk.qeeper.domain.MobileNumber;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the MobileNumber entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MobileNumberRepository extends JpaRepository<MobileNumber, Long>, JpaSpecificationExecutor<MobileNumber> {

}
