package com.qk.qeeper.repository;

import com.qk.qeeper.domain.CardGroup;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the CardGroup entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CardGroupRepository extends JpaRepository<CardGroup, Long>, JpaSpecificationExecutor<CardGroup> {

}
