package com.qk.qeeper.repository;

import com.qk.qeeper.domain.BankBranch;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the BankBranch entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BankBranchRepository extends JpaRepository<BankBranch, Long>, JpaSpecificationExecutor<BankBranch> {

}
