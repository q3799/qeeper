package com.qk.qeeper.repository;

import com.qk.qeeper.domain.NonBankClient;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the NonBankClient entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NonBankClientRepository extends JpaRepository<NonBankClient, Long>, JpaSpecificationExecutor<NonBankClient> {

}
