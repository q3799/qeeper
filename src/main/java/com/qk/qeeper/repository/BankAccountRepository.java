package com.qk.qeeper.repository;

import com.qk.qeeper.domain.BankAccount;
import com.qk.qeeper.domain.BankAccountGroup;
import com.qk.qeeper.domain.enumeration.AccountStatus;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * Spring Data  repository for the BankAccount entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BankAccountRepository extends JpaRepository<BankAccount, Long>, JpaSpecificationExecutor<BankAccount> {
    Optional<BankAccount> findTopByBankAccountGroupAndAccountStatus(BankAccountGroup bankAccountGroup, AccountStatus accountStatus);
}
