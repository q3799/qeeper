package com.qk.qeeper.repository;

import com.qk.qeeper.domain.NonBankAccountGroup;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the NonBankAccountGroup entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NonBankAccountGroupRepository extends JpaRepository<NonBankAccountGroup, Long>, JpaSpecificationExecutor<NonBankAccountGroup> {

}
