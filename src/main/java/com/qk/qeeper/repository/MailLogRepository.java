package com.qk.qeeper.repository;

import com.qk.qeeper.domain.MailLog;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the MailLog entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MailLogRepository extends JpaRepository<MailLog, Long>, JpaSpecificationExecutor<MailLog> {

}
