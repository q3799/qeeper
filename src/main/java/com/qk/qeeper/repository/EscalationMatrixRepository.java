package com.qk.qeeper.repository;

import com.qk.qeeper.domain.EscalationMatrix;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the EscalationMatrix entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EscalationMatrixRepository extends JpaRepository<EscalationMatrix, Long>, JpaSpecificationExecutor<EscalationMatrix> {

}
