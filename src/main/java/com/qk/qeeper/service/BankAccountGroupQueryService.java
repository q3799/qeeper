package com.qk.qeeper.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.qk.qeeper.domain.BankAccountGroup;
import com.qk.qeeper.domain.*; // for static metamodels
import com.qk.qeeper.repository.BankAccountGroupRepository;
import com.qk.qeeper.service.dto.BankAccountGroupCriteria;
import com.qk.qeeper.service.dto.BankAccountGroupDTO;
import com.qk.qeeper.service.mapper.BankAccountGroupMapper;

/**
 * Service for executing complex queries for {@link BankAccountGroup} entities in the database.
 * The main input is a {@link BankAccountGroupCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link BankAccountGroupDTO} or a {@link Page} of {@link BankAccountGroupDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class BankAccountGroupQueryService extends QueryService<BankAccountGroup> {

    private final Logger log = LoggerFactory.getLogger(BankAccountGroupQueryService.class);

    private final BankAccountGroupRepository bankAccountGroupRepository;

    private final BankAccountGroupMapper bankAccountGroupMapper;

    public BankAccountGroupQueryService(BankAccountGroupRepository bankAccountGroupRepository, BankAccountGroupMapper bankAccountGroupMapper) {
        this.bankAccountGroupRepository = bankAccountGroupRepository;
        this.bankAccountGroupMapper = bankAccountGroupMapper;
    }

    /**
     * Return a {@link List} of {@link BankAccountGroupDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<BankAccountGroupDTO> findByCriteria(BankAccountGroupCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<BankAccountGroup> specification = createSpecification(criteria);
        return bankAccountGroupMapper.toDto(bankAccountGroupRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link BankAccountGroupDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<BankAccountGroupDTO> findByCriteria(BankAccountGroupCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<BankAccountGroup> specification = createSpecification(criteria);
        return bankAccountGroupRepository.findAll(specification, page)
            .map(bankAccountGroupMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(BankAccountGroupCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<BankAccountGroup> specification = createSpecification(criteria);
        return bankAccountGroupRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    private Specification<BankAccountGroup> createSpecification(BankAccountGroupCriteria criteria) {
        Specification<BankAccountGroup> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), BankAccountGroup_.id));
            }
            if (criteria.getCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCode(), BankAccountGroup_.code));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), BankAccountGroup_.name));
            }
            if (criteria.getBankAccountsId() != null) {
                specification = specification.and(buildSpecification(criteria.getBankAccountsId(),
                    root -> root.join(BankAccountGroup_.bankAccounts, JoinType.LEFT).get(BankAccount_.id)));
            }
        }
        return specification;
    }
}
