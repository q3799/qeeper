package com.qk.qeeper.service;

import com.qk.qeeper.domain.BankAccountGroup;
import com.qk.qeeper.repository.BankAccountGroupRepository;
import com.qk.qeeper.service.dto.BankAccountGroupDTO;
import com.qk.qeeper.service.mapper.BankAccountGroupMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link BankAccountGroup}.
 */
@Service
@Transactional
public class BankAccountGroupService {

    private final Logger log = LoggerFactory.getLogger(BankAccountGroupService.class);

    private final BankAccountGroupRepository bankAccountGroupRepository;

    private final BankAccountGroupMapper bankAccountGroupMapper;

    public BankAccountGroupService(BankAccountGroupRepository bankAccountGroupRepository, BankAccountGroupMapper bankAccountGroupMapper) {
        this.bankAccountGroupRepository = bankAccountGroupRepository;
        this.bankAccountGroupMapper = bankAccountGroupMapper;
    }

    /**
     * Save a bankAccountGroup.
     *
     * @param bankAccountGroupDTO the entity to save.
     * @return the persisted entity.
     */
    public BankAccountGroupDTO save(BankAccountGroupDTO bankAccountGroupDTO) {
        log.debug("Request to save BankAccountGroup : {}", bankAccountGroupDTO);
        BankAccountGroup bankAccountGroup = bankAccountGroupMapper.toEntity(bankAccountGroupDTO);
        bankAccountGroup = bankAccountGroupRepository.save(bankAccountGroup);
        return bankAccountGroupMapper.toDto(bankAccountGroup);
    }

    /**
     * Get all the bankAccountGroups.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<BankAccountGroupDTO> findAll(Pageable pageable) {
        log.debug("Request to get all BankAccountGroups");
        return bankAccountGroupRepository.findAll(pageable)
            .map(bankAccountGroupMapper::toDto);
    }


    /**
     * Get one bankAccountGroup by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<BankAccountGroupDTO> findOne(Long id) {
        log.debug("Request to get BankAccountGroup : {}", id);
        return bankAccountGroupRepository.findById(id)
            .map(bankAccountGroupMapper::toDto);
    }

    /**
     * Delete the bankAccountGroup by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete BankAccountGroup : {}", id);
        bankAccountGroupRepository.deleteById(id);
    }
}
