package com.qk.qeeper.service;

import com.qk.qeeper.domain.EscalationMatrix;
import com.qk.qeeper.repository.EscalationMatrixRepository;
import com.qk.qeeper.service.dto.EscalationMatrixDTO;
import com.qk.qeeper.service.mapper.EscalationMatrixMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link EscalationMatrix}.
 */
@Service
@Transactional
public class EscalationMatrixService {

    private final Logger log = LoggerFactory.getLogger(EscalationMatrixService.class);

    private final EscalationMatrixRepository escalationMatrixRepository;

    private final EscalationMatrixMapper escalationMatrixMapper;

    public EscalationMatrixService(EscalationMatrixRepository escalationMatrixRepository, EscalationMatrixMapper escalationMatrixMapper) {
        this.escalationMatrixRepository = escalationMatrixRepository;
        this.escalationMatrixMapper = escalationMatrixMapper;
    }

    /**
     * Save a escalationMatrix.
     *
     * @param escalationMatrixDTO the entity to save.
     * @return the persisted entity.
     */
    public EscalationMatrixDTO save(EscalationMatrixDTO escalationMatrixDTO) {
        log.debug("Request to save EscalationMatrix : {}", escalationMatrixDTO);
        EscalationMatrix escalationMatrix = escalationMatrixMapper.toEntity(escalationMatrixDTO);
        escalationMatrix = escalationMatrixRepository.save(escalationMatrix);
        return escalationMatrixMapper.toDto(escalationMatrix);
    }

    /**
     * Get all the escalationMatrices.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<EscalationMatrixDTO> findAll(Pageable pageable) {
        log.debug("Request to get all EscalationMatrices");
        return escalationMatrixRepository.findAll(pageable)
            .map(escalationMatrixMapper::toDto);
    }


    /**
     * Get one escalationMatrix by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<EscalationMatrixDTO> findOne(Long id) {
        log.debug("Request to get EscalationMatrix : {}", id);
        return escalationMatrixRepository.findById(id)
            .map(escalationMatrixMapper::toDto);
    }

    /**
     * Delete the escalationMatrix by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete EscalationMatrix : {}", id);
        escalationMatrixRepository.deleteById(id);
    }
}
