package com.qk.qeeper.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.qk.qeeper.domain.MobileNumber;
import com.qk.qeeper.domain.*; // for static metamodels
import com.qk.qeeper.repository.MobileNumberRepository;
import com.qk.qeeper.service.dto.MobileNumberCriteria;
import com.qk.qeeper.service.dto.MobileNumberDTO;
import com.qk.qeeper.service.mapper.MobileNumberMapper;

/**
 * Service for executing complex queries for {@link MobileNumber} entities in the database.
 * The main input is a {@link MobileNumberCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link MobileNumberDTO} or a {@link Page} of {@link MobileNumberDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class MobileNumberQueryService extends QueryService<MobileNumber> {

    private final Logger log = LoggerFactory.getLogger(MobileNumberQueryService.class);

    private final MobileNumberRepository mobileNumberRepository;

    private final MobileNumberMapper mobileNumberMapper;

    public MobileNumberQueryService(MobileNumberRepository mobileNumberRepository, MobileNumberMapper mobileNumberMapper) {
        this.mobileNumberRepository = mobileNumberRepository;
        this.mobileNumberMapper = mobileNumberMapper;
    }

    /**
     * Return a {@link List} of {@link MobileNumberDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<MobileNumberDTO> findByCriteria(MobileNumberCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<MobileNumber> specification = createSpecification(criteria);
        return mobileNumberMapper.toDto(mobileNumberRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link MobileNumberDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<MobileNumberDTO> findByCriteria(MobileNumberCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<MobileNumber> specification = createSpecification(criteria);
        return mobileNumberRepository.findAll(specification, page)
            .map(mobileNumberMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(MobileNumberCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<MobileNumber> specification = createSpecification(criteria);
        return mobileNumberRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    private Specification<MobileNumber> createSpecification(MobileNumberCriteria criteria) {
        Specification<MobileNumber> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), MobileNumber_.id));
            }
            if (criteria.getNumber() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNumber(), MobileNumber_.number));
            }
            if (criteria.getAssetTag() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAssetTag(), MobileNumber_.assetTag));
            }
            if (criteria.getOperator() != null) {
                specification = specification.and(buildSpecification(criteria.getOperator(), MobileNumber_.operator));
            }
            if (criteria.getMobileConnectionPlan() != null) {
                specification = specification.and(buildSpecification(criteria.getMobileConnectionPlan(), MobileNumber_.mobileConnectionPlan));
            }
            if (criteria.getRemarks() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRemarks(), MobileNumber_.remarks));
            }
            if (criteria.getActive() != null) {
                specification = specification.and(buildSpecification(criteria.getActive(), MobileNumber_.active));
            }
            if (criteria.getRegisteredToId() != null) {
                specification = specification.and(buildSpecification(criteria.getRegisteredToId(),
                    root -> root.join(MobileNumber_.registeredTo, JoinType.LEFT).get(Employee_.id)));
            }
        }
        return specification;
    }
}
