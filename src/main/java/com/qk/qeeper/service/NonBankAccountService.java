package com.qk.qeeper.service;

import com.qk.qeeper.domain.NonBankAccount;
import com.qk.qeeper.repository.NonBankAccountRepository;
import com.qk.qeeper.service.dto.CredentialResponseDTO;
import com.qk.qeeper.service.dto.NonBankAccountDTO;
import com.qk.qeeper.service.mapper.NonBankAccountMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link NonBankAccount}.
 */
@Service
@Transactional
public class NonBankAccountService implements CredentialService {

    private final Logger log = LoggerFactory.getLogger(NonBankAccountService.class);

    private final NonBankAccountRepository nonBankAccountRepository;

    private final NonBankAccountMapper nonBankAccountMapper;

    public NonBankAccountService(NonBankAccountRepository nonBankAccountRepository, NonBankAccountMapper nonBankAccountMapper) {
        this.nonBankAccountRepository = nonBankAccountRepository;
        this.nonBankAccountMapper = nonBankAccountMapper;
    }

    /**
     * Save a nonBankAccount.
     *
     * @param nonBankAccountDTO the entity to save.
     * @return the persisted entity.
     */
    public NonBankAccountDTO save(NonBankAccountDTO nonBankAccountDTO) {
        log.debug("Request to save NonBankAccount : {}", nonBankAccountDTO);
        NonBankAccount nonBankAccount = nonBankAccountMapper.toEntity(nonBankAccountDTO);
        nonBankAccount = nonBankAccountRepository.save(nonBankAccount);
        return nonBankAccountMapper.toDto(nonBankAccount);
    }

    /**
     * Get all the nonBankAccounts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<NonBankAccountDTO> findAll(Pageable pageable) {
        log.debug("Request to get all NonBankAccounts");
        return nonBankAccountRepository.findAll(pageable)
            .map(nonBankAccountMapper::toDto);
    }


    /**
     * Get one nonBankAccount by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<NonBankAccountDTO> findOne(Long id) {
        log.debug("Request to get NonBankAccount : {}", id);
        return nonBankAccountRepository.findById(id)
            .map(nonBankAccountMapper::toDto);
    }

    /**
     * Delete the nonBankAccount by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete NonBankAccount : {}", id);
        nonBankAccountRepository.deleteById(id);
    }

    @Override
    public CredentialResponseDTO getCredentials(String groupCode) {
        return new CredentialResponseDTO();
    }
}
