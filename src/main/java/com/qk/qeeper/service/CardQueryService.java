package com.qk.qeeper.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.qk.qeeper.domain.Card;
import com.qk.qeeper.domain.*; // for static metamodels
import com.qk.qeeper.repository.CardRepository;
import com.qk.qeeper.service.dto.CardCriteria;
import com.qk.qeeper.service.dto.CardDTO;
import com.qk.qeeper.service.mapper.CardMapper;

/**
 * Service for executing complex queries for {@link Card} entities in the database.
 * The main input is a {@link CardCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link CardDTO} or a {@link Page} of {@link CardDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CardQueryService extends QueryService<Card> {

    private final Logger log = LoggerFactory.getLogger(CardQueryService.class);

    private final CardRepository cardRepository;

    private final CardMapper cardMapper;

    public CardQueryService(CardRepository cardRepository, CardMapper cardMapper) {
        this.cardRepository = cardRepository;
        this.cardMapper = cardMapper;
    }

    /**
     * Return a {@link List} of {@link CardDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<CardDTO> findByCriteria(CardCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Card> specification = createSpecification(criteria);
        return cardMapper.toDto(cardRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link CardDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<CardDTO> findByCriteria(CardCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Card> specification = createSpecification(criteria);
        return cardRepository.findAll(specification, page)
            .map(cardMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(CardCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Card> specification = createSpecification(criteria);
        return cardRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    private Specification<Card> createSpecification(CardCriteria criteria) {
        Specification<Card> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Card_.id));
            }
            if (criteria.getCardType() != null) {
                specification = specification.and(buildSpecification(criteria.getCardType(), Card_.cardType));
            }
            if (criteria.getPaymentNetwork() != null) {
                specification = specification.and(buildSpecification(criteria.getPaymentNetwork(), Card_.paymentNetwork));
            }
            if (criteria.getCardNumber() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCardNumber(), Card_.cardNumber));
            }
            if (criteria.getValidFrom() != null) {
                specification = specification.and(buildStringSpecification(criteria.getValidFrom(), Card_.validFrom));
            }
            if (criteria.getValidTill() != null) {
                specification = specification.and(buildStringSpecification(criteria.getValidTill(), Card_.validTill));
            }
            if (criteria.getPaymentNetworkPin() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPaymentNetworkPin(), Card_.paymentNetworkPin));
            }
            if (criteria.getCvv() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCvv(), Card_.cvv));
            }
            if (criteria.getNameOnCard() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNameOnCard(), Card_.nameOnCard));
            }
            if (criteria.getRemarks() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRemarks(), Card_.remarks));
            }
            if (criteria.getPin() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPin(), Card_.pin));
            }
            if (criteria.getTpin() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTpin(), Card_.tpin));
            }
            if (criteria.getMobileNumberMappedId() != null) {
                specification = specification.and(buildSpecification(criteria.getMobileNumberMappedId(),
                    root -> root.join(Card_.mobileNumberMapped, JoinType.LEFT).get(MobileNumber_.id)));
            }
            if (criteria.getRegisteredToId() != null) {
                specification = specification.and(buildSpecification(criteria.getRegisteredToId(),
                    root -> root.join(Card_.registeredTo, JoinType.LEFT).get(Employee_.id)));
            }
            if (criteria.getBankId() != null) {
                specification = specification.and(buildSpecification(criteria.getBankId(),
                    root -> root.join(Card_.bank, JoinType.LEFT).get(Bank_.id)));
            }
            if (criteria.getBankAccountId() != null) {
                specification = specification.and(buildSpecification(criteria.getBankAccountId(),
                    root -> root.join(Card_.bankAccount, JoinType.LEFT).get(BankAccount_.id)));
            }
            if (criteria.getCardGroupId() != null) {
                specification = specification.and(buildSpecification(criteria.getCardGroupId(),
                    root -> root.join(Card_.cardGroup, JoinType.LEFT).get(CardGroup_.id)));
            }
        }
        return specification;
    }
}
