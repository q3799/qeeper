package com.qk.qeeper.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.qk.qeeper.domain.MailLog;
import com.qk.qeeper.domain.*; // for static metamodels
import com.qk.qeeper.repository.MailLogRepository;
import com.qk.qeeper.service.dto.MailLogCriteria;
import com.qk.qeeper.service.dto.MailLogDTO;
import com.qk.qeeper.service.mapper.MailLogMapper;

/**
 * Service for executing complex queries for {@link MailLog} entities in the database.
 * The main input is a {@link MailLogCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link MailLogDTO} or a {@link Page} of {@link MailLogDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class MailLogQueryService extends QueryService<MailLog> {

    private final Logger log = LoggerFactory.getLogger(MailLogQueryService.class);

    private final MailLogRepository mailLogRepository;

    private final MailLogMapper mailLogMapper;

    public MailLogQueryService(MailLogRepository mailLogRepository, MailLogMapper mailLogMapper) {
        this.mailLogRepository = mailLogRepository;
        this.mailLogMapper = mailLogMapper;
    }

    /**
     * Return a {@link List} of {@link MailLogDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<MailLogDTO> findByCriteria(MailLogCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<MailLog> specification = createSpecification(criteria);
        return mailLogMapper.toDto(mailLogRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link MailLogDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<MailLogDTO> findByCriteria(MailLogCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<MailLog> specification = createSpecification(criteria);
        return mailLogRepository.findAll(specification, page)
            .map(mailLogMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(MailLogCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<MailLog> specification = createSpecification(criteria);
        return mailLogRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    private Specification<MailLog> createSpecification(MailLogCriteria criteria) {
        Specification<MailLog> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), MailLog_.id));
            }
            if (criteria.getSubject() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSubject(), MailLog_.subject));
            }
            if (criteria.getContent() != null) {
                specification = specification.and(buildStringSpecification(criteria.getContent(), MailLog_.content));
            }
            if (criteria.getMailDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getMailDate(), MailLog_.mailDate));
            }
            if (criteria.getMailServer() != null) {
                specification = specification.and(buildSpecification(criteria.getMailServer(), MailLog_.mailServer));
            }
            if (criteria.getResponseReceived() != null) {
                specification = specification.and(buildStringSpecification(criteria.getResponseReceived(), MailLog_.responseReceived));
            }
            if (criteria.getRemarks() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRemarks(), MailLog_.remarks));
            }
            if (criteria.getToList() != null) {
                specification = specification.and(buildStringSpecification(criteria.getToList(), MailLog_.toList));
            }
            if (criteria.getCcList() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCcList(), MailLog_.ccList));
            }
            if (criteria.getProcessingTime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getProcessingTime(), MailLog_.processingTime));
            }
        }
        return specification;
    }
}
