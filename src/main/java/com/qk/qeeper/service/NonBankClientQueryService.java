package com.qk.qeeper.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.qk.qeeper.domain.NonBankClient;
import com.qk.qeeper.domain.*; // for static metamodels
import com.qk.qeeper.repository.NonBankClientRepository;
import com.qk.qeeper.service.dto.NonBankClientCriteria;
import com.qk.qeeper.service.dto.NonBankClientDTO;
import com.qk.qeeper.service.mapper.NonBankClientMapper;

/**
 * Service for executing complex queries for {@link NonBankClient} entities in the database.
 * The main input is a {@link NonBankClientCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link NonBankClientDTO} or a {@link Page} of {@link NonBankClientDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class NonBankClientQueryService extends QueryService<NonBankClient> {

    private final Logger log = LoggerFactory.getLogger(NonBankClientQueryService.class);

    private final NonBankClientRepository nonBankClientRepository;

    private final NonBankClientMapper nonBankClientMapper;

    public NonBankClientQueryService(NonBankClientRepository nonBankClientRepository, NonBankClientMapper nonBankClientMapper) {
        this.nonBankClientRepository = nonBankClientRepository;
        this.nonBankClientMapper = nonBankClientMapper;
    }

    /**
     * Return a {@link List} of {@link NonBankClientDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<NonBankClientDTO> findByCriteria(NonBankClientCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<NonBankClient> specification = createSpecification(criteria);
        return nonBankClientMapper.toDto(nonBankClientRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link NonBankClientDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<NonBankClientDTO> findByCriteria(NonBankClientCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<NonBankClient> specification = createSpecification(criteria);
        return nonBankClientRepository.findAll(specification, page)
            .map(nonBankClientMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(NonBankClientCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<NonBankClient> specification = createSpecification(criteria);
        return nonBankClientRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    private Specification<NonBankClient> createSpecification(NonBankClientCriteria criteria) {
        Specification<NonBankClient> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), NonBankClient_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), NonBankClient_.name));
            }
            if (criteria.getUrl() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUrl(), NonBankClient_.url));
            }
        }
        return specification;
    }
}
