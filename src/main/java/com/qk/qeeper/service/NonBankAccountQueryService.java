package com.qk.qeeper.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.qk.qeeper.domain.NonBankAccount;
import com.qk.qeeper.domain.*; // for static metamodels
import com.qk.qeeper.repository.NonBankAccountRepository;
import com.qk.qeeper.service.dto.NonBankAccountCriteria;
import com.qk.qeeper.service.dto.NonBankAccountDTO;
import com.qk.qeeper.service.mapper.NonBankAccountMapper;

/**
 * Service for executing complex queries for {@link NonBankAccount} entities in the database.
 * The main input is a {@link NonBankAccountCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link NonBankAccountDTO} or a {@link Page} of {@link NonBankAccountDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class NonBankAccountQueryService extends QueryService<NonBankAccount> {

    private final Logger log = LoggerFactory.getLogger(NonBankAccountQueryService.class);

    private final NonBankAccountRepository nonBankAccountRepository;

    private final NonBankAccountMapper nonBankAccountMapper;

    public NonBankAccountQueryService(NonBankAccountRepository nonBankAccountRepository, NonBankAccountMapper nonBankAccountMapper) {
        this.nonBankAccountRepository = nonBankAccountRepository;
        this.nonBankAccountMapper = nonBankAccountMapper;
    }

    /**
     * Return a {@link List} of {@link NonBankAccountDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<NonBankAccountDTO> findByCriteria(NonBankAccountCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<NonBankAccount> specification = createSpecification(criteria);
        return nonBankAccountMapper.toDto(nonBankAccountRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link NonBankAccountDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<NonBankAccountDTO> findByCriteria(NonBankAccountCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<NonBankAccount> specification = createSpecification(criteria);
        return nonBankAccountRepository.findAll(specification, page)
            .map(nonBankAccountMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(NonBankAccountCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<NonBankAccount> specification = createSpecification(criteria);
        return nonBankAccountRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    private Specification<NonBankAccount> createSpecification(NonBankAccountCriteria criteria) {
        Specification<NonBankAccount> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), NonBankAccount_.id));
            }
            if (criteria.getLoginId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLoginId(), NonBankAccount_.loginId));
            }
            if (criteria.getLoginPassword() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLoginPassword(), NonBankAccount_.loginPassword));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), NonBankAccount_.name));
            }
            if (criteria.getEmail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEmail(), NonBankAccount_.email));
            }
            if (criteria.getPhone() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPhone(), NonBankAccount_.phone));
            }
            if (criteria.getPasswordGetsExpired() != null) {
                specification = specification.and(buildSpecification(criteria.getPasswordGetsExpired(), NonBankAccount_.passwordGetsExpired));
            }
            if (criteria.getPasswordUpdatedOn() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPasswordUpdatedOn(), NonBankAccount_.passwordUpdatedOn));
            }
            if (criteria.getExpirationPeriodDays() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getExpirationPeriodDays(), NonBankAccount_.expirationPeriodDays));
            }
            if (criteria.getAccountStatus() != null) {
                specification = specification.and(buildSpecification(criteria.getAccountStatus(), NonBankAccount_.accountStatus));
            }
            if (criteria.getRemarks() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRemarks(), NonBankAccount_.remarks));
            }
            if (criteria.getMultipleSessionAllowed() != null) {
                specification = specification.and(buildSpecification(criteria.getMultipleSessionAllowed(), NonBankAccount_.multipleSessionAllowed));
            }
            if (criteria.getAccountId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAccountId(), NonBankAccount_.accountId));
            }
            if (criteria.getNonBankClientId() != null) {
                specification = specification.and(buildSpecification(criteria.getNonBankClientId(),
                    root -> root.join(NonBankAccount_.nonBankClient, JoinType.LEFT).get(NonBankClient_.id)));
            }
            if (criteria.getNonBankAccountGroupId() != null) {
                specification = specification.and(buildSpecification(criteria.getNonBankAccountGroupId(),
                    root -> root.join(NonBankAccount_.nonBankAccountGroup, JoinType.LEFT).get(NonBankAccountGroup_.id)));
            }
        }
        return specification;
    }
}
