package com.qk.qeeper.service;

import com.qk.qeeper.service.dto.CredentialResponseDTO;

public interface CredentialService {

    public CredentialResponseDTO getCredentials(String groupCode);
}
