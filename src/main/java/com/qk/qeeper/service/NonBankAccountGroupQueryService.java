package com.qk.qeeper.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.qk.qeeper.domain.NonBankAccountGroup;
import com.qk.qeeper.domain.*; // for static metamodels
import com.qk.qeeper.repository.NonBankAccountGroupRepository;
import com.qk.qeeper.service.dto.NonBankAccountGroupCriteria;
import com.qk.qeeper.service.dto.NonBankAccountGroupDTO;
import com.qk.qeeper.service.mapper.NonBankAccountGroupMapper;

/**
 * Service for executing complex queries for {@link NonBankAccountGroup} entities in the database.
 * The main input is a {@link NonBankAccountGroupCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link NonBankAccountGroupDTO} or a {@link Page} of {@link NonBankAccountGroupDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class NonBankAccountGroupQueryService extends QueryService<NonBankAccountGroup> {

    private final Logger log = LoggerFactory.getLogger(NonBankAccountGroupQueryService.class);

    private final NonBankAccountGroupRepository nonBankAccountGroupRepository;

    private final NonBankAccountGroupMapper nonBankAccountGroupMapper;

    public NonBankAccountGroupQueryService(NonBankAccountGroupRepository nonBankAccountGroupRepository, NonBankAccountGroupMapper nonBankAccountGroupMapper) {
        this.nonBankAccountGroupRepository = nonBankAccountGroupRepository;
        this.nonBankAccountGroupMapper = nonBankAccountGroupMapper;
    }

    /**
     * Return a {@link List} of {@link NonBankAccountGroupDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<NonBankAccountGroupDTO> findByCriteria(NonBankAccountGroupCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<NonBankAccountGroup> specification = createSpecification(criteria);
        return nonBankAccountGroupMapper.toDto(nonBankAccountGroupRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link NonBankAccountGroupDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<NonBankAccountGroupDTO> findByCriteria(NonBankAccountGroupCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<NonBankAccountGroup> specification = createSpecification(criteria);
        return nonBankAccountGroupRepository.findAll(specification, page)
            .map(nonBankAccountGroupMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(NonBankAccountGroupCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<NonBankAccountGroup> specification = createSpecification(criteria);
        return nonBankAccountGroupRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    private Specification<NonBankAccountGroup> createSpecification(NonBankAccountGroupCriteria criteria) {
        Specification<NonBankAccountGroup> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), NonBankAccountGroup_.id));
            }
            if (criteria.getCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCode(), NonBankAccountGroup_.code));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), NonBankAccountGroup_.name));
            }
            if (criteria.getNonBankAccountsId() != null) {
                specification = specification.and(buildSpecification(criteria.getNonBankAccountsId(),
                    root -> root.join(NonBankAccountGroup_.nonBankAccounts, JoinType.LEFT).get(NonBankAccount_.id)));
            }
        }
        return specification;
    }
}
