package com.qk.qeeper.service;

import com.qk.qeeper.domain.NonBankAccountGroup;
import com.qk.qeeper.repository.NonBankAccountGroupRepository;
import com.qk.qeeper.service.dto.NonBankAccountGroupDTO;
import com.qk.qeeper.service.mapper.NonBankAccountGroupMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link NonBankAccountGroup}.
 */
@Service
@Transactional
public class NonBankAccountGroupService {

    private final Logger log = LoggerFactory.getLogger(NonBankAccountGroupService.class);

    private final NonBankAccountGroupRepository nonBankAccountGroupRepository;

    private final NonBankAccountGroupMapper nonBankAccountGroupMapper;

    public NonBankAccountGroupService(NonBankAccountGroupRepository nonBankAccountGroupRepository, NonBankAccountGroupMapper nonBankAccountGroupMapper) {
        this.nonBankAccountGroupRepository = nonBankAccountGroupRepository;
        this.nonBankAccountGroupMapper = nonBankAccountGroupMapper;
    }

    /**
     * Save a nonBankAccountGroup.
     *
     * @param nonBankAccountGroupDTO the entity to save.
     * @return the persisted entity.
     */
    public NonBankAccountGroupDTO save(NonBankAccountGroupDTO nonBankAccountGroupDTO) {
        log.debug("Request to save NonBankAccountGroup : {}", nonBankAccountGroupDTO);
        NonBankAccountGroup nonBankAccountGroup = nonBankAccountGroupMapper.toEntity(nonBankAccountGroupDTO);
        nonBankAccountGroup = nonBankAccountGroupRepository.save(nonBankAccountGroup);
        return nonBankAccountGroupMapper.toDto(nonBankAccountGroup);
    }

    /**
     * Get all the nonBankAccountGroups.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<NonBankAccountGroupDTO> findAll(Pageable pageable) {
        log.debug("Request to get all NonBankAccountGroups");
        return nonBankAccountGroupRepository.findAll(pageable)
            .map(nonBankAccountGroupMapper::toDto);
    }


    /**
     * Get one nonBankAccountGroup by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<NonBankAccountGroupDTO> findOne(Long id) {
        log.debug("Request to get NonBankAccountGroup : {}", id);
        return nonBankAccountGroupRepository.findById(id)
            .map(nonBankAccountGroupMapper::toDto);
    }

    /**
     * Delete the nonBankAccountGroup by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete NonBankAccountGroup : {}", id);
        nonBankAccountGroupRepository.deleteById(id);
    }
}
