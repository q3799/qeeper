package com.qk.qeeper.service;

import com.qk.qeeper.domain.SecretQuestion;
import com.qk.qeeper.repository.SecretQuestionRepository;
import com.qk.qeeper.service.dto.SecretQuestionDTO;
import com.qk.qeeper.service.mapper.SecretQuestionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link SecretQuestion}.
 */
@Service
@Transactional
public class SecretQuestionService {

    private final Logger log = LoggerFactory.getLogger(SecretQuestionService.class);

    private final SecretQuestionRepository secretQuestionRepository;

    private final SecretQuestionMapper secretQuestionMapper;

    public SecretQuestionService(SecretQuestionRepository secretQuestionRepository, SecretQuestionMapper secretQuestionMapper) {
        this.secretQuestionRepository = secretQuestionRepository;
        this.secretQuestionMapper = secretQuestionMapper;
    }

    /**
     * Save a secretQuestion.
     *
     * @param secretQuestionDTO the entity to save.
     * @return the persisted entity.
     */
    public SecretQuestionDTO save(SecretQuestionDTO secretQuestionDTO) {
        log.debug("Request to save SecretQuestion : {}", secretQuestionDTO);
        SecretQuestion secretQuestion = secretQuestionMapper.toEntity(secretQuestionDTO);
        secretQuestion = secretQuestionRepository.save(secretQuestion);
        return secretQuestionMapper.toDto(secretQuestion);
    }

    /**
     * Get all the secretQuestions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<SecretQuestionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all SecretQuestions");
        return secretQuestionRepository.findAll(pageable)
            .map(secretQuestionMapper::toDto);
    }


    /**
     * Get one secretQuestion by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<SecretQuestionDTO> findOne(Long id) {
        log.debug("Request to get SecretQuestion : {}", id);
        return secretQuestionRepository.findById(id)
            .map(secretQuestionMapper::toDto);
    }

    /**
     * Delete the secretQuestion by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete SecretQuestion : {}", id);
        secretQuestionRepository.deleteById(id);
    }
}
