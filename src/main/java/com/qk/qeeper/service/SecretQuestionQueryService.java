package com.qk.qeeper.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.qk.qeeper.domain.SecretQuestion;
import com.qk.qeeper.domain.*; // for static metamodels
import com.qk.qeeper.repository.SecretQuestionRepository;
import com.qk.qeeper.service.dto.SecretQuestionCriteria;
import com.qk.qeeper.service.dto.SecretQuestionDTO;
import com.qk.qeeper.service.mapper.SecretQuestionMapper;

/**
 * Service for executing complex queries for {@link SecretQuestion} entities in the database.
 * The main input is a {@link SecretQuestionCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link SecretQuestionDTO} or a {@link Page} of {@link SecretQuestionDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class SecretQuestionQueryService extends QueryService<SecretQuestion> {

    private final Logger log = LoggerFactory.getLogger(SecretQuestionQueryService.class);

    private final SecretQuestionRepository secretQuestionRepository;

    private final SecretQuestionMapper secretQuestionMapper;

    public SecretQuestionQueryService(SecretQuestionRepository secretQuestionRepository, SecretQuestionMapper secretQuestionMapper) {
        this.secretQuestionRepository = secretQuestionRepository;
        this.secretQuestionMapper = secretQuestionMapper;
    }

    /**
     * Return a {@link List} of {@link SecretQuestionDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<SecretQuestionDTO> findByCriteria(SecretQuestionCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<SecretQuestion> specification = createSpecification(criteria);
        return secretQuestionMapper.toDto(secretQuestionRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link SecretQuestionDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<SecretQuestionDTO> findByCriteria(SecretQuestionCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<SecretQuestion> specification = createSpecification(criteria);
        return secretQuestionRepository.findAll(specification, page)
            .map(secretQuestionMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(SecretQuestionCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<SecretQuestion> specification = createSpecification(criteria);
        return secretQuestionRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    private Specification<SecretQuestion> createSpecification(SecretQuestionCriteria criteria) {
        Specification<SecretQuestion> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), SecretQuestion_.id));
            }
            if (criteria.getQuestion() != null) {
                specification = specification.and(buildStringSpecification(criteria.getQuestion(), SecretQuestion_.question));
            }
            if (criteria.getAnswer() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAnswer(), SecretQuestion_.answer));
            }
            if (criteria.getBankAccountId() != null) {
                specification = specification.and(buildSpecification(criteria.getBankAccountId(),
                    root -> root.join(SecretQuestion_.bankAccount, JoinType.LEFT).get(BankAccount_.id)));
            }
        }
        return specification;
    }
}
