package com.qk.qeeper.service;

import com.qk.qeeper.domain.MobileNumber;
import com.qk.qeeper.repository.MobileNumberRepository;
import com.qk.qeeper.service.dto.MobileNumberDTO;
import com.qk.qeeper.service.mapper.MobileNumberMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link MobileNumber}.
 */
@Service
@Transactional
public class MobileNumberService {

    private final Logger log = LoggerFactory.getLogger(MobileNumberService.class);

    private final MobileNumberRepository mobileNumberRepository;

    private final MobileNumberMapper mobileNumberMapper;

    public MobileNumberService(MobileNumberRepository mobileNumberRepository, MobileNumberMapper mobileNumberMapper) {
        this.mobileNumberRepository = mobileNumberRepository;
        this.mobileNumberMapper = mobileNumberMapper;
    }

    /**
     * Save a mobileNumber.
     *
     * @param mobileNumberDTO the entity to save.
     * @return the persisted entity.
     */
    public MobileNumberDTO save(MobileNumberDTO mobileNumberDTO) {
        log.debug("Request to save MobileNumber : {}", mobileNumberDTO);
        MobileNumber mobileNumber = mobileNumberMapper.toEntity(mobileNumberDTO);
        mobileNumber = mobileNumberRepository.save(mobileNumber);
        return mobileNumberMapper.toDto(mobileNumber);
    }

    /**
     * Get all the mobileNumbers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<MobileNumberDTO> findAll(Pageable pageable) {
        log.debug("Request to get all MobileNumbers");
        return mobileNumberRepository.findAll(pageable)
            .map(mobileNumberMapper::toDto);
    }


    /**
     * Get one mobileNumber by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<MobileNumberDTO> findOne(Long id) {
        log.debug("Request to get MobileNumber : {}", id);
        return mobileNumberRepository.findById(id)
            .map(mobileNumberMapper::toDto);
    }

    /**
     * Delete the mobileNumber by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete MobileNumber : {}", id);
        mobileNumberRepository.deleteById(id);
    }
}
