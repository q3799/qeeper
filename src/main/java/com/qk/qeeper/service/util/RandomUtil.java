package com.qk.qeeper.service.util;

import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.apache.commons.lang3.RandomStringUtils;

import java.security.Key;

/**
 * Utility class for generating random Strings.
 */
public final class RandomUtil {

    private static final int DEF_COUNT = 20;

    private RandomUtil() {
    }

    /**
     * Generate a password.
     *
     * @return the generated password.
     */
    public static String generatePassword() {
        return RandomStringUtils.randomAlphanumeric(DEF_COUNT);
    }

    /**
     * Generate an activation key.
     *
     * @return the generated activation key.
     */
    public static String generateActivationKey() {
        return RandomStringUtils.randomNumeric(DEF_COUNT);
    }

    /**
     * Generate a reset key.
     *
     * @return the generated reset key.
     */
    public static String generateResetKey() {
        return RandomStringUtils.randomNumeric(DEF_COUNT);
    }

    public static void main(String[] args){
        String accessKey = "645b51183e1f56b323f3417f66a6b9a6ee380b4e09f84ef71afaf80e3d846648df4967acd59b95eca7afb63f4ee056b9140202de543679ec1b72b40f609acd1c";
        byte[] keyBytes = Decoders.BASE64.decode("NjQ1YjUxMTgzZTFmNTZiMzIzZjM0MTdmNjZhNmI5YTZlZTM4MGI0ZTA5Zjg0ZWY3MWFmYWY4MGUzZDg0NjY0OGRmNDk2N2FjZDU5Yjk1ZWNhN2FmYjYzZjRlZTA1NmI5MTQwMjAyZGU1NDM2NzllYzFiNzJiNDBmNjA5YWNkMWM=");

        if (accessKey.equals(new String(keyBytes))) {
            System.out.println("equals");
        } else {
            System.out.println("not equals");
        }
    }
}
