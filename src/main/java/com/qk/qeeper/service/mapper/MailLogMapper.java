package com.qk.qeeper.service.mapper;

import com.qk.qeeper.domain.*;
import com.qk.qeeper.service.dto.MailLogDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link MailLog} and its DTO {@link MailLogDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface MailLogMapper extends EntityMapper<MailLogDTO, MailLog> {



    default MailLog fromId(Long id) {
        if (id == null) {
            return null;
        }
        MailLog mailLog = new MailLog();
        mailLog.setId(id);
        return mailLog;
    }
}
