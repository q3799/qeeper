package com.qk.qeeper.service.mapper;

import com.qk.qeeper.domain.*;
import com.qk.qeeper.service.dto.ContactPersonDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ContactPerson} and its DTO {@link ContactPersonDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ContactPersonMapper extends EntityMapper<ContactPersonDTO, ContactPerson> {



    default ContactPerson fromId(Long id) {
        if (id == null) {
            return null;
        }
        ContactPerson contactPerson = new ContactPerson();
        contactPerson.setId(id);
        return contactPerson;
    }
}
