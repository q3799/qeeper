package com.qk.qeeper.service.mapper;

import com.qk.qeeper.domain.*;
import com.qk.qeeper.service.dto.EscalationMatrixDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link EscalationMatrix} and its DTO {@link EscalationMatrixDTO}.
 */
@Mapper(componentModel = "spring", uses = {ContactPersonMapper.class})
public interface EscalationMatrixMapper extends EntityMapper<EscalationMatrixDTO, EscalationMatrix> {

    @Mapping(source = "contactPerson.id", target = "contactPersonId")
    @Mapping(source = "contactPerson.name", target = "contactPersonName")
    EscalationMatrixDTO toDto(EscalationMatrix escalationMatrix);

    @Mapping(source = "contactPersonId", target = "contactPerson")
    EscalationMatrix toEntity(EscalationMatrixDTO escalationMatrixDTO);

    default EscalationMatrix fromId(Long id) {
        if (id == null) {
            return null;
        }
        EscalationMatrix escalationMatrix = new EscalationMatrix();
        escalationMatrix.setId(id);
        return escalationMatrix;
    }
}
