package com.qk.qeeper.service.mapper;

import com.qk.qeeper.domain.*;
import com.qk.qeeper.service.dto.BankAccountGroupDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link BankAccountGroup} and its DTO {@link BankAccountGroupDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface BankAccountGroupMapper extends EntityMapper<BankAccountGroupDTO, BankAccountGroup> {


    @Mapping(target = "bankAccounts", ignore = true)
    @Mapping(target = "removeBankAccounts", ignore = true)
    BankAccountGroup toEntity(BankAccountGroupDTO bankAccountGroupDTO);

    default BankAccountGroup fromId(Long id) {
        if (id == null) {
            return null;
        }
        BankAccountGroup bankAccountGroup = new BankAccountGroup();
        bankAccountGroup.setId(id);
        return bankAccountGroup;
    }
}
