package com.qk.qeeper.service.mapper;

import com.qk.qeeper.domain.*;
import com.qk.qeeper.service.dto.MobileNumberDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link MobileNumber} and its DTO {@link MobileNumberDTO}.
 */
@Mapper(componentModel = "spring", uses = {EmployeeMapper.class})
public interface MobileNumberMapper extends EntityMapper<MobileNumberDTO, MobileNumber> {

    @Mapping(source = "registeredTo.id", target = "registeredToId")
    @Mapping(source = "registeredTo.name", target = "registeredToName")
    MobileNumberDTO toDto(MobileNumber mobileNumber);

    @Mapping(source = "registeredToId", target = "registeredTo")
    MobileNumber toEntity(MobileNumberDTO mobileNumberDTO);

    default MobileNumber fromId(Long id) {
        if (id == null) {
            return null;
        }
        MobileNumber mobileNumber = new MobileNumber();
        mobileNumber.setId(id);
        return mobileNumber;
    }
}
