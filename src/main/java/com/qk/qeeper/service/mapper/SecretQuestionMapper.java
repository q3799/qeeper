package com.qk.qeeper.service.mapper;

import com.qk.qeeper.domain.*;
import com.qk.qeeper.service.dto.SecretQuestionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link SecretQuestion} and its DTO {@link SecretQuestionDTO}.
 */
@Mapper(componentModel = "spring", uses = {BankAccountMapper.class})
public interface SecretQuestionMapper extends EntityMapper<SecretQuestionDTO, SecretQuestion> {

    @Mapping(source = "bankAccount.id", target = "bankAccountId")
    SecretQuestionDTO toDto(SecretQuestion secretQuestion);

    @Mapping(source = "bankAccountId", target = "bankAccount")
    SecretQuestion toEntity(SecretQuestionDTO secretQuestionDTO);

    default SecretQuestion fromId(Long id) {
        if (id == null) {
            return null;
        }
        SecretQuestion secretQuestion = new SecretQuestion();
        secretQuestion.setId(id);
        return secretQuestion;
    }
}
