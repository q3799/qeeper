package com.qk.qeeper.service.mapper;

import com.qk.qeeper.domain.*;
import com.qk.qeeper.service.dto.NonBankAccountDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link NonBankAccount} and its DTO {@link NonBankAccountDTO}.
 */
@Mapper(componentModel = "spring", uses = {NonBankClientMapper.class, NonBankAccountGroupMapper.class})
public interface NonBankAccountMapper extends EntityMapper<NonBankAccountDTO, NonBankAccount> {

    @Mapping(source = "nonBankClient.id", target = "nonBankClientId")
    @Mapping(source = "nonBankClient.name", target = "nonBankClientName")
    @Mapping(source = "nonBankAccountGroup.id", target = "nonBankAccountGroupId")
    @Mapping(source = "nonBankAccountGroup.name", target = "nonBankAccountGroupName")
    NonBankAccountDTO toDto(NonBankAccount nonBankAccount);

    @Mapping(source = "nonBankClientId", target = "nonBankClient")
    @Mapping(source = "nonBankAccountGroupId", target = "nonBankAccountGroup")
    NonBankAccount toEntity(NonBankAccountDTO nonBankAccountDTO);

    default NonBankAccount fromId(Long id) {
        if (id == null) {
            return null;
        }
        NonBankAccount nonBankAccount = new NonBankAccount();
        nonBankAccount.setId(id);
        return nonBankAccount;
    }
}
