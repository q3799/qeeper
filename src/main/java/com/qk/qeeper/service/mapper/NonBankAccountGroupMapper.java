package com.qk.qeeper.service.mapper;

import com.qk.qeeper.domain.*;
import com.qk.qeeper.service.dto.NonBankAccountGroupDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link NonBankAccountGroup} and its DTO {@link NonBankAccountGroupDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface NonBankAccountGroupMapper extends EntityMapper<NonBankAccountGroupDTO, NonBankAccountGroup> {


    @Mapping(target = "nonBankAccounts", ignore = true)
    @Mapping(target = "removeNonBankAccounts", ignore = true)
    NonBankAccountGroup toEntity(NonBankAccountGroupDTO nonBankAccountGroupDTO);

    default NonBankAccountGroup fromId(Long id) {
        if (id == null) {
            return null;
        }
        NonBankAccountGroup nonBankAccountGroup = new NonBankAccountGroup();
        nonBankAccountGroup.setId(id);
        return nonBankAccountGroup;
    }
}
