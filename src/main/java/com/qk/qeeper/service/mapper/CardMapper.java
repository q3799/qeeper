package com.qk.qeeper.service.mapper;

import com.qk.qeeper.domain.*;
import com.qk.qeeper.service.dto.CardDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Card} and its DTO {@link CardDTO}.
 */
@Mapper(componentModel = "spring", uses = {MobileNumberMapper.class, EmployeeMapper.class, BankMapper.class, BankAccountMapper.class, CardGroupMapper.class})
public interface CardMapper extends EntityMapper<CardDTO, Card> {

    @Mapping(source = "mobileNumberMapped.id", target = "mobileNumberMappedId")
    @Mapping(source = "mobileNumberMapped.number", target = "mobileNumberMappedNumber")
    @Mapping(source = "registeredTo.id", target = "registeredToId")
    @Mapping(source = "registeredTo.name", target = "registeredToName")
    @Mapping(source = "bank.id", target = "bankId")
    @Mapping(source = "bank.name", target = "bankName")
    @Mapping(source = "bankAccount.id", target = "bankAccountId")
    @Mapping(source = "cardGroup.id", target = "cardGroupId")
    @Mapping(source = "cardGroup.name", target = "cardGroupName")
    CardDTO toDto(Card card);

    @Mapping(source = "mobileNumberMappedId", target = "mobileNumberMapped")
    @Mapping(source = "registeredToId", target = "registeredTo")
    @Mapping(source = "bankId", target = "bank")
    @Mapping(source = "bankAccountId", target = "bankAccount")
    @Mapping(source = "cardGroupId", target = "cardGroup")
    Card toEntity(CardDTO cardDTO);

    default Card fromId(Long id) {
        if (id == null) {
            return null;
        }
        Card card = new Card();
        card.setId(id);
        return card;
    }
}
