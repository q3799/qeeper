package com.qk.qeeper.service.mapper;

import com.qk.qeeper.domain.*;
import com.qk.qeeper.service.dto.NonBankClientDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link NonBankClient} and its DTO {@link NonBankClientDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface NonBankClientMapper extends EntityMapper<NonBankClientDTO, NonBankClient> {



    default NonBankClient fromId(Long id) {
        if (id == null) {
            return null;
        }
        NonBankClient nonBankClient = new NonBankClient();
        nonBankClient.setId(id);
        return nonBankClient;
    }
}
