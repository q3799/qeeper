package com.qk.qeeper.service.mapper;

import com.qk.qeeper.domain.*;
import com.qk.qeeper.service.dto.CardGroupDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link CardGroup} and its DTO {@link CardGroupDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CardGroupMapper extends EntityMapper<CardGroupDTO, CardGroup> {


    @Mapping(target = "cards", ignore = true)
    @Mapping(target = "removeCards", ignore = true)
    CardGroup toEntity(CardGroupDTO cardGroupDTO);

    default CardGroup fromId(Long id) {
        if (id == null) {
            return null;
        }
        CardGroup cardGroup = new CardGroup();
        cardGroup.setId(id);
        return cardGroup;
    }
}
