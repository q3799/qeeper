package com.qk.qeeper.service.mapper;

import com.qk.qeeper.domain.*;
import com.qk.qeeper.service.dto.BankAccountDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link BankAccount} and its DTO {@link BankAccountDTO}.
 */
@Mapper(componentModel = "spring", uses = {EmployeeMapper.class, BankBranchMapper.class, MobileNumberMapper.class, BankAccountGroupMapper.class})
public interface BankAccountMapper extends EntityMapper<BankAccountDTO, BankAccount> {

    @Mapping(source = "accountHolder.id", target = "accountHolderId")
    @Mapping(source = "accountHolder.name", target = "accountHolderName")
    @Mapping(source = "bankBranch.id", target = "bankBranchId")
    @Mapping(source = "bankBranch.location", target = "bankBranchLocation")
    @Mapping(source = "mobileNumberMapped.id", target = "mobileNumberMappedId")
    @Mapping(source = "mobileNumberMapped.number", target = "mobileNumberMappedNumber")
    @Mapping(source = "parentAccount.id", target = "parentAccountId")
    @Mapping(source = "bankAccountGroup.id", target = "bankAccountGroupId")
    @Mapping(source = "bankAccountGroup.name", target = "bankAccountGroupName")
    BankAccountDTO toDto(BankAccount bankAccount);

    @Mapping(source = "accountHolderId", target = "accountHolder")
    @Mapping(source = "bankBranchId", target = "bankBranch")
    @Mapping(source = "mobileNumberMappedId", target = "mobileNumberMapped")
    @Mapping(source = "parentAccountId", target = "parentAccount")
    @Mapping(target = "cards", ignore = true)
    @Mapping(target = "removeCards", ignore = true)
    @Mapping(source = "bankAccountGroupId", target = "bankAccountGroup")
    @Mapping(target = "secretQuestions", ignore = true)
    @Mapping(target = "removeSecretQuestions", ignore = true)
    BankAccount toEntity(BankAccountDTO bankAccountDTO);

    default BankAccount fromId(Long id) {
        if (id == null) {
            return null;
        }
        BankAccount bankAccount = new BankAccount();
        bankAccount.setId(id);
        return bankAccount;
    }
}
