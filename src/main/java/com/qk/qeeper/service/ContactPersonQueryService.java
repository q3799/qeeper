package com.qk.qeeper.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.qk.qeeper.domain.ContactPerson;
import com.qk.qeeper.domain.*; // for static metamodels
import com.qk.qeeper.repository.ContactPersonRepository;
import com.qk.qeeper.service.dto.ContactPersonCriteria;
import com.qk.qeeper.service.dto.ContactPersonDTO;
import com.qk.qeeper.service.mapper.ContactPersonMapper;

/**
 * Service for executing complex queries for {@link ContactPerson} entities in the database.
 * The main input is a {@link ContactPersonCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ContactPersonDTO} or a {@link Page} of {@link ContactPersonDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ContactPersonQueryService extends QueryService<ContactPerson> {

    private final Logger log = LoggerFactory.getLogger(ContactPersonQueryService.class);

    private final ContactPersonRepository contactPersonRepository;

    private final ContactPersonMapper contactPersonMapper;

    public ContactPersonQueryService(ContactPersonRepository contactPersonRepository, ContactPersonMapper contactPersonMapper) {
        this.contactPersonRepository = contactPersonRepository;
        this.contactPersonMapper = contactPersonMapper;
    }

    /**
     * Return a {@link List} of {@link ContactPersonDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ContactPersonDTO> findByCriteria(ContactPersonCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ContactPerson> specification = createSpecification(criteria);
        return contactPersonMapper.toDto(contactPersonRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ContactPersonDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ContactPersonDTO> findByCriteria(ContactPersonCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ContactPerson> specification = createSpecification(criteria);
        return contactPersonRepository.findAll(specification, page)
            .map(contactPersonMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ContactPersonCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ContactPerson> specification = createSpecification(criteria);
        return contactPersonRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    private Specification<ContactPerson> createSpecification(ContactPersonCriteria criteria) {
        Specification<ContactPerson> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ContactPerson_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), ContactPerson_.name));
            }
            if (criteria.getPrimaryEmail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPrimaryEmail(), ContactPerson_.primaryEmail));
            }
            if (criteria.getSecondaryEmail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSecondaryEmail(), ContactPerson_.secondaryEmail));
            }
            if (criteria.getPrimaryPhone() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPrimaryPhone(), ContactPerson_.primaryPhone));
            }
            if (criteria.getSecondaryPhone() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSecondaryPhone(), ContactPerson_.secondaryPhone));
            }
            if (criteria.getActive() != null) {
                specification = specification.and(buildSpecification(criteria.getActive(), ContactPerson_.active));
            }
        }
        return specification;
    }
}
