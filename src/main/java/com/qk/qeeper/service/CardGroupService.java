package com.qk.qeeper.service;

import com.qk.qeeper.domain.CardGroup;
import com.qk.qeeper.repository.CardGroupRepository;
import com.qk.qeeper.service.dto.CardGroupDTO;
import com.qk.qeeper.service.mapper.CardGroupMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link CardGroup}.
 */
@Service
@Transactional
public class CardGroupService {

    private final Logger log = LoggerFactory.getLogger(CardGroupService.class);

    private final CardGroupRepository cardGroupRepository;

    private final CardGroupMapper cardGroupMapper;

    public CardGroupService(CardGroupRepository cardGroupRepository, CardGroupMapper cardGroupMapper) {
        this.cardGroupRepository = cardGroupRepository;
        this.cardGroupMapper = cardGroupMapper;
    }

    /**
     * Save a cardGroup.
     *
     * @param cardGroupDTO the entity to save.
     * @return the persisted entity.
     */
    public CardGroupDTO save(CardGroupDTO cardGroupDTO) {
        log.debug("Request to save CardGroup : {}", cardGroupDTO);
        CardGroup cardGroup = cardGroupMapper.toEntity(cardGroupDTO);
        cardGroup = cardGroupRepository.save(cardGroup);
        return cardGroupMapper.toDto(cardGroup);
    }

    /**
     * Get all the cardGroups.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<CardGroupDTO> findAll(Pageable pageable) {
        log.debug("Request to get all CardGroups");
        return cardGroupRepository.findAll(pageable)
            .map(cardGroupMapper::toDto);
    }


    /**
     * Get one cardGroup by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<CardGroupDTO> findOne(Long id) {
        log.debug("Request to get CardGroup : {}", id);
        return cardGroupRepository.findById(id)
            .map(cardGroupMapper::toDto);
    }

    /**
     * Delete the cardGroup by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete CardGroup : {}", id);
        cardGroupRepository.deleteById(id);
    }
}
