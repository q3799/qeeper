package com.qk.qeeper.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.qk.qeeper.domain.EscalationMatrix;
import com.qk.qeeper.domain.*; // for static metamodels
import com.qk.qeeper.repository.EscalationMatrixRepository;
import com.qk.qeeper.service.dto.EscalationMatrixCriteria;
import com.qk.qeeper.service.dto.EscalationMatrixDTO;
import com.qk.qeeper.service.mapper.EscalationMatrixMapper;

/**
 * Service for executing complex queries for {@link EscalationMatrix} entities in the database.
 * The main input is a {@link EscalationMatrixCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link EscalationMatrixDTO} or a {@link Page} of {@link EscalationMatrixDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class EscalationMatrixQueryService extends QueryService<EscalationMatrix> {

    private final Logger log = LoggerFactory.getLogger(EscalationMatrixQueryService.class);

    private final EscalationMatrixRepository escalationMatrixRepository;

    private final EscalationMatrixMapper escalationMatrixMapper;

    public EscalationMatrixQueryService(EscalationMatrixRepository escalationMatrixRepository, EscalationMatrixMapper escalationMatrixMapper) {
        this.escalationMatrixRepository = escalationMatrixRepository;
        this.escalationMatrixMapper = escalationMatrixMapper;
    }

    /**
     * Return a {@link List} of {@link EscalationMatrixDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<EscalationMatrixDTO> findByCriteria(EscalationMatrixCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<EscalationMatrix> specification = createSpecification(criteria);
        return escalationMatrixMapper.toDto(escalationMatrixRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link EscalationMatrixDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<EscalationMatrixDTO> findByCriteria(EscalationMatrixCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<EscalationMatrix> specification = createSpecification(criteria);
        return escalationMatrixRepository.findAll(specification, page)
            .map(escalationMatrixMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(EscalationMatrixCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<EscalationMatrix> specification = createSpecification(criteria);
        return escalationMatrixRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    private Specification<EscalationMatrix> createSpecification(EscalationMatrixCriteria criteria) {
        Specification<EscalationMatrix> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), EscalationMatrix_.id));
            }
            if (criteria.getEscalationType() != null) {
                specification = specification.and(buildSpecification(criteria.getEscalationType(), EscalationMatrix_.escalationType));
            }
            if (criteria.getLevel() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLevel(), EscalationMatrix_.level));
            }
            if (criteria.getReceiveSms() != null) {
                specification = specification.and(buildSpecification(criteria.getReceiveSms(), EscalationMatrix_.receiveSms));
            }
            if (criteria.getReceiveEmail() != null) {
                specification = specification.and(buildSpecification(criteria.getReceiveEmail(), EscalationMatrix_.receiveEmail));
            }
            if (criteria.getContactPersonId() != null) {
                specification = specification.and(buildSpecification(criteria.getContactPersonId(),
                    root -> root.join(EscalationMatrix_.contactPerson, JoinType.LEFT).get(ContactPerson_.id)));
            }
        }
        return specification;
    }
}
