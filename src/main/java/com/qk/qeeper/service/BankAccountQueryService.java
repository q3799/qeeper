package com.qk.qeeper.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.qk.qeeper.domain.BankAccount;
import com.qk.qeeper.domain.*; // for static metamodels
import com.qk.qeeper.repository.BankAccountRepository;
import com.qk.qeeper.service.dto.BankAccountCriteria;
import com.qk.qeeper.service.dto.BankAccountDTO;
import com.qk.qeeper.service.mapper.BankAccountMapper;

/**
 * Service for executing complex queries for {@link BankAccount} entities in the database.
 * The main input is a {@link BankAccountCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link BankAccountDTO} or a {@link Page} of {@link BankAccountDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class BankAccountQueryService extends QueryService<BankAccount> {

    private final Logger log = LoggerFactory.getLogger(BankAccountQueryService.class);

    private final BankAccountRepository bankAccountRepository;

    private final BankAccountMapper bankAccountMapper;

    public BankAccountQueryService(BankAccountRepository bankAccountRepository, BankAccountMapper bankAccountMapper) {
        this.bankAccountRepository = bankAccountRepository;
        this.bankAccountMapper = bankAccountMapper;
    }

    /**
     * Return a {@link List} of {@link BankAccountDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<BankAccountDTO> findByCriteria(BankAccountCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<BankAccount> specification = createSpecification(criteria);
        return bankAccountMapper.toDto(bankAccountRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link BankAccountDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<BankAccountDTO> findByCriteria(BankAccountCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<BankAccount> specification = createSpecification(criteria);
        return bankAccountRepository.findAll(specification, page)
            .map(bankAccountMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(BankAccountCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<BankAccount> specification = createSpecification(criteria);
        return bankAccountRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    private Specification<BankAccount> createSpecification(BankAccountCriteria criteria) {
        Specification<BankAccount> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), BankAccount_.id));
            }
            if (criteria.getDateOfOpening() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDateOfOpening(), BankAccount_.dateOfOpening));
            }
            if (criteria.getOpenedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOpenedBy(), BankAccount_.openedBy));
            }
            if (criteria.getCustomerId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCustomerId(), BankAccount_.customerId));
            }
            if (criteria.getAccountNumber() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAccountNumber(), BankAccount_.accountNumber));
            }
            if (criteria.getLoginId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLoginId(), BankAccount_.loginId));
            }
            if (criteria.getLoginPassword() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLoginPassword(), BankAccount_.loginPassword));
            }
            if (criteria.getEmail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEmail(), BankAccount_.email));
            }
            if (criteria.getPhone() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPhone(), BankAccount_.phone));
            }
            if (criteria.getBankAccountType() != null) {
                specification = specification.and(buildSpecification(criteria.getBankAccountType(), BankAccount_.bankAccountType));
            }
            if (criteria.getChequeBookIssued() != null) {
                specification = specification.and(buildSpecification(criteria.getChequeBookIssued(), BankAccount_.chequeBookIssued));
            }
            if (criteria.getAccountBalance() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getAccountBalance(), BankAccount_.accountBalance));
            }
            if (criteria.getBalanceCheckedOn() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getBalanceCheckedOn(), BankAccount_.balanceCheckedOn));
            }
            if (criteria.getMinimumBalance() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getMinimumBalance(), BankAccount_.minimumBalance));
            }
            if (criteria.getPasswordGetsExpired() != null) {
                specification = specification.and(buildSpecification(criteria.getPasswordGetsExpired(), BankAccount_.passwordGetsExpired));
            }
            if (criteria.getExpirationPeriodDays() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getExpirationPeriodDays(), BankAccount_.expirationPeriodDays));
            }
            if (criteria.getPasswordUpdatedOn() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPasswordUpdatedOn(), BankAccount_.passwordUpdatedOn));
            }
            if (criteria.getRemarks() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRemarks(), BankAccount_.remarks));
            }
            if (criteria.getAccountStatus() != null) {
                specification = specification.and(buildSpecification(criteria.getAccountStatus(), BankAccount_.accountStatus));
            }
            if (criteria.getMultipleSessionAllowed() != null) {
                specification = specification.and(buildSpecification(criteria.getMultipleSessionAllowed(), BankAccount_.multipleSessionAllowed));
            }
            if (criteria.getProjectName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getProjectName(), BankAccount_.projectName));
            }
            if (criteria.getProjectCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getProjectCode(), BankAccount_.projectCode));
            }
            if (criteria.getRequestedOn() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getRequestedOn(), BankAccount_.requestedOn));
            }
            if (criteria.getRequestedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRequestedBy(), BankAccount_.requestedBy));
            }
            if (criteria.getAccountHolderId() != null) {
                specification = specification.and(buildSpecification(criteria.getAccountHolderId(),
                    root -> root.join(BankAccount_.accountHolder, JoinType.LEFT).get(Employee_.id)));
            }
            if (criteria.getBankBranchId() != null) {
                specification = specification.and(buildSpecification(criteria.getBankBranchId(),
                    root -> root.join(BankAccount_.bankBranch, JoinType.LEFT).get(BankBranch_.id)));
            }
            if (criteria.getMobileNumberMappedId() != null) {
                specification = specification.and(buildSpecification(criteria.getMobileNumberMappedId(),
                    root -> root.join(BankAccount_.mobileNumberMapped, JoinType.LEFT).get(MobileNumber_.id)));
            }
            if (criteria.getParentAccountId() != null) {
                specification = specification.and(buildSpecification(criteria.getParentAccountId(),
                    root -> root.join(BankAccount_.parentAccount, JoinType.LEFT).get(BankAccount_.id)));
            }
            if (criteria.getCardsId() != null) {
                specification = specification.and(buildSpecification(criteria.getCardsId(),
                    root -> root.join(BankAccount_.cards, JoinType.LEFT).get(Card_.id)));
            }
            if (criteria.getBankAccountGroupId() != null) {
                specification = specification.and(buildSpecification(criteria.getBankAccountGroupId(),
                    root -> root.join(BankAccount_.bankAccountGroup, JoinType.LEFT).get(BankAccountGroup_.id)));
            }
            if (criteria.getSecretQuestionsId() != null) {
                specification = specification.and(buildSpecification(criteria.getSecretQuestionsId(),
                    root -> root.join(BankAccount_.secretQuestions, JoinType.LEFT).get(SecretQuestion_.id)));
            }
        }
        return specification;
    }
}
