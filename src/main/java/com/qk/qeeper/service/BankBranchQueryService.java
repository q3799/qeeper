package com.qk.qeeper.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.qk.qeeper.domain.BankBranch;
import com.qk.qeeper.domain.*; // for static metamodels
import com.qk.qeeper.repository.BankBranchRepository;
import com.qk.qeeper.service.dto.BankBranchCriteria;
import com.qk.qeeper.service.dto.BankBranchDTO;
import com.qk.qeeper.service.mapper.BankBranchMapper;

/**
 * Service for executing complex queries for {@link BankBranch} entities in the database.
 * The main input is a {@link BankBranchCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link BankBranchDTO} or a {@link Page} of {@link BankBranchDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class BankBranchQueryService extends QueryService<BankBranch> {

    private final Logger log = LoggerFactory.getLogger(BankBranchQueryService.class);

    private final BankBranchRepository bankBranchRepository;

    private final BankBranchMapper bankBranchMapper;

    public BankBranchQueryService(BankBranchRepository bankBranchRepository, BankBranchMapper bankBranchMapper) {
        this.bankBranchRepository = bankBranchRepository;
        this.bankBranchMapper = bankBranchMapper;
    }

    /**
     * Return a {@link List} of {@link BankBranchDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<BankBranchDTO> findByCriteria(BankBranchCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<BankBranch> specification = createSpecification(criteria);
        return bankBranchMapper.toDto(bankBranchRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link BankBranchDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<BankBranchDTO> findByCriteria(BankBranchCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<BankBranch> specification = createSpecification(criteria);
        return bankBranchRepository.findAll(specification, page)
            .map(bankBranchMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(BankBranchCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<BankBranch> specification = createSpecification(criteria);
        return bankBranchRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    private Specification<BankBranch> createSpecification(BankBranchCriteria criteria) {
        Specification<BankBranch> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), BankBranch_.id));
            }
            if (criteria.getLocation() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLocation(), BankBranch_.location));
            }
            if (criteria.getAddress() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAddress(), BankBranch_.address));
            }
            if (criteria.getIfscCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIfscCode(), BankBranch_.ifscCode));
            }
            if (criteria.getMicrCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMicrCode(), BankBranch_.micrCode));
            }
            if (criteria.getRemarks() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRemarks(), BankBranch_.remarks));
            }
            if (criteria.getBankId() != null) {
                specification = specification.and(buildSpecification(criteria.getBankId(),
                    root -> root.join(BankBranch_.bank, JoinType.LEFT).get(Bank_.id)));
            }
        }
        return specification;
    }
}
