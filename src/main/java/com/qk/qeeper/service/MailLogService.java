package com.qk.qeeper.service;

import com.qk.qeeper.domain.MailLog;
import com.qk.qeeper.repository.MailLogRepository;
import com.qk.qeeper.service.dto.MailLogDTO;
import com.qk.qeeper.service.mapper.MailLogMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link MailLog}.
 */
@Service
@Transactional
public class MailLogService {

    private final Logger log = LoggerFactory.getLogger(MailLogService.class);

    private final MailLogRepository mailLogRepository;

    private final MailLogMapper mailLogMapper;

    /**
     * Constructor test
     *
     * @param mailLogRepository
     * @param mailLogMapper
     */
    public MailLogService(MailLogRepository mailLogRepository, MailLogMapper mailLogMapper) {
        this.mailLogRepository = mailLogRepository;
        this.mailLogMapper = mailLogMapper;
    }

    /**
     * Save a mailLog.
     *
     * @param mailLogDTO the entity to save.
     * @return the persisted entity.
     */
    public MailLogDTO save(MailLogDTO mailLogDTO) {
        log.debug("Request to save MailLog : {}", mailLogDTO);
        MailLog mailLog = mailLogMapper.toEntity(mailLogDTO);
        mailLog = mailLogRepository.save(mailLog);
        return mailLogMapper.toDto(mailLog);
    }

    /**
     * Get all the mailLogs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<MailLogDTO> findAll(Pageable pageable) {
        log.debug("Request to get all MailLogs");
        return mailLogRepository.findAll(pageable)
            .map(mailLogMapper::toDto);
    }


    /**
     * Get one mailLog by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<MailLogDTO> findOne(Long id) {
        log.debug("Request to get MailLog : {}", id);
        return mailLogRepository.findById(id)
            .map(mailLogMapper::toDto);
    }

    /**
     * Delete the mailLog by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete MailLog : {}", id);
        mailLogRepository.deleteById(id);
    }
}
