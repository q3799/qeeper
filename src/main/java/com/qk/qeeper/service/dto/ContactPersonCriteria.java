package com.qk.qeeper.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.qk.qeeper.domain.ContactPerson} entity. This class is used
 * in {@link com.qk.qeeper.web.rest.ContactPersonResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /contact-people?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ContactPersonCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private StringFilter primaryEmail;

    private StringFilter secondaryEmail;

    private StringFilter primaryPhone;

    private StringFilter secondaryPhone;

    private BooleanFilter active;

    public ContactPersonCriteria(){
    }

    public ContactPersonCriteria(ContactPersonCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.primaryEmail = other.primaryEmail == null ? null : other.primaryEmail.copy();
        this.secondaryEmail = other.secondaryEmail == null ? null : other.secondaryEmail.copy();
        this.primaryPhone = other.primaryPhone == null ? null : other.primaryPhone.copy();
        this.secondaryPhone = other.secondaryPhone == null ? null : other.secondaryPhone.copy();
        this.active = other.active == null ? null : other.active.copy();
    }

    @Override
    public ContactPersonCriteria copy() {
        return new ContactPersonCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getPrimaryEmail() {
        return primaryEmail;
    }

    public void setPrimaryEmail(StringFilter primaryEmail) {
        this.primaryEmail = primaryEmail;
    }

    public StringFilter getSecondaryEmail() {
        return secondaryEmail;
    }

    public void setSecondaryEmail(StringFilter secondaryEmail) {
        this.secondaryEmail = secondaryEmail;
    }

    public StringFilter getPrimaryPhone() {
        return primaryPhone;
    }

    public void setPrimaryPhone(StringFilter primaryPhone) {
        this.primaryPhone = primaryPhone;
    }

    public StringFilter getSecondaryPhone() {
        return secondaryPhone;
    }

    public void setSecondaryPhone(StringFilter secondaryPhone) {
        this.secondaryPhone = secondaryPhone;
    }

    public BooleanFilter getActive() {
        return active;
    }

    public void setActive(BooleanFilter active) {
        this.active = active;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ContactPersonCriteria that = (ContactPersonCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(primaryEmail, that.primaryEmail) &&
            Objects.equals(secondaryEmail, that.secondaryEmail) &&
            Objects.equals(primaryPhone, that.primaryPhone) &&
            Objects.equals(secondaryPhone, that.secondaryPhone) &&
            Objects.equals(active, that.active);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        name,
        primaryEmail,
        secondaryEmail,
        primaryPhone,
        secondaryPhone,
        active
        );
    }

    @Override
    public String toString() {
        return "ContactPersonCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (primaryEmail != null ? "primaryEmail=" + primaryEmail + ", " : "") +
                (secondaryEmail != null ? "secondaryEmail=" + secondaryEmail + ", " : "") +
                (primaryPhone != null ? "primaryPhone=" + primaryPhone + ", " : "") +
                (secondaryPhone != null ? "secondaryPhone=" + secondaryPhone + ", " : "") +
                (active != null ? "active=" + active + ", " : "") +
            "}";
    }

}
