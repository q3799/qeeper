package com.qk.qeeper.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import com.qk.qeeper.domain.enumeration.EscalationType;

/**
 * A DTO for the {@link com.qk.qeeper.domain.EscalationMatrix} entity.
 */
public class EscalationMatrixDTO implements Serializable {

    private Long id;

    @NotNull
    private EscalationType escalationType;

    @NotNull
    @Min(value = 1)
    @Max(value = 100)
    private Integer level;

    @NotNull
    private Boolean receiveSms;

    @NotNull
    private Boolean receiveEmail;


    private Long contactPersonId;

    private String contactPersonName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EscalationType getEscalationType() {
        return escalationType;
    }

    public void setEscalationType(EscalationType escalationType) {
        this.escalationType = escalationType;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Boolean isReceiveSms() {
        return receiveSms;
    }

    public void setReceiveSms(Boolean receiveSms) {
        this.receiveSms = receiveSms;
    }

    public Boolean isReceiveEmail() {
        return receiveEmail;
    }

    public void setReceiveEmail(Boolean receiveEmail) {
        this.receiveEmail = receiveEmail;
    }

    public Long getContactPersonId() {
        return contactPersonId;
    }

    public void setContactPersonId(Long contactPersonId) {
        this.contactPersonId = contactPersonId;
    }

    public String getContactPersonName() {
        return contactPersonName;
    }

    public void setContactPersonName(String contactPersonName) {
        this.contactPersonName = contactPersonName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EscalationMatrixDTO escalationMatrixDTO = (EscalationMatrixDTO) o;
        if (escalationMatrixDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), escalationMatrixDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "EscalationMatrixDTO{" +
            "id=" + getId() +
            ", escalationType='" + getEscalationType() + "'" +
            ", level=" + getLevel() +
            ", receiveSms='" + isReceiveSms() + "'" +
            ", receiveEmail='" + isReceiveEmail() + "'" +
            ", contactPerson=" + getContactPersonId() +
            ", contactPerson='" + getContactPersonName() + "'" +
            "}";
    }
}
