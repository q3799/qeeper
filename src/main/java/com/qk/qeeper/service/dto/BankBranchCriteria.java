package com.qk.qeeper.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.qk.qeeper.domain.BankBranch} entity. This class is used
 * in {@link com.qk.qeeper.web.rest.BankBranchResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /bank-branches?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class BankBranchCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter location;

    private StringFilter address;

    private StringFilter ifscCode;

    private StringFilter micrCode;

    private StringFilter remarks;

    private LongFilter bankId;

    public BankBranchCriteria(){
    }

    public BankBranchCriteria(BankBranchCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.location = other.location == null ? null : other.location.copy();
        this.address = other.address == null ? null : other.address.copy();
        this.ifscCode = other.ifscCode == null ? null : other.ifscCode.copy();
        this.micrCode = other.micrCode == null ? null : other.micrCode.copy();
        this.remarks = other.remarks == null ? null : other.remarks.copy();
        this.bankId = other.bankId == null ? null : other.bankId.copy();
    }

    @Override
    public BankBranchCriteria copy() {
        return new BankBranchCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getLocation() {
        return location;
    }

    public void setLocation(StringFilter location) {
        this.location = location;
    }

    public StringFilter getAddress() {
        return address;
    }

    public void setAddress(StringFilter address) {
        this.address = address;
    }

    public StringFilter getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(StringFilter ifscCode) {
        this.ifscCode = ifscCode;
    }

    public StringFilter getMicrCode() {
        return micrCode;
    }

    public void setMicrCode(StringFilter micrCode) {
        this.micrCode = micrCode;
    }

    public StringFilter getRemarks() {
        return remarks;
    }

    public void setRemarks(StringFilter remarks) {
        this.remarks = remarks;
    }

    public LongFilter getBankId() {
        return bankId;
    }

    public void setBankId(LongFilter bankId) {
        this.bankId = bankId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final BankBranchCriteria that = (BankBranchCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(location, that.location) &&
            Objects.equals(address, that.address) &&
            Objects.equals(ifscCode, that.ifscCode) &&
            Objects.equals(micrCode, that.micrCode) &&
            Objects.equals(remarks, that.remarks) &&
            Objects.equals(bankId, that.bankId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        location,
        address,
        ifscCode,
        micrCode,
        remarks,
        bankId
        );
    }

    @Override
    public String toString() {
        return "BankBranchCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (location != null ? "location=" + location + ", " : "") +
                (address != null ? "address=" + address + ", " : "") +
                (ifscCode != null ? "ifscCode=" + ifscCode + ", " : "") +
                (micrCode != null ? "micrCode=" + micrCode + ", " : "") +
                (remarks != null ? "remarks=" + remarks + ", " : "") +
                (bankId != null ? "bankId=" + bankId + ", " : "") +
            "}";
    }

}
