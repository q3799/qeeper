package com.qk.qeeper.service.dto;

import com.qk.qeeper.domain.enumeration.ResourceType;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

public class CredentialRequestDTO implements Serializable {

    @NotNull
    private String groupCode;

    @NotNull
    private ResourceType resourceType;

    public String getGroupCode() {
        return groupCode;
    }

    public CredentialRequestDTO groupCode(String groupCode) {
        this.groupCode = groupCode;
        return this;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public ResourceType getResourceType() {
        return resourceType;
    }

    public CredentialRequestDTO resourceType(ResourceType resourceType) {
        this.resourceType = resourceType;
        return this;
    }

    public void setResourceType(ResourceType resourceType) {
        this.resourceType = resourceType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CredentialRequestDTO)) return false;
        CredentialRequestDTO that = (CredentialRequestDTO) o;
        return Objects.equals(getGroupCode(), that.getGroupCode()) &&
            getResourceType() == that.getResourceType();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getGroupCode(), getResourceType());
    }

    @Override
    public String toString() {
        return "CredentialRequestDTO{" +
            "groupCode='" + groupCode + '\'' +
            ", resourceType=" + resourceType +
            '}';
    }
}
