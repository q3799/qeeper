package com.qk.qeeper.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import com.qk.qeeper.domain.enumeration.MailServer;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link com.qk.qeeper.domain.MailLog} entity. This class is used
 * in {@link com.qk.qeeper.web.rest.MailLogResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /mail-logs?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class MailLogCriteria implements Serializable, Criteria {
    /**
     * Class for filtering MailServer
     */
    public static class MailServerFilter extends Filter<MailServer> {

        public MailServerFilter() {
        }

        public MailServerFilter(MailServerFilter filter) {
            super(filter);
        }

        @Override
        public MailServerFilter copy() {
            return new MailServerFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter subject;

    private StringFilter content;

    private InstantFilter mailDate;

    private MailServerFilter mailServer;

    private StringFilter responseReceived;

    private StringFilter remarks;

    private StringFilter toList;

    private StringFilter ccList;

    private LongFilter processingTime;

    public MailLogCriteria(){
    }

    public MailLogCriteria(MailLogCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.subject = other.subject == null ? null : other.subject.copy();
        this.content = other.content == null ? null : other.content.copy();
        this.mailDate = other.mailDate == null ? null : other.mailDate.copy();
        this.mailServer = other.mailServer == null ? null : other.mailServer.copy();
        this.responseReceived = other.responseReceived == null ? null : other.responseReceived.copy();
        this.remarks = other.remarks == null ? null : other.remarks.copy();
        this.toList = other.toList == null ? null : other.toList.copy();
        this.ccList = other.ccList == null ? null : other.ccList.copy();
        this.processingTime = other.processingTime == null ? null : other.processingTime.copy();
    }

    @Override
    public MailLogCriteria copy() {
        return new MailLogCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getSubject() {
        return subject;
    }

    public void setSubject(StringFilter subject) {
        this.subject = subject;
    }

    public StringFilter getContent() {
        return content;
    }

    public void setContent(StringFilter content) {
        this.content = content;
    }

    public InstantFilter getMailDate() {
        return mailDate;
    }

    public void setMailDate(InstantFilter mailDate) {
        this.mailDate = mailDate;
    }

    public MailServerFilter getMailServer() {
        return mailServer;
    }

    public void setMailServer(MailServerFilter mailServer) {
        this.mailServer = mailServer;
    }

    public StringFilter getResponseReceived() {
        return responseReceived;
    }

    public void setResponseReceived(StringFilter responseReceived) {
        this.responseReceived = responseReceived;
    }

    public StringFilter getRemarks() {
        return remarks;
    }

    public void setRemarks(StringFilter remarks) {
        this.remarks = remarks;
    }

    public StringFilter getToList() {
        return toList;
    }

    public void setToList(StringFilter toList) {
        this.toList = toList;
    }

    public StringFilter getCcList() {
        return ccList;
    }

    public void setCcList(StringFilter ccList) {
        this.ccList = ccList;
    }

    public LongFilter getProcessingTime() {
        return processingTime;
    }

    public void setProcessingTime(LongFilter processingTime) {
        this.processingTime = processingTime;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final MailLogCriteria that = (MailLogCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(subject, that.subject) &&
            Objects.equals(content, that.content) &&
            Objects.equals(mailDate, that.mailDate) &&
            Objects.equals(mailServer, that.mailServer) &&
            Objects.equals(responseReceived, that.responseReceived) &&
            Objects.equals(remarks, that.remarks) &&
            Objects.equals(toList, that.toList) &&
            Objects.equals(ccList, that.ccList) &&
            Objects.equals(processingTime, that.processingTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        subject,
        content,
        mailDate,
        mailServer,
        responseReceived,
        remarks,
        toList,
        ccList,
        processingTime
        );
    }

    @Override
    public String toString() {
        return "MailLogCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (subject != null ? "subject=" + subject + ", " : "") +
                (content != null ? "content=" + content + ", " : "") +
                (mailDate != null ? "mailDate=" + mailDate + ", " : "") +
                (mailServer != null ? "mailServer=" + mailServer + ", " : "") +
                (responseReceived != null ? "responseReceived=" + responseReceived + ", " : "") +
                (remarks != null ? "remarks=" + remarks + ", " : "") +
                (toList != null ? "toList=" + toList + ", " : "") +
                (ccList != null ? "ccList=" + ccList + ", " : "") +
                (processingTime != null ? "processingTime=" + processingTime + ", " : "") +
            "}";
    }

}
