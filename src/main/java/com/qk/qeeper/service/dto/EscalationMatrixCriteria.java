package com.qk.qeeper.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import com.qk.qeeper.domain.enumeration.EscalationType;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.qk.qeeper.domain.EscalationMatrix} entity. This class is used
 * in {@link com.qk.qeeper.web.rest.EscalationMatrixResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /escalation-matrices?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class EscalationMatrixCriteria implements Serializable, Criteria {
    /**
     * Class for filtering EscalationType
     */
    public static class EscalationTypeFilter extends Filter<EscalationType> {

        public EscalationTypeFilter() {
        }

        public EscalationTypeFilter(EscalationTypeFilter filter) {
            super(filter);
        }

        @Override
        public EscalationTypeFilter copy() {
            return new EscalationTypeFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private EscalationTypeFilter escalationType;

    private IntegerFilter level;

    private BooleanFilter receiveSms;

    private BooleanFilter receiveEmail;

    private LongFilter contactPersonId;

    public EscalationMatrixCriteria(){
    }

    public EscalationMatrixCriteria(EscalationMatrixCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.escalationType = other.escalationType == null ? null : other.escalationType.copy();
        this.level = other.level == null ? null : other.level.copy();
        this.receiveSms = other.receiveSms == null ? null : other.receiveSms.copy();
        this.receiveEmail = other.receiveEmail == null ? null : other.receiveEmail.copy();
        this.contactPersonId = other.contactPersonId == null ? null : other.contactPersonId.copy();
    }

    @Override
    public EscalationMatrixCriteria copy() {
        return new EscalationMatrixCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public EscalationTypeFilter getEscalationType() {
        return escalationType;
    }

    public void setEscalationType(EscalationTypeFilter escalationType) {
        this.escalationType = escalationType;
    }

    public IntegerFilter getLevel() {
        return level;
    }

    public void setLevel(IntegerFilter level) {
        this.level = level;
    }

    public BooleanFilter getReceiveSms() {
        return receiveSms;
    }

    public void setReceiveSms(BooleanFilter receiveSms) {
        this.receiveSms = receiveSms;
    }

    public BooleanFilter getReceiveEmail() {
        return receiveEmail;
    }

    public void setReceiveEmail(BooleanFilter receiveEmail) {
        this.receiveEmail = receiveEmail;
    }

    public LongFilter getContactPersonId() {
        return contactPersonId;
    }

    public void setContactPersonId(LongFilter contactPersonId) {
        this.contactPersonId = contactPersonId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final EscalationMatrixCriteria that = (EscalationMatrixCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(escalationType, that.escalationType) &&
            Objects.equals(level, that.level) &&
            Objects.equals(receiveSms, that.receiveSms) &&
            Objects.equals(receiveEmail, that.receiveEmail) &&
            Objects.equals(contactPersonId, that.contactPersonId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        escalationType,
        level,
        receiveSms,
        receiveEmail,
        contactPersonId
        );
    }

    @Override
    public String toString() {
        return "EscalationMatrixCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (escalationType != null ? "escalationType=" + escalationType + ", " : "") +
                (level != null ? "level=" + level + ", " : "") +
                (receiveSms != null ? "receiveSms=" + receiveSms + ", " : "") +
                (receiveEmail != null ? "receiveEmail=" + receiveEmail + ", " : "") +
                (contactPersonId != null ? "contactPersonId=" + contactPersonId + ", " : "") +
            "}";
    }

}
