package com.qk.qeeper.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.qk.qeeper.domain.SecretQuestion} entity.
 */
public class SecretQuestionDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 200)
    private String question;

    @NotNull
    @Size(max = 200)
    private String answer;


    private Long bankAccountId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Long getBankAccountId() {
        return bankAccountId;
    }

    public void setBankAccountId(Long bankAccountId) {
        this.bankAccountId = bankAccountId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SecretQuestionDTO secretQuestionDTO = (SecretQuestionDTO) o;
        if (secretQuestionDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), secretQuestionDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SecretQuestionDTO{" +
            "id=" + getId() +
            ", question='" + getQuestion() + "'" +
            ", answer='" + getAnswer() + "'" +
            ", bankAccount=" + getBankAccountId() +
            "}";
    }
}
