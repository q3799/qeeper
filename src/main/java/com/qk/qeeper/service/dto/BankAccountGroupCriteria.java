package com.qk.qeeper.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.qk.qeeper.domain.BankAccountGroup} entity. This class is used
 * in {@link com.qk.qeeper.web.rest.BankAccountGroupResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /bank-account-groups?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class BankAccountGroupCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter code;

    private StringFilter name;

    private LongFilter bankAccountsId;

    public BankAccountGroupCriteria(){
    }

    public BankAccountGroupCriteria(BankAccountGroupCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.code = other.code == null ? null : other.code.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.bankAccountsId = other.bankAccountsId == null ? null : other.bankAccountsId.copy();
    }

    @Override
    public BankAccountGroupCriteria copy() {
        return new BankAccountGroupCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCode() {
        return code;
    }

    public void setCode(StringFilter code) {
        this.code = code;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public LongFilter getBankAccountsId() {
        return bankAccountsId;
    }

    public void setBankAccountsId(LongFilter bankAccountsId) {
        this.bankAccountsId = bankAccountsId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final BankAccountGroupCriteria that = (BankAccountGroupCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(code, that.code) &&
            Objects.equals(name, that.name) &&
            Objects.equals(bankAccountsId, that.bankAccountsId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        code,
        name,
        bankAccountsId
        );
    }

    @Override
    public String toString() {
        return "BankAccountGroupCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (code != null ? "code=" + code + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (bankAccountsId != null ? "bankAccountsId=" + bankAccountsId + ", " : "") +
            "}";
    }

}
