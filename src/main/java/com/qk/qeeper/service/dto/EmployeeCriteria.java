package com.qk.qeeper.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.qk.qeeper.domain.Employee} entity. This class is used
 * in {@link com.qk.qeeper.web.rest.EmployeeResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /employees?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class EmployeeCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private StringFilter pan;

    private StringFilter dateOfBirth;

    private StringFilter aadhaar;

    private StringFilter email;

    private StringFilter phone;

    private StringFilter communicationAddress;

    private StringFilter permanentAddress;

    private StringFilter remarks;

    private BooleanFilter active;

    public EmployeeCriteria(){
    }

    public EmployeeCriteria(EmployeeCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.pan = other.pan == null ? null : other.pan.copy();
        this.dateOfBirth = other.dateOfBirth == null ? null : other.dateOfBirth.copy();
        this.aadhaar = other.aadhaar == null ? null : other.aadhaar.copy();
        this.email = other.email == null ? null : other.email.copy();
        this.phone = other.phone == null ? null : other.phone.copy();
        this.communicationAddress = other.communicationAddress == null ? null : other.communicationAddress.copy();
        this.permanentAddress = other.permanentAddress == null ? null : other.permanentAddress.copy();
        this.remarks = other.remarks == null ? null : other.remarks.copy();
        this.active = other.active == null ? null : other.active.copy();
    }

    @Override
    public EmployeeCriteria copy() {
        return new EmployeeCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getPan() {
        return pan;
    }

    public void setPan(StringFilter pan) {
        this.pan = pan;
    }

    public StringFilter getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(StringFilter dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public StringFilter getAadhaar() {
        return aadhaar;
    }

    public void setAadhaar(StringFilter aadhaar) {
        this.aadhaar = aadhaar;
    }

    public StringFilter getEmail() {
        return email;
    }

    public void setEmail(StringFilter email) {
        this.email = email;
    }

    public StringFilter getPhone() {
        return phone;
    }

    public void setPhone(StringFilter phone) {
        this.phone = phone;
    }

    public StringFilter getCommunicationAddress() {
        return communicationAddress;
    }

    public void setCommunicationAddress(StringFilter communicationAddress) {
        this.communicationAddress = communicationAddress;
    }

    public StringFilter getPermanentAddress() {
        return permanentAddress;
    }

    public void setPermanentAddress(StringFilter permanentAddress) {
        this.permanentAddress = permanentAddress;
    }

    public StringFilter getRemarks() {
        return remarks;
    }

    public void setRemarks(StringFilter remarks) {
        this.remarks = remarks;
    }

    public BooleanFilter getActive() {
        return active;
    }

    public void setActive(BooleanFilter active) {
        this.active = active;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final EmployeeCriteria that = (EmployeeCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(pan, that.pan) &&
            Objects.equals(dateOfBirth, that.dateOfBirth) &&
            Objects.equals(aadhaar, that.aadhaar) &&
            Objects.equals(email, that.email) &&
            Objects.equals(phone, that.phone) &&
            Objects.equals(communicationAddress, that.communicationAddress) &&
            Objects.equals(permanentAddress, that.permanentAddress) &&
            Objects.equals(remarks, that.remarks) &&
            Objects.equals(active, that.active);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        name,
        pan,
        dateOfBirth,
        aadhaar,
        email,
        phone,
        communicationAddress,
        permanentAddress,
        remarks,
        active
        );
    }

    @Override
    public String toString() {
        return "EmployeeCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (pan != null ? "pan=" + pan + ", " : "") +
                (dateOfBirth != null ? "dateOfBirth=" + dateOfBirth + ", " : "") +
                (aadhaar != null ? "aadhaar=" + aadhaar + ", " : "") +
                (email != null ? "email=" + email + ", " : "") +
                (phone != null ? "phone=" + phone + ", " : "") +
                (communicationAddress != null ? "communicationAddress=" + communicationAddress + ", " : "") +
                (permanentAddress != null ? "permanentAddress=" + permanentAddress + ", " : "") +
                (remarks != null ? "remarks=" + remarks + ", " : "") +
                (active != null ? "active=" + active + ", " : "") +
            "}";
    }

}
