package com.qk.qeeper.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import com.qk.qeeper.domain.enumeration.CardType;
import com.qk.qeeper.domain.enumeration.PaymentNetwork;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.qk.qeeper.domain.Card} entity. This class is used
 * in {@link com.qk.qeeper.web.rest.CardResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /cards?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class CardCriteria implements Serializable, Criteria {
    /**
     * Class for filtering CardType
     */
    public static class CardTypeFilter extends Filter<CardType> {

        public CardTypeFilter() {
        }

        public CardTypeFilter(CardTypeFilter filter) {
            super(filter);
        }

        @Override
        public CardTypeFilter copy() {
            return new CardTypeFilter(this);
        }

    }
    /**
     * Class for filtering PaymentNetwork
     */
    public static class PaymentNetworkFilter extends Filter<PaymentNetwork> {

        public PaymentNetworkFilter() {
        }

        public PaymentNetworkFilter(PaymentNetworkFilter filter) {
            super(filter);
        }

        @Override
        public PaymentNetworkFilter copy() {
            return new PaymentNetworkFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private CardTypeFilter cardType;

    private PaymentNetworkFilter paymentNetwork;

    private StringFilter cardNumber;

    private StringFilter validFrom;

    private StringFilter validTill;

    private StringFilter paymentNetworkPin;

    private StringFilter cvv;

    private StringFilter nameOnCard;

    private StringFilter remarks;

    private StringFilter pin;

    private StringFilter tpin;

    private LongFilter mobileNumberMappedId;

    private LongFilter registeredToId;

    private LongFilter bankId;

    private LongFilter bankAccountId;

    private LongFilter cardGroupId;

    public CardCriteria(){
    }

    public CardCriteria(CardCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.cardType = other.cardType == null ? null : other.cardType.copy();
        this.paymentNetwork = other.paymentNetwork == null ? null : other.paymentNetwork.copy();
        this.cardNumber = other.cardNumber == null ? null : other.cardNumber.copy();
        this.validFrom = other.validFrom == null ? null : other.validFrom.copy();
        this.validTill = other.validTill == null ? null : other.validTill.copy();
        this.paymentNetworkPin = other.paymentNetworkPin == null ? null : other.paymentNetworkPin.copy();
        this.cvv = other.cvv == null ? null : other.cvv.copy();
        this.nameOnCard = other.nameOnCard == null ? null : other.nameOnCard.copy();
        this.remarks = other.remarks == null ? null : other.remarks.copy();
        this.pin = other.pin == null ? null : other.pin.copy();
        this.tpin = other.tpin == null ? null : other.tpin.copy();
        this.mobileNumberMappedId = other.mobileNumberMappedId == null ? null : other.mobileNumberMappedId.copy();
        this.registeredToId = other.registeredToId == null ? null : other.registeredToId.copy();
        this.bankId = other.bankId == null ? null : other.bankId.copy();
        this.bankAccountId = other.bankAccountId == null ? null : other.bankAccountId.copy();
        this.cardGroupId = other.cardGroupId == null ? null : other.cardGroupId.copy();
    }

    @Override
    public CardCriteria copy() {
        return new CardCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public CardTypeFilter getCardType() {
        return cardType;
    }

    public void setCardType(CardTypeFilter cardType) {
        this.cardType = cardType;
    }

    public PaymentNetworkFilter getPaymentNetwork() {
        return paymentNetwork;
    }

    public void setPaymentNetwork(PaymentNetworkFilter paymentNetwork) {
        this.paymentNetwork = paymentNetwork;
    }

    public StringFilter getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(StringFilter cardNumber) {
        this.cardNumber = cardNumber;
    }

    public StringFilter getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(StringFilter validFrom) {
        this.validFrom = validFrom;
    }

    public StringFilter getValidTill() {
        return validTill;
    }

    public void setValidTill(StringFilter validTill) {
        this.validTill = validTill;
    }

    public StringFilter getPaymentNetworkPin() {
        return paymentNetworkPin;
    }

    public void setPaymentNetworkPin(StringFilter paymentNetworkPin) {
        this.paymentNetworkPin = paymentNetworkPin;
    }

    public StringFilter getCvv() {
        return cvv;
    }

    public void setCvv(StringFilter cvv) {
        this.cvv = cvv;
    }

    public StringFilter getNameOnCard() {
        return nameOnCard;
    }

    public void setNameOnCard(StringFilter nameOnCard) {
        this.nameOnCard = nameOnCard;
    }

    public StringFilter getRemarks() {
        return remarks;
    }

    public void setRemarks(StringFilter remarks) {
        this.remarks = remarks;
    }

    public StringFilter getPin() {
        return pin;
    }

    public void setPin(StringFilter pin) {
        this.pin = pin;
    }

    public StringFilter getTpin() {
        return tpin;
    }

    public void setTpin(StringFilter tpin) {
        this.tpin = tpin;
    }

    public LongFilter getMobileNumberMappedId() {
        return mobileNumberMappedId;
    }

    public void setMobileNumberMappedId(LongFilter mobileNumberMappedId) {
        this.mobileNumberMappedId = mobileNumberMappedId;
    }

    public LongFilter getRegisteredToId() {
        return registeredToId;
    }

    public void setRegisteredToId(LongFilter registeredToId) {
        this.registeredToId = registeredToId;
    }

    public LongFilter getBankId() {
        return bankId;
    }

    public void setBankId(LongFilter bankId) {
        this.bankId = bankId;
    }

    public LongFilter getBankAccountId() {
        return bankAccountId;
    }

    public void setBankAccountId(LongFilter bankAccountId) {
        this.bankAccountId = bankAccountId;
    }

    public LongFilter getCardGroupId() {
        return cardGroupId;
    }

    public void setCardGroupId(LongFilter cardGroupId) {
        this.cardGroupId = cardGroupId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final CardCriteria that = (CardCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(cardType, that.cardType) &&
            Objects.equals(paymentNetwork, that.paymentNetwork) &&
            Objects.equals(cardNumber, that.cardNumber) &&
            Objects.equals(validFrom, that.validFrom) &&
            Objects.equals(validTill, that.validTill) &&
            Objects.equals(paymentNetworkPin, that.paymentNetworkPin) &&
            Objects.equals(cvv, that.cvv) &&
            Objects.equals(nameOnCard, that.nameOnCard) &&
            Objects.equals(remarks, that.remarks) &&
            Objects.equals(pin, that.pin) &&
            Objects.equals(tpin, that.tpin) &&
            Objects.equals(mobileNumberMappedId, that.mobileNumberMappedId) &&
            Objects.equals(registeredToId, that.registeredToId) &&
            Objects.equals(bankId, that.bankId) &&
            Objects.equals(bankAccountId, that.bankAccountId) &&
            Objects.equals(cardGroupId, that.cardGroupId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        cardType,
        paymentNetwork,
        cardNumber,
        validFrom,
        validTill,
        paymentNetworkPin,
        cvv,
        nameOnCard,
        remarks,
        pin,
        tpin,
        mobileNumberMappedId,
        registeredToId,
        bankId,
        bankAccountId,
        cardGroupId
        );
    }

    @Override
    public String toString() {
        return "CardCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (cardType != null ? "cardType=" + cardType + ", " : "") +
                (paymentNetwork != null ? "paymentNetwork=" + paymentNetwork + ", " : "") +
                (cardNumber != null ? "cardNumber=" + cardNumber + ", " : "") +
                (validFrom != null ? "validFrom=" + validFrom + ", " : "") +
                (validTill != null ? "validTill=" + validTill + ", " : "") +
                (paymentNetworkPin != null ? "paymentNetworkPin=" + paymentNetworkPin + ", " : "") +
                (cvv != null ? "cvv=" + cvv + ", " : "") +
                (nameOnCard != null ? "nameOnCard=" + nameOnCard + ", " : "") +
                (remarks != null ? "remarks=" + remarks + ", " : "") +
                (pin != null ? "pin=" + pin + ", " : "") +
                (tpin != null ? "tpin=" + tpin + ", " : "") +
                (mobileNumberMappedId != null ? "mobileNumberMappedId=" + mobileNumberMappedId + ", " : "") +
                (registeredToId != null ? "registeredToId=" + registeredToId + ", " : "") +
                (bankId != null ? "bankId=" + bankId + ", " : "") +
                (bankAccountId != null ? "bankAccountId=" + bankAccountId + ", " : "") +
                (cardGroupId != null ? "cardGroupId=" + cardGroupId + ", " : "") +
            "}";
    }

}
