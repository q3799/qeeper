package com.qk.qeeper.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import com.qk.qeeper.domain.enumeration.Operator;
import com.qk.qeeper.domain.enumeration.MobileConnectionPlan;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.qk.qeeper.domain.MobileNumber} entity. This class is used
 * in {@link com.qk.qeeper.web.rest.MobileNumberResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /mobile-numbers?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class MobileNumberCriteria implements Serializable, Criteria {
    /**
     * Class for filtering Operator
     */
    public static class OperatorFilter extends Filter<Operator> {

        public OperatorFilter() {
        }

        public OperatorFilter(OperatorFilter filter) {
            super(filter);
        }

        @Override
        public OperatorFilter copy() {
            return new OperatorFilter(this);
        }

    }
    /**
     * Class for filtering MobileConnectionPlan
     */
    public static class MobileConnectionPlanFilter extends Filter<MobileConnectionPlan> {

        public MobileConnectionPlanFilter() {
        }

        public MobileConnectionPlanFilter(MobileConnectionPlanFilter filter) {
            super(filter);
        }

        @Override
        public MobileConnectionPlanFilter copy() {
            return new MobileConnectionPlanFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter number;

    private StringFilter assetTag;

    private OperatorFilter operator;

    private MobileConnectionPlanFilter mobileConnectionPlan;

    private StringFilter remarks;

    private BooleanFilter active;

    private LongFilter registeredToId;

    public MobileNumberCriteria(){
    }

    public MobileNumberCriteria(MobileNumberCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.number = other.number == null ? null : other.number.copy();
        this.assetTag = other.assetTag == null ? null : other.assetTag.copy();
        this.operator = other.operator == null ? null : other.operator.copy();
        this.mobileConnectionPlan = other.mobileConnectionPlan == null ? null : other.mobileConnectionPlan.copy();
        this.remarks = other.remarks == null ? null : other.remarks.copy();
        this.active = other.active == null ? null : other.active.copy();
        this.registeredToId = other.registeredToId == null ? null : other.registeredToId.copy();
    }

    @Override
    public MobileNumberCriteria copy() {
        return new MobileNumberCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getNumber() {
        return number;
    }

    public void setNumber(StringFilter number) {
        this.number = number;
    }

    public StringFilter getAssetTag() {
        return assetTag;
    }

    public void setAssetTag(StringFilter assetTag) {
        this.assetTag = assetTag;
    }

    public OperatorFilter getOperator() {
        return operator;
    }

    public void setOperator(OperatorFilter operator) {
        this.operator = operator;
    }

    public MobileConnectionPlanFilter getMobileConnectionPlan() {
        return mobileConnectionPlan;
    }

    public void setMobileConnectionPlan(MobileConnectionPlanFilter mobileConnectionPlan) {
        this.mobileConnectionPlan = mobileConnectionPlan;
    }

    public StringFilter getRemarks() {
        return remarks;
    }

    public void setRemarks(StringFilter remarks) {
        this.remarks = remarks;
    }

    public BooleanFilter getActive() {
        return active;
    }

    public void setActive(BooleanFilter active) {
        this.active = active;
    }

    public LongFilter getRegisteredToId() {
        return registeredToId;
    }

    public void setRegisteredToId(LongFilter registeredToId) {
        this.registeredToId = registeredToId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final MobileNumberCriteria that = (MobileNumberCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(number, that.number) &&
            Objects.equals(assetTag, that.assetTag) &&
            Objects.equals(operator, that.operator) &&
            Objects.equals(mobileConnectionPlan, that.mobileConnectionPlan) &&
            Objects.equals(remarks, that.remarks) &&
            Objects.equals(active, that.active) &&
            Objects.equals(registeredToId, that.registeredToId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        number,
        assetTag,
        operator,
        mobileConnectionPlan,
        remarks,
        active,
        registeredToId
        );
    }

    @Override
    public String toString() {
        return "MobileNumberCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (number != null ? "number=" + number + ", " : "") +
                (assetTag != null ? "assetTag=" + assetTag + ", " : "") +
                (operator != null ? "operator=" + operator + ", " : "") +
                (mobileConnectionPlan != null ? "mobileConnectionPlan=" + mobileConnectionPlan + ", " : "") +
                (remarks != null ? "remarks=" + remarks + ", " : "") +
                (active != null ? "active=" + active + ", " : "") +
                (registeredToId != null ? "registeredToId=" + registeredToId + ", " : "") +
            "}";
    }

}
