package com.qk.qeeper.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.qk.qeeper.domain.NonBankAccountGroup} entity. This class is used
 * in {@link com.qk.qeeper.web.rest.NonBankAccountGroupResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /non-bank-account-groups?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class NonBankAccountGroupCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter code;

    private StringFilter name;

    private LongFilter nonBankAccountsId;

    public NonBankAccountGroupCriteria(){
    }

    public NonBankAccountGroupCriteria(NonBankAccountGroupCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.code = other.code == null ? null : other.code.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.nonBankAccountsId = other.nonBankAccountsId == null ? null : other.nonBankAccountsId.copy();
    }

    @Override
    public NonBankAccountGroupCriteria copy() {
        return new NonBankAccountGroupCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCode() {
        return code;
    }

    public void setCode(StringFilter code) {
        this.code = code;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public LongFilter getNonBankAccountsId() {
        return nonBankAccountsId;
    }

    public void setNonBankAccountsId(LongFilter nonBankAccountsId) {
        this.nonBankAccountsId = nonBankAccountsId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final NonBankAccountGroupCriteria that = (NonBankAccountGroupCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(code, that.code) &&
            Objects.equals(name, that.name) &&
            Objects.equals(nonBankAccountsId, that.nonBankAccountsId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        code,
        name,
        nonBankAccountsId
        );
    }

    @Override
    public String toString() {
        return "NonBankAccountGroupCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (code != null ? "code=" + code + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (nonBankAccountsId != null ? "nonBankAccountsId=" + nonBankAccountsId + ", " : "") +
            "}";
    }

}
