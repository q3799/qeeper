package com.qk.qeeper.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.qk.qeeper.domain.BankBranch} entity.
 */
public class BankBranchDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 100)
    private String location;

    @Size(max = 1000)
    private String address;

    @Size(max = 50)
    @Pattern(regexp = "^[A-Za-z]{4}[a-zA-Z0-9]{7}$")
    private String ifscCode;

    @Size(max = 50)
    private String micrCode;

    @Size(max = 1000)
    private String remarks;


    private Long bankId;

    private String bankName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }

    public String getMicrCode() {
        return micrCode;
    }

    public void setMicrCode(String micrCode) {
        this.micrCode = micrCode;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Long getBankId() {
        return bankId;
    }

    public void setBankId(Long bankId) {
        this.bankId = bankId;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BankBranchDTO bankBranchDTO = (BankBranchDTO) o;
        if (bankBranchDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), bankBranchDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BankBranchDTO{" +
            "id=" + getId() +
            ", location='" + getLocation() + "'" +
            ", address='" + getAddress() + "'" +
            ", ifscCode='" + getIfscCode() + "'" +
            ", micrCode='" + getMicrCode() + "'" +
            ", remarks='" + getRemarks() + "'" +
            ", bank=" + getBankId() +
            ", bank='" + getBankName() + "'" +
            "}";
    }
}
