package com.qk.qeeper.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import com.qk.qeeper.domain.enumeration.Operator;
import com.qk.qeeper.domain.enumeration.MobileConnectionPlan;

/**
 * A DTO for the {@link com.qk.qeeper.domain.MobileNumber} entity.
 */
public class MobileNumberDTO implements Serializable {

    private Long id;

    @Size(max = 20)
    @Pattern(regexp = "^\\+?[1-9]\\d{1,14}$")
    private String number;

    @Size(max = 50)
    private String assetTag;

    @NotNull
    private Operator operator;

    private MobileConnectionPlan mobileConnectionPlan;

    @Size(max = 2000)
    private String remarks;

    @NotNull
    private Boolean active;


    private Long registeredToId;

    private String registeredToName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getAssetTag() {
        return assetTag;
    }

    public void setAssetTag(String assetTag) {
        this.assetTag = assetTag;
    }

    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    public MobileConnectionPlan getMobileConnectionPlan() {
        return mobileConnectionPlan;
    }

    public void setMobileConnectionPlan(MobileConnectionPlan mobileConnectionPlan) {
        this.mobileConnectionPlan = mobileConnectionPlan;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Long getRegisteredToId() {
        return registeredToId;
    }

    public void setRegisteredToId(Long employeeId) {
        this.registeredToId = employeeId;
    }

    public String getRegisteredToName() {
        return registeredToName;
    }

    public void setRegisteredToName(String employeeName) {
        this.registeredToName = employeeName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MobileNumberDTO mobileNumberDTO = (MobileNumberDTO) o;
        if (mobileNumberDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), mobileNumberDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "MobileNumberDTO{" +
            "id=" + getId() +
            ", number='" + getNumber() + "'" +
            ", assetTag='" + getAssetTag() + "'" +
            ", operator='" + getOperator() + "'" +
            ", mobileConnectionPlan='" + getMobileConnectionPlan() + "'" +
            ", remarks='" + getRemarks() + "'" +
            ", active='" + isActive() + "'" +
            ", registeredTo=" + getRegisteredToId() +
            ", registeredTo='" + getRegisteredToName() + "'" +
            "}";
    }
}
