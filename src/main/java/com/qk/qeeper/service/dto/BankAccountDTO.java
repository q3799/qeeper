package com.qk.qeeper.service.dto;
import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import com.qk.qeeper.domain.enumeration.BankAccountType;
import com.qk.qeeper.domain.enumeration.AccountStatus;

/**
 * A DTO for the {@link com.qk.qeeper.domain.BankAccount} entity.
 */
public class BankAccountDTO implements Serializable {

    private Long id;

    private Instant dateOfOpening;

    @Size(max = 100)
    private String openedBy;

    @NotNull
    @Size(max = 50)
    private String customerId;

    @Size(max = 100)
    private String accountNumber;

    @NotNull
    @Size(max = 50)
    private String loginId;

    @NotNull
    @Size(max = 50)
    private String loginPassword;

    @NotNull
    @Size(max = 200)
    @Pattern(regexp = "^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$")
    private String email;

    @Pattern(regexp = "^\\+?[1-9]\\d{1,14}$")
    private String phone;

    @NotNull
    private BankAccountType bankAccountType;

    @NotNull
    private Boolean chequeBookIssued;

    @DecimalMin(value = "0")
    private Double accountBalance;

    private Instant balanceCheckedOn;

    private Double minimumBalance;

    @NotNull
    private Boolean passwordGetsExpired;

    @Min(value = 0)
    private Integer expirationPeriodDays;

    private Instant passwordUpdatedOn;

    @Size(max = 1000)
    private String remarks;

    @NotNull
    private AccountStatus accountStatus;

    @NotNull
    private Boolean multipleSessionAllowed;

    @Size(max = 100)
    private String projectName;

    @Size(max = 50)
    private String projectCode;

    private Instant requestedOn;

    @Size(max = 100)
    private String requestedBy;


    private Long accountHolderId;

    private String accountHolderName;

    private Long bankBranchId;

    private String bankBranchLocation;

    private Long mobileNumberMappedId;

    private String mobileNumberMappedNumber;

    private Long parentAccountId;

    private Long bankAccountGroupId;

    private String bankAccountGroupName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getDateOfOpening() {
        return dateOfOpening;
    }

    public void setDateOfOpening(Instant dateOfOpening) {
        this.dateOfOpening = dateOfOpening;
    }

    public String getOpenedBy() {
        return openedBy;
    }

    public void setOpenedBy(String openedBy) {
        this.openedBy = openedBy;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getLoginPassword() {
        return loginPassword;
    }

    public void setLoginPassword(String loginPassword) {
        this.loginPassword = loginPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public BankAccountType getBankAccountType() {
        return bankAccountType;
    }

    public void setBankAccountType(BankAccountType bankAccountType) {
        this.bankAccountType = bankAccountType;
    }

    public Boolean isChequeBookIssued() {
        return chequeBookIssued;
    }

    public void setChequeBookIssued(Boolean chequeBookIssued) {
        this.chequeBookIssued = chequeBookIssued;
    }

    public Double getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(Double accountBalance) {
        this.accountBalance = accountBalance;
    }

    public Instant getBalanceCheckedOn() {
        return balanceCheckedOn;
    }

    public void setBalanceCheckedOn(Instant balanceCheckedOn) {
        this.balanceCheckedOn = balanceCheckedOn;
    }

    public Double getMinimumBalance() {
        return minimumBalance;
    }

    public void setMinimumBalance(Double minimumBalance) {
        this.minimumBalance = minimumBalance;
    }

    public Boolean isPasswordGetsExpired() {
        return passwordGetsExpired;
    }

    public void setPasswordGetsExpired(Boolean passwordGetsExpired) {
        this.passwordGetsExpired = passwordGetsExpired;
    }

    public Integer getExpirationPeriodDays() {
        return expirationPeriodDays;
    }

    public void setExpirationPeriodDays(Integer expirationPeriodDays) {
        this.expirationPeriodDays = expirationPeriodDays;
    }

    public Instant getPasswordUpdatedOn() {
        return passwordUpdatedOn;
    }

    public void setPasswordUpdatedOn(Instant passwordUpdatedOn) {
        this.passwordUpdatedOn = passwordUpdatedOn;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public AccountStatus getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(AccountStatus accountStatus) {
        this.accountStatus = accountStatus;
    }

    public Boolean isMultipleSessionAllowed() {
        return multipleSessionAllowed;
    }

    public void setMultipleSessionAllowed(Boolean multipleSessionAllowed) {
        this.multipleSessionAllowed = multipleSessionAllowed;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectCode() {
        return projectCode;
    }

    public void setProjectCode(String projectCode) {
        this.projectCode = projectCode;
    }

    public Instant getRequestedOn() {
        return requestedOn;
    }

    public void setRequestedOn(Instant requestedOn) {
        this.requestedOn = requestedOn;
    }

    public String getRequestedBy() {
        return requestedBy;
    }

    public void setRequestedBy(String requestedBy) {
        this.requestedBy = requestedBy;
    }

    public Long getAccountHolderId() {
        return accountHolderId;
    }

    public void setAccountHolderId(Long employeeId) {
        this.accountHolderId = employeeId;
    }

    public String getAccountHolderName() {
        return accountHolderName;
    }

    public void setAccountHolderName(String employeeName) {
        this.accountHolderName = employeeName;
    }

    public Long getBankBranchId() {
        return bankBranchId;
    }

    public void setBankBranchId(Long bankBranchId) {
        this.bankBranchId = bankBranchId;
    }

    public String getBankBranchLocation() {
        return bankBranchLocation;
    }

    public void setBankBranchLocation(String bankBranchLocation) {
        this.bankBranchLocation = bankBranchLocation;
    }

    public Long getMobileNumberMappedId() {
        return mobileNumberMappedId;
    }

    public void setMobileNumberMappedId(Long mobileNumberId) {
        this.mobileNumberMappedId = mobileNumberId;
    }

    public String getMobileNumberMappedNumber() {
        return mobileNumberMappedNumber;
    }

    public void setMobileNumberMappedNumber(String mobileNumberNumber) {
        this.mobileNumberMappedNumber = mobileNumberNumber;
    }

    public Long getParentAccountId() {
        return parentAccountId;
    }

    public void setParentAccountId(Long bankAccountId) {
        this.parentAccountId = bankAccountId;
    }

    public Long getBankAccountGroupId() {
        return bankAccountGroupId;
    }

    public void setBankAccountGroupId(Long bankAccountGroupId) {
        this.bankAccountGroupId = bankAccountGroupId;
    }

    public String getBankAccountGroupName() {
        return bankAccountGroupName;
    }

    public void setBankAccountGroupName(String bankAccountGroupName) {
        this.bankAccountGroupName = bankAccountGroupName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BankAccountDTO bankAccountDTO = (BankAccountDTO) o;
        if (bankAccountDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), bankAccountDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BankAccountDTO{" +
            "id=" + getId() +
            ", dateOfOpening='" + getDateOfOpening() + "'" +
            ", openedBy='" + getOpenedBy() + "'" +
            ", customerId='" + getCustomerId() + "'" +
            ", accountNumber='" + getAccountNumber() + "'" +
            ", loginId='" + getLoginId() + "'" +
            ", loginPassword='" + getLoginPassword() + "'" +
            ", email='" + getEmail() + "'" +
            ", phone='" + getPhone() + "'" +
            ", bankAccountType='" + getBankAccountType() + "'" +
            ", chequeBookIssued='" + isChequeBookIssued() + "'" +
            ", accountBalance=" + getAccountBalance() +
            ", balanceCheckedOn='" + getBalanceCheckedOn() + "'" +
            ", minimumBalance=" + getMinimumBalance() +
            ", passwordGetsExpired='" + isPasswordGetsExpired() + "'" +
            ", expirationPeriodDays=" + getExpirationPeriodDays() +
            ", passwordUpdatedOn='" + getPasswordUpdatedOn() + "'" +
            ", remarks='" + getRemarks() + "'" +
            ", accountStatus='" + getAccountStatus() + "'" +
            ", multipleSessionAllowed='" + isMultipleSessionAllowed() + "'" +
            ", projectName='" + getProjectName() + "'" +
            ", projectCode='" + getProjectCode() + "'" +
            ", requestedOn='" + getRequestedOn() + "'" +
            ", requestedBy='" + getRequestedBy() + "'" +
            ", accountHolder=" + getAccountHolderId() +
            ", accountHolder='" + getAccountHolderName() + "'" +
            ", bankBranch=" + getBankBranchId() +
            ", bankBranch='" + getBankBranchLocation() + "'" +
            ", mobileNumberMapped=" + getMobileNumberMappedId() +
            ", mobileNumberMapped='" + getMobileNumberMappedNumber() + "'" +
            ", parentAccount=" + getParentAccountId() +
            ", bankAccountGroup=" + getBankAccountGroupId() +
            ", bankAccountGroup='" + getBankAccountGroupName() + "'" +
            "}";
    }
}
