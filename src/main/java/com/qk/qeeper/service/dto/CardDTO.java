package com.qk.qeeper.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import com.qk.qeeper.domain.enumeration.CardType;
import com.qk.qeeper.domain.enumeration.PaymentNetwork;

/**
 * A DTO for the {@link com.qk.qeeper.domain.Card} entity.
 */
public class CardDTO implements Serializable {

    private Long id;

    @NotNull
    private CardType cardType;

    @NotNull
    private PaymentNetwork paymentNetwork;

    @NotNull
    @Size(max = 30)
    @Pattern(regexp = "^(?:4[0-9]{12}(?:[0-9]{3})?|[25][1-7][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\\d{3})\\d{11})$")
    private String cardNumber;

    @NotNull
    @Size(max = 30)
    @Pattern(regexp = "^(0[1-9]|1[0-2])\\/([0-9]{4}|[0-9]{2})$")
    private String validFrom;

    @NotNull
    @Size(max = 30)
    @Pattern(regexp = "^(0[1-9]|1[0-2])\\/([0-9]{4}|[0-9]{2})$")
    private String validTill;

    @Size(min = 1, max = 10)
    @Pattern(regexp = "^\\d{1,8}$")
    private String paymentNetworkPin;

    @NotNull
    @Size(min = 3, max = 5)
    @Pattern(regexp = "^\\d{3,5}$")
    private String cvv;

    @NotNull
    @Size(max = 100)
    private String nameOnCard;

    @Size(max = 1000)
    private String remarks;

    @Size(min = 3, max = 6)
    @Pattern(regexp = "^\\d{3,6}$")
    private String pin;

    @Size(min = 3, max = 6)
    @Pattern(regexp = "^\\d{3,6}$")
    private String tpin;


    private Long mobileNumberMappedId;

    private String mobileNumberMappedNumber;

    private Long registeredToId;

    private String registeredToName;

    private Long bankId;

    private String bankName;

    private Long bankAccountId;

    private Long cardGroupId;

    private String cardGroupName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CardType getCardType() {
        return cardType;
    }

    public void setCardType(CardType cardType) {
        this.cardType = cardType;
    }

    public PaymentNetwork getPaymentNetwork() {
        return paymentNetwork;
    }

    public void setPaymentNetwork(PaymentNetwork paymentNetwork) {
        this.paymentNetwork = paymentNetwork;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }

    public String getValidTill() {
        return validTill;
    }

    public void setValidTill(String validTill) {
        this.validTill = validTill;
    }

    public String getPaymentNetworkPin() {
        return paymentNetworkPin;
    }

    public void setPaymentNetworkPin(String paymentNetworkPin) {
        this.paymentNetworkPin = paymentNetworkPin;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public String getNameOnCard() {
        return nameOnCard;
    }

    public void setNameOnCard(String nameOnCard) {
        this.nameOnCard = nameOnCard;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getTpin() {
        return tpin;
    }

    public void setTpin(String tpin) {
        this.tpin = tpin;
    }

    public Long getMobileNumberMappedId() {
        return mobileNumberMappedId;
    }

    public void setMobileNumberMappedId(Long mobileNumberId) {
        this.mobileNumberMappedId = mobileNumberId;
    }

    public String getMobileNumberMappedNumber() {
        return mobileNumberMappedNumber;
    }

    public void setMobileNumberMappedNumber(String mobileNumberNumber) {
        this.mobileNumberMappedNumber = mobileNumberNumber;
    }

    public Long getRegisteredToId() {
        return registeredToId;
    }

    public void setRegisteredToId(Long employeeId) {
        this.registeredToId = employeeId;
    }

    public String getRegisteredToName() {
        return registeredToName;
    }

    public void setRegisteredToName(String employeeName) {
        this.registeredToName = employeeName;
    }

    public Long getBankId() {
        return bankId;
    }

    public void setBankId(Long bankId) {
        this.bankId = bankId;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public Long getBankAccountId() {
        return bankAccountId;
    }

    public void setBankAccountId(Long bankAccountId) {
        this.bankAccountId = bankAccountId;
    }

    public Long getCardGroupId() {
        return cardGroupId;
    }

    public void setCardGroupId(Long cardGroupId) {
        this.cardGroupId = cardGroupId;
    }

    public String getCardGroupName() {
        return cardGroupName;
    }

    public void setCardGroupName(String cardGroupName) {
        this.cardGroupName = cardGroupName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CardDTO cardDTO = (CardDTO) o;
        if (cardDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), cardDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CardDTO{" +
            "id=" + getId() +
            ", cardType='" + getCardType() + "'" +
            ", paymentNetwork='" + getPaymentNetwork() + "'" +
            ", cardNumber='" + getCardNumber() + "'" +
            ", validFrom='" + getValidFrom() + "'" +
            ", validTill='" + getValidTill() + "'" +
            ", paymentNetworkPin='" + getPaymentNetworkPin() + "'" +
            ", cvv='" + getCvv() + "'" +
            ", nameOnCard='" + getNameOnCard() + "'" +
            ", remarks='" + getRemarks() + "'" +
            ", pin='" + getPin() + "'" +
            ", tpin='" + getTpin() + "'" +
            ", mobileNumberMapped=" + getMobileNumberMappedId() +
            ", mobileNumberMapped='" + getMobileNumberMappedNumber() + "'" +
            ", registeredTo=" + getRegisteredToId() +
            ", registeredTo='" + getRegisteredToName() + "'" +
            ", bank=" + getBankId() +
            ", bank='" + getBankName() + "'" +
            ", bankAccount=" + getBankAccountId() +
            ", cardGroup=" + getCardGroupId() +
            ", cardGroup='" + getCardGroupName() + "'" +
            "}";
    }
}
