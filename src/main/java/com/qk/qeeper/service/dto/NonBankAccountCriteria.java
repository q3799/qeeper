package com.qk.qeeper.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import com.qk.qeeper.domain.enumeration.AccountStatus;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link com.qk.qeeper.domain.NonBankAccount} entity. This class is used
 * in {@link com.qk.qeeper.web.rest.NonBankAccountResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /non-bank-accounts?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class NonBankAccountCriteria implements Serializable, Criteria {
    /**
     * Class for filtering AccountStatus
     */
    public static class AccountStatusFilter extends Filter<AccountStatus> {

        public AccountStatusFilter() {
        }

        public AccountStatusFilter(AccountStatusFilter filter) {
            super(filter);
        }

        @Override
        public AccountStatusFilter copy() {
            return new AccountStatusFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter loginId;

    private StringFilter loginPassword;

    private StringFilter name;

    private StringFilter email;

    private StringFilter phone;

    private BooleanFilter passwordGetsExpired;

    private InstantFilter passwordUpdatedOn;

    private IntegerFilter expirationPeriodDays;

    private AccountStatusFilter accountStatus;

    private StringFilter remarks;

    private BooleanFilter multipleSessionAllowed;

    private StringFilter accountId;

    private LongFilter nonBankClientId;

    private LongFilter nonBankAccountGroupId;

    public NonBankAccountCriteria(){
    }

    public NonBankAccountCriteria(NonBankAccountCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.loginId = other.loginId == null ? null : other.loginId.copy();
        this.loginPassword = other.loginPassword == null ? null : other.loginPassword.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.email = other.email == null ? null : other.email.copy();
        this.phone = other.phone == null ? null : other.phone.copy();
        this.passwordGetsExpired = other.passwordGetsExpired == null ? null : other.passwordGetsExpired.copy();
        this.passwordUpdatedOn = other.passwordUpdatedOn == null ? null : other.passwordUpdatedOn.copy();
        this.expirationPeriodDays = other.expirationPeriodDays == null ? null : other.expirationPeriodDays.copy();
        this.accountStatus = other.accountStatus == null ? null : other.accountStatus.copy();
        this.remarks = other.remarks == null ? null : other.remarks.copy();
        this.multipleSessionAllowed = other.multipleSessionAllowed == null ? null : other.multipleSessionAllowed.copy();
        this.accountId = other.accountId == null ? null : other.accountId.copy();
        this.nonBankClientId = other.nonBankClientId == null ? null : other.nonBankClientId.copy();
        this.nonBankAccountGroupId = other.nonBankAccountGroupId == null ? null : other.nonBankAccountGroupId.copy();
    }

    @Override
    public NonBankAccountCriteria copy() {
        return new NonBankAccountCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getLoginId() {
        return loginId;
    }

    public void setLoginId(StringFilter loginId) {
        this.loginId = loginId;
    }

    public StringFilter getLoginPassword() {
        return loginPassword;
    }

    public void setLoginPassword(StringFilter loginPassword) {
        this.loginPassword = loginPassword;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getEmail() {
        return email;
    }

    public void setEmail(StringFilter email) {
        this.email = email;
    }

    public StringFilter getPhone() {
        return phone;
    }

    public void setPhone(StringFilter phone) {
        this.phone = phone;
    }

    public BooleanFilter getPasswordGetsExpired() {
        return passwordGetsExpired;
    }

    public void setPasswordGetsExpired(BooleanFilter passwordGetsExpired) {
        this.passwordGetsExpired = passwordGetsExpired;
    }

    public InstantFilter getPasswordUpdatedOn() {
        return passwordUpdatedOn;
    }

    public void setPasswordUpdatedOn(InstantFilter passwordUpdatedOn) {
        this.passwordUpdatedOn = passwordUpdatedOn;
    }

    public IntegerFilter getExpirationPeriodDays() {
        return expirationPeriodDays;
    }

    public void setExpirationPeriodDays(IntegerFilter expirationPeriodDays) {
        this.expirationPeriodDays = expirationPeriodDays;
    }

    public AccountStatusFilter getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(AccountStatusFilter accountStatus) {
        this.accountStatus = accountStatus;
    }

    public StringFilter getRemarks() {
        return remarks;
    }

    public void setRemarks(StringFilter remarks) {
        this.remarks = remarks;
    }

    public BooleanFilter getMultipleSessionAllowed() {
        return multipleSessionAllowed;
    }

    public void setMultipleSessionAllowed(BooleanFilter multipleSessionAllowed) {
        this.multipleSessionAllowed = multipleSessionAllowed;
    }

    public StringFilter getAccountId() {
        return accountId;
    }

    public void setAccountId(StringFilter accountId) {
        this.accountId = accountId;
    }

    public LongFilter getNonBankClientId() {
        return nonBankClientId;
    }

    public void setNonBankClientId(LongFilter nonBankClientId) {
        this.nonBankClientId = nonBankClientId;
    }

    public LongFilter getNonBankAccountGroupId() {
        return nonBankAccountGroupId;
    }

    public void setNonBankAccountGroupId(LongFilter nonBankAccountGroupId) {
        this.nonBankAccountGroupId = nonBankAccountGroupId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final NonBankAccountCriteria that = (NonBankAccountCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(loginId, that.loginId) &&
            Objects.equals(loginPassword, that.loginPassword) &&
            Objects.equals(name, that.name) &&
            Objects.equals(email, that.email) &&
            Objects.equals(phone, that.phone) &&
            Objects.equals(passwordGetsExpired, that.passwordGetsExpired) &&
            Objects.equals(passwordUpdatedOn, that.passwordUpdatedOn) &&
            Objects.equals(expirationPeriodDays, that.expirationPeriodDays) &&
            Objects.equals(accountStatus, that.accountStatus) &&
            Objects.equals(remarks, that.remarks) &&
            Objects.equals(multipleSessionAllowed, that.multipleSessionAllowed) &&
            Objects.equals(accountId, that.accountId) &&
            Objects.equals(nonBankClientId, that.nonBankClientId) &&
            Objects.equals(nonBankAccountGroupId, that.nonBankAccountGroupId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        loginId,
        loginPassword,
        name,
        email,
        phone,
        passwordGetsExpired,
        passwordUpdatedOn,
        expirationPeriodDays,
        accountStatus,
        remarks,
        multipleSessionAllowed,
        accountId,
        nonBankClientId,
        nonBankAccountGroupId
        );
    }

    @Override
    public String toString() {
        return "NonBankAccountCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (loginId != null ? "loginId=" + loginId + ", " : "") +
                (loginPassword != null ? "loginPassword=" + loginPassword + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (email != null ? "email=" + email + ", " : "") +
                (phone != null ? "phone=" + phone + ", " : "") +
                (passwordGetsExpired != null ? "passwordGetsExpired=" + passwordGetsExpired + ", " : "") +
                (passwordUpdatedOn != null ? "passwordUpdatedOn=" + passwordUpdatedOn + ", " : "") +
                (expirationPeriodDays != null ? "expirationPeriodDays=" + expirationPeriodDays + ", " : "") +
                (accountStatus != null ? "accountStatus=" + accountStatus + ", " : "") +
                (remarks != null ? "remarks=" + remarks + ", " : "") +
                (multipleSessionAllowed != null ? "multipleSessionAllowed=" + multipleSessionAllowed + ", " : "") +
                (accountId != null ? "accountId=" + accountId + ", " : "") +
                (nonBankClientId != null ? "nonBankClientId=" + nonBankClientId + ", " : "") +
                (nonBankAccountGroupId != null ? "nonBankAccountGroupId=" + nonBankAccountGroupId + ", " : "") +
            "}";
    }

}
