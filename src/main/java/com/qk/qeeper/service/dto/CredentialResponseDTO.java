package com.qk.qeeper.service.dto;

import java.io.Serializable;
import java.util.Objects;

public class CredentialResponseDTO implements Serializable {

    private String userName;

    private String password;

    public String getUserName() {
        return userName;
    }

    public CredentialResponseDTO userName(String userName) {
        this.userName = userName;
        return this;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public CredentialResponseDTO password(String password) {
        this.password = password;
        return this;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CredentialResponseDTO)) return false;
        CredentialResponseDTO that = (CredentialResponseDTO) o;
        return Objects.equals(getUserName(), that.getUserName()) &&
            Objects.equals(getPassword(), that.getPassword());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUserName(), getPassword());
    }

    @Override
    public String toString() {
        return "CredentialResponseDTO{" +
            "userName='" + userName + '\'' +
            ", password='" + password + '\'' +
            '}';
    }
}
