package com.qk.qeeper.service.dto;
import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import com.qk.qeeper.domain.enumeration.AccountStatus;

/**
 * A DTO for the {@link com.qk.qeeper.domain.NonBankAccount} entity.
 */
public class NonBankAccountDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 50)
    private String loginId;

    @NotNull
    @Size(max = 50)
    private String loginPassword;

    @Size(max = 100)
    private String name;

    @NotNull
    @Size(max = 200)
    @Pattern(regexp = "^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$")
    private String email;

    @Pattern(regexp = "^\\+?[1-9]\\d{1,14}$")
    private String phone;

    @NotNull
    private Boolean passwordGetsExpired;

    private Instant passwordUpdatedOn;

    @Min(value = 0)
    private Integer expirationPeriodDays;

    @NotNull
    private AccountStatus accountStatus;

    @Size(max = 1000)
    private String remarks;

    @NotNull
    private Boolean multipleSessionAllowed;

    @Size(max = 100)
    private String accountId;


    private Long nonBankClientId;

    private String nonBankClientName;

    private Long nonBankAccountGroupId;

    private String nonBankAccountGroupName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getLoginPassword() {
        return loginPassword;
    }

    public void setLoginPassword(String loginPassword) {
        this.loginPassword = loginPassword;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Boolean isPasswordGetsExpired() {
        return passwordGetsExpired;
    }

    public void setPasswordGetsExpired(Boolean passwordGetsExpired) {
        this.passwordGetsExpired = passwordGetsExpired;
    }

    public Instant getPasswordUpdatedOn() {
        return passwordUpdatedOn;
    }

    public void setPasswordUpdatedOn(Instant passwordUpdatedOn) {
        this.passwordUpdatedOn = passwordUpdatedOn;
    }

    public Integer getExpirationPeriodDays() {
        return expirationPeriodDays;
    }

    public void setExpirationPeriodDays(Integer expirationPeriodDays) {
        this.expirationPeriodDays = expirationPeriodDays;
    }

    public AccountStatus getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(AccountStatus accountStatus) {
        this.accountStatus = accountStatus;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Boolean isMultipleSessionAllowed() {
        return multipleSessionAllowed;
    }

    public void setMultipleSessionAllowed(Boolean multipleSessionAllowed) {
        this.multipleSessionAllowed = multipleSessionAllowed;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public Long getNonBankClientId() {
        return nonBankClientId;
    }

    public void setNonBankClientId(Long nonBankClientId) {
        this.nonBankClientId = nonBankClientId;
    }

    public String getNonBankClientName() {
        return nonBankClientName;
    }

    public void setNonBankClientName(String nonBankClientName) {
        this.nonBankClientName = nonBankClientName;
    }

    public Long getNonBankAccountGroupId() {
        return nonBankAccountGroupId;
    }

    public void setNonBankAccountGroupId(Long nonBankAccountGroupId) {
        this.nonBankAccountGroupId = nonBankAccountGroupId;
    }

    public String getNonBankAccountGroupName() {
        return nonBankAccountGroupName;
    }

    public void setNonBankAccountGroupName(String nonBankAccountGroupName) {
        this.nonBankAccountGroupName = nonBankAccountGroupName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        NonBankAccountDTO nonBankAccountDTO = (NonBankAccountDTO) o;
        if (nonBankAccountDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), nonBankAccountDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "NonBankAccountDTO{" +
            "id=" + getId() +
            ", loginId='" + getLoginId() + "'" +
            ", loginPassword='" + getLoginPassword() + "'" +
            ", name='" + getName() + "'" +
            ", email='" + getEmail() + "'" +
            ", phone='" + getPhone() + "'" +
            ", passwordGetsExpired='" + isPasswordGetsExpired() + "'" +
            ", passwordUpdatedOn='" + getPasswordUpdatedOn() + "'" +
            ", expirationPeriodDays=" + getExpirationPeriodDays() +
            ", accountStatus='" + getAccountStatus() + "'" +
            ", remarks='" + getRemarks() + "'" +
            ", multipleSessionAllowed='" + isMultipleSessionAllowed() + "'" +
            ", accountId='" + getAccountId() + "'" +
            ", nonBankClient=" + getNonBankClientId() +
            ", nonBankClient='" + getNonBankClientName() + "'" +
            ", nonBankAccountGroup=" + getNonBankAccountGroupId() +
            ", nonBankAccountGroup='" + getNonBankAccountGroupName() + "'" +
            "}";
    }
}
