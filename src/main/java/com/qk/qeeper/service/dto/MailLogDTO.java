package com.qk.qeeper.service.dto;
import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import com.qk.qeeper.domain.enumeration.MailServer;

/**
 * A DTO for the {@link com.qk.qeeper.domain.MailLog} entity.
 */
public class MailLogDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 400)
    private String subject;

    @Size(max = 4000)
    private String content;

    private Instant mailDate;

    private MailServer mailServer;

    @Size(max = 1000)
    private String responseReceived;

    @Size(max = 2000)
    private String remarks;

    @Size(max = 4000)
    private String toList;

    @Size(max = 4000)
    private String ccList;

    @Min(value = 0L)
    private Long processingTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Instant getMailDate() {
        return mailDate;
    }

    public void setMailDate(Instant mailDate) {
        this.mailDate = mailDate;
    }

    public MailServer getMailServer() {
        return mailServer;
    }

    public void setMailServer(MailServer mailServer) {
        this.mailServer = mailServer;
    }

    public String getResponseReceived() {
        return responseReceived;
    }

    public void setResponseReceived(String responseReceived) {
        this.responseReceived = responseReceived;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getToList() {
        return toList;
    }

    public void setToList(String toList) {
        this.toList = toList;
    }

    public String getCcList() {
        return ccList;
    }

    public void setCcList(String ccList) {
        this.ccList = ccList;
    }

    public Long getProcessingTime() {
        return processingTime;
    }

    public void setProcessingTime(Long processingTime) {
        this.processingTime = processingTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MailLogDTO mailLogDTO = (MailLogDTO) o;
        if (mailLogDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), mailLogDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "MailLogDTO{" +
            "id=" + getId() +
            ", subject='" + getSubject() + "'" +
            ", content='" + getContent() + "'" +
            ", mailDate='" + getMailDate() + "'" +
            ", mailServer='" + getMailServer() + "'" +
            ", responseReceived='" + getResponseReceived() + "'" +
            ", remarks='" + getRemarks() + "'" +
            ", toList='" + getToList() + "'" +
            ", ccList='" + getCcList() + "'" +
            ", processingTime=" + getProcessingTime() +
            "}";
    }
}
