package com.qk.qeeper.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import com.qk.qeeper.domain.enumeration.BankAccountType;
import com.qk.qeeper.domain.enumeration.AccountStatus;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link com.qk.qeeper.domain.BankAccount} entity. This class is used
 * in {@link com.qk.qeeper.web.rest.BankAccountResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /bank-accounts?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class BankAccountCriteria implements Serializable, Criteria {
    /**
     * Class for filtering BankAccountType
     */
    public static class BankAccountTypeFilter extends Filter<BankAccountType> {

        public BankAccountTypeFilter() {
        }

        public BankAccountTypeFilter(BankAccountTypeFilter filter) {
            super(filter);
        }

        @Override
        public BankAccountTypeFilter copy() {
            return new BankAccountTypeFilter(this);
        }

    }
    /**
     * Class for filtering AccountStatus
     */
    public static class AccountStatusFilter extends Filter<AccountStatus> {

        public AccountStatusFilter() {
        }

        public AccountStatusFilter(AccountStatusFilter filter) {
            super(filter);
        }

        @Override
        public AccountStatusFilter copy() {
            return new AccountStatusFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private InstantFilter dateOfOpening;

    private StringFilter openedBy;

    private StringFilter customerId;

    private StringFilter accountNumber;

    private StringFilter loginId;

    private StringFilter loginPassword;

    private StringFilter email;

    private StringFilter phone;

    private BankAccountTypeFilter bankAccountType;

    private BooleanFilter chequeBookIssued;

    private DoubleFilter accountBalance;

    private InstantFilter balanceCheckedOn;

    private DoubleFilter minimumBalance;

    private BooleanFilter passwordGetsExpired;

    private IntegerFilter expirationPeriodDays;

    private InstantFilter passwordUpdatedOn;

    private StringFilter remarks;

    private AccountStatusFilter accountStatus;

    private BooleanFilter multipleSessionAllowed;

    private StringFilter projectName;

    private StringFilter projectCode;

    private InstantFilter requestedOn;

    private StringFilter requestedBy;

    private LongFilter accountHolderId;

    private LongFilter bankBranchId;

    private LongFilter mobileNumberMappedId;

    private LongFilter parentAccountId;

    private LongFilter cardsId;

    private LongFilter bankAccountGroupId;

    private LongFilter secretQuestionsId;

    public BankAccountCriteria(){
    }

    public BankAccountCriteria(BankAccountCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.dateOfOpening = other.dateOfOpening == null ? null : other.dateOfOpening.copy();
        this.openedBy = other.openedBy == null ? null : other.openedBy.copy();
        this.customerId = other.customerId == null ? null : other.customerId.copy();
        this.accountNumber = other.accountNumber == null ? null : other.accountNumber.copy();
        this.loginId = other.loginId == null ? null : other.loginId.copy();
        this.loginPassword = other.loginPassword == null ? null : other.loginPassword.copy();
        this.email = other.email == null ? null : other.email.copy();
        this.phone = other.phone == null ? null : other.phone.copy();
        this.bankAccountType = other.bankAccountType == null ? null : other.bankAccountType.copy();
        this.chequeBookIssued = other.chequeBookIssued == null ? null : other.chequeBookIssued.copy();
        this.accountBalance = other.accountBalance == null ? null : other.accountBalance.copy();
        this.balanceCheckedOn = other.balanceCheckedOn == null ? null : other.balanceCheckedOn.copy();
        this.minimumBalance = other.minimumBalance == null ? null : other.minimumBalance.copy();
        this.passwordGetsExpired = other.passwordGetsExpired == null ? null : other.passwordGetsExpired.copy();
        this.expirationPeriodDays = other.expirationPeriodDays == null ? null : other.expirationPeriodDays.copy();
        this.passwordUpdatedOn = other.passwordUpdatedOn == null ? null : other.passwordUpdatedOn.copy();
        this.remarks = other.remarks == null ? null : other.remarks.copy();
        this.accountStatus = other.accountStatus == null ? null : other.accountStatus.copy();
        this.multipleSessionAllowed = other.multipleSessionAllowed == null ? null : other.multipleSessionAllowed.copy();
        this.projectName = other.projectName == null ? null : other.projectName.copy();
        this.projectCode = other.projectCode == null ? null : other.projectCode.copy();
        this.requestedOn = other.requestedOn == null ? null : other.requestedOn.copy();
        this.requestedBy = other.requestedBy == null ? null : other.requestedBy.copy();
        this.accountHolderId = other.accountHolderId == null ? null : other.accountHolderId.copy();
        this.bankBranchId = other.bankBranchId == null ? null : other.bankBranchId.copy();
        this.mobileNumberMappedId = other.mobileNumberMappedId == null ? null : other.mobileNumberMappedId.copy();
        this.parentAccountId = other.parentAccountId == null ? null : other.parentAccountId.copy();
        this.cardsId = other.cardsId == null ? null : other.cardsId.copy();
        this.bankAccountGroupId = other.bankAccountGroupId == null ? null : other.bankAccountGroupId.copy();
        this.secretQuestionsId = other.secretQuestionsId == null ? null : other.secretQuestionsId.copy();
    }

    @Override
    public BankAccountCriteria copy() {
        return new BankAccountCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public InstantFilter getDateOfOpening() {
        return dateOfOpening;
    }

    public void setDateOfOpening(InstantFilter dateOfOpening) {
        this.dateOfOpening = dateOfOpening;
    }

    public StringFilter getOpenedBy() {
        return openedBy;
    }

    public void setOpenedBy(StringFilter openedBy) {
        this.openedBy = openedBy;
    }

    public StringFilter getCustomerId() {
        return customerId;
    }

    public void setCustomerId(StringFilter customerId) {
        this.customerId = customerId;
    }

    public StringFilter getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(StringFilter accountNumber) {
        this.accountNumber = accountNumber;
    }

    public StringFilter getLoginId() {
        return loginId;
    }

    public void setLoginId(StringFilter loginId) {
        this.loginId = loginId;
    }

    public StringFilter getLoginPassword() {
        return loginPassword;
    }

    public void setLoginPassword(StringFilter loginPassword) {
        this.loginPassword = loginPassword;
    }

    public StringFilter getEmail() {
        return email;
    }

    public void setEmail(StringFilter email) {
        this.email = email;
    }

    public StringFilter getPhone() {
        return phone;
    }

    public void setPhone(StringFilter phone) {
        this.phone = phone;
    }

    public BankAccountTypeFilter getBankAccountType() {
        return bankAccountType;
    }

    public void setBankAccountType(BankAccountTypeFilter bankAccountType) {
        this.bankAccountType = bankAccountType;
    }

    public BooleanFilter getChequeBookIssued() {
        return chequeBookIssued;
    }

    public void setChequeBookIssued(BooleanFilter chequeBookIssued) {
        this.chequeBookIssued = chequeBookIssued;
    }

    public DoubleFilter getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(DoubleFilter accountBalance) {
        this.accountBalance = accountBalance;
    }

    public InstantFilter getBalanceCheckedOn() {
        return balanceCheckedOn;
    }

    public void setBalanceCheckedOn(InstantFilter balanceCheckedOn) {
        this.balanceCheckedOn = balanceCheckedOn;
    }

    public DoubleFilter getMinimumBalance() {
        return minimumBalance;
    }

    public void setMinimumBalance(DoubleFilter minimumBalance) {
        this.minimumBalance = minimumBalance;
    }

    public BooleanFilter getPasswordGetsExpired() {
        return passwordGetsExpired;
    }

    public void setPasswordGetsExpired(BooleanFilter passwordGetsExpired) {
        this.passwordGetsExpired = passwordGetsExpired;
    }

    public IntegerFilter getExpirationPeriodDays() {
        return expirationPeriodDays;
    }

    public void setExpirationPeriodDays(IntegerFilter expirationPeriodDays) {
        this.expirationPeriodDays = expirationPeriodDays;
    }

    public InstantFilter getPasswordUpdatedOn() {
        return passwordUpdatedOn;
    }

    public void setPasswordUpdatedOn(InstantFilter passwordUpdatedOn) {
        this.passwordUpdatedOn = passwordUpdatedOn;
    }

    public StringFilter getRemarks() {
        return remarks;
    }

    public void setRemarks(StringFilter remarks) {
        this.remarks = remarks;
    }

    public AccountStatusFilter getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(AccountStatusFilter accountStatus) {
        this.accountStatus = accountStatus;
    }

    public BooleanFilter getMultipleSessionAllowed() {
        return multipleSessionAllowed;
    }

    public void setMultipleSessionAllowed(BooleanFilter multipleSessionAllowed) {
        this.multipleSessionAllowed = multipleSessionAllowed;
    }

    public StringFilter getProjectName() {
        return projectName;
    }

    public void setProjectName(StringFilter projectName) {
        this.projectName = projectName;
    }

    public StringFilter getProjectCode() {
        return projectCode;
    }

    public void setProjectCode(StringFilter projectCode) {
        this.projectCode = projectCode;
    }

    public InstantFilter getRequestedOn() {
        return requestedOn;
    }

    public void setRequestedOn(InstantFilter requestedOn) {
        this.requestedOn = requestedOn;
    }

    public StringFilter getRequestedBy() {
        return requestedBy;
    }

    public void setRequestedBy(StringFilter requestedBy) {
        this.requestedBy = requestedBy;
    }

    public LongFilter getAccountHolderId() {
        return accountHolderId;
    }

    public void setAccountHolderId(LongFilter accountHolderId) {
        this.accountHolderId = accountHolderId;
    }

    public LongFilter getBankBranchId() {
        return bankBranchId;
    }

    public void setBankBranchId(LongFilter bankBranchId) {
        this.bankBranchId = bankBranchId;
    }

    public LongFilter getMobileNumberMappedId() {
        return mobileNumberMappedId;
    }

    public void setMobileNumberMappedId(LongFilter mobileNumberMappedId) {
        this.mobileNumberMappedId = mobileNumberMappedId;
    }

    public LongFilter getParentAccountId() {
        return parentAccountId;
    }

    public void setParentAccountId(LongFilter parentAccountId) {
        this.parentAccountId = parentAccountId;
    }

    public LongFilter getCardsId() {
        return cardsId;
    }

    public void setCardsId(LongFilter cardsId) {
        this.cardsId = cardsId;
    }

    public LongFilter getBankAccountGroupId() {
        return bankAccountGroupId;
    }

    public void setBankAccountGroupId(LongFilter bankAccountGroupId) {
        this.bankAccountGroupId = bankAccountGroupId;
    }

    public LongFilter getSecretQuestionsId() {
        return secretQuestionsId;
    }

    public void setSecretQuestionsId(LongFilter secretQuestionsId) {
        this.secretQuestionsId = secretQuestionsId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final BankAccountCriteria that = (BankAccountCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(dateOfOpening, that.dateOfOpening) &&
            Objects.equals(openedBy, that.openedBy) &&
            Objects.equals(customerId, that.customerId) &&
            Objects.equals(accountNumber, that.accountNumber) &&
            Objects.equals(loginId, that.loginId) &&
            Objects.equals(loginPassword, that.loginPassword) &&
            Objects.equals(email, that.email) &&
            Objects.equals(phone, that.phone) &&
            Objects.equals(bankAccountType, that.bankAccountType) &&
            Objects.equals(chequeBookIssued, that.chequeBookIssued) &&
            Objects.equals(accountBalance, that.accountBalance) &&
            Objects.equals(balanceCheckedOn, that.balanceCheckedOn) &&
            Objects.equals(minimumBalance, that.minimumBalance) &&
            Objects.equals(passwordGetsExpired, that.passwordGetsExpired) &&
            Objects.equals(expirationPeriodDays, that.expirationPeriodDays) &&
            Objects.equals(passwordUpdatedOn, that.passwordUpdatedOn) &&
            Objects.equals(remarks, that.remarks) &&
            Objects.equals(accountStatus, that.accountStatus) &&
            Objects.equals(multipleSessionAllowed, that.multipleSessionAllowed) &&
            Objects.equals(projectName, that.projectName) &&
            Objects.equals(projectCode, that.projectCode) &&
            Objects.equals(requestedOn, that.requestedOn) &&
            Objects.equals(requestedBy, that.requestedBy) &&
            Objects.equals(accountHolderId, that.accountHolderId) &&
            Objects.equals(bankBranchId, that.bankBranchId) &&
            Objects.equals(mobileNumberMappedId, that.mobileNumberMappedId) &&
            Objects.equals(parentAccountId, that.parentAccountId) &&
            Objects.equals(cardsId, that.cardsId) &&
            Objects.equals(bankAccountGroupId, that.bankAccountGroupId) &&
            Objects.equals(secretQuestionsId, that.secretQuestionsId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        dateOfOpening,
        openedBy,
        customerId,
        accountNumber,
        loginId,
        loginPassword,
        email,
        phone,
        bankAccountType,
        chequeBookIssued,
        accountBalance,
        balanceCheckedOn,
        minimumBalance,
        passwordGetsExpired,
        expirationPeriodDays,
        passwordUpdatedOn,
        remarks,
        accountStatus,
        multipleSessionAllowed,
        projectName,
        projectCode,
        requestedOn,
        requestedBy,
        accountHolderId,
        bankBranchId,
        mobileNumberMappedId,
        parentAccountId,
        cardsId,
        bankAccountGroupId,
        secretQuestionsId
        );
    }

    @Override
    public String toString() {
        return "BankAccountCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (dateOfOpening != null ? "dateOfOpening=" + dateOfOpening + ", " : "") +
                (openedBy != null ? "openedBy=" + openedBy + ", " : "") +
                (customerId != null ? "customerId=" + customerId + ", " : "") +
                (accountNumber != null ? "accountNumber=" + accountNumber + ", " : "") +
                (loginId != null ? "loginId=" + loginId + ", " : "") +
                (loginPassword != null ? "loginPassword=" + loginPassword + ", " : "") +
                (email != null ? "email=" + email + ", " : "") +
                (phone != null ? "phone=" + phone + ", " : "") +
                (bankAccountType != null ? "bankAccountType=" + bankAccountType + ", " : "") +
                (chequeBookIssued != null ? "chequeBookIssued=" + chequeBookIssued + ", " : "") +
                (accountBalance != null ? "accountBalance=" + accountBalance + ", " : "") +
                (balanceCheckedOn != null ? "balanceCheckedOn=" + balanceCheckedOn + ", " : "") +
                (minimumBalance != null ? "minimumBalance=" + minimumBalance + ", " : "") +
                (passwordGetsExpired != null ? "passwordGetsExpired=" + passwordGetsExpired + ", " : "") +
                (expirationPeriodDays != null ? "expirationPeriodDays=" + expirationPeriodDays + ", " : "") +
                (passwordUpdatedOn != null ? "passwordUpdatedOn=" + passwordUpdatedOn + ", " : "") +
                (remarks != null ? "remarks=" + remarks + ", " : "") +
                (accountStatus != null ? "accountStatus=" + accountStatus + ", " : "") +
                (multipleSessionAllowed != null ? "multipleSessionAllowed=" + multipleSessionAllowed + ", " : "") +
                (projectName != null ? "projectName=" + projectName + ", " : "") +
                (projectCode != null ? "projectCode=" + projectCode + ", " : "") +
                (requestedOn != null ? "requestedOn=" + requestedOn + ", " : "") +
                (requestedBy != null ? "requestedBy=" + requestedBy + ", " : "") +
                (accountHolderId != null ? "accountHolderId=" + accountHolderId + ", " : "") +
                (bankBranchId != null ? "bankBranchId=" + bankBranchId + ", " : "") +
                (mobileNumberMappedId != null ? "mobileNumberMappedId=" + mobileNumberMappedId + ", " : "") +
                (parentAccountId != null ? "parentAccountId=" + parentAccountId + ", " : "") +
                (cardsId != null ? "cardsId=" + cardsId + ", " : "") +
                (bankAccountGroupId != null ? "bankAccountGroupId=" + bankAccountGroupId + ", " : "") +
                (secretQuestionsId != null ? "secretQuestionsId=" + secretQuestionsId + ", " : "") +
            "}";
    }

}
