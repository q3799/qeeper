package com.qk.qeeper.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.qk.qeeper.domain.CardGroup;
import com.qk.qeeper.domain.*; // for static metamodels
import com.qk.qeeper.repository.CardGroupRepository;
import com.qk.qeeper.service.dto.CardGroupCriteria;
import com.qk.qeeper.service.dto.CardGroupDTO;
import com.qk.qeeper.service.mapper.CardGroupMapper;

/**
 * Service for executing complex queries for {@link CardGroup} entities in the database.
 * The main input is a {@link CardGroupCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link CardGroupDTO} or a {@link Page} of {@link CardGroupDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CardGroupQueryService extends QueryService<CardGroup> {

    private final Logger log = LoggerFactory.getLogger(CardGroupQueryService.class);

    private final CardGroupRepository cardGroupRepository;

    private final CardGroupMapper cardGroupMapper;

    public CardGroupQueryService(CardGroupRepository cardGroupRepository, CardGroupMapper cardGroupMapper) {
        this.cardGroupRepository = cardGroupRepository;
        this.cardGroupMapper = cardGroupMapper;
    }

    /**
     * Return a {@link List} of {@link CardGroupDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<CardGroupDTO> findByCriteria(CardGroupCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<CardGroup> specification = createSpecification(criteria);
        return cardGroupMapper.toDto(cardGroupRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link CardGroupDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<CardGroupDTO> findByCriteria(CardGroupCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<CardGroup> specification = createSpecification(criteria);
        return cardGroupRepository.findAll(specification, page)
            .map(cardGroupMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(CardGroupCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<CardGroup> specification = createSpecification(criteria);
        return cardGroupRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    private Specification<CardGroup> createSpecification(CardGroupCriteria criteria) {
        Specification<CardGroup> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), CardGroup_.id));
            }
            if (criteria.getCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCode(), CardGroup_.code));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), CardGroup_.name));
            }
            if (criteria.getCardsId() != null) {
                specification = specification.and(buildSpecification(criteria.getCardsId(),
                    root -> root.join(CardGroup_.cards, JoinType.LEFT).get(Card_.id)));
            }
        }
        return specification;
    }
}
