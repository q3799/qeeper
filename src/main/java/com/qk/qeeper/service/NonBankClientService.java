package com.qk.qeeper.service;

import com.qk.qeeper.domain.NonBankClient;
import com.qk.qeeper.repository.NonBankClientRepository;
import com.qk.qeeper.service.dto.NonBankClientDTO;
import com.qk.qeeper.service.mapper.NonBankClientMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link NonBankClient}.
 */
@Service
@Transactional
public class NonBankClientService {

    private final Logger log = LoggerFactory.getLogger(NonBankClientService.class);

    private final NonBankClientRepository nonBankClientRepository;

    private final NonBankClientMapper nonBankClientMapper;

    public NonBankClientService(NonBankClientRepository nonBankClientRepository, NonBankClientMapper nonBankClientMapper) {
        this.nonBankClientRepository = nonBankClientRepository;
        this.nonBankClientMapper = nonBankClientMapper;
    }

    /**
     * Save a nonBankClient.
     *
     * @param nonBankClientDTO the entity to save.
     * @return the persisted entity.
     */
    public NonBankClientDTO save(NonBankClientDTO nonBankClientDTO) {
        log.debug("Request to save NonBankClient : {}", nonBankClientDTO);
        NonBankClient nonBankClient = nonBankClientMapper.toEntity(nonBankClientDTO);
        nonBankClient = nonBankClientRepository.save(nonBankClient);
        return nonBankClientMapper.toDto(nonBankClient);
    }

    /**
     * Get all the nonBankClients.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<NonBankClientDTO> findAll(Pageable pageable) {
        log.debug("Request to get all NonBankClients");
        return nonBankClientRepository.findAll(pageable)
            .map(nonBankClientMapper::toDto);
    }


    /**
     * Get one nonBankClient by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<NonBankClientDTO> findOne(Long id) {
        log.debug("Request to get NonBankClient : {}", id);
        return nonBankClientRepository.findById(id)
            .map(nonBankClientMapper::toDto);
    }

    /**
     * Delete the nonBankClient by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete NonBankClient : {}", id);
        nonBankClientRepository.deleteById(id);
    }
}
