package com.qk.qeeper.service;

import com.qk.qeeper.domain.BankAccount;
import com.qk.qeeper.domain.BankAccountGroup;
import com.qk.qeeper.domain.enumeration.AccountStatus;
import com.qk.qeeper.repository.BankAccountGroupRepository;
import com.qk.qeeper.repository.BankAccountRepository;
import com.qk.qeeper.service.dto.BankAccountDTO;
import com.qk.qeeper.service.dto.CredentialResponseDTO;
import com.qk.qeeper.service.mapper.BankAccountMapper;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link BankAccount}.
 */
@Service
@Transactional
public class BankAccountService implements CredentialService {

    private final Logger log = LoggerFactory.getLogger(BankAccountService.class);

    private final BankAccountRepository bankAccountRepository;

    private final BankAccountMapper bankAccountMapper;

    private final BankAccountGroupRepository bankAccountGroupRepository;

    public BankAccountService(BankAccountRepository bankAccountRepository, BankAccountMapper bankAccountMapper, BankAccountGroupRepository bankAccountGroupRepository) {
        this.bankAccountRepository = bankAccountRepository;
        this.bankAccountMapper = bankAccountMapper;
        this.bankAccountGroupRepository = bankAccountGroupRepository;
    }

    /**
     * Save a bankAccount.
     *
     * @param bankAccountDTO the entity to save.
     * @return the persisted entity.
     */
    public BankAccountDTO save(BankAccountDTO bankAccountDTO) {
        log.debug("Request to save BankAccount : {}", bankAccountDTO);
        BankAccount bankAccount = bankAccountMapper.toEntity(bankAccountDTO);
        bankAccount = bankAccountRepository.save(bankAccount);
        return bankAccountMapper.toDto(bankAccount);
    }

    /**
     * Get all the bankAccounts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<BankAccountDTO> findAll(Pageable pageable) {
        log.debug("Request to get all BankAccounts");
        return bankAccountRepository.findAll(pageable)
            .map(bankAccountMapper::toDto);
    }


    /**
     * Get one bankAccount by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<BankAccountDTO> findOne(Long id) {
        log.debug("Request to get BankAccount : {}", id);
        return bankAccountRepository.findById(id)
            .map(bankAccountMapper::toDto);
    }

    /**
     * Delete the bankAccount by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete BankAccount : {}", id);
        bankAccountRepository.deleteById(id);
    }

    @Override
    public CredentialResponseDTO getCredentials(String groupCode) {
        if (StringUtils.isBlank(groupCode)) return null;

        // first find the BankAccountGroup
        Optional<BankAccountGroup> bankAccountGroupOptional = bankAccountGroupRepository.findTopByCode(groupCode);
        CredentialResponseDTO credentialResponseDTO = null;

        if (bankAccountGroupOptional.isPresent()) {

            Optional<BankAccount> bankAccountOptional = bankAccountRepository.findTopByBankAccountGroupAndAccountStatus(bankAccountGroupOptional.get(), AccountStatus.ACTIVE);

            if (bankAccountOptional.isPresent()) {
                credentialResponseDTO = new CredentialResponseDTO()
                    .userName(bankAccountOptional.get().getLoginId())
                    .password(bankAccountOptional.get().getLoginPassword());

                // locking the bank account
            }
        }

        return credentialResponseDTO;
    }
}
