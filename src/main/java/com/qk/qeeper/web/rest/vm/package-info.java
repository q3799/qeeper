/**
 * View Models used by Spring MVC REST controllers.
 */
package com.qk.qeeper.web.rest.vm;
