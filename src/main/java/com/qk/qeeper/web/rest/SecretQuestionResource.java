package com.qk.qeeper.web.rest;

import com.qk.qeeper.service.SecretQuestionService;
import com.qk.qeeper.web.rest.errors.BadRequestAlertException;
import com.qk.qeeper.service.dto.SecretQuestionDTO;
import com.qk.qeeper.service.dto.SecretQuestionCriteria;
import com.qk.qeeper.service.SecretQuestionQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.qk.qeeper.domain.SecretQuestion}.
 */
@RestController
@RequestMapping("/api")
public class SecretQuestionResource {

    private final Logger log = LoggerFactory.getLogger(SecretQuestionResource.class);

    private static final String ENTITY_NAME = "secretQuestion";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SecretQuestionService secretQuestionService;

    private final SecretQuestionQueryService secretQuestionQueryService;

    public SecretQuestionResource(SecretQuestionService secretQuestionService, SecretQuestionQueryService secretQuestionQueryService) {
        this.secretQuestionService = secretQuestionService;
        this.secretQuestionQueryService = secretQuestionQueryService;
    }

    /**
     * {@code POST  /secret-questions} : Create a new secretQuestion.
     *
     * @param secretQuestionDTO the secretQuestionDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new secretQuestionDTO, or with status {@code 400 (Bad Request)} if the secretQuestion has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/secret-questions")
    public ResponseEntity<SecretQuestionDTO> createSecretQuestion(@Valid @RequestBody SecretQuestionDTO secretQuestionDTO) throws URISyntaxException {
        log.debug("REST request to save SecretQuestion : {}", secretQuestionDTO);
        if (secretQuestionDTO.getId() != null) {
            throw new BadRequestAlertException("A new secretQuestion cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SecretQuestionDTO result = secretQuestionService.save(secretQuestionDTO);
        return ResponseEntity.created(new URI("/api/secret-questions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /secret-questions} : Updates an existing secretQuestion.
     *
     * @param secretQuestionDTO the secretQuestionDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated secretQuestionDTO,
     * or with status {@code 400 (Bad Request)} if the secretQuestionDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the secretQuestionDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/secret-questions")
    public ResponseEntity<SecretQuestionDTO> updateSecretQuestion(@Valid @RequestBody SecretQuestionDTO secretQuestionDTO) throws URISyntaxException {
        log.debug("REST request to update SecretQuestion : {}", secretQuestionDTO);
        if (secretQuestionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SecretQuestionDTO result = secretQuestionService.save(secretQuestionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, secretQuestionDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /secret-questions} : get all the secretQuestions.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of secretQuestions in body.
     */
    @GetMapping("/secret-questions")
    public ResponseEntity<List<SecretQuestionDTO>> getAllSecretQuestions(SecretQuestionCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get SecretQuestions by criteria: {}", criteria);
        Page<SecretQuestionDTO> page = secretQuestionQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /secret-questions/count} : count all the secretQuestions.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/secret-questions/count")
    public ResponseEntity<Long> countSecretQuestions(SecretQuestionCriteria criteria) {
        log.debug("REST request to count SecretQuestions by criteria: {}", criteria);
        return ResponseEntity.ok().body(secretQuestionQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /secret-questions/:id} : get the "id" secretQuestion.
     *
     * @param id the id of the secretQuestionDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the secretQuestionDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/secret-questions/{id}")
    public ResponseEntity<SecretQuestionDTO> getSecretQuestion(@PathVariable Long id) {
        log.debug("REST request to get SecretQuestion : {}", id);
        Optional<SecretQuestionDTO> secretQuestionDTO = secretQuestionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(secretQuestionDTO);
    }

    /**
     * {@code DELETE  /secret-questions/:id} : delete the "id" secretQuestion.
     *
     * @param id the id of the secretQuestionDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/secret-questions/{id}")
    public ResponseEntity<Void> deleteSecretQuestion(@PathVariable Long id) {
        log.debug("REST request to delete SecretQuestion : {}", id);
        secretQuestionService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
