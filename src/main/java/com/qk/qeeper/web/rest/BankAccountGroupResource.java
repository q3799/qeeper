package com.qk.qeeper.web.rest;

import com.qk.qeeper.service.BankAccountGroupService;
import com.qk.qeeper.web.rest.errors.BadRequestAlertException;
import com.qk.qeeper.service.dto.BankAccountGroupDTO;
import com.qk.qeeper.service.dto.BankAccountGroupCriteria;
import com.qk.qeeper.service.BankAccountGroupQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.qk.qeeper.domain.BankAccountGroup}.
 */
@RestController
@RequestMapping("/api")
public class BankAccountGroupResource {

    private final Logger log = LoggerFactory.getLogger(BankAccountGroupResource.class);

    private static final String ENTITY_NAME = "bankAccountGroup";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BankAccountGroupService bankAccountGroupService;

    private final BankAccountGroupQueryService bankAccountGroupQueryService;

    public BankAccountGroupResource(BankAccountGroupService bankAccountGroupService, BankAccountGroupQueryService bankAccountGroupQueryService) {
        this.bankAccountGroupService = bankAccountGroupService;
        this.bankAccountGroupQueryService = bankAccountGroupQueryService;
    }

    /**
     * {@code POST  /bank-account-groups} : Create a new bankAccountGroup.
     *
     * @param bankAccountGroupDTO the bankAccountGroupDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new bankAccountGroupDTO, or with status {@code 400 (Bad Request)} if the bankAccountGroup has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/bank-account-groups")
    public ResponseEntity<BankAccountGroupDTO> createBankAccountGroup(@Valid @RequestBody BankAccountGroupDTO bankAccountGroupDTO) throws URISyntaxException {
        log.debug("REST request to save BankAccountGroup : {}", bankAccountGroupDTO);
        if (bankAccountGroupDTO.getId() != null) {
            throw new BadRequestAlertException("A new bankAccountGroup cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BankAccountGroupDTO result = bankAccountGroupService.save(bankAccountGroupDTO);
        return ResponseEntity.created(new URI("/api/bank-account-groups/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /bank-account-groups} : Updates an existing bankAccountGroup.
     *
     * @param bankAccountGroupDTO the bankAccountGroupDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated bankAccountGroupDTO,
     * or with status {@code 400 (Bad Request)} if the bankAccountGroupDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the bankAccountGroupDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/bank-account-groups")
    public ResponseEntity<BankAccountGroupDTO> updateBankAccountGroup(@Valid @RequestBody BankAccountGroupDTO bankAccountGroupDTO) throws URISyntaxException {
        log.debug("REST request to update BankAccountGroup : {}", bankAccountGroupDTO);
        if (bankAccountGroupDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        BankAccountGroupDTO result = bankAccountGroupService.save(bankAccountGroupDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, bankAccountGroupDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /bank-account-groups} : get all the bankAccountGroups.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of bankAccountGroups in body.
     */
    @GetMapping("/bank-account-groups")
    public ResponseEntity<List<BankAccountGroupDTO>> getAllBankAccountGroups(BankAccountGroupCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get BankAccountGroups by criteria: {}", criteria);
        Page<BankAccountGroupDTO> page = bankAccountGroupQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /bank-account-groups/count} : count all the bankAccountGroups.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/bank-account-groups/count")
    public ResponseEntity<Long> countBankAccountGroups(BankAccountGroupCriteria criteria) {
        log.debug("REST request to count BankAccountGroups by criteria: {}", criteria);
        return ResponseEntity.ok().body(bankAccountGroupQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /bank-account-groups/:id} : get the "id" bankAccountGroup.
     *
     * @param id the id of the bankAccountGroupDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the bankAccountGroupDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/bank-account-groups/{id}")
    public ResponseEntity<BankAccountGroupDTO> getBankAccountGroup(@PathVariable Long id) {
        log.debug("REST request to get BankAccountGroup : {}", id);
        Optional<BankAccountGroupDTO> bankAccountGroupDTO = bankAccountGroupService.findOne(id);
        return ResponseUtil.wrapOrNotFound(bankAccountGroupDTO);
    }

    /**
     * {@code DELETE  /bank-account-groups/:id} : delete the "id" bankAccountGroup.
     *
     * @param id the id of the bankAccountGroupDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/bank-account-groups/{id}")
    public ResponseEntity<Void> deleteBankAccountGroup(@PathVariable Long id) {
        log.debug("REST request to delete BankAccountGroup : {}", id);
        bankAccountGroupService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
