package com.qk.qeeper.web.rest;

import com.qk.qeeper.service.CardGroupService;
import com.qk.qeeper.web.rest.errors.BadRequestAlertException;
import com.qk.qeeper.service.dto.CardGroupDTO;
import com.qk.qeeper.service.dto.CardGroupCriteria;
import com.qk.qeeper.service.CardGroupQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.qk.qeeper.domain.CardGroup}.
 */
@RestController
@RequestMapping("/api")
public class CardGroupResource {

    private final Logger log = LoggerFactory.getLogger(CardGroupResource.class);

    private static final String ENTITY_NAME = "cardGroup";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CardGroupService cardGroupService;

    private final CardGroupQueryService cardGroupQueryService;

    public CardGroupResource(CardGroupService cardGroupService, CardGroupQueryService cardGroupQueryService) {
        this.cardGroupService = cardGroupService;
        this.cardGroupQueryService = cardGroupQueryService;
    }

    /**
     * {@code POST  /card-groups} : Create a new cardGroup.
     *
     * @param cardGroupDTO the cardGroupDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new cardGroupDTO, or with status {@code 400 (Bad Request)} if the cardGroup has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/card-groups")
    public ResponseEntity<CardGroupDTO> createCardGroup(@Valid @RequestBody CardGroupDTO cardGroupDTO) throws URISyntaxException {
        log.debug("REST request to save CardGroup : {}", cardGroupDTO);
        if (cardGroupDTO.getId() != null) {
            throw new BadRequestAlertException("A new cardGroup cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CardGroupDTO result = cardGroupService.save(cardGroupDTO);
        return ResponseEntity.created(new URI("/api/card-groups/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /card-groups} : Updates an existing cardGroup.
     *
     * @param cardGroupDTO the cardGroupDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated cardGroupDTO,
     * or with status {@code 400 (Bad Request)} if the cardGroupDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the cardGroupDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/card-groups")
    public ResponseEntity<CardGroupDTO> updateCardGroup(@Valid @RequestBody CardGroupDTO cardGroupDTO) throws URISyntaxException {
        log.debug("REST request to update CardGroup : {}", cardGroupDTO);
        if (cardGroupDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CardGroupDTO result = cardGroupService.save(cardGroupDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, cardGroupDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /card-groups} : get all the cardGroups.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of cardGroups in body.
     */
    @GetMapping("/card-groups")
    public ResponseEntity<List<CardGroupDTO>> getAllCardGroups(CardGroupCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get CardGroups by criteria: {}", criteria);
        Page<CardGroupDTO> page = cardGroupQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /card-groups/count} : count all the cardGroups.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/card-groups/count")
    public ResponseEntity<Long> countCardGroups(CardGroupCriteria criteria) {
        log.debug("REST request to count CardGroups by criteria: {}", criteria);
        return ResponseEntity.ok().body(cardGroupQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /card-groups/:id} : get the "id" cardGroup.
     *
     * @param id the id of the cardGroupDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the cardGroupDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/card-groups/{id}")
    public ResponseEntity<CardGroupDTO> getCardGroup(@PathVariable Long id) {
        log.debug("REST request to get CardGroup : {}", id);
        Optional<CardGroupDTO> cardGroupDTO = cardGroupService.findOne(id);
        return ResponseUtil.wrapOrNotFound(cardGroupDTO);
    }

    /**
     * {@code DELETE  /card-groups/:id} : delete the "id" cardGroup.
     *
     * @param id the id of the cardGroupDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/card-groups/{id}")
    public ResponseEntity<Void> deleteCardGroup(@PathVariable Long id) {
        log.debug("REST request to delete CardGroup : {}", id);
        cardGroupService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
