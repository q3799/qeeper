package com.qk.qeeper.web.rest;

import com.qk.qeeper.config.Constants;
import com.qk.qeeper.domain.enumeration.ResourceType;
import com.qk.qeeper.service.BankAccountService;
import com.qk.qeeper.service.CardService;
import com.qk.qeeper.service.NonBankAccountService;
import com.qk.qeeper.service.dto.CredentialRequestDTO;
import com.qk.qeeper.service.dto.CredentialResponseDTO;
import com.qk.qeeper.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.config.JHipsterProperties;
import io.github.jhipster.web.util.HeaderUtil;
import io.jsonwebtoken.io.Decoders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URISyntaxException;

/**
 * ExposedResource controller
 */
@RestController
@RequestMapping("/api/exposed")
public class ExposedResource {

    private final Logger log = LoggerFactory.getLogger(ExposedResource.class);

    private final JHipsterProperties jHipsterProperties;

    private final BankAccountService bankAccountService;

    private final CardService cardService;

    private final NonBankAccountService nonBankAccountService;

    public ExposedResource(JHipsterProperties jHipsterProperties, BankAccountService bankAccountService, CardService cardService, NonBankAccountService nonBankAccountService) {
        this.jHipsterProperties = jHipsterProperties;
        this.bankAccountService = bankAccountService;
        this.cardService = cardService;
        this.nonBankAccountService = nonBankAccountService;
    }

    /**
     * GET getCredentials
     */
    @PostMapping("/credentials")
    public ResponseEntity<CredentialResponseDTO> getCredentials(@Valid @RequestBody CredentialRequestDTO credentialRequestDTO, @RequestHeader HttpHeaders headers) throws URISyntaxException {
        log.debug("REST request to get credentials credentialRequestDTO : {}", credentialRequestDTO);

        String accessKey = headers.getFirst(Constants.ACCESS_KEY);
        byte[] keyBytes = Decoders.BASE64.decode(jHipsterProperties.getSecurity().getAuthentication().getJwt().getBase64Secret());

        if (accessKey == null || !accessKey.equals(new String(keyBytes))) {
            throw new BadRequestAlertException("Invalid access key", "CREDENTIALS", "invalidAccessKey");
        }

        CredentialResponseDTO credentialResponseDTO;

        // now based on the resource type, fetch the required credentials
        if (credentialRequestDTO.getResourceType().equals(ResourceType.BANK_ACCOUNT)) {
            credentialResponseDTO = bankAccountService.getCredentials(credentialRequestDTO.getGroupCode());
        } else if (credentialRequestDTO.getResourceType().equals(ResourceType.CARD)) {
            credentialResponseDTO = cardService.getCredentials(credentialRequestDTO.getGroupCode());
        } else if (credentialRequestDTO.getResourceType().equals(ResourceType.NON_BANK_ACCOUNT)) {
            credentialResponseDTO = nonBankAccountService.getCredentials(credentialRequestDTO.getGroupCode());
        } else {
            credentialResponseDTO = null;
        }

        return ResponseEntity.ok()
            .headers(headers)
            .body(credentialResponseDTO);
    }
}
