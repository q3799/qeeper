package com.qk.qeeper.web.rest;

import com.qk.qeeper.service.EscalationMatrixService;
import com.qk.qeeper.web.rest.errors.BadRequestAlertException;
import com.qk.qeeper.service.dto.EscalationMatrixDTO;
import com.qk.qeeper.service.dto.EscalationMatrixCriteria;
import com.qk.qeeper.service.EscalationMatrixQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.qk.qeeper.domain.EscalationMatrix}.
 */
@RestController
@RequestMapping("/api")
public class EscalationMatrixResource {

    private final Logger log = LoggerFactory.getLogger(EscalationMatrixResource.class);

    private static final String ENTITY_NAME = "escalationMatrix";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EscalationMatrixService escalationMatrixService;

    private final EscalationMatrixQueryService escalationMatrixQueryService;

    public EscalationMatrixResource(EscalationMatrixService escalationMatrixService, EscalationMatrixQueryService escalationMatrixQueryService) {
        this.escalationMatrixService = escalationMatrixService;
        this.escalationMatrixQueryService = escalationMatrixQueryService;
    }

    /**
     * {@code POST  /escalation-matrices} : Create a new escalationMatrix.
     *
     * @param escalationMatrixDTO the escalationMatrixDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new escalationMatrixDTO, or with status {@code 400 (Bad Request)} if the escalationMatrix has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/escalation-matrices")
    public ResponseEntity<EscalationMatrixDTO> createEscalationMatrix(@Valid @RequestBody EscalationMatrixDTO escalationMatrixDTO) throws URISyntaxException {
        log.debug("REST request to save EscalationMatrix : {}", escalationMatrixDTO);
        if (escalationMatrixDTO.getId() != null) {
            throw new BadRequestAlertException("A new escalationMatrix cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EscalationMatrixDTO result = escalationMatrixService.save(escalationMatrixDTO);
        return ResponseEntity.created(new URI("/api/escalation-matrices/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /escalation-matrices} : Updates an existing escalationMatrix.
     *
     * @param escalationMatrixDTO the escalationMatrixDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated escalationMatrixDTO,
     * or with status {@code 400 (Bad Request)} if the escalationMatrixDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the escalationMatrixDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/escalation-matrices")
    public ResponseEntity<EscalationMatrixDTO> updateEscalationMatrix(@Valid @RequestBody EscalationMatrixDTO escalationMatrixDTO) throws URISyntaxException {
        log.debug("REST request to update EscalationMatrix : {}", escalationMatrixDTO);
        if (escalationMatrixDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        EscalationMatrixDTO result = escalationMatrixService.save(escalationMatrixDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, escalationMatrixDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /escalation-matrices} : get all the escalationMatrices.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of escalationMatrices in body.
     */
    @GetMapping("/escalation-matrices")
    public ResponseEntity<List<EscalationMatrixDTO>> getAllEscalationMatrices(EscalationMatrixCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get EscalationMatrices by criteria: {}", criteria);
        Page<EscalationMatrixDTO> page = escalationMatrixQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /escalation-matrices/count} : count all the escalationMatrices.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/escalation-matrices/count")
    public ResponseEntity<Long> countEscalationMatrices(EscalationMatrixCriteria criteria) {
        log.debug("REST request to count EscalationMatrices by criteria: {}", criteria);
        return ResponseEntity.ok().body(escalationMatrixQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /escalation-matrices/:id} : get the "id" escalationMatrix.
     *
     * @param id the id of the escalationMatrixDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the escalationMatrixDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/escalation-matrices/{id}")
    public ResponseEntity<EscalationMatrixDTO> getEscalationMatrix(@PathVariable Long id) {
        log.debug("REST request to get EscalationMatrix : {}", id);
        Optional<EscalationMatrixDTO> escalationMatrixDTO = escalationMatrixService.findOne(id);
        return ResponseUtil.wrapOrNotFound(escalationMatrixDTO);
    }

    /**
     * {@code DELETE  /escalation-matrices/:id} : delete the "id" escalationMatrix.
     *
     * @param id the id of the escalationMatrixDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/escalation-matrices/{id}")
    public ResponseEntity<Void> deleteEscalationMatrix(@PathVariable Long id) {
        log.debug("REST request to delete EscalationMatrix : {}", id);
        escalationMatrixService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
