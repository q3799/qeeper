package com.qk.qeeper.web.rest;

import com.qk.qeeper.service.MobileNumberService;
import com.qk.qeeper.web.rest.errors.BadRequestAlertException;
import com.qk.qeeper.service.dto.MobileNumberDTO;
import com.qk.qeeper.service.dto.MobileNumberCriteria;
import com.qk.qeeper.service.MobileNumberQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.qk.qeeper.domain.MobileNumber}.
 */
@RestController
@RequestMapping("/api")
public class MobileNumberResource {

    private final Logger log = LoggerFactory.getLogger(MobileNumberResource.class);

    private static final String ENTITY_NAME = "mobileNumber";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MobileNumberService mobileNumberService;

    private final MobileNumberQueryService mobileNumberQueryService;

    public MobileNumberResource(MobileNumberService mobileNumberService, MobileNumberQueryService mobileNumberQueryService) {
        this.mobileNumberService = mobileNumberService;
        this.mobileNumberQueryService = mobileNumberQueryService;
    }

    /**
     * {@code POST  /mobile-numbers} : Create a new mobileNumber.
     *
     * @param mobileNumberDTO the mobileNumberDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new mobileNumberDTO, or with status {@code 400 (Bad Request)} if the mobileNumber has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/mobile-numbers")
    public ResponseEntity<MobileNumberDTO> createMobileNumber(@Valid @RequestBody MobileNumberDTO mobileNumberDTO) throws URISyntaxException {
        log.debug("REST request to save MobileNumber : {}", mobileNumberDTO);
        if (mobileNumberDTO.getId() != null) {
            throw new BadRequestAlertException("A new mobileNumber cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MobileNumberDTO result = mobileNumberService.save(mobileNumberDTO);
        return ResponseEntity.created(new URI("/api/mobile-numbers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /mobile-numbers} : Updates an existing mobileNumber.
     *
     * @param mobileNumberDTO the mobileNumberDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated mobileNumberDTO,
     * or with status {@code 400 (Bad Request)} if the mobileNumberDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the mobileNumberDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/mobile-numbers")
    public ResponseEntity<MobileNumberDTO> updateMobileNumber(@Valid @RequestBody MobileNumberDTO mobileNumberDTO) throws URISyntaxException {
        log.debug("REST request to update MobileNumber : {}", mobileNumberDTO);
        if (mobileNumberDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        MobileNumberDTO result = mobileNumberService.save(mobileNumberDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, mobileNumberDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /mobile-numbers} : get all the mobileNumbers.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of mobileNumbers in body.
     */
    @GetMapping("/mobile-numbers")
    public ResponseEntity<List<MobileNumberDTO>> getAllMobileNumbers(MobileNumberCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get MobileNumbers by criteria: {}", criteria);
        Page<MobileNumberDTO> page = mobileNumberQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /mobile-numbers/count} : count all the mobileNumbers.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/mobile-numbers/count")
    public ResponseEntity<Long> countMobileNumbers(MobileNumberCriteria criteria) {
        log.debug("REST request to count MobileNumbers by criteria: {}", criteria);
        return ResponseEntity.ok().body(mobileNumberQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /mobile-numbers/:id} : get the "id" mobileNumber.
     *
     * @param id the id of the mobileNumberDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the mobileNumberDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/mobile-numbers/{id}")
    public ResponseEntity<MobileNumberDTO> getMobileNumber(@PathVariable Long id) {
        log.debug("REST request to get MobileNumber : {}", id);
        Optional<MobileNumberDTO> mobileNumberDTO = mobileNumberService.findOne(id);
        return ResponseUtil.wrapOrNotFound(mobileNumberDTO);
    }

    /**
     * {@code DELETE  /mobile-numbers/:id} : delete the "id" mobileNumber.
     *
     * @param id the id of the mobileNumberDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/mobile-numbers/{id}")
    public ResponseEntity<Void> deleteMobileNumber(@PathVariable Long id) {
        log.debug("REST request to delete MobileNumber : {}", id);
        mobileNumberService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
