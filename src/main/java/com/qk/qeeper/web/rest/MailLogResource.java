package com.qk.qeeper.web.rest;

import com.qk.qeeper.service.MailLogService;
import com.qk.qeeper.web.rest.errors.BadRequestAlertException;
import com.qk.qeeper.service.dto.MailLogDTO;
import com.qk.qeeper.service.dto.MailLogCriteria;
import com.qk.qeeper.service.MailLogQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.qk.qeeper.domain.MailLog}.
 */
@RestController
@RequestMapping("/api")
public class MailLogResource {

    private final Logger log = LoggerFactory.getLogger(MailLogResource.class);

    private static final String ENTITY_NAME = "mailLog";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MailLogService mailLogService;

    private final MailLogQueryService mailLogQueryService;

    public MailLogResource(MailLogService mailLogService, MailLogQueryService mailLogQueryService) {
        this.mailLogService = mailLogService;
        this.mailLogQueryService = mailLogQueryService;
    }

    /**
     * {@code POST  /mail-logs} : Create a new mailLog.
     *
     * @param mailLogDTO the mailLogDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new mailLogDTO, or with status {@code 400 (Bad Request)} if the mailLog has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/mail-logs")
    public ResponseEntity<MailLogDTO> createMailLog(@Valid @RequestBody MailLogDTO mailLogDTO) throws URISyntaxException {
        log.debug("REST request to save MailLog : {}", mailLogDTO);
        if (mailLogDTO.getId() != null) {
            throw new BadRequestAlertException("A new mailLog cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MailLogDTO result = mailLogService.save(mailLogDTO);
        return ResponseEntity.created(new URI("/api/mail-logs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /mail-logs} : Updates an existing mailLog.
     *
     * @param mailLogDTO the mailLogDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated mailLogDTO,
     * or with status {@code 400 (Bad Request)} if the mailLogDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the mailLogDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/mail-logs")
    public ResponseEntity<MailLogDTO> updateMailLog(@Valid @RequestBody MailLogDTO mailLogDTO) throws URISyntaxException {
        log.debug("REST request to update MailLog : {}", mailLogDTO);
        if (mailLogDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        MailLogDTO result = mailLogService.save(mailLogDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, mailLogDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /mail-logs} : get all the mailLogs.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of mailLogs in body.
     */
    @GetMapping("/mail-logs")
    public ResponseEntity<List<MailLogDTO>> getAllMailLogs(MailLogCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get MailLogs by criteria: {}", criteria);
        Page<MailLogDTO> page = mailLogQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /mail-logs/count} : count all the mailLogs.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/mail-logs/count")
    public ResponseEntity<Long> countMailLogs(MailLogCriteria criteria) {
        log.debug("REST request to count MailLogs by criteria: {}", criteria);
        return ResponseEntity.ok().body(mailLogQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /mail-logs/:id} : get the "id" mailLog.
     *
     * @param id the id of the mailLogDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the mailLogDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/mail-logs/{id}")
    public ResponseEntity<MailLogDTO> getMailLog(@PathVariable Long id) {
        log.debug("REST request to get MailLog : {}", id);
        Optional<MailLogDTO> mailLogDTO = mailLogService.findOne(id);
        return ResponseUtil.wrapOrNotFound(mailLogDTO);
    }

    /**
     * {@code DELETE  /mail-logs/:id} : delete the "id" mailLog.
     *
     * @param id the id of the mailLogDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/mail-logs/{id}")
    public ResponseEntity<Void> deleteMailLog(@PathVariable Long id) {
        log.debug("REST request to delete MailLog : {}", id);
        mailLogService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
