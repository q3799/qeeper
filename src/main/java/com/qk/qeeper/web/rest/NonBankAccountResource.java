package com.qk.qeeper.web.rest;

import com.qk.qeeper.service.NonBankAccountService;
import com.qk.qeeper.web.rest.errors.BadRequestAlertException;
import com.qk.qeeper.service.dto.NonBankAccountDTO;
import com.qk.qeeper.service.dto.NonBankAccountCriteria;
import com.qk.qeeper.service.NonBankAccountQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.qk.qeeper.domain.NonBankAccount}.
 */
@RestController
@RequestMapping("/api")
public class NonBankAccountResource {

    private final Logger log = LoggerFactory.getLogger(NonBankAccountResource.class);

    private static final String ENTITY_NAME = "nonBankAccount";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final NonBankAccountService nonBankAccountService;

    private final NonBankAccountQueryService nonBankAccountQueryService;

    public NonBankAccountResource(NonBankAccountService nonBankAccountService, NonBankAccountQueryService nonBankAccountQueryService) {
        this.nonBankAccountService = nonBankAccountService;
        this.nonBankAccountQueryService = nonBankAccountQueryService;
    }

    /**
     * {@code POST  /non-bank-accounts} : Create a new nonBankAccount.
     *
     * @param nonBankAccountDTO the nonBankAccountDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new nonBankAccountDTO, or with status {@code 400 (Bad Request)} if the nonBankAccount has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/non-bank-accounts")
    public ResponseEntity<NonBankAccountDTO> createNonBankAccount(@Valid @RequestBody NonBankAccountDTO nonBankAccountDTO) throws URISyntaxException {
        log.debug("REST request to save NonBankAccount : {}", nonBankAccountDTO);
        if (nonBankAccountDTO.getId() != null) {
            throw new BadRequestAlertException("A new nonBankAccount cannot already have an ID", ENTITY_NAME, "idexists");
        }
        NonBankAccountDTO result = nonBankAccountService.save(nonBankAccountDTO);
        return ResponseEntity.created(new URI("/api/non-bank-accounts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /non-bank-accounts} : Updates an existing nonBankAccount.
     *
     * @param nonBankAccountDTO the nonBankAccountDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated nonBankAccountDTO,
     * or with status {@code 400 (Bad Request)} if the nonBankAccountDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the nonBankAccountDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/non-bank-accounts")
    public ResponseEntity<NonBankAccountDTO> updateNonBankAccount(@Valid @RequestBody NonBankAccountDTO nonBankAccountDTO) throws URISyntaxException {
        log.debug("REST request to update NonBankAccount : {}", nonBankAccountDTO);
        if (nonBankAccountDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        NonBankAccountDTO result = nonBankAccountService.save(nonBankAccountDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, nonBankAccountDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /non-bank-accounts} : get all the nonBankAccounts.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of nonBankAccounts in body.
     */
    @GetMapping("/non-bank-accounts")
    public ResponseEntity<List<NonBankAccountDTO>> getAllNonBankAccounts(NonBankAccountCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get NonBankAccounts by criteria: {}", criteria);
        Page<NonBankAccountDTO> page = nonBankAccountQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /non-bank-accounts/count} : count all the nonBankAccounts.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/non-bank-accounts/count")
    public ResponseEntity<Long> countNonBankAccounts(NonBankAccountCriteria criteria) {
        log.debug("REST request to count NonBankAccounts by criteria: {}", criteria);
        return ResponseEntity.ok().body(nonBankAccountQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /non-bank-accounts/:id} : get the "id" nonBankAccount.
     *
     * @param id the id of the nonBankAccountDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the nonBankAccountDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/non-bank-accounts/{id}")
    public ResponseEntity<NonBankAccountDTO> getNonBankAccount(@PathVariable Long id) {
        log.debug("REST request to get NonBankAccount : {}", id);
        Optional<NonBankAccountDTO> nonBankAccountDTO = nonBankAccountService.findOne(id);
        return ResponseUtil.wrapOrNotFound(nonBankAccountDTO);
    }

    /**
     * {@code DELETE  /non-bank-accounts/:id} : delete the "id" nonBankAccount.
     *
     * @param id the id of the nonBankAccountDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/non-bank-accounts/{id}")
    public ResponseEntity<Void> deleteNonBankAccount(@PathVariable Long id) {
        log.debug("REST request to delete NonBankAccount : {}", id);
        nonBankAccountService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
