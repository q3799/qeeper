package com.qk.qeeper.web.rest;

import com.qk.qeeper.service.NonBankAccountGroupService;
import com.qk.qeeper.web.rest.errors.BadRequestAlertException;
import com.qk.qeeper.service.dto.NonBankAccountGroupDTO;
import com.qk.qeeper.service.dto.NonBankAccountGroupCriteria;
import com.qk.qeeper.service.NonBankAccountGroupQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.qk.qeeper.domain.NonBankAccountGroup}.
 */
@RestController
@RequestMapping("/api")
public class NonBankAccountGroupResource {

    private final Logger log = LoggerFactory.getLogger(NonBankAccountGroupResource.class);

    private static final String ENTITY_NAME = "nonBankAccountGroup";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final NonBankAccountGroupService nonBankAccountGroupService;

    private final NonBankAccountGroupQueryService nonBankAccountGroupQueryService;

    public NonBankAccountGroupResource(NonBankAccountGroupService nonBankAccountGroupService, NonBankAccountGroupQueryService nonBankAccountGroupQueryService) {
        this.nonBankAccountGroupService = nonBankAccountGroupService;
        this.nonBankAccountGroupQueryService = nonBankAccountGroupQueryService;
    }

    /**
     * {@code POST  /non-bank-account-groups} : Create a new nonBankAccountGroup.
     *
     * @param nonBankAccountGroupDTO the nonBankAccountGroupDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new nonBankAccountGroupDTO, or with status {@code 400 (Bad Request)} if the nonBankAccountGroup has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/non-bank-account-groups")
    public ResponseEntity<NonBankAccountGroupDTO> createNonBankAccountGroup(@Valid @RequestBody NonBankAccountGroupDTO nonBankAccountGroupDTO) throws URISyntaxException {
        log.debug("REST request to save NonBankAccountGroup : {}", nonBankAccountGroupDTO);
        if (nonBankAccountGroupDTO.getId() != null) {
            throw new BadRequestAlertException("A new nonBankAccountGroup cannot already have an ID", ENTITY_NAME, "idexists");
        }
        NonBankAccountGroupDTO result = nonBankAccountGroupService.save(nonBankAccountGroupDTO);
        return ResponseEntity.created(new URI("/api/non-bank-account-groups/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /non-bank-account-groups} : Updates an existing nonBankAccountGroup.
     *
     * @param nonBankAccountGroupDTO the nonBankAccountGroupDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated nonBankAccountGroupDTO,
     * or with status {@code 400 (Bad Request)} if the nonBankAccountGroupDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the nonBankAccountGroupDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/non-bank-account-groups")
    public ResponseEntity<NonBankAccountGroupDTO> updateNonBankAccountGroup(@Valid @RequestBody NonBankAccountGroupDTO nonBankAccountGroupDTO) throws URISyntaxException {
        log.debug("REST request to update NonBankAccountGroup : {}", nonBankAccountGroupDTO);
        if (nonBankAccountGroupDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        NonBankAccountGroupDTO result = nonBankAccountGroupService.save(nonBankAccountGroupDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, nonBankAccountGroupDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /non-bank-account-groups} : get all the nonBankAccountGroups.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of nonBankAccountGroups in body.
     */
    @GetMapping("/non-bank-account-groups")
    public ResponseEntity<List<NonBankAccountGroupDTO>> getAllNonBankAccountGroups(NonBankAccountGroupCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get NonBankAccountGroups by criteria: {}", criteria);
        Page<NonBankAccountGroupDTO> page = nonBankAccountGroupQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /non-bank-account-groups/count} : count all the nonBankAccountGroups.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/non-bank-account-groups/count")
    public ResponseEntity<Long> countNonBankAccountGroups(NonBankAccountGroupCriteria criteria) {
        log.debug("REST request to count NonBankAccountGroups by criteria: {}", criteria);
        return ResponseEntity.ok().body(nonBankAccountGroupQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /non-bank-account-groups/:id} : get the "id" nonBankAccountGroup.
     *
     * @param id the id of the nonBankAccountGroupDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the nonBankAccountGroupDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/non-bank-account-groups/{id}")
    public ResponseEntity<NonBankAccountGroupDTO> getNonBankAccountGroup(@PathVariable Long id) {
        log.debug("REST request to get NonBankAccountGroup : {}", id);
        Optional<NonBankAccountGroupDTO> nonBankAccountGroupDTO = nonBankAccountGroupService.findOne(id);
        return ResponseUtil.wrapOrNotFound(nonBankAccountGroupDTO);
    }

    /**
     * {@code DELETE  /non-bank-account-groups/:id} : delete the "id" nonBankAccountGroup.
     *
     * @param id the id of the nonBankAccountGroupDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/non-bank-account-groups/{id}")
    public ResponseEntity<Void> deleteNonBankAccountGroup(@PathVariable Long id) {
        log.debug("REST request to delete NonBankAccountGroup : {}", id);
        nonBankAccountGroupService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
