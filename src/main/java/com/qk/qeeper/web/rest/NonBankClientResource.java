package com.qk.qeeper.web.rest;

import com.qk.qeeper.service.NonBankClientService;
import com.qk.qeeper.web.rest.errors.BadRequestAlertException;
import com.qk.qeeper.service.dto.NonBankClientDTO;
import com.qk.qeeper.service.dto.NonBankClientCriteria;
import com.qk.qeeper.service.NonBankClientQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.qk.qeeper.domain.NonBankClient}.
 */
@RestController
@RequestMapping("/api")
public class NonBankClientResource {

    private final Logger log = LoggerFactory.getLogger(NonBankClientResource.class);

    private static final String ENTITY_NAME = "nonBankClient";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final NonBankClientService nonBankClientService;

    private final NonBankClientQueryService nonBankClientQueryService;

    public NonBankClientResource(NonBankClientService nonBankClientService, NonBankClientQueryService nonBankClientQueryService) {
        this.nonBankClientService = nonBankClientService;
        this.nonBankClientQueryService = nonBankClientQueryService;
    }

    /**
     * {@code POST  /non-bank-clients} : Create a new nonBankClient.
     *
     * @param nonBankClientDTO the nonBankClientDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new nonBankClientDTO, or with status {@code 400 (Bad Request)} if the nonBankClient has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/non-bank-clients")
    public ResponseEntity<NonBankClientDTO> createNonBankClient(@Valid @RequestBody NonBankClientDTO nonBankClientDTO) throws URISyntaxException {
        log.debug("REST request to save NonBankClient : {}", nonBankClientDTO);
        if (nonBankClientDTO.getId() != null) {
            throw new BadRequestAlertException("A new nonBankClient cannot already have an ID", ENTITY_NAME, "idexists");
        }
        NonBankClientDTO result = nonBankClientService.save(nonBankClientDTO);
        return ResponseEntity.created(new URI("/api/non-bank-clients/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /non-bank-clients} : Updates an existing nonBankClient.
     *
     * @param nonBankClientDTO the nonBankClientDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated nonBankClientDTO,
     * or with status {@code 400 (Bad Request)} if the nonBankClientDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the nonBankClientDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/non-bank-clients")
    public ResponseEntity<NonBankClientDTO> updateNonBankClient(@Valid @RequestBody NonBankClientDTO nonBankClientDTO) throws URISyntaxException {
        log.debug("REST request to update NonBankClient : {}", nonBankClientDTO);
        if (nonBankClientDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        NonBankClientDTO result = nonBankClientService.save(nonBankClientDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, nonBankClientDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /non-bank-clients} : get all the nonBankClients.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of nonBankClients in body.
     */
    @GetMapping("/non-bank-clients")
    public ResponseEntity<List<NonBankClientDTO>> getAllNonBankClients(NonBankClientCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get NonBankClients by criteria: {}", criteria);
        Page<NonBankClientDTO> page = nonBankClientQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /non-bank-clients/count} : count all the nonBankClients.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/non-bank-clients/count")
    public ResponseEntity<Long> countNonBankClients(NonBankClientCriteria criteria) {
        log.debug("REST request to count NonBankClients by criteria: {}", criteria);
        return ResponseEntity.ok().body(nonBankClientQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /non-bank-clients/:id} : get the "id" nonBankClient.
     *
     * @param id the id of the nonBankClientDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the nonBankClientDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/non-bank-clients/{id}")
    public ResponseEntity<NonBankClientDTO> getNonBankClient(@PathVariable Long id) {
        log.debug("REST request to get NonBankClient : {}", id);
        Optional<NonBankClientDTO> nonBankClientDTO = nonBankClientService.findOne(id);
        return ResponseUtil.wrapOrNotFound(nonBankClientDTO);
    }

    /**
     * {@code DELETE  /non-bank-clients/:id} : delete the "id" nonBankClient.
     *
     * @param id the id of the nonBankClientDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/non-bank-clients/{id}")
    public ResponseEntity<Void> deleteNonBankClient(@PathVariable Long id) {
        log.debug("REST request to delete NonBankClient : {}", id);
        nonBankClientService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
