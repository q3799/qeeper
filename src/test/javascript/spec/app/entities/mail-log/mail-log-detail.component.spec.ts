/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { QeeperTestModule } from '../../../test.module';
import { MailLogDetailComponent } from 'app/entities/mail-log/mail-log-detail.component';
import { MailLog } from 'app/shared/model/mail-log.model';

describe('Component Tests', () => {
  describe('MailLog Management Detail Component', () => {
    let comp: MailLogDetailComponent;
    let fixture: ComponentFixture<MailLogDetailComponent>;
    const route = ({ data: of({ mailLog: new MailLog(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [QeeperTestModule],
        declarations: [MailLogDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(MailLogDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(MailLogDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.mailLog).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
