/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { MailLogService } from 'app/entities/mail-log/mail-log.service';
import { IMailLog, MailLog, MailServer } from 'app/shared/model/mail-log.model';

describe('Service Tests', () => {
  describe('MailLog Service', () => {
    let injector: TestBed;
    let service: MailLogService;
    let httpMock: HttpTestingController;
    let elemDefault: IMailLog;
    let expectedResult;
    let currentDate: moment.Moment;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(MailLogService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new MailLog(0, 'AAAAAAA', 'AAAAAAA', currentDate, MailServer.PRIMARY, 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 0);
    });

    describe('Service methods', () => {
      it('should find an element', async () => {
        const returnedFromService = Object.assign(
          {
            mailDate: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a MailLog', async () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            mailDate: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            mailDate: currentDate
          },
          returnedFromService
        );
        service
          .create(new MailLog(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a MailLog', async () => {
        const returnedFromService = Object.assign(
          {
            subject: 'BBBBBB',
            content: 'BBBBBB',
            mailDate: currentDate.format(DATE_TIME_FORMAT),
            mailServer: 'BBBBBB',
            responseReceived: 'BBBBBB',
            remarks: 'BBBBBB',
            toList: 'BBBBBB',
            ccList: 'BBBBBB',
            processingTime: 1
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            mailDate: currentDate
          },
          returnedFromService
        );
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of MailLog', async () => {
        const returnedFromService = Object.assign(
          {
            subject: 'BBBBBB',
            content: 'BBBBBB',
            mailDate: currentDate.format(DATE_TIME_FORMAT),
            mailServer: 'BBBBBB',
            responseReceived: 'BBBBBB',
            remarks: 'BBBBBB',
            toList: 'BBBBBB',
            ccList: 'BBBBBB',
            processingTime: 1
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            mailDate: currentDate
          },
          returnedFromService
        );
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a MailLog', async () => {
        const rxPromise = service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
