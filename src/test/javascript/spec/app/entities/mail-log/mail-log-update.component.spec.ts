/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { QeeperTestModule } from '../../../test.module';
import { MailLogUpdateComponent } from 'app/entities/mail-log/mail-log-update.component';
import { MailLogService } from 'app/entities/mail-log/mail-log.service';
import { MailLog } from 'app/shared/model/mail-log.model';

describe('Component Tests', () => {
  describe('MailLog Management Update Component', () => {
    let comp: MailLogUpdateComponent;
    let fixture: ComponentFixture<MailLogUpdateComponent>;
    let service: MailLogService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [QeeperTestModule],
        declarations: [MailLogUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(MailLogUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(MailLogUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MailLogService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new MailLog(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new MailLog();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
