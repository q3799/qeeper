/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import { MobileNumberService } from 'app/entities/mobile-number/mobile-number.service';
import { IMobileNumber, MobileNumber, Operator, MobileConnectionPlan } from 'app/shared/model/mobile-number.model';

describe('Service Tests', () => {
  describe('MobileNumber Service', () => {
    let injector: TestBed;
    let service: MobileNumberService;
    let httpMock: HttpTestingController;
    let elemDefault: IMobileNumber;
    let expectedResult;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(MobileNumberService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new MobileNumber(0, 'AAAAAAA', 'AAAAAAA', Operator.AIRTEL, MobileConnectionPlan.PREPAID, 'AAAAAAA', false);
    });

    describe('Service methods', () => {
      it('should find an element', async () => {
        const returnedFromService = Object.assign({}, elemDefault);
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a MobileNumber', async () => {
        const returnedFromService = Object.assign(
          {
            id: 0
          },
          elemDefault
        );
        const expected = Object.assign({}, returnedFromService);
        service
          .create(new MobileNumber(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a MobileNumber', async () => {
        const returnedFromService = Object.assign(
          {
            number: 'BBBBBB',
            assetTag: 'BBBBBB',
            operator: 'BBBBBB',
            mobileConnectionPlan: 'BBBBBB',
            remarks: 'BBBBBB',
            active: true
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of MobileNumber', async () => {
        const returnedFromService = Object.assign(
          {
            number: 'BBBBBB',
            assetTag: 'BBBBBB',
            operator: 'BBBBBB',
            mobileConnectionPlan: 'BBBBBB',
            remarks: 'BBBBBB',
            active: true
          },
          elemDefault
        );
        const expected = Object.assign({}, returnedFromService);
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a MobileNumber', async () => {
        const rxPromise = service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
