/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { QeeperTestModule } from '../../../test.module';
import { MobileNumberDeleteDialogComponent } from 'app/entities/mobile-number/mobile-number-delete-dialog.component';
import { MobileNumberService } from 'app/entities/mobile-number/mobile-number.service';

describe('Component Tests', () => {
  describe('MobileNumber Management Delete Component', () => {
    let comp: MobileNumberDeleteDialogComponent;
    let fixture: ComponentFixture<MobileNumberDeleteDialogComponent>;
    let service: MobileNumberService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [QeeperTestModule],
        declarations: [MobileNumberDeleteDialogComponent]
      })
        .overrideTemplate(MobileNumberDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(MobileNumberDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MobileNumberService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
