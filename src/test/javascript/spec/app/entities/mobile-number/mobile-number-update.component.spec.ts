/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { QeeperTestModule } from '../../../test.module';
import { MobileNumberUpdateComponent } from 'app/entities/mobile-number/mobile-number-update.component';
import { MobileNumberService } from 'app/entities/mobile-number/mobile-number.service';
import { MobileNumber } from 'app/shared/model/mobile-number.model';

describe('Component Tests', () => {
  describe('MobileNumber Management Update Component', () => {
    let comp: MobileNumberUpdateComponent;
    let fixture: ComponentFixture<MobileNumberUpdateComponent>;
    let service: MobileNumberService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [QeeperTestModule],
        declarations: [MobileNumberUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(MobileNumberUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(MobileNumberUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MobileNumberService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new MobileNumber(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new MobileNumber();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
