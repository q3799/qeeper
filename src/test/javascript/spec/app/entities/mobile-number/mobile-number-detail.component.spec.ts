/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { QeeperTestModule } from '../../../test.module';
import { MobileNumberDetailComponent } from 'app/entities/mobile-number/mobile-number-detail.component';
import { MobileNumber } from 'app/shared/model/mobile-number.model';

describe('Component Tests', () => {
  describe('MobileNumber Management Detail Component', () => {
    let comp: MobileNumberDetailComponent;
    let fixture: ComponentFixture<MobileNumberDetailComponent>;
    const route = ({ data: of({ mobileNumber: new MobileNumber(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [QeeperTestModule],
        declarations: [MobileNumberDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(MobileNumberDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(MobileNumberDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.mobileNumber).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
