/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { QeeperTestModule } from '../../../test.module';
import { ContactPersonDeleteDialogComponent } from 'app/entities/contact-person/contact-person-delete-dialog.component';
import { ContactPersonService } from 'app/entities/contact-person/contact-person.service';

describe('Component Tests', () => {
  describe('ContactPerson Management Delete Component', () => {
    let comp: ContactPersonDeleteDialogComponent;
    let fixture: ComponentFixture<ContactPersonDeleteDialogComponent>;
    let service: ContactPersonService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [QeeperTestModule],
        declarations: [ContactPersonDeleteDialogComponent]
      })
        .overrideTemplate(ContactPersonDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ContactPersonDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ContactPersonService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
