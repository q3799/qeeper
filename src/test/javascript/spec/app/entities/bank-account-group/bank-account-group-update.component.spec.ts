/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { QeeperTestModule } from '../../../test.module';
import { BankAccountGroupUpdateComponent } from 'app/entities/bank-account-group/bank-account-group-update.component';
import { BankAccountGroupService } from 'app/entities/bank-account-group/bank-account-group.service';
import { BankAccountGroup } from 'app/shared/model/bank-account-group.model';

describe('Component Tests', () => {
  describe('BankAccountGroup Management Update Component', () => {
    let comp: BankAccountGroupUpdateComponent;
    let fixture: ComponentFixture<BankAccountGroupUpdateComponent>;
    let service: BankAccountGroupService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [QeeperTestModule],
        declarations: [BankAccountGroupUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(BankAccountGroupUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BankAccountGroupUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BankAccountGroupService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new BankAccountGroup(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new BankAccountGroup();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
