/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { QeeperTestModule } from '../../../test.module';
import { BankAccountGroupDetailComponent } from 'app/entities/bank-account-group/bank-account-group-detail.component';
import { BankAccountGroup } from 'app/shared/model/bank-account-group.model';

describe('Component Tests', () => {
  describe('BankAccountGroup Management Detail Component', () => {
    let comp: BankAccountGroupDetailComponent;
    let fixture: ComponentFixture<BankAccountGroupDetailComponent>;
    const route = ({ data: of({ bankAccountGroup: new BankAccountGroup(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [QeeperTestModule],
        declarations: [BankAccountGroupDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(BankAccountGroupDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BankAccountGroupDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.bankAccountGroup).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
