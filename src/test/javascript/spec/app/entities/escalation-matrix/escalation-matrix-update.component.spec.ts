/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { QeeperTestModule } from '../../../test.module';
import { EscalationMatrixUpdateComponent } from 'app/entities/escalation-matrix/escalation-matrix-update.component';
import { EscalationMatrixService } from 'app/entities/escalation-matrix/escalation-matrix.service';
import { EscalationMatrix } from 'app/shared/model/escalation-matrix.model';

describe('Component Tests', () => {
  describe('EscalationMatrix Management Update Component', () => {
    let comp: EscalationMatrixUpdateComponent;
    let fixture: ComponentFixture<EscalationMatrixUpdateComponent>;
    let service: EscalationMatrixService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [QeeperTestModule],
        declarations: [EscalationMatrixUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(EscalationMatrixUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(EscalationMatrixUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(EscalationMatrixService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new EscalationMatrix(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new EscalationMatrix();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
