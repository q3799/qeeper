/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { QeeperTestModule } from '../../../test.module';
import { EscalationMatrixDetailComponent } from 'app/entities/escalation-matrix/escalation-matrix-detail.component';
import { EscalationMatrix } from 'app/shared/model/escalation-matrix.model';

describe('Component Tests', () => {
  describe('EscalationMatrix Management Detail Component', () => {
    let comp: EscalationMatrixDetailComponent;
    let fixture: ComponentFixture<EscalationMatrixDetailComponent>;
    const route = ({ data: of({ escalationMatrix: new EscalationMatrix(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [QeeperTestModule],
        declarations: [EscalationMatrixDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(EscalationMatrixDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(EscalationMatrixDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.escalationMatrix).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
