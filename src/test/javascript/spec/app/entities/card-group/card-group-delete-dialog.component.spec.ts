/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { QeeperTestModule } from '../../../test.module';
import { CardGroupDeleteDialogComponent } from 'app/entities/card-group/card-group-delete-dialog.component';
import { CardGroupService } from 'app/entities/card-group/card-group.service';

describe('Component Tests', () => {
  describe('CardGroup Management Delete Component', () => {
    let comp: CardGroupDeleteDialogComponent;
    let fixture: ComponentFixture<CardGroupDeleteDialogComponent>;
    let service: CardGroupService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [QeeperTestModule],
        declarations: [CardGroupDeleteDialogComponent]
      })
        .overrideTemplate(CardGroupDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CardGroupDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CardGroupService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
