/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { QeeperTestModule } from '../../../test.module';
import { SecretQuestionDetailComponent } from 'app/entities/secret-question/secret-question-detail.component';
import { SecretQuestion } from 'app/shared/model/secret-question.model';

describe('Component Tests', () => {
  describe('SecretQuestion Management Detail Component', () => {
    let comp: SecretQuestionDetailComponent;
    let fixture: ComponentFixture<SecretQuestionDetailComponent>;
    const route = ({ data: of({ secretQuestion: new SecretQuestion(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [QeeperTestModule],
        declarations: [SecretQuestionDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(SecretQuestionDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(SecretQuestionDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.secretQuestion).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
