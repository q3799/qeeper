/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { BankAccountService } from 'app/entities/bank-account/bank-account.service';
import { IBankAccount, BankAccount, BankAccountType, AccountStatus } from 'app/shared/model/bank-account.model';

describe('Service Tests', () => {
  describe('BankAccount Service', () => {
    let injector: TestBed;
    let service: BankAccountService;
    let httpMock: HttpTestingController;
    let elemDefault: IBankAccount;
    let expectedResult;
    let currentDate: moment.Moment;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(BankAccountService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new BankAccount(
        0,
        currentDate,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        BankAccountType.SAVING,
        false,
        0,
        currentDate,
        0,
        false,
        0,
        currentDate,
        'AAAAAAA',
        AccountStatus.ACTIVE,
        false,
        'AAAAAAA',
        'AAAAAAA',
        currentDate,
        'AAAAAAA'
      );
    });

    describe('Service methods', () => {
      it('should find an element', async () => {
        const returnedFromService = Object.assign(
          {
            dateOfOpening: currentDate.format(DATE_TIME_FORMAT),
            balanceCheckedOn: currentDate.format(DATE_TIME_FORMAT),
            passwordUpdatedOn: currentDate.format(DATE_TIME_FORMAT),
            requestedOn: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a BankAccount', async () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            dateOfOpening: currentDate.format(DATE_TIME_FORMAT),
            balanceCheckedOn: currentDate.format(DATE_TIME_FORMAT),
            passwordUpdatedOn: currentDate.format(DATE_TIME_FORMAT),
            requestedOn: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            dateOfOpening: currentDate,
            balanceCheckedOn: currentDate,
            passwordUpdatedOn: currentDate,
            requestedOn: currentDate
          },
          returnedFromService
        );
        service
          .create(new BankAccount(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a BankAccount', async () => {
        const returnedFromService = Object.assign(
          {
            dateOfOpening: currentDate.format(DATE_TIME_FORMAT),
            openedBy: 'BBBBBB',
            customerId: 'BBBBBB',
            accountNumber: 'BBBBBB',
            loginId: 'BBBBBB',
            loginPassword: 'BBBBBB',
            email: 'BBBBBB',
            phone: 'BBBBBB',
            bankAccountType: 'BBBBBB',
            chequeBookIssued: true,
            accountBalance: 1,
            balanceCheckedOn: currentDate.format(DATE_TIME_FORMAT),
            minimumBalance: 1,
            passwordGetsExpired: true,
            expirationPeriodDays: 1,
            passwordUpdatedOn: currentDate.format(DATE_TIME_FORMAT),
            remarks: 'BBBBBB',
            accountStatus: 'BBBBBB',
            multipleSessionAllowed: true,
            projectName: 'BBBBBB',
            projectCode: 'BBBBBB',
            requestedOn: currentDate.format(DATE_TIME_FORMAT),
            requestedBy: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            dateOfOpening: currentDate,
            balanceCheckedOn: currentDate,
            passwordUpdatedOn: currentDate,
            requestedOn: currentDate
          },
          returnedFromService
        );
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of BankAccount', async () => {
        const returnedFromService = Object.assign(
          {
            dateOfOpening: currentDate.format(DATE_TIME_FORMAT),
            openedBy: 'BBBBBB',
            customerId: 'BBBBBB',
            accountNumber: 'BBBBBB',
            loginId: 'BBBBBB',
            loginPassword: 'BBBBBB',
            email: 'BBBBBB',
            phone: 'BBBBBB',
            bankAccountType: 'BBBBBB',
            chequeBookIssued: true,
            accountBalance: 1,
            balanceCheckedOn: currentDate.format(DATE_TIME_FORMAT),
            minimumBalance: 1,
            passwordGetsExpired: true,
            expirationPeriodDays: 1,
            passwordUpdatedOn: currentDate.format(DATE_TIME_FORMAT),
            remarks: 'BBBBBB',
            accountStatus: 'BBBBBB',
            multipleSessionAllowed: true,
            projectName: 'BBBBBB',
            projectCode: 'BBBBBB',
            requestedOn: currentDate.format(DATE_TIME_FORMAT),
            requestedBy: 'BBBBBB'
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            dateOfOpening: currentDate,
            balanceCheckedOn: currentDate,
            passwordUpdatedOn: currentDate,
            requestedOn: currentDate
          },
          returnedFromService
        );
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a BankAccount', async () => {
        const rxPromise = service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
