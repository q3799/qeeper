/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { QeeperTestModule } from '../../../test.module';
import { NonBankAccountDeleteDialogComponent } from 'app/entities/non-bank-account/non-bank-account-delete-dialog.component';
import { NonBankAccountService } from 'app/entities/non-bank-account/non-bank-account.service';

describe('Component Tests', () => {
  describe('NonBankAccount Management Delete Component', () => {
    let comp: NonBankAccountDeleteDialogComponent;
    let fixture: ComponentFixture<NonBankAccountDeleteDialogComponent>;
    let service: NonBankAccountService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [QeeperTestModule],
        declarations: [NonBankAccountDeleteDialogComponent]
      })
        .overrideTemplate(NonBankAccountDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(NonBankAccountDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(NonBankAccountService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
