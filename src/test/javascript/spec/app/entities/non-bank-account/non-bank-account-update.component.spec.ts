/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { QeeperTestModule } from '../../../test.module';
import { NonBankAccountUpdateComponent } from 'app/entities/non-bank-account/non-bank-account-update.component';
import { NonBankAccountService } from 'app/entities/non-bank-account/non-bank-account.service';
import { NonBankAccount } from 'app/shared/model/non-bank-account.model';

describe('Component Tests', () => {
  describe('NonBankAccount Management Update Component', () => {
    let comp: NonBankAccountUpdateComponent;
    let fixture: ComponentFixture<NonBankAccountUpdateComponent>;
    let service: NonBankAccountService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [QeeperTestModule],
        declarations: [NonBankAccountUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(NonBankAccountUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(NonBankAccountUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(NonBankAccountService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new NonBankAccount(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new NonBankAccount();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
