/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { NonBankAccountService } from 'app/entities/non-bank-account/non-bank-account.service';
import { INonBankAccount, NonBankAccount, AccountStatus } from 'app/shared/model/non-bank-account.model';

describe('Service Tests', () => {
  describe('NonBankAccount Service', () => {
    let injector: TestBed;
    let service: NonBankAccountService;
    let httpMock: HttpTestingController;
    let elemDefault: INonBankAccount;
    let expectedResult;
    let currentDate: moment.Moment;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(NonBankAccountService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new NonBankAccount(
        0,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        false,
        currentDate,
        0,
        AccountStatus.ACTIVE,
        'AAAAAAA',
        false,
        'AAAAAAA'
      );
    });

    describe('Service methods', () => {
      it('should find an element', async () => {
        const returnedFromService = Object.assign(
          {
            passwordUpdatedOn: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a NonBankAccount', async () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            passwordUpdatedOn: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            passwordUpdatedOn: currentDate
          },
          returnedFromService
        );
        service
          .create(new NonBankAccount(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a NonBankAccount', async () => {
        const returnedFromService = Object.assign(
          {
            loginId: 'BBBBBB',
            loginPassword: 'BBBBBB',
            name: 'BBBBBB',
            email: 'BBBBBB',
            phone: 'BBBBBB',
            passwordGetsExpired: true,
            passwordUpdatedOn: currentDate.format(DATE_TIME_FORMAT),
            expirationPeriodDays: 1,
            accountStatus: 'BBBBBB',
            remarks: 'BBBBBB',
            multipleSessionAllowed: true,
            accountId: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            passwordUpdatedOn: currentDate
          },
          returnedFromService
        );
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of NonBankAccount', async () => {
        const returnedFromService = Object.assign(
          {
            loginId: 'BBBBBB',
            loginPassword: 'BBBBBB',
            name: 'BBBBBB',
            email: 'BBBBBB',
            phone: 'BBBBBB',
            passwordGetsExpired: true,
            passwordUpdatedOn: currentDate.format(DATE_TIME_FORMAT),
            expirationPeriodDays: 1,
            accountStatus: 'BBBBBB',
            remarks: 'BBBBBB',
            multipleSessionAllowed: true,
            accountId: 'BBBBBB'
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            passwordUpdatedOn: currentDate
          },
          returnedFromService
        );
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a NonBankAccount', async () => {
        const rxPromise = service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
