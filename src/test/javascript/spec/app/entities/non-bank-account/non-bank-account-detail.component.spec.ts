/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { QeeperTestModule } from '../../../test.module';
import { NonBankAccountDetailComponent } from 'app/entities/non-bank-account/non-bank-account-detail.component';
import { NonBankAccount } from 'app/shared/model/non-bank-account.model';

describe('Component Tests', () => {
  describe('NonBankAccount Management Detail Component', () => {
    let comp: NonBankAccountDetailComponent;
    let fixture: ComponentFixture<NonBankAccountDetailComponent>;
    const route = ({ data: of({ nonBankAccount: new NonBankAccount(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [QeeperTestModule],
        declarations: [NonBankAccountDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(NonBankAccountDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(NonBankAccountDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.nonBankAccount).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
