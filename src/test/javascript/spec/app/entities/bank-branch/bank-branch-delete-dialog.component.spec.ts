/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { QeeperTestModule } from '../../../test.module';
import { BankBranchDeleteDialogComponent } from 'app/entities/bank-branch/bank-branch-delete-dialog.component';
import { BankBranchService } from 'app/entities/bank-branch/bank-branch.service';

describe('Component Tests', () => {
  describe('BankBranch Management Delete Component', () => {
    let comp: BankBranchDeleteDialogComponent;
    let fixture: ComponentFixture<BankBranchDeleteDialogComponent>;
    let service: BankBranchService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [QeeperTestModule],
        declarations: [BankBranchDeleteDialogComponent]
      })
        .overrideTemplate(BankBranchDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BankBranchDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BankBranchService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
