/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { QeeperTestModule } from '../../../test.module';
import { BankBranchUpdateComponent } from 'app/entities/bank-branch/bank-branch-update.component';
import { BankBranchService } from 'app/entities/bank-branch/bank-branch.service';
import { BankBranch } from 'app/shared/model/bank-branch.model';

describe('Component Tests', () => {
  describe('BankBranch Management Update Component', () => {
    let comp: BankBranchUpdateComponent;
    let fixture: ComponentFixture<BankBranchUpdateComponent>;
    let service: BankBranchService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [QeeperTestModule],
        declarations: [BankBranchUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(BankBranchUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BankBranchUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BankBranchService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new BankBranch(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new BankBranch();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
