/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { QeeperTestModule } from '../../../test.module';
import { BankBranchDetailComponent } from 'app/entities/bank-branch/bank-branch-detail.component';
import { BankBranch } from 'app/shared/model/bank-branch.model';

describe('Component Tests', () => {
  describe('BankBranch Management Detail Component', () => {
    let comp: BankBranchDetailComponent;
    let fixture: ComponentFixture<BankBranchDetailComponent>;
    const route = ({ data: of({ bankBranch: new BankBranch(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [QeeperTestModule],
        declarations: [BankBranchDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(BankBranchDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BankBranchDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.bankBranch).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
