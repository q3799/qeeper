/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { QeeperTestModule } from '../../../test.module';
import { NonBankClientDeleteDialogComponent } from 'app/entities/non-bank-client/non-bank-client-delete-dialog.component';
import { NonBankClientService } from 'app/entities/non-bank-client/non-bank-client.service';

describe('Component Tests', () => {
  describe('NonBankClient Management Delete Component', () => {
    let comp: NonBankClientDeleteDialogComponent;
    let fixture: ComponentFixture<NonBankClientDeleteDialogComponent>;
    let service: NonBankClientService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [QeeperTestModule],
        declarations: [NonBankClientDeleteDialogComponent]
      })
        .overrideTemplate(NonBankClientDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(NonBankClientDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(NonBankClientService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
