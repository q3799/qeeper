/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { QeeperTestModule } from '../../../test.module';
import { NonBankClientUpdateComponent } from 'app/entities/non-bank-client/non-bank-client-update.component';
import { NonBankClientService } from 'app/entities/non-bank-client/non-bank-client.service';
import { NonBankClient } from 'app/shared/model/non-bank-client.model';

describe('Component Tests', () => {
  describe('NonBankClient Management Update Component', () => {
    let comp: NonBankClientUpdateComponent;
    let fixture: ComponentFixture<NonBankClientUpdateComponent>;
    let service: NonBankClientService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [QeeperTestModule],
        declarations: [NonBankClientUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(NonBankClientUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(NonBankClientUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(NonBankClientService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new NonBankClient(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new NonBankClient();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
