/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { QeeperTestModule } from '../../../test.module';
import { NonBankClientDetailComponent } from 'app/entities/non-bank-client/non-bank-client-detail.component';
import { NonBankClient } from 'app/shared/model/non-bank-client.model';

describe('Component Tests', () => {
  describe('NonBankClient Management Detail Component', () => {
    let comp: NonBankClientDetailComponent;
    let fixture: ComponentFixture<NonBankClientDetailComponent>;
    const route = ({ data: of({ nonBankClient: new NonBankClient(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [QeeperTestModule],
        declarations: [NonBankClientDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(NonBankClientDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(NonBankClientDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.nonBankClient).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
