/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { QeeperTestModule } from '../../../test.module';
import { NonBankAccountGroupUpdateComponent } from 'app/entities/non-bank-account-group/non-bank-account-group-update.component';
import { NonBankAccountGroupService } from 'app/entities/non-bank-account-group/non-bank-account-group.service';
import { NonBankAccountGroup } from 'app/shared/model/non-bank-account-group.model';

describe('Component Tests', () => {
  describe('NonBankAccountGroup Management Update Component', () => {
    let comp: NonBankAccountGroupUpdateComponent;
    let fixture: ComponentFixture<NonBankAccountGroupUpdateComponent>;
    let service: NonBankAccountGroupService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [QeeperTestModule],
        declarations: [NonBankAccountGroupUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(NonBankAccountGroupUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(NonBankAccountGroupUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(NonBankAccountGroupService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new NonBankAccountGroup(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new NonBankAccountGroup();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
