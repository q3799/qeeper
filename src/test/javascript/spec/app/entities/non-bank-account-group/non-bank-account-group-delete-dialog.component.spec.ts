/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { QeeperTestModule } from '../../../test.module';
import { NonBankAccountGroupDeleteDialogComponent } from 'app/entities/non-bank-account-group/non-bank-account-group-delete-dialog.component';
import { NonBankAccountGroupService } from 'app/entities/non-bank-account-group/non-bank-account-group.service';

describe('Component Tests', () => {
  describe('NonBankAccountGroup Management Delete Component', () => {
    let comp: NonBankAccountGroupDeleteDialogComponent;
    let fixture: ComponentFixture<NonBankAccountGroupDeleteDialogComponent>;
    let service: NonBankAccountGroupService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [QeeperTestModule],
        declarations: [NonBankAccountGroupDeleteDialogComponent]
      })
        .overrideTemplate(NonBankAccountGroupDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(NonBankAccountGroupDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(NonBankAccountGroupService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
