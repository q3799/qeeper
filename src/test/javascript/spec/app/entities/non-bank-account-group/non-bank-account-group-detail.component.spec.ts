/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { QeeperTestModule } from '../../../test.module';
import { NonBankAccountGroupDetailComponent } from 'app/entities/non-bank-account-group/non-bank-account-group-detail.component';
import { NonBankAccountGroup } from 'app/shared/model/non-bank-account-group.model';

describe('Component Tests', () => {
  describe('NonBankAccountGroup Management Detail Component', () => {
    let comp: NonBankAccountGroupDetailComponent;
    let fixture: ComponentFixture<NonBankAccountGroupDetailComponent>;
    const route = ({ data: of({ nonBankAccountGroup: new NonBankAccountGroup(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [QeeperTestModule],
        declarations: [NonBankAccountGroupDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(NonBankAccountGroupDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(NonBankAccountGroupDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.nonBankAccountGroup).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
