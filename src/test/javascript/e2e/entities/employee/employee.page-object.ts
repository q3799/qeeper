import { browser, ExpectedConditions, element, by, ElementFinder } from 'protractor';

export class EmployeeComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-employee div table .btn-danger'));
  title = element.all(by.css('jhi-employee div h2#page-heading span')).first();

  async clickOnCreateButton(timeout?: number) {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(timeout?: number) {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class EmployeeUpdatePage {
  pageTitle = element(by.id('jhi-employee-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  nameInput = element(by.id('field_name'));
  panInput = element(by.id('field_pan'));
  dateOfBirthInput = element(by.id('field_dateOfBirth'));
  aadhaarInput = element(by.id('field_aadhaar'));
  emailInput = element(by.id('field_email'));
  phoneInput = element(by.id('field_phone'));
  communicationAddressInput = element(by.id('field_communicationAddress'));
  permanentAddressInput = element(by.id('field_permanentAddress'));
  remarksInput = element(by.id('field_remarks'));
  activeInput = element(by.id('field_active'));

  async getPageTitle() {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setNameInput(name) {
    await this.nameInput.sendKeys(name);
  }

  async getNameInput() {
    return await this.nameInput.getAttribute('value');
  }

  async setPanInput(pan) {
    await this.panInput.sendKeys(pan);
  }

  async getPanInput() {
    return await this.panInput.getAttribute('value');
  }

  async setDateOfBirthInput(dateOfBirth) {
    await this.dateOfBirthInput.sendKeys(dateOfBirth);
  }

  async getDateOfBirthInput() {
    return await this.dateOfBirthInput.getAttribute('value');
  }

  async setAadhaarInput(aadhaar) {
    await this.aadhaarInput.sendKeys(aadhaar);
  }

  async getAadhaarInput() {
    return await this.aadhaarInput.getAttribute('value');
  }

  async setEmailInput(email) {
    await this.emailInput.sendKeys(email);
  }

  async getEmailInput() {
    return await this.emailInput.getAttribute('value');
  }

  async setPhoneInput(phone) {
    await this.phoneInput.sendKeys(phone);
  }

  async getPhoneInput() {
    return await this.phoneInput.getAttribute('value');
  }

  async setCommunicationAddressInput(communicationAddress) {
    await this.communicationAddressInput.sendKeys(communicationAddress);
  }

  async getCommunicationAddressInput() {
    return await this.communicationAddressInput.getAttribute('value');
  }

  async setPermanentAddressInput(permanentAddress) {
    await this.permanentAddressInput.sendKeys(permanentAddress);
  }

  async getPermanentAddressInput() {
    return await this.permanentAddressInput.getAttribute('value');
  }

  async setRemarksInput(remarks) {
    await this.remarksInput.sendKeys(remarks);
  }

  async getRemarksInput() {
    return await this.remarksInput.getAttribute('value');
  }

  getActiveInput(timeout?: number) {
    return this.activeInput;
  }
  async save(timeout?: number) {
    await this.saveButton.click();
  }

  async cancel(timeout?: number) {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class EmployeeDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-employee-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-employee'));

  async getDialogTitle() {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(timeout?: number) {
    await this.confirmButton.click();
  }
}
