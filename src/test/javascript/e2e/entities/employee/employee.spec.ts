/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { EmployeeComponentsPage, EmployeeDeleteDialog, EmployeeUpdatePage } from './employee.page-object';

const expect = chai.expect;

describe('Employee e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let employeeUpdatePage: EmployeeUpdatePage;
  let employeeComponentsPage: EmployeeComponentsPage;
  let employeeDeleteDialog: EmployeeDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Employees', async () => {
    await navBarPage.goToEntity('employee');
    employeeComponentsPage = new EmployeeComponentsPage();
    await browser.wait(ec.visibilityOf(employeeComponentsPage.title), 5000);
    expect(await employeeComponentsPage.getTitle()).to.eq('qeeperApp.employee.home.title');
  });

  it('should load create Employee page', async () => {
    await employeeComponentsPage.clickOnCreateButton();
    employeeUpdatePage = new EmployeeUpdatePage();
    expect(await employeeUpdatePage.getPageTitle()).to.eq('qeeperApp.employee.home.createOrEditLabel');
    await employeeUpdatePage.cancel();
  });

  it('should create and save Employees', async () => {
    const nbButtonsBeforeCreate = await employeeComponentsPage.countDeleteButtons();

    await employeeComponentsPage.clickOnCreateButton();
    await promise.all([
      employeeUpdatePage.setNameInput('name'),
      employeeUpdatePage.setPanInput('pan'),
      employeeUpdatePage.setDateOfBirthInput('dateOfBirth'),
      employeeUpdatePage.setAadhaarInput('aadhaar'),
      employeeUpdatePage.setEmailInput('email'),
      employeeUpdatePage.setPhoneInput('phone'),
      employeeUpdatePage.setCommunicationAddressInput('communicationAddress'),
      employeeUpdatePage.setPermanentAddressInput('permanentAddress'),
      employeeUpdatePage.setRemarksInput('remarks')
    ]);
    expect(await employeeUpdatePage.getNameInput()).to.eq('name', 'Expected Name value to be equals to name');
    expect(await employeeUpdatePage.getPanInput()).to.eq('pan', 'Expected Pan value to be equals to pan');
    expect(await employeeUpdatePage.getDateOfBirthInput()).to.eq('dateOfBirth', 'Expected DateOfBirth value to be equals to dateOfBirth');
    expect(await employeeUpdatePage.getAadhaarInput()).to.eq('aadhaar', 'Expected Aadhaar value to be equals to aadhaar');
    expect(await employeeUpdatePage.getEmailInput()).to.eq('email', 'Expected Email value to be equals to email');
    expect(await employeeUpdatePage.getPhoneInput()).to.eq('phone', 'Expected Phone value to be equals to phone');
    expect(await employeeUpdatePage.getCommunicationAddressInput()).to.eq(
      'communicationAddress',
      'Expected CommunicationAddress value to be equals to communicationAddress'
    );
    expect(await employeeUpdatePage.getPermanentAddressInput()).to.eq(
      'permanentAddress',
      'Expected PermanentAddress value to be equals to permanentAddress'
    );
    expect(await employeeUpdatePage.getRemarksInput()).to.eq('remarks', 'Expected Remarks value to be equals to remarks');
    const selectedActive = employeeUpdatePage.getActiveInput();
    if (await selectedActive.isSelected()) {
      await employeeUpdatePage.getActiveInput().click();
      expect(await employeeUpdatePage.getActiveInput().isSelected(), 'Expected active not to be selected').to.be.false;
    } else {
      await employeeUpdatePage.getActiveInput().click();
      expect(await employeeUpdatePage.getActiveInput().isSelected(), 'Expected active to be selected').to.be.true;
    }
    await employeeUpdatePage.save();
    expect(await employeeUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await employeeComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Employee', async () => {
    const nbButtonsBeforeDelete = await employeeComponentsPage.countDeleteButtons();
    await employeeComponentsPage.clickOnLastDeleteButton();

    employeeDeleteDialog = new EmployeeDeleteDialog();
    expect(await employeeDeleteDialog.getDialogTitle()).to.eq('qeeperApp.employee.delete.question');
    await employeeDeleteDialog.clickOnConfirmButton();

    expect(await employeeComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
