/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { BankAccountGroupComponentsPage, BankAccountGroupDeleteDialog, BankAccountGroupUpdatePage } from './bank-account-group.page-object';

const expect = chai.expect;

describe('BankAccountGroup e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let bankAccountGroupUpdatePage: BankAccountGroupUpdatePage;
  let bankAccountGroupComponentsPage: BankAccountGroupComponentsPage;
  let bankAccountGroupDeleteDialog: BankAccountGroupDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load BankAccountGroups', async () => {
    await navBarPage.goToEntity('bank-account-group');
    bankAccountGroupComponentsPage = new BankAccountGroupComponentsPage();
    await browser.wait(ec.visibilityOf(bankAccountGroupComponentsPage.title), 5000);
    expect(await bankAccountGroupComponentsPage.getTitle()).to.eq('qeeperApp.bankAccountGroup.home.title');
  });

  it('should load create BankAccountGroup page', async () => {
    await bankAccountGroupComponentsPage.clickOnCreateButton();
    bankAccountGroupUpdatePage = new BankAccountGroupUpdatePage();
    expect(await bankAccountGroupUpdatePage.getPageTitle()).to.eq('qeeperApp.bankAccountGroup.home.createOrEditLabel');
    await bankAccountGroupUpdatePage.cancel();
  });

  it('should create and save BankAccountGroups', async () => {
    const nbButtonsBeforeCreate = await bankAccountGroupComponentsPage.countDeleteButtons();

    await bankAccountGroupComponentsPage.clickOnCreateButton();
    await promise.all([bankAccountGroupUpdatePage.setCodeInput('code'), bankAccountGroupUpdatePage.setNameInput('name')]);
    expect(await bankAccountGroupUpdatePage.getCodeInput()).to.eq('code', 'Expected Code value to be equals to code');
    expect(await bankAccountGroupUpdatePage.getNameInput()).to.eq('name', 'Expected Name value to be equals to name');
    await bankAccountGroupUpdatePage.save();
    expect(await bankAccountGroupUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await bankAccountGroupComponentsPage.countDeleteButtons()).to.eq(
      nbButtonsBeforeCreate + 1,
      'Expected one more entry in the table'
    );
  });

  it('should delete last BankAccountGroup', async () => {
    const nbButtonsBeforeDelete = await bankAccountGroupComponentsPage.countDeleteButtons();
    await bankAccountGroupComponentsPage.clickOnLastDeleteButton();

    bankAccountGroupDeleteDialog = new BankAccountGroupDeleteDialog();
    expect(await bankAccountGroupDeleteDialog.getDialogTitle()).to.eq('qeeperApp.bankAccountGroup.delete.question');
    await bankAccountGroupDeleteDialog.clickOnConfirmButton();

    expect(await bankAccountGroupComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
