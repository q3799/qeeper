/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { EscalationMatrixComponentsPage, EscalationMatrixDeleteDialog, EscalationMatrixUpdatePage } from './escalation-matrix.page-object';

const expect = chai.expect;

describe('EscalationMatrix e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let escalationMatrixUpdatePage: EscalationMatrixUpdatePage;
  let escalationMatrixComponentsPage: EscalationMatrixComponentsPage;
  /*let escalationMatrixDeleteDialog: EscalationMatrixDeleteDialog;*/

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load EscalationMatrices', async () => {
    await navBarPage.goToEntity('escalation-matrix');
    escalationMatrixComponentsPage = new EscalationMatrixComponentsPage();
    await browser.wait(ec.visibilityOf(escalationMatrixComponentsPage.title), 5000);
    expect(await escalationMatrixComponentsPage.getTitle()).to.eq('qeeperApp.escalationMatrix.home.title');
  });

  it('should load create EscalationMatrix page', async () => {
    await escalationMatrixComponentsPage.clickOnCreateButton();
    escalationMatrixUpdatePage = new EscalationMatrixUpdatePage();
    expect(await escalationMatrixUpdatePage.getPageTitle()).to.eq('qeeperApp.escalationMatrix.home.createOrEditLabel');
    await escalationMatrixUpdatePage.cancel();
  });

  /* it('should create and save EscalationMatrices', async () => {
        const nbButtonsBeforeCreate = await escalationMatrixComponentsPage.countDeleteButtons();

        await escalationMatrixComponentsPage.clickOnCreateButton();
        await promise.all([
            escalationMatrixUpdatePage.escalationTypeSelectLastOption(),
            escalationMatrixUpdatePage.setLevelInput('5'),
            escalationMatrixUpdatePage.contactPersonSelectLastOption(),
        ]);
        expect(await escalationMatrixUpdatePage.getLevelInput()).to.eq('5', 'Expected level value to be equals to 5');
        const selectedReceiveSms = escalationMatrixUpdatePage.getReceiveSmsInput();
        if (await selectedReceiveSms.isSelected()) {
            await escalationMatrixUpdatePage.getReceiveSmsInput().click();
            expect(await escalationMatrixUpdatePage.getReceiveSmsInput().isSelected(), 'Expected receiveSms not to be selected').to.be.false;
        } else {
            await escalationMatrixUpdatePage.getReceiveSmsInput().click();
            expect(await escalationMatrixUpdatePage.getReceiveSmsInput().isSelected(), 'Expected receiveSms to be selected').to.be.true;
        }
        const selectedReceiveEmail = escalationMatrixUpdatePage.getReceiveEmailInput();
        if (await selectedReceiveEmail.isSelected()) {
            await escalationMatrixUpdatePage.getReceiveEmailInput().click();
            expect(await escalationMatrixUpdatePage.getReceiveEmailInput().isSelected(), 'Expected receiveEmail not to be selected').to.be.false;
        } else {
            await escalationMatrixUpdatePage.getReceiveEmailInput().click();
            expect(await escalationMatrixUpdatePage.getReceiveEmailInput().isSelected(), 'Expected receiveEmail to be selected').to.be.true;
        }
        await escalationMatrixUpdatePage.save();
        expect(await escalationMatrixUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

        expect(await escalationMatrixComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
    });*/

  /* it('should delete last EscalationMatrix', async () => {
        const nbButtonsBeforeDelete = await escalationMatrixComponentsPage.countDeleteButtons();
        await escalationMatrixComponentsPage.clickOnLastDeleteButton();

        escalationMatrixDeleteDialog = new EscalationMatrixDeleteDialog();
        expect(await escalationMatrixDeleteDialog.getDialogTitle())
            .to.eq('qeeperApp.escalationMatrix.delete.question');
        await escalationMatrixDeleteDialog.clickOnConfirmButton();

        expect(await escalationMatrixComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });*/

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
