import { browser, ExpectedConditions, element, by, ElementFinder } from 'protractor';

export class EscalationMatrixComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-escalation-matrix div table .btn-danger'));
  title = element.all(by.css('jhi-escalation-matrix div h2#page-heading span')).first();

  async clickOnCreateButton(timeout?: number) {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(timeout?: number) {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class EscalationMatrixUpdatePage {
  pageTitle = element(by.id('jhi-escalation-matrix-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  escalationTypeSelect = element(by.id('field_escalationType'));
  levelInput = element(by.id('field_level'));
  receiveSmsInput = element(by.id('field_receiveSms'));
  receiveEmailInput = element(by.id('field_receiveEmail'));
  contactPersonSelect = element(by.id('field_contactPerson'));

  async getPageTitle() {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setEscalationTypeSelect(escalationType) {
    await this.escalationTypeSelect.sendKeys(escalationType);
  }

  async getEscalationTypeSelect() {
    return await this.escalationTypeSelect.element(by.css('option:checked')).getText();
  }

  async escalationTypeSelectLastOption(timeout?: number) {
    await this.escalationTypeSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async setLevelInput(level) {
    await this.levelInput.sendKeys(level);
  }

  async getLevelInput() {
    return await this.levelInput.getAttribute('value');
  }

  getReceiveSmsInput(timeout?: number) {
    return this.receiveSmsInput;
  }
  getReceiveEmailInput(timeout?: number) {
    return this.receiveEmailInput;
  }

  async contactPersonSelectLastOption(timeout?: number) {
    await this.contactPersonSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async contactPersonSelectOption(option) {
    await this.contactPersonSelect.sendKeys(option);
  }

  getContactPersonSelect(): ElementFinder {
    return this.contactPersonSelect;
  }

  async getContactPersonSelectedOption() {
    return await this.contactPersonSelect.element(by.css('option:checked')).getText();
  }

  async save(timeout?: number) {
    await this.saveButton.click();
  }

  async cancel(timeout?: number) {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class EscalationMatrixDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-escalationMatrix-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-escalationMatrix'));

  async getDialogTitle() {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(timeout?: number) {
    await this.confirmButton.click();
  }
}
