/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { NonBankClientComponentsPage, NonBankClientDeleteDialog, NonBankClientUpdatePage } from './non-bank-client.page-object';

const expect = chai.expect;

describe('NonBankClient e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let nonBankClientUpdatePage: NonBankClientUpdatePage;
  let nonBankClientComponentsPage: NonBankClientComponentsPage;
  let nonBankClientDeleteDialog: NonBankClientDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load NonBankClients', async () => {
    await navBarPage.goToEntity('non-bank-client');
    nonBankClientComponentsPage = new NonBankClientComponentsPage();
    await browser.wait(ec.visibilityOf(nonBankClientComponentsPage.title), 5000);
    expect(await nonBankClientComponentsPage.getTitle()).to.eq('qeeperApp.nonBankClient.home.title');
  });

  it('should load create NonBankClient page', async () => {
    await nonBankClientComponentsPage.clickOnCreateButton();
    nonBankClientUpdatePage = new NonBankClientUpdatePage();
    expect(await nonBankClientUpdatePage.getPageTitle()).to.eq('qeeperApp.nonBankClient.home.createOrEditLabel');
    await nonBankClientUpdatePage.cancel();
  });

  it('should create and save NonBankClients', async () => {
    const nbButtonsBeforeCreate = await nonBankClientComponentsPage.countDeleteButtons();

    await nonBankClientComponentsPage.clickOnCreateButton();
    await promise.all([nonBankClientUpdatePage.setNameInput('name'), nonBankClientUpdatePage.setUrlInput('url')]);
    expect(await nonBankClientUpdatePage.getNameInput()).to.eq('name', 'Expected Name value to be equals to name');
    expect(await nonBankClientUpdatePage.getUrlInput()).to.eq('url', 'Expected Url value to be equals to url');
    await nonBankClientUpdatePage.save();
    expect(await nonBankClientUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await nonBankClientComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last NonBankClient', async () => {
    const nbButtonsBeforeDelete = await nonBankClientComponentsPage.countDeleteButtons();
    await nonBankClientComponentsPage.clickOnLastDeleteButton();

    nonBankClientDeleteDialog = new NonBankClientDeleteDialog();
    expect(await nonBankClientDeleteDialog.getDialogTitle()).to.eq('qeeperApp.nonBankClient.delete.question');
    await nonBankClientDeleteDialog.clickOnConfirmButton();

    expect(await nonBankClientComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
