/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { SecretQuestionComponentsPage, SecretQuestionDeleteDialog, SecretQuestionUpdatePage } from './secret-question.page-object';

const expect = chai.expect;

describe('SecretQuestion e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let secretQuestionUpdatePage: SecretQuestionUpdatePage;
  let secretQuestionComponentsPage: SecretQuestionComponentsPage;
  /*let secretQuestionDeleteDialog: SecretQuestionDeleteDialog;*/

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load SecretQuestions', async () => {
    await navBarPage.goToEntity('secret-question');
    secretQuestionComponentsPage = new SecretQuestionComponentsPage();
    await browser.wait(ec.visibilityOf(secretQuestionComponentsPage.title), 5000);
    expect(await secretQuestionComponentsPage.getTitle()).to.eq('qeeperApp.secretQuestion.home.title');
  });

  it('should load create SecretQuestion page', async () => {
    await secretQuestionComponentsPage.clickOnCreateButton();
    secretQuestionUpdatePage = new SecretQuestionUpdatePage();
    expect(await secretQuestionUpdatePage.getPageTitle()).to.eq('qeeperApp.secretQuestion.home.createOrEditLabel');
    await secretQuestionUpdatePage.cancel();
  });

  /* it('should create and save SecretQuestions', async () => {
        const nbButtonsBeforeCreate = await secretQuestionComponentsPage.countDeleteButtons();

        await secretQuestionComponentsPage.clickOnCreateButton();
        await promise.all([
            secretQuestionUpdatePage.setQuestionInput('question'),
            secretQuestionUpdatePage.setAnswerInput('answer'),
            secretQuestionUpdatePage.bankAccountSelectLastOption(),
        ]);
        expect(await secretQuestionUpdatePage.getQuestionInput()).to.eq('question', 'Expected Question value to be equals to question');
        expect(await secretQuestionUpdatePage.getAnswerInput()).to.eq('answer', 'Expected Answer value to be equals to answer');
        await secretQuestionUpdatePage.save();
        expect(await secretQuestionUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

        expect(await secretQuestionComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
    });*/

  /* it('should delete last SecretQuestion', async () => {
        const nbButtonsBeforeDelete = await secretQuestionComponentsPage.countDeleteButtons();
        await secretQuestionComponentsPage.clickOnLastDeleteButton();

        secretQuestionDeleteDialog = new SecretQuestionDeleteDialog();
        expect(await secretQuestionDeleteDialog.getDialogTitle())
            .to.eq('qeeperApp.secretQuestion.delete.question');
        await secretQuestionDeleteDialog.clickOnConfirmButton();

        expect(await secretQuestionComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });*/

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
