import { browser, ExpectedConditions, element, by, ElementFinder } from 'protractor';

export class SecretQuestionComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-secret-question div table .btn-danger'));
  title = element.all(by.css('jhi-secret-question div h2#page-heading span')).first();

  async clickOnCreateButton(timeout?: number) {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(timeout?: number) {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class SecretQuestionUpdatePage {
  pageTitle = element(by.id('jhi-secret-question-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  questionInput = element(by.id('field_question'));
  answerInput = element(by.id('field_answer'));
  bankAccountSelect = element(by.id('field_bankAccount'));

  async getPageTitle() {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setQuestionInput(question) {
    await this.questionInput.sendKeys(question);
  }

  async getQuestionInput() {
    return await this.questionInput.getAttribute('value');
  }

  async setAnswerInput(answer) {
    await this.answerInput.sendKeys(answer);
  }

  async getAnswerInput() {
    return await this.answerInput.getAttribute('value');
  }

  async bankAccountSelectLastOption(timeout?: number) {
    await this.bankAccountSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async bankAccountSelectOption(option) {
    await this.bankAccountSelect.sendKeys(option);
  }

  getBankAccountSelect(): ElementFinder {
    return this.bankAccountSelect;
  }

  async getBankAccountSelectedOption() {
    return await this.bankAccountSelect.element(by.css('option:checked')).getText();
  }

  async save(timeout?: number) {
    await this.saveButton.click();
  }

  async cancel(timeout?: number) {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class SecretQuestionDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-secretQuestion-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-secretQuestion'));

  async getDialogTitle() {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(timeout?: number) {
    await this.confirmButton.click();
  }
}
