/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { BankComponentsPage, BankDeleteDialog, BankUpdatePage } from './bank.page-object';

const expect = chai.expect;

describe('Bank e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let bankUpdatePage: BankUpdatePage;
  let bankComponentsPage: BankComponentsPage;
  let bankDeleteDialog: BankDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Banks', async () => {
    await navBarPage.goToEntity('bank');
    bankComponentsPage = new BankComponentsPage();
    await browser.wait(ec.visibilityOf(bankComponentsPage.title), 5000);
    expect(await bankComponentsPage.getTitle()).to.eq('qeeperApp.bank.home.title');
  });

  it('should load create Bank page', async () => {
    await bankComponentsPage.clickOnCreateButton();
    bankUpdatePage = new BankUpdatePage();
    expect(await bankUpdatePage.getPageTitle()).to.eq('qeeperApp.bank.home.createOrEditLabel');
    await bankUpdatePage.cancel();
  });

  it('should create and save Banks', async () => {
    const nbButtonsBeforeCreate = await bankComponentsPage.countDeleteButtons();

    await bankComponentsPage.clickOnCreateButton();
    await promise.all([bankUpdatePage.setNameInput('name'), bankUpdatePage.setUrlInput('url')]);
    expect(await bankUpdatePage.getNameInput()).to.eq('name', 'Expected Name value to be equals to name');
    expect(await bankUpdatePage.getUrlInput()).to.eq('url', 'Expected Url value to be equals to url');
    await bankUpdatePage.save();
    expect(await bankUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await bankComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Bank', async () => {
    const nbButtonsBeforeDelete = await bankComponentsPage.countDeleteButtons();
    await bankComponentsPage.clickOnLastDeleteButton();

    bankDeleteDialog = new BankDeleteDialog();
    expect(await bankDeleteDialog.getDialogTitle()).to.eq('qeeperApp.bank.delete.question');
    await bankDeleteDialog.clickOnConfirmButton();

    expect(await bankComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
