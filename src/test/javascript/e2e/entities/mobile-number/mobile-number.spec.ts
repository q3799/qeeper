/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { MobileNumberComponentsPage, MobileNumberDeleteDialog, MobileNumberUpdatePage } from './mobile-number.page-object';

const expect = chai.expect;

describe('MobileNumber e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let mobileNumberUpdatePage: MobileNumberUpdatePage;
  let mobileNumberComponentsPage: MobileNumberComponentsPage;
  /*let mobileNumberDeleteDialog: MobileNumberDeleteDialog;*/

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load MobileNumbers', async () => {
    await navBarPage.goToEntity('mobile-number');
    mobileNumberComponentsPage = new MobileNumberComponentsPage();
    await browser.wait(ec.visibilityOf(mobileNumberComponentsPage.title), 5000);
    expect(await mobileNumberComponentsPage.getTitle()).to.eq('qeeperApp.mobileNumber.home.title');
  });

  it('should load create MobileNumber page', async () => {
    await mobileNumberComponentsPage.clickOnCreateButton();
    mobileNumberUpdatePage = new MobileNumberUpdatePage();
    expect(await mobileNumberUpdatePage.getPageTitle()).to.eq('qeeperApp.mobileNumber.home.createOrEditLabel');
    await mobileNumberUpdatePage.cancel();
  });

  /* it('should create and save MobileNumbers', async () => {
        const nbButtonsBeforeCreate = await mobileNumberComponentsPage.countDeleteButtons();

        await mobileNumberComponentsPage.clickOnCreateButton();
        await promise.all([
            mobileNumberUpdatePage.setNumberInput('number'),
            mobileNumberUpdatePage.setAssetTagInput('assetTag'),
            mobileNumberUpdatePage.operatorSelectLastOption(),
            mobileNumberUpdatePage.mobileConnectionPlanSelectLastOption(),
            mobileNumberUpdatePage.setRemarksInput('remarks'),
            mobileNumberUpdatePage.registeredToSelectLastOption(),
        ]);
        expect(await mobileNumberUpdatePage.getNumberInput()).to.eq('number', 'Expected Number value to be equals to number');
        expect(await mobileNumberUpdatePage.getAssetTagInput()).to.eq('assetTag', 'Expected AssetTag value to be equals to assetTag');
        expect(await mobileNumberUpdatePage.getRemarksInput()).to.eq('remarks', 'Expected Remarks value to be equals to remarks');
        const selectedActive = mobileNumberUpdatePage.getActiveInput();
        if (await selectedActive.isSelected()) {
            await mobileNumberUpdatePage.getActiveInput().click();
            expect(await mobileNumberUpdatePage.getActiveInput().isSelected(), 'Expected active not to be selected').to.be.false;
        } else {
            await mobileNumberUpdatePage.getActiveInput().click();
            expect(await mobileNumberUpdatePage.getActiveInput().isSelected(), 'Expected active to be selected').to.be.true;
        }
        await mobileNumberUpdatePage.save();
        expect(await mobileNumberUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

        expect(await mobileNumberComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
    });*/

  /* it('should delete last MobileNumber', async () => {
        const nbButtonsBeforeDelete = await mobileNumberComponentsPage.countDeleteButtons();
        await mobileNumberComponentsPage.clickOnLastDeleteButton();

        mobileNumberDeleteDialog = new MobileNumberDeleteDialog();
        expect(await mobileNumberDeleteDialog.getDialogTitle())
            .to.eq('qeeperApp.mobileNumber.delete.question');
        await mobileNumberDeleteDialog.clickOnConfirmButton();

        expect(await mobileNumberComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });*/

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
