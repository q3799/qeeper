import { browser, ExpectedConditions, element, by, ElementFinder } from 'protractor';

export class MobileNumberComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-mobile-number div table .btn-danger'));
  title = element.all(by.css('jhi-mobile-number div h2#page-heading span')).first();

  async clickOnCreateButton(timeout?: number) {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(timeout?: number) {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class MobileNumberUpdatePage {
  pageTitle = element(by.id('jhi-mobile-number-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  numberInput = element(by.id('field_number'));
  assetTagInput = element(by.id('field_assetTag'));
  operatorSelect = element(by.id('field_operator'));
  mobileConnectionPlanSelect = element(by.id('field_mobileConnectionPlan'));
  remarksInput = element(by.id('field_remarks'));
  activeInput = element(by.id('field_active'));
  registeredToSelect = element(by.id('field_registeredTo'));

  async getPageTitle() {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setNumberInput(number) {
    await this.numberInput.sendKeys(number);
  }

  async getNumberInput() {
    return await this.numberInput.getAttribute('value');
  }

  async setAssetTagInput(assetTag) {
    await this.assetTagInput.sendKeys(assetTag);
  }

  async getAssetTagInput() {
    return await this.assetTagInput.getAttribute('value');
  }

  async setOperatorSelect(operator) {
    await this.operatorSelect.sendKeys(operator);
  }

  async getOperatorSelect() {
    return await this.operatorSelect.element(by.css('option:checked')).getText();
  }

  async operatorSelectLastOption(timeout?: number) {
    await this.operatorSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async setMobileConnectionPlanSelect(mobileConnectionPlan) {
    await this.mobileConnectionPlanSelect.sendKeys(mobileConnectionPlan);
  }

  async getMobileConnectionPlanSelect() {
    return await this.mobileConnectionPlanSelect.element(by.css('option:checked')).getText();
  }

  async mobileConnectionPlanSelectLastOption(timeout?: number) {
    await this.mobileConnectionPlanSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async setRemarksInput(remarks) {
    await this.remarksInput.sendKeys(remarks);
  }

  async getRemarksInput() {
    return await this.remarksInput.getAttribute('value');
  }

  getActiveInput(timeout?: number) {
    return this.activeInput;
  }

  async registeredToSelectLastOption(timeout?: number) {
    await this.registeredToSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async registeredToSelectOption(option) {
    await this.registeredToSelect.sendKeys(option);
  }

  getRegisteredToSelect(): ElementFinder {
    return this.registeredToSelect;
  }

  async getRegisteredToSelectedOption() {
    return await this.registeredToSelect.element(by.css('option:checked')).getText();
  }

  async save(timeout?: number) {
    await this.saveButton.click();
  }

  async cancel(timeout?: number) {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class MobileNumberDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-mobileNumber-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-mobileNumber'));

  async getDialogTitle() {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(timeout?: number) {
    await this.confirmButton.click();
  }
}
