/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import {
  NonBankAccountGroupComponentsPage,
  NonBankAccountGroupDeleteDialog,
  NonBankAccountGroupUpdatePage
} from './non-bank-account-group.page-object';

const expect = chai.expect;

describe('NonBankAccountGroup e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let nonBankAccountGroupUpdatePage: NonBankAccountGroupUpdatePage;
  let nonBankAccountGroupComponentsPage: NonBankAccountGroupComponentsPage;
  let nonBankAccountGroupDeleteDialog: NonBankAccountGroupDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load NonBankAccountGroups', async () => {
    await navBarPage.goToEntity('non-bank-account-group');
    nonBankAccountGroupComponentsPage = new NonBankAccountGroupComponentsPage();
    await browser.wait(ec.visibilityOf(nonBankAccountGroupComponentsPage.title), 5000);
    expect(await nonBankAccountGroupComponentsPage.getTitle()).to.eq('qeeperApp.nonBankAccountGroup.home.title');
  });

  it('should load create NonBankAccountGroup page', async () => {
    await nonBankAccountGroupComponentsPage.clickOnCreateButton();
    nonBankAccountGroupUpdatePage = new NonBankAccountGroupUpdatePage();
    expect(await nonBankAccountGroupUpdatePage.getPageTitle()).to.eq('qeeperApp.nonBankAccountGroup.home.createOrEditLabel');
    await nonBankAccountGroupUpdatePage.cancel();
  });

  it('should create and save NonBankAccountGroups', async () => {
    const nbButtonsBeforeCreate = await nonBankAccountGroupComponentsPage.countDeleteButtons();

    await nonBankAccountGroupComponentsPage.clickOnCreateButton();
    await promise.all([nonBankAccountGroupUpdatePage.setCodeInput('code'), nonBankAccountGroupUpdatePage.setNameInput('name')]);
    expect(await nonBankAccountGroupUpdatePage.getCodeInput()).to.eq('code', 'Expected Code value to be equals to code');
    expect(await nonBankAccountGroupUpdatePage.getNameInput()).to.eq('name', 'Expected Name value to be equals to name');
    await nonBankAccountGroupUpdatePage.save();
    expect(await nonBankAccountGroupUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await nonBankAccountGroupComponentsPage.countDeleteButtons()).to.eq(
      nbButtonsBeforeCreate + 1,
      'Expected one more entry in the table'
    );
  });

  it('should delete last NonBankAccountGroup', async () => {
    const nbButtonsBeforeDelete = await nonBankAccountGroupComponentsPage.countDeleteButtons();
    await nonBankAccountGroupComponentsPage.clickOnLastDeleteButton();

    nonBankAccountGroupDeleteDialog = new NonBankAccountGroupDeleteDialog();
    expect(await nonBankAccountGroupDeleteDialog.getDialogTitle()).to.eq('qeeperApp.nonBankAccountGroup.delete.question');
    await nonBankAccountGroupDeleteDialog.clickOnConfirmButton();

    expect(await nonBankAccountGroupComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
