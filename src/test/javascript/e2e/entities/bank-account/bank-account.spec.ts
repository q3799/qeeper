/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, protractor, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { BankAccountComponentsPage, BankAccountDeleteDialog, BankAccountUpdatePage } from './bank-account.page-object';

const expect = chai.expect;

describe('BankAccount e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let bankAccountUpdatePage: BankAccountUpdatePage;
  let bankAccountComponentsPage: BankAccountComponentsPage;
  /*let bankAccountDeleteDialog: BankAccountDeleteDialog;*/

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load BankAccounts', async () => {
    await navBarPage.goToEntity('bank-account');
    bankAccountComponentsPage = new BankAccountComponentsPage();
    await browser.wait(ec.visibilityOf(bankAccountComponentsPage.title), 5000);
    expect(await bankAccountComponentsPage.getTitle()).to.eq('qeeperApp.bankAccount.home.title');
  });

  it('should load create BankAccount page', async () => {
    await bankAccountComponentsPage.clickOnCreateButton();
    bankAccountUpdatePage = new BankAccountUpdatePage();
    expect(await bankAccountUpdatePage.getPageTitle()).to.eq('qeeperApp.bankAccount.home.createOrEditLabel');
    await bankAccountUpdatePage.cancel();
  });

  /* it('should create and save BankAccounts', async () => {
        const nbButtonsBeforeCreate = await bankAccountComponentsPage.countDeleteButtons();

        await bankAccountComponentsPage.clickOnCreateButton();
        await promise.all([
            bankAccountUpdatePage.setDateOfOpeningInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
            bankAccountUpdatePage.setOpenedByInput('openedBy'),
            bankAccountUpdatePage.setCustomerIdInput('customerId'),
            bankAccountUpdatePage.setAccountNumberInput('accountNumber'),
            bankAccountUpdatePage.setLoginIdInput('loginId'),
            bankAccountUpdatePage.setLoginPasswordInput('loginPassword'),
            bankAccountUpdatePage.setEmailInput('email'),
            bankAccountUpdatePage.setPhoneInput('phone'),
            bankAccountUpdatePage.bankAccountTypeSelectLastOption(),
            bankAccountUpdatePage.setAccountBalanceInput('5'),
            bankAccountUpdatePage.setBalanceCheckedOnInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
            bankAccountUpdatePage.setMinimumBalanceInput('5'),
            bankAccountUpdatePage.setExpirationPeriodDaysInput('5'),
            bankAccountUpdatePage.setPasswordUpdatedOnInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
            bankAccountUpdatePage.setRemarksInput('remarks'),
            bankAccountUpdatePage.accountStatusSelectLastOption(),
            bankAccountUpdatePage.setProjectNameInput('projectName'),
            bankAccountUpdatePage.setProjectCodeInput('projectCode'),
            bankAccountUpdatePage.setRequestedOnInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
            bankAccountUpdatePage.setRequestedByInput('requestedBy'),
            bankAccountUpdatePage.accountHolderSelectLastOption(),
            bankAccountUpdatePage.bankBranchSelectLastOption(),
            bankAccountUpdatePage.mobileNumberMappedSelectLastOption(),
            bankAccountUpdatePage.parentAccountSelectLastOption(),
            bankAccountUpdatePage.bankAccountGroupSelectLastOption(),
        ]);
        expect(await bankAccountUpdatePage.getDateOfOpeningInput()).to.contain('2001-01-01T02:30', 'Expected dateOfOpening value to be equals to 2000-12-31');
        expect(await bankAccountUpdatePage.getOpenedByInput()).to.eq('openedBy', 'Expected OpenedBy value to be equals to openedBy');
        expect(await bankAccountUpdatePage.getCustomerIdInput()).to.eq('customerId', 'Expected CustomerId value to be equals to customerId');
        expect(await bankAccountUpdatePage.getAccountNumberInput()).to.eq('accountNumber', 'Expected AccountNumber value to be equals to accountNumber');
        expect(await bankAccountUpdatePage.getLoginIdInput()).to.eq('loginId', 'Expected LoginId value to be equals to loginId');
        expect(await bankAccountUpdatePage.getLoginPasswordInput()).to.eq('loginPassword', 'Expected LoginPassword value to be equals to loginPassword');
        expect(await bankAccountUpdatePage.getEmailInput()).to.eq('email', 'Expected Email value to be equals to email');
        expect(await bankAccountUpdatePage.getPhoneInput()).to.eq('phone', 'Expected Phone value to be equals to phone');
        const selectedChequeBookIssued = bankAccountUpdatePage.getChequeBookIssuedInput();
        if (await selectedChequeBookIssued.isSelected()) {
            await bankAccountUpdatePage.getChequeBookIssuedInput().click();
            expect(await bankAccountUpdatePage.getChequeBookIssuedInput().isSelected(), 'Expected chequeBookIssued not to be selected').to.be.false;
        } else {
            await bankAccountUpdatePage.getChequeBookIssuedInput().click();
            expect(await bankAccountUpdatePage.getChequeBookIssuedInput().isSelected(), 'Expected chequeBookIssued to be selected').to.be.true;
        }
        expect(await bankAccountUpdatePage.getAccountBalanceInput()).to.eq('5', 'Expected accountBalance value to be equals to 5');
        expect(await bankAccountUpdatePage.getBalanceCheckedOnInput()).to.contain('2001-01-01T02:30', 'Expected balanceCheckedOn value to be equals to 2000-12-31');
        expect(await bankAccountUpdatePage.getMinimumBalanceInput()).to.eq('5', 'Expected minimumBalance value to be equals to 5');
        const selectedPasswordGetsExpired = bankAccountUpdatePage.getPasswordGetsExpiredInput();
        if (await selectedPasswordGetsExpired.isSelected()) {
            await bankAccountUpdatePage.getPasswordGetsExpiredInput().click();
            expect(await bankAccountUpdatePage.getPasswordGetsExpiredInput().isSelected(), 'Expected passwordGetsExpired not to be selected').to.be.false;
        } else {
            await bankAccountUpdatePage.getPasswordGetsExpiredInput().click();
            expect(await bankAccountUpdatePage.getPasswordGetsExpiredInput().isSelected(), 'Expected passwordGetsExpired to be selected').to.be.true;
        }
        expect(await bankAccountUpdatePage.getExpirationPeriodDaysInput()).to.eq('5', 'Expected expirationPeriodDays value to be equals to 5');
        expect(await bankAccountUpdatePage.getPasswordUpdatedOnInput()).to.contain('2001-01-01T02:30', 'Expected passwordUpdatedOn value to be equals to 2000-12-31');
        expect(await bankAccountUpdatePage.getRemarksInput()).to.eq('remarks', 'Expected Remarks value to be equals to remarks');
        const selectedMultipleSessionAllowed = bankAccountUpdatePage.getMultipleSessionAllowedInput();
        if (await selectedMultipleSessionAllowed.isSelected()) {
            await bankAccountUpdatePage.getMultipleSessionAllowedInput().click();
            expect(await bankAccountUpdatePage.getMultipleSessionAllowedInput().isSelected(), 'Expected multipleSessionAllowed not to be selected').to.be.false;
        } else {
            await bankAccountUpdatePage.getMultipleSessionAllowedInput().click();
            expect(await bankAccountUpdatePage.getMultipleSessionAllowedInput().isSelected(), 'Expected multipleSessionAllowed to be selected').to.be.true;
        }
        expect(await bankAccountUpdatePage.getProjectNameInput()).to.eq('projectName', 'Expected ProjectName value to be equals to projectName');
        expect(await bankAccountUpdatePage.getProjectCodeInput()).to.eq('projectCode', 'Expected ProjectCode value to be equals to projectCode');
        expect(await bankAccountUpdatePage.getRequestedOnInput()).to.contain('2001-01-01T02:30', 'Expected requestedOn value to be equals to 2000-12-31');
        expect(await bankAccountUpdatePage.getRequestedByInput()).to.eq('requestedBy', 'Expected RequestedBy value to be equals to requestedBy');
        await bankAccountUpdatePage.save();
        expect(await bankAccountUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

        expect(await bankAccountComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
    });*/

  /* it('should delete last BankAccount', async () => {
        const nbButtonsBeforeDelete = await bankAccountComponentsPage.countDeleteButtons();
        await bankAccountComponentsPage.clickOnLastDeleteButton();

        bankAccountDeleteDialog = new BankAccountDeleteDialog();
        expect(await bankAccountDeleteDialog.getDialogTitle())
            .to.eq('qeeperApp.bankAccount.delete.question');
        await bankAccountDeleteDialog.clickOnConfirmButton();

        expect(await bankAccountComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });*/

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
