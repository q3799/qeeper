import { browser, ExpectedConditions, element, by, ElementFinder } from 'protractor';

export class BankAccountComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-bank-account div table .btn-danger'));
  title = element.all(by.css('jhi-bank-account div h2#page-heading span')).first();

  async clickOnCreateButton(timeout?: number) {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(timeout?: number) {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class BankAccountUpdatePage {
  pageTitle = element(by.id('jhi-bank-account-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  dateOfOpeningInput = element(by.id('field_dateOfOpening'));
  openedByInput = element(by.id('field_openedBy'));
  customerIdInput = element(by.id('field_customerId'));
  accountNumberInput = element(by.id('field_accountNumber'));
  loginIdInput = element(by.id('field_loginId'));
  loginPasswordInput = element(by.id('field_loginPassword'));
  emailInput = element(by.id('field_email'));
  phoneInput = element(by.id('field_phone'));
  bankAccountTypeSelect = element(by.id('field_bankAccountType'));
  chequeBookIssuedInput = element(by.id('field_chequeBookIssued'));
  accountBalanceInput = element(by.id('field_accountBalance'));
  balanceCheckedOnInput = element(by.id('field_balanceCheckedOn'));
  minimumBalanceInput = element(by.id('field_minimumBalance'));
  passwordGetsExpiredInput = element(by.id('field_passwordGetsExpired'));
  expirationPeriodDaysInput = element(by.id('field_expirationPeriodDays'));
  passwordUpdatedOnInput = element(by.id('field_passwordUpdatedOn'));
  remarksInput = element(by.id('field_remarks'));
  accountStatusSelect = element(by.id('field_accountStatus'));
  multipleSessionAllowedInput = element(by.id('field_multipleSessionAllowed'));
  projectNameInput = element(by.id('field_projectName'));
  projectCodeInput = element(by.id('field_projectCode'));
  requestedOnInput = element(by.id('field_requestedOn'));
  requestedByInput = element(by.id('field_requestedBy'));
  accountHolderSelect = element(by.id('field_accountHolder'));
  bankBranchSelect = element(by.id('field_bankBranch'));
  mobileNumberMappedSelect = element(by.id('field_mobileNumberMapped'));
  parentAccountSelect = element(by.id('field_parentAccount'));
  bankAccountGroupSelect = element(by.id('field_bankAccountGroup'));

  async getPageTitle() {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setDateOfOpeningInput(dateOfOpening) {
    await this.dateOfOpeningInput.sendKeys(dateOfOpening);
  }

  async getDateOfOpeningInput() {
    return await this.dateOfOpeningInput.getAttribute('value');
  }

  async setOpenedByInput(openedBy) {
    await this.openedByInput.sendKeys(openedBy);
  }

  async getOpenedByInput() {
    return await this.openedByInput.getAttribute('value');
  }

  async setCustomerIdInput(customerId) {
    await this.customerIdInput.sendKeys(customerId);
  }

  async getCustomerIdInput() {
    return await this.customerIdInput.getAttribute('value');
  }

  async setAccountNumberInput(accountNumber) {
    await this.accountNumberInput.sendKeys(accountNumber);
  }

  async getAccountNumberInput() {
    return await this.accountNumberInput.getAttribute('value');
  }

  async setLoginIdInput(loginId) {
    await this.loginIdInput.sendKeys(loginId);
  }

  async getLoginIdInput() {
    return await this.loginIdInput.getAttribute('value');
  }

  async setLoginPasswordInput(loginPassword) {
    await this.loginPasswordInput.sendKeys(loginPassword);
  }

  async getLoginPasswordInput() {
    return await this.loginPasswordInput.getAttribute('value');
  }

  async setEmailInput(email) {
    await this.emailInput.sendKeys(email);
  }

  async getEmailInput() {
    return await this.emailInput.getAttribute('value');
  }

  async setPhoneInput(phone) {
    await this.phoneInput.sendKeys(phone);
  }

  async getPhoneInput() {
    return await this.phoneInput.getAttribute('value');
  }

  async setBankAccountTypeSelect(bankAccountType) {
    await this.bankAccountTypeSelect.sendKeys(bankAccountType);
  }

  async getBankAccountTypeSelect() {
    return await this.bankAccountTypeSelect.element(by.css('option:checked')).getText();
  }

  async bankAccountTypeSelectLastOption(timeout?: number) {
    await this.bankAccountTypeSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  getChequeBookIssuedInput(timeout?: number) {
    return this.chequeBookIssuedInput;
  }
  async setAccountBalanceInput(accountBalance) {
    await this.accountBalanceInput.sendKeys(accountBalance);
  }

  async getAccountBalanceInput() {
    return await this.accountBalanceInput.getAttribute('value');
  }

  async setBalanceCheckedOnInput(balanceCheckedOn) {
    await this.balanceCheckedOnInput.sendKeys(balanceCheckedOn);
  }

  async getBalanceCheckedOnInput() {
    return await this.balanceCheckedOnInput.getAttribute('value');
  }

  async setMinimumBalanceInput(minimumBalance) {
    await this.minimumBalanceInput.sendKeys(minimumBalance);
  }

  async getMinimumBalanceInput() {
    return await this.minimumBalanceInput.getAttribute('value');
  }

  getPasswordGetsExpiredInput(timeout?: number) {
    return this.passwordGetsExpiredInput;
  }
  async setExpirationPeriodDaysInput(expirationPeriodDays) {
    await this.expirationPeriodDaysInput.sendKeys(expirationPeriodDays);
  }

  async getExpirationPeriodDaysInput() {
    return await this.expirationPeriodDaysInput.getAttribute('value');
  }

  async setPasswordUpdatedOnInput(passwordUpdatedOn) {
    await this.passwordUpdatedOnInput.sendKeys(passwordUpdatedOn);
  }

  async getPasswordUpdatedOnInput() {
    return await this.passwordUpdatedOnInput.getAttribute('value');
  }

  async setRemarksInput(remarks) {
    await this.remarksInput.sendKeys(remarks);
  }

  async getRemarksInput() {
    return await this.remarksInput.getAttribute('value');
  }

  async setAccountStatusSelect(accountStatus) {
    await this.accountStatusSelect.sendKeys(accountStatus);
  }

  async getAccountStatusSelect() {
    return await this.accountStatusSelect.element(by.css('option:checked')).getText();
  }

  async accountStatusSelectLastOption(timeout?: number) {
    await this.accountStatusSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  getMultipleSessionAllowedInput(timeout?: number) {
    return this.multipleSessionAllowedInput;
  }
  async setProjectNameInput(projectName) {
    await this.projectNameInput.sendKeys(projectName);
  }

  async getProjectNameInput() {
    return await this.projectNameInput.getAttribute('value');
  }

  async setProjectCodeInput(projectCode) {
    await this.projectCodeInput.sendKeys(projectCode);
  }

  async getProjectCodeInput() {
    return await this.projectCodeInput.getAttribute('value');
  }

  async setRequestedOnInput(requestedOn) {
    await this.requestedOnInput.sendKeys(requestedOn);
  }

  async getRequestedOnInput() {
    return await this.requestedOnInput.getAttribute('value');
  }

  async setRequestedByInput(requestedBy) {
    await this.requestedByInput.sendKeys(requestedBy);
  }

  async getRequestedByInput() {
    return await this.requestedByInput.getAttribute('value');
  }

  async accountHolderSelectLastOption(timeout?: number) {
    await this.accountHolderSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async accountHolderSelectOption(option) {
    await this.accountHolderSelect.sendKeys(option);
  }

  getAccountHolderSelect(): ElementFinder {
    return this.accountHolderSelect;
  }

  async getAccountHolderSelectedOption() {
    return await this.accountHolderSelect.element(by.css('option:checked')).getText();
  }

  async bankBranchSelectLastOption(timeout?: number) {
    await this.bankBranchSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async bankBranchSelectOption(option) {
    await this.bankBranchSelect.sendKeys(option);
  }

  getBankBranchSelect(): ElementFinder {
    return this.bankBranchSelect;
  }

  async getBankBranchSelectedOption() {
    return await this.bankBranchSelect.element(by.css('option:checked')).getText();
  }

  async mobileNumberMappedSelectLastOption(timeout?: number) {
    await this.mobileNumberMappedSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async mobileNumberMappedSelectOption(option) {
    await this.mobileNumberMappedSelect.sendKeys(option);
  }

  getMobileNumberMappedSelect(): ElementFinder {
    return this.mobileNumberMappedSelect;
  }

  async getMobileNumberMappedSelectedOption() {
    return await this.mobileNumberMappedSelect.element(by.css('option:checked')).getText();
  }

  async parentAccountSelectLastOption(timeout?: number) {
    await this.parentAccountSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async parentAccountSelectOption(option) {
    await this.parentAccountSelect.sendKeys(option);
  }

  getParentAccountSelect(): ElementFinder {
    return this.parentAccountSelect;
  }

  async getParentAccountSelectedOption() {
    return await this.parentAccountSelect.element(by.css('option:checked')).getText();
  }

  async bankAccountGroupSelectLastOption(timeout?: number) {
    await this.bankAccountGroupSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async bankAccountGroupSelectOption(option) {
    await this.bankAccountGroupSelect.sendKeys(option);
  }

  getBankAccountGroupSelect(): ElementFinder {
    return this.bankAccountGroupSelect;
  }

  async getBankAccountGroupSelectedOption() {
    return await this.bankAccountGroupSelect.element(by.css('option:checked')).getText();
  }

  async save(timeout?: number) {
    await this.saveButton.click();
  }

  async cancel(timeout?: number) {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class BankAccountDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-bankAccount-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-bankAccount'));

  async getDialogTitle() {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(timeout?: number) {
    await this.confirmButton.click();
  }
}
