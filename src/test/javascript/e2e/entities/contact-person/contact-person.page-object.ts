import { browser, ExpectedConditions, element, by, ElementFinder } from 'protractor';

export class ContactPersonComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-contact-person div table .btn-danger'));
  title = element.all(by.css('jhi-contact-person div h2#page-heading span')).first();

  async clickOnCreateButton(timeout?: number) {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(timeout?: number) {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class ContactPersonUpdatePage {
  pageTitle = element(by.id('jhi-contact-person-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  nameInput = element(by.id('field_name'));
  primaryEmailInput = element(by.id('field_primaryEmail'));
  secondaryEmailInput = element(by.id('field_secondaryEmail'));
  primaryPhoneInput = element(by.id('field_primaryPhone'));
  secondaryPhoneInput = element(by.id('field_secondaryPhone'));
  activeInput = element(by.id('field_active'));

  async getPageTitle() {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setNameInput(name) {
    await this.nameInput.sendKeys(name);
  }

  async getNameInput() {
    return await this.nameInput.getAttribute('value');
  }

  async setPrimaryEmailInput(primaryEmail) {
    await this.primaryEmailInput.sendKeys(primaryEmail);
  }

  async getPrimaryEmailInput() {
    return await this.primaryEmailInput.getAttribute('value');
  }

  async setSecondaryEmailInput(secondaryEmail) {
    await this.secondaryEmailInput.sendKeys(secondaryEmail);
  }

  async getSecondaryEmailInput() {
    return await this.secondaryEmailInput.getAttribute('value');
  }

  async setPrimaryPhoneInput(primaryPhone) {
    await this.primaryPhoneInput.sendKeys(primaryPhone);
  }

  async getPrimaryPhoneInput() {
    return await this.primaryPhoneInput.getAttribute('value');
  }

  async setSecondaryPhoneInput(secondaryPhone) {
    await this.secondaryPhoneInput.sendKeys(secondaryPhone);
  }

  async getSecondaryPhoneInput() {
    return await this.secondaryPhoneInput.getAttribute('value');
  }

  getActiveInput(timeout?: number) {
    return this.activeInput;
  }
  async save(timeout?: number) {
    await this.saveButton.click();
  }

  async cancel(timeout?: number) {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class ContactPersonDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-contactPerson-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-contactPerson'));

  async getDialogTitle() {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(timeout?: number) {
    await this.confirmButton.click();
  }
}
