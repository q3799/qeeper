/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { ContactPersonComponentsPage, ContactPersonDeleteDialog, ContactPersonUpdatePage } from './contact-person.page-object';

const expect = chai.expect;

describe('ContactPerson e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let contactPersonUpdatePage: ContactPersonUpdatePage;
  let contactPersonComponentsPage: ContactPersonComponentsPage;
  let contactPersonDeleteDialog: ContactPersonDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load ContactPeople', async () => {
    await navBarPage.goToEntity('contact-person');
    contactPersonComponentsPage = new ContactPersonComponentsPage();
    await browser.wait(ec.visibilityOf(contactPersonComponentsPage.title), 5000);
    expect(await contactPersonComponentsPage.getTitle()).to.eq('qeeperApp.contactPerson.home.title');
  });

  it('should load create ContactPerson page', async () => {
    await contactPersonComponentsPage.clickOnCreateButton();
    contactPersonUpdatePage = new ContactPersonUpdatePage();
    expect(await contactPersonUpdatePage.getPageTitle()).to.eq('qeeperApp.contactPerson.home.createOrEditLabel');
    await contactPersonUpdatePage.cancel();
  });

  it('should create and save ContactPeople', async () => {
    const nbButtonsBeforeCreate = await contactPersonComponentsPage.countDeleteButtons();

    await contactPersonComponentsPage.clickOnCreateButton();
    await promise.all([
      contactPersonUpdatePage.setNameInput('name'),
      contactPersonUpdatePage.setPrimaryEmailInput('primaryEmail'),
      contactPersonUpdatePage.setSecondaryEmailInput('secondaryEmail'),
      contactPersonUpdatePage.setPrimaryPhoneInput('primaryPhone'),
      contactPersonUpdatePage.setSecondaryPhoneInput('secondaryPhone')
    ]);
    expect(await contactPersonUpdatePage.getNameInput()).to.eq('name', 'Expected Name value to be equals to name');
    expect(await contactPersonUpdatePage.getPrimaryEmailInput()).to.eq(
      'primaryEmail',
      'Expected PrimaryEmail value to be equals to primaryEmail'
    );
    expect(await contactPersonUpdatePage.getSecondaryEmailInput()).to.eq(
      'secondaryEmail',
      'Expected SecondaryEmail value to be equals to secondaryEmail'
    );
    expect(await contactPersonUpdatePage.getPrimaryPhoneInput()).to.eq(
      'primaryPhone',
      'Expected PrimaryPhone value to be equals to primaryPhone'
    );
    expect(await contactPersonUpdatePage.getSecondaryPhoneInput()).to.eq(
      'secondaryPhone',
      'Expected SecondaryPhone value to be equals to secondaryPhone'
    );
    const selectedActive = contactPersonUpdatePage.getActiveInput();
    if (await selectedActive.isSelected()) {
      await contactPersonUpdatePage.getActiveInput().click();
      expect(await contactPersonUpdatePage.getActiveInput().isSelected(), 'Expected active not to be selected').to.be.false;
    } else {
      await contactPersonUpdatePage.getActiveInput().click();
      expect(await contactPersonUpdatePage.getActiveInput().isSelected(), 'Expected active to be selected').to.be.true;
    }
    await contactPersonUpdatePage.save();
    expect(await contactPersonUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await contactPersonComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last ContactPerson', async () => {
    const nbButtonsBeforeDelete = await contactPersonComponentsPage.countDeleteButtons();
    await contactPersonComponentsPage.clickOnLastDeleteButton();

    contactPersonDeleteDialog = new ContactPersonDeleteDialog();
    expect(await contactPersonDeleteDialog.getDialogTitle()).to.eq('qeeperApp.contactPerson.delete.question');
    await contactPersonDeleteDialog.clickOnConfirmButton();

    expect(await contactPersonComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
