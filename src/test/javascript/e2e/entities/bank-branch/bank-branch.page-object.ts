import { browser, ExpectedConditions, element, by, ElementFinder } from 'protractor';

export class BankBranchComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-bank-branch div table .btn-danger'));
  title = element.all(by.css('jhi-bank-branch div h2#page-heading span')).first();

  async clickOnCreateButton(timeout?: number) {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(timeout?: number) {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class BankBranchUpdatePage {
  pageTitle = element(by.id('jhi-bank-branch-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  locationInput = element(by.id('field_location'));
  addressInput = element(by.id('field_address'));
  ifscCodeInput = element(by.id('field_ifscCode'));
  micrCodeInput = element(by.id('field_micrCode'));
  remarksInput = element(by.id('field_remarks'));
  bankSelect = element(by.id('field_bank'));

  async getPageTitle() {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setLocationInput(location) {
    await this.locationInput.sendKeys(location);
  }

  async getLocationInput() {
    return await this.locationInput.getAttribute('value');
  }

  async setAddressInput(address) {
    await this.addressInput.sendKeys(address);
  }

  async getAddressInput() {
    return await this.addressInput.getAttribute('value');
  }

  async setIfscCodeInput(ifscCode) {
    await this.ifscCodeInput.sendKeys(ifscCode);
  }

  async getIfscCodeInput() {
    return await this.ifscCodeInput.getAttribute('value');
  }

  async setMicrCodeInput(micrCode) {
    await this.micrCodeInput.sendKeys(micrCode);
  }

  async getMicrCodeInput() {
    return await this.micrCodeInput.getAttribute('value');
  }

  async setRemarksInput(remarks) {
    await this.remarksInput.sendKeys(remarks);
  }

  async getRemarksInput() {
    return await this.remarksInput.getAttribute('value');
  }

  async bankSelectLastOption(timeout?: number) {
    await this.bankSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async bankSelectOption(option) {
    await this.bankSelect.sendKeys(option);
  }

  getBankSelect(): ElementFinder {
    return this.bankSelect;
  }

  async getBankSelectedOption() {
    return await this.bankSelect.element(by.css('option:checked')).getText();
  }

  async save(timeout?: number) {
    await this.saveButton.click();
  }

  async cancel(timeout?: number) {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class BankBranchDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-bankBranch-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-bankBranch'));

  async getDialogTitle() {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(timeout?: number) {
    await this.confirmButton.click();
  }
}
