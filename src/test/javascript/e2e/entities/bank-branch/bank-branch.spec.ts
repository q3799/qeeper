/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { BankBranchComponentsPage, BankBranchDeleteDialog, BankBranchUpdatePage } from './bank-branch.page-object';

const expect = chai.expect;

describe('BankBranch e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let bankBranchUpdatePage: BankBranchUpdatePage;
  let bankBranchComponentsPage: BankBranchComponentsPage;
  /*let bankBranchDeleteDialog: BankBranchDeleteDialog;*/

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load BankBranches', async () => {
    await navBarPage.goToEntity('bank-branch');
    bankBranchComponentsPage = new BankBranchComponentsPage();
    await browser.wait(ec.visibilityOf(bankBranchComponentsPage.title), 5000);
    expect(await bankBranchComponentsPage.getTitle()).to.eq('qeeperApp.bankBranch.home.title');
  });

  it('should load create BankBranch page', async () => {
    await bankBranchComponentsPage.clickOnCreateButton();
    bankBranchUpdatePage = new BankBranchUpdatePage();
    expect(await bankBranchUpdatePage.getPageTitle()).to.eq('qeeperApp.bankBranch.home.createOrEditLabel');
    await bankBranchUpdatePage.cancel();
  });

  /* it('should create and save BankBranches', async () => {
        const nbButtonsBeforeCreate = await bankBranchComponentsPage.countDeleteButtons();

        await bankBranchComponentsPage.clickOnCreateButton();
        await promise.all([
            bankBranchUpdatePage.setLocationInput('location'),
            bankBranchUpdatePage.setAddressInput('address'),
            bankBranchUpdatePage.setIfscCodeInput('ifscCode'),
            bankBranchUpdatePage.setMicrCodeInput('micrCode'),
            bankBranchUpdatePage.setRemarksInput('remarks'),
            bankBranchUpdatePage.bankSelectLastOption(),
        ]);
        expect(await bankBranchUpdatePage.getLocationInput()).to.eq('location', 'Expected Location value to be equals to location');
        expect(await bankBranchUpdatePage.getAddressInput()).to.eq('address', 'Expected Address value to be equals to address');
        expect(await bankBranchUpdatePage.getIfscCodeInput()).to.eq('ifscCode', 'Expected IfscCode value to be equals to ifscCode');
        expect(await bankBranchUpdatePage.getMicrCodeInput()).to.eq('micrCode', 'Expected MicrCode value to be equals to micrCode');
        expect(await bankBranchUpdatePage.getRemarksInput()).to.eq('remarks', 'Expected Remarks value to be equals to remarks');
        await bankBranchUpdatePage.save();
        expect(await bankBranchUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

        expect(await bankBranchComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
    });*/

  /* it('should delete last BankBranch', async () => {
        const nbButtonsBeforeDelete = await bankBranchComponentsPage.countDeleteButtons();
        await bankBranchComponentsPage.clickOnLastDeleteButton();

        bankBranchDeleteDialog = new BankBranchDeleteDialog();
        expect(await bankBranchDeleteDialog.getDialogTitle())
            .to.eq('qeeperApp.bankBranch.delete.question');
        await bankBranchDeleteDialog.clickOnConfirmButton();

        expect(await bankBranchComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });*/

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
