/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { CardComponentsPage, CardDeleteDialog, CardUpdatePage } from './card.page-object';

const expect = chai.expect;

describe('Card e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let cardUpdatePage: CardUpdatePage;
  let cardComponentsPage: CardComponentsPage;
  /*let cardDeleteDialog: CardDeleteDialog;*/

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Cards', async () => {
    await navBarPage.goToEntity('card');
    cardComponentsPage = new CardComponentsPage();
    await browser.wait(ec.visibilityOf(cardComponentsPage.title), 5000);
    expect(await cardComponentsPage.getTitle()).to.eq('qeeperApp.card.home.title');
  });

  it('should load create Card page', async () => {
    await cardComponentsPage.clickOnCreateButton();
    cardUpdatePage = new CardUpdatePage();
    expect(await cardUpdatePage.getPageTitle()).to.eq('qeeperApp.card.home.createOrEditLabel');
    await cardUpdatePage.cancel();
  });

  /* it('should create and save Cards', async () => {
        const nbButtonsBeforeCreate = await cardComponentsPage.countDeleteButtons();

        await cardComponentsPage.clickOnCreateButton();
        await promise.all([
            cardUpdatePage.cardTypeSelectLastOption(),
            cardUpdatePage.paymentNetworkSelectLastOption(),
            cardUpdatePage.setCardNumberInput('cardNumber'),
            cardUpdatePage.setValidFromInput('validFrom'),
            cardUpdatePage.setValidTillInput('validTill'),
            cardUpdatePage.setPaymentNetworkPinInput('paymentNetworkPin'),
            cardUpdatePage.setCvvInput('cvv'),
            cardUpdatePage.setNameOnCardInput('nameOnCard'),
            cardUpdatePage.setRemarksInput('remarks'),
            cardUpdatePage.setPinInput('pin'),
            cardUpdatePage.setTpinInput('tpin'),
            cardUpdatePage.mobileNumberMappedSelectLastOption(),
            cardUpdatePage.registeredToSelectLastOption(),
            cardUpdatePage.bankSelectLastOption(),
            cardUpdatePage.bankAccountSelectLastOption(),
            cardUpdatePage.cardGroupSelectLastOption(),
        ]);
        expect(await cardUpdatePage.getCardNumberInput()).to.eq('cardNumber', 'Expected CardNumber value to be equals to cardNumber');
        expect(await cardUpdatePage.getValidFromInput()).to.eq('validFrom', 'Expected ValidFrom value to be equals to validFrom');
        expect(await cardUpdatePage.getValidTillInput()).to.eq('validTill', 'Expected ValidTill value to be equals to validTill');
        expect(await cardUpdatePage.getPaymentNetworkPinInput()).to.eq('paymentNetworkPin', 'Expected PaymentNetworkPin value to be equals to paymentNetworkPin');
        expect(await cardUpdatePage.getCvvInput()).to.eq('cvv', 'Expected Cvv value to be equals to cvv');
        expect(await cardUpdatePage.getNameOnCardInput()).to.eq('nameOnCard', 'Expected NameOnCard value to be equals to nameOnCard');
        expect(await cardUpdatePage.getRemarksInput()).to.eq('remarks', 'Expected Remarks value to be equals to remarks');
        expect(await cardUpdatePage.getPinInput()).to.eq('pin', 'Expected Pin value to be equals to pin');
        expect(await cardUpdatePage.getTpinInput()).to.eq('tpin', 'Expected Tpin value to be equals to tpin');
        await cardUpdatePage.save();
        expect(await cardUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

        expect(await cardComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
    });*/

  /* it('should delete last Card', async () => {
        const nbButtonsBeforeDelete = await cardComponentsPage.countDeleteButtons();
        await cardComponentsPage.clickOnLastDeleteButton();

        cardDeleteDialog = new CardDeleteDialog();
        expect(await cardDeleteDialog.getDialogTitle())
            .to.eq('qeeperApp.card.delete.question');
        await cardDeleteDialog.clickOnConfirmButton();

        expect(await cardComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });*/

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
