import { browser, ExpectedConditions, element, by, ElementFinder } from 'protractor';

export class CardComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-card div table .btn-danger'));
  title = element.all(by.css('jhi-card div h2#page-heading span')).first();

  async clickOnCreateButton(timeout?: number) {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(timeout?: number) {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class CardUpdatePage {
  pageTitle = element(by.id('jhi-card-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  cardTypeSelect = element(by.id('field_cardType'));
  paymentNetworkSelect = element(by.id('field_paymentNetwork'));
  cardNumberInput = element(by.id('field_cardNumber'));
  validFromInput = element(by.id('field_validFrom'));
  validTillInput = element(by.id('field_validTill'));
  paymentNetworkPinInput = element(by.id('field_paymentNetworkPin'));
  cvvInput = element(by.id('field_cvv'));
  nameOnCardInput = element(by.id('field_nameOnCard'));
  remarksInput = element(by.id('field_remarks'));
  pinInput = element(by.id('field_pin'));
  tpinInput = element(by.id('field_tpin'));
  mobileNumberMappedSelect = element(by.id('field_mobileNumberMapped'));
  registeredToSelect = element(by.id('field_registeredTo'));
  bankSelect = element(by.id('field_bank'));
  bankAccountSelect = element(by.id('field_bankAccount'));
  cardGroupSelect = element(by.id('field_cardGroup'));

  async getPageTitle() {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setCardTypeSelect(cardType) {
    await this.cardTypeSelect.sendKeys(cardType);
  }

  async getCardTypeSelect() {
    return await this.cardTypeSelect.element(by.css('option:checked')).getText();
  }

  async cardTypeSelectLastOption(timeout?: number) {
    await this.cardTypeSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async setPaymentNetworkSelect(paymentNetwork) {
    await this.paymentNetworkSelect.sendKeys(paymentNetwork);
  }

  async getPaymentNetworkSelect() {
    return await this.paymentNetworkSelect.element(by.css('option:checked')).getText();
  }

  async paymentNetworkSelectLastOption(timeout?: number) {
    await this.paymentNetworkSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async setCardNumberInput(cardNumber) {
    await this.cardNumberInput.sendKeys(cardNumber);
  }

  async getCardNumberInput() {
    return await this.cardNumberInput.getAttribute('value');
  }

  async setValidFromInput(validFrom) {
    await this.validFromInput.sendKeys(validFrom);
  }

  async getValidFromInput() {
    return await this.validFromInput.getAttribute('value');
  }

  async setValidTillInput(validTill) {
    await this.validTillInput.sendKeys(validTill);
  }

  async getValidTillInput() {
    return await this.validTillInput.getAttribute('value');
  }

  async setPaymentNetworkPinInput(paymentNetworkPin) {
    await this.paymentNetworkPinInput.sendKeys(paymentNetworkPin);
  }

  async getPaymentNetworkPinInput() {
    return await this.paymentNetworkPinInput.getAttribute('value');
  }

  async setCvvInput(cvv) {
    await this.cvvInput.sendKeys(cvv);
  }

  async getCvvInput() {
    return await this.cvvInput.getAttribute('value');
  }

  async setNameOnCardInput(nameOnCard) {
    await this.nameOnCardInput.sendKeys(nameOnCard);
  }

  async getNameOnCardInput() {
    return await this.nameOnCardInput.getAttribute('value');
  }

  async setRemarksInput(remarks) {
    await this.remarksInput.sendKeys(remarks);
  }

  async getRemarksInput() {
    return await this.remarksInput.getAttribute('value');
  }

  async setPinInput(pin) {
    await this.pinInput.sendKeys(pin);
  }

  async getPinInput() {
    return await this.pinInput.getAttribute('value');
  }

  async setTpinInput(tpin) {
    await this.tpinInput.sendKeys(tpin);
  }

  async getTpinInput() {
    return await this.tpinInput.getAttribute('value');
  }

  async mobileNumberMappedSelectLastOption(timeout?: number) {
    await this.mobileNumberMappedSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async mobileNumberMappedSelectOption(option) {
    await this.mobileNumberMappedSelect.sendKeys(option);
  }

  getMobileNumberMappedSelect(): ElementFinder {
    return this.mobileNumberMappedSelect;
  }

  async getMobileNumberMappedSelectedOption() {
    return await this.mobileNumberMappedSelect.element(by.css('option:checked')).getText();
  }

  async registeredToSelectLastOption(timeout?: number) {
    await this.registeredToSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async registeredToSelectOption(option) {
    await this.registeredToSelect.sendKeys(option);
  }

  getRegisteredToSelect(): ElementFinder {
    return this.registeredToSelect;
  }

  async getRegisteredToSelectedOption() {
    return await this.registeredToSelect.element(by.css('option:checked')).getText();
  }

  async bankSelectLastOption(timeout?: number) {
    await this.bankSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async bankSelectOption(option) {
    await this.bankSelect.sendKeys(option);
  }

  getBankSelect(): ElementFinder {
    return this.bankSelect;
  }

  async getBankSelectedOption() {
    return await this.bankSelect.element(by.css('option:checked')).getText();
  }

  async bankAccountSelectLastOption(timeout?: number) {
    await this.bankAccountSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async bankAccountSelectOption(option) {
    await this.bankAccountSelect.sendKeys(option);
  }

  getBankAccountSelect(): ElementFinder {
    return this.bankAccountSelect;
  }

  async getBankAccountSelectedOption() {
    return await this.bankAccountSelect.element(by.css('option:checked')).getText();
  }

  async cardGroupSelectLastOption(timeout?: number) {
    await this.cardGroupSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async cardGroupSelectOption(option) {
    await this.cardGroupSelect.sendKeys(option);
  }

  getCardGroupSelect(): ElementFinder {
    return this.cardGroupSelect;
  }

  async getCardGroupSelectedOption() {
    return await this.cardGroupSelect.element(by.css('option:checked')).getText();
  }

  async save(timeout?: number) {
    await this.saveButton.click();
  }

  async cancel(timeout?: number) {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class CardDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-card-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-card'));

  async getDialogTitle() {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(timeout?: number) {
    await this.confirmButton.click();
  }
}
