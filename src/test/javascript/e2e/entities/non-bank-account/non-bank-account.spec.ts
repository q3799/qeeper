/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, protractor, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { NonBankAccountComponentsPage, NonBankAccountDeleteDialog, NonBankAccountUpdatePage } from './non-bank-account.page-object';

const expect = chai.expect;

describe('NonBankAccount e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let nonBankAccountUpdatePage: NonBankAccountUpdatePage;
  let nonBankAccountComponentsPage: NonBankAccountComponentsPage;
  /*let nonBankAccountDeleteDialog: NonBankAccountDeleteDialog;*/

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load NonBankAccounts', async () => {
    await navBarPage.goToEntity('non-bank-account');
    nonBankAccountComponentsPage = new NonBankAccountComponentsPage();
    await browser.wait(ec.visibilityOf(nonBankAccountComponentsPage.title), 5000);
    expect(await nonBankAccountComponentsPage.getTitle()).to.eq('qeeperApp.nonBankAccount.home.title');
  });

  it('should load create NonBankAccount page', async () => {
    await nonBankAccountComponentsPage.clickOnCreateButton();
    nonBankAccountUpdatePage = new NonBankAccountUpdatePage();
    expect(await nonBankAccountUpdatePage.getPageTitle()).to.eq('qeeperApp.nonBankAccount.home.createOrEditLabel');
    await nonBankAccountUpdatePage.cancel();
  });

  /* it('should create and save NonBankAccounts', async () => {
        const nbButtonsBeforeCreate = await nonBankAccountComponentsPage.countDeleteButtons();

        await nonBankAccountComponentsPage.clickOnCreateButton();
        await promise.all([
            nonBankAccountUpdatePage.setLoginIdInput('loginId'),
            nonBankAccountUpdatePage.setLoginPasswordInput('loginPassword'),
            nonBankAccountUpdatePage.setNameInput('name'),
            nonBankAccountUpdatePage.setEmailInput('email'),
            nonBankAccountUpdatePage.setPhoneInput('phone'),
            nonBankAccountUpdatePage.setPasswordUpdatedOnInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
            nonBankAccountUpdatePage.setExpirationPeriodDaysInput('5'),
            nonBankAccountUpdatePage.accountStatusSelectLastOption(),
            nonBankAccountUpdatePage.setRemarksInput('remarks'),
            nonBankAccountUpdatePage.setAccountIdInput('accountId'),
            nonBankAccountUpdatePage.nonBankClientSelectLastOption(),
            nonBankAccountUpdatePage.nonBankAccountGroupSelectLastOption(),
        ]);
        expect(await nonBankAccountUpdatePage.getLoginIdInput()).to.eq('loginId', 'Expected LoginId value to be equals to loginId');
        expect(await nonBankAccountUpdatePage.getLoginPasswordInput()).to.eq('loginPassword', 'Expected LoginPassword value to be equals to loginPassword');
        expect(await nonBankAccountUpdatePage.getNameInput()).to.eq('name', 'Expected Name value to be equals to name');
        expect(await nonBankAccountUpdatePage.getEmailInput()).to.eq('email', 'Expected Email value to be equals to email');
        expect(await nonBankAccountUpdatePage.getPhoneInput()).to.eq('phone', 'Expected Phone value to be equals to phone');
        const selectedPasswordGetsExpired = nonBankAccountUpdatePage.getPasswordGetsExpiredInput();
        if (await selectedPasswordGetsExpired.isSelected()) {
            await nonBankAccountUpdatePage.getPasswordGetsExpiredInput().click();
            expect(await nonBankAccountUpdatePage.getPasswordGetsExpiredInput().isSelected(), 'Expected passwordGetsExpired not to be selected').to.be.false;
        } else {
            await nonBankAccountUpdatePage.getPasswordGetsExpiredInput().click();
            expect(await nonBankAccountUpdatePage.getPasswordGetsExpiredInput().isSelected(), 'Expected passwordGetsExpired to be selected').to.be.true;
        }
        expect(await nonBankAccountUpdatePage.getPasswordUpdatedOnInput()).to.contain('2001-01-01T02:30', 'Expected passwordUpdatedOn value to be equals to 2000-12-31');
        expect(await nonBankAccountUpdatePage.getExpirationPeriodDaysInput()).to.eq('5', 'Expected expirationPeriodDays value to be equals to 5');
        expect(await nonBankAccountUpdatePage.getRemarksInput()).to.eq('remarks', 'Expected Remarks value to be equals to remarks');
        const selectedMultipleSessionAllowed = nonBankAccountUpdatePage.getMultipleSessionAllowedInput();
        if (await selectedMultipleSessionAllowed.isSelected()) {
            await nonBankAccountUpdatePage.getMultipleSessionAllowedInput().click();
            expect(await nonBankAccountUpdatePage.getMultipleSessionAllowedInput().isSelected(), 'Expected multipleSessionAllowed not to be selected').to.be.false;
        } else {
            await nonBankAccountUpdatePage.getMultipleSessionAllowedInput().click();
            expect(await nonBankAccountUpdatePage.getMultipleSessionAllowedInput().isSelected(), 'Expected multipleSessionAllowed to be selected').to.be.true;
        }
        expect(await nonBankAccountUpdatePage.getAccountIdInput()).to.eq('accountId', 'Expected AccountId value to be equals to accountId');
        await nonBankAccountUpdatePage.save();
        expect(await nonBankAccountUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

        expect(await nonBankAccountComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
    });*/

  /* it('should delete last NonBankAccount', async () => {
        const nbButtonsBeforeDelete = await nonBankAccountComponentsPage.countDeleteButtons();
        await nonBankAccountComponentsPage.clickOnLastDeleteButton();

        nonBankAccountDeleteDialog = new NonBankAccountDeleteDialog();
        expect(await nonBankAccountDeleteDialog.getDialogTitle())
            .to.eq('qeeperApp.nonBankAccount.delete.question');
        await nonBankAccountDeleteDialog.clickOnConfirmButton();

        expect(await nonBankAccountComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });*/

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
