import { browser, ExpectedConditions, element, by, ElementFinder } from 'protractor';

export class NonBankAccountComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-non-bank-account div table .btn-danger'));
  title = element.all(by.css('jhi-non-bank-account div h2#page-heading span')).first();

  async clickOnCreateButton(timeout?: number) {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(timeout?: number) {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class NonBankAccountUpdatePage {
  pageTitle = element(by.id('jhi-non-bank-account-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  loginIdInput = element(by.id('field_loginId'));
  loginPasswordInput = element(by.id('field_loginPassword'));
  nameInput = element(by.id('field_name'));
  emailInput = element(by.id('field_email'));
  phoneInput = element(by.id('field_phone'));
  passwordGetsExpiredInput = element(by.id('field_passwordGetsExpired'));
  passwordUpdatedOnInput = element(by.id('field_passwordUpdatedOn'));
  expirationPeriodDaysInput = element(by.id('field_expirationPeriodDays'));
  accountStatusSelect = element(by.id('field_accountStatus'));
  remarksInput = element(by.id('field_remarks'));
  multipleSessionAllowedInput = element(by.id('field_multipleSessionAllowed'));
  accountIdInput = element(by.id('field_accountId'));
  nonBankClientSelect = element(by.id('field_nonBankClient'));
  nonBankAccountGroupSelect = element(by.id('field_nonBankAccountGroup'));

  async getPageTitle() {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setLoginIdInput(loginId) {
    await this.loginIdInput.sendKeys(loginId);
  }

  async getLoginIdInput() {
    return await this.loginIdInput.getAttribute('value');
  }

  async setLoginPasswordInput(loginPassword) {
    await this.loginPasswordInput.sendKeys(loginPassword);
  }

  async getLoginPasswordInput() {
    return await this.loginPasswordInput.getAttribute('value');
  }

  async setNameInput(name) {
    await this.nameInput.sendKeys(name);
  }

  async getNameInput() {
    return await this.nameInput.getAttribute('value');
  }

  async setEmailInput(email) {
    await this.emailInput.sendKeys(email);
  }

  async getEmailInput() {
    return await this.emailInput.getAttribute('value');
  }

  async setPhoneInput(phone) {
    await this.phoneInput.sendKeys(phone);
  }

  async getPhoneInput() {
    return await this.phoneInput.getAttribute('value');
  }

  getPasswordGetsExpiredInput(timeout?: number) {
    return this.passwordGetsExpiredInput;
  }
  async setPasswordUpdatedOnInput(passwordUpdatedOn) {
    await this.passwordUpdatedOnInput.sendKeys(passwordUpdatedOn);
  }

  async getPasswordUpdatedOnInput() {
    return await this.passwordUpdatedOnInput.getAttribute('value');
  }

  async setExpirationPeriodDaysInput(expirationPeriodDays) {
    await this.expirationPeriodDaysInput.sendKeys(expirationPeriodDays);
  }

  async getExpirationPeriodDaysInput() {
    return await this.expirationPeriodDaysInput.getAttribute('value');
  }

  async setAccountStatusSelect(accountStatus) {
    await this.accountStatusSelect.sendKeys(accountStatus);
  }

  async getAccountStatusSelect() {
    return await this.accountStatusSelect.element(by.css('option:checked')).getText();
  }

  async accountStatusSelectLastOption(timeout?: number) {
    await this.accountStatusSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async setRemarksInput(remarks) {
    await this.remarksInput.sendKeys(remarks);
  }

  async getRemarksInput() {
    return await this.remarksInput.getAttribute('value');
  }

  getMultipleSessionAllowedInput(timeout?: number) {
    return this.multipleSessionAllowedInput;
  }
  async setAccountIdInput(accountId) {
    await this.accountIdInput.sendKeys(accountId);
  }

  async getAccountIdInput() {
    return await this.accountIdInput.getAttribute('value');
  }

  async nonBankClientSelectLastOption(timeout?: number) {
    await this.nonBankClientSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async nonBankClientSelectOption(option) {
    await this.nonBankClientSelect.sendKeys(option);
  }

  getNonBankClientSelect(): ElementFinder {
    return this.nonBankClientSelect;
  }

  async getNonBankClientSelectedOption() {
    return await this.nonBankClientSelect.element(by.css('option:checked')).getText();
  }

  async nonBankAccountGroupSelectLastOption(timeout?: number) {
    await this.nonBankAccountGroupSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async nonBankAccountGroupSelectOption(option) {
    await this.nonBankAccountGroupSelect.sendKeys(option);
  }

  getNonBankAccountGroupSelect(): ElementFinder {
    return this.nonBankAccountGroupSelect;
  }

  async getNonBankAccountGroupSelectedOption() {
    return await this.nonBankAccountGroupSelect.element(by.css('option:checked')).getText();
  }

  async save(timeout?: number) {
    await this.saveButton.click();
  }

  async cancel(timeout?: number) {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class NonBankAccountDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-nonBankAccount-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-nonBankAccount'));

  async getDialogTitle() {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(timeout?: number) {
    await this.confirmButton.click();
  }
}
