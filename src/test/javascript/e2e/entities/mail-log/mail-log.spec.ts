/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, protractor, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { MailLogComponentsPage, MailLogDeleteDialog, MailLogUpdatePage } from './mail-log.page-object';

const expect = chai.expect;

describe('MailLog e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let mailLogUpdatePage: MailLogUpdatePage;
  let mailLogComponentsPage: MailLogComponentsPage;
  let mailLogDeleteDialog: MailLogDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load MailLogs', async () => {
    await navBarPage.goToEntity('mail-log');
    mailLogComponentsPage = new MailLogComponentsPage();
    await browser.wait(ec.visibilityOf(mailLogComponentsPage.title), 5000);
    expect(await mailLogComponentsPage.getTitle()).to.eq('qeeperApp.mailLog.home.title');
  });

  it('should load create MailLog page', async () => {
    await mailLogComponentsPage.clickOnCreateButton();
    mailLogUpdatePage = new MailLogUpdatePage();
    expect(await mailLogUpdatePage.getPageTitle()).to.eq('qeeperApp.mailLog.home.createOrEditLabel');
    await mailLogUpdatePage.cancel();
  });

  it('should create and save MailLogs', async () => {
    const nbButtonsBeforeCreate = await mailLogComponentsPage.countDeleteButtons();

    await mailLogComponentsPage.clickOnCreateButton();
    await promise.all([
      mailLogUpdatePage.setSubjectInput('subject'),
      mailLogUpdatePage.setContentInput('content'),
      mailLogUpdatePage.setMailDateInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
      mailLogUpdatePage.mailServerSelectLastOption(),
      mailLogUpdatePage.setResponseReceivedInput('responseReceived'),
      mailLogUpdatePage.setRemarksInput('remarks'),
      mailLogUpdatePage.setToListInput('toList'),
      mailLogUpdatePage.setCcListInput('ccList'),
      mailLogUpdatePage.setProcessingTimeInput('5')
    ]);
    expect(await mailLogUpdatePage.getSubjectInput()).to.eq('subject', 'Expected Subject value to be equals to subject');
    expect(await mailLogUpdatePage.getContentInput()).to.eq('content', 'Expected Content value to be equals to content');
    expect(await mailLogUpdatePage.getMailDateInput()).to.contain('2001-01-01T02:30', 'Expected mailDate value to be equals to 2000-12-31');
    expect(await mailLogUpdatePage.getResponseReceivedInput()).to.eq(
      'responseReceived',
      'Expected ResponseReceived value to be equals to responseReceived'
    );
    expect(await mailLogUpdatePage.getRemarksInput()).to.eq('remarks', 'Expected Remarks value to be equals to remarks');
    expect(await mailLogUpdatePage.getToListInput()).to.eq('toList', 'Expected ToList value to be equals to toList');
    expect(await mailLogUpdatePage.getCcListInput()).to.eq('ccList', 'Expected CcList value to be equals to ccList');
    expect(await mailLogUpdatePage.getProcessingTimeInput()).to.eq('5', 'Expected processingTime value to be equals to 5');
    await mailLogUpdatePage.save();
    expect(await mailLogUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await mailLogComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last MailLog', async () => {
    const nbButtonsBeforeDelete = await mailLogComponentsPage.countDeleteButtons();
    await mailLogComponentsPage.clickOnLastDeleteButton();

    mailLogDeleteDialog = new MailLogDeleteDialog();
    expect(await mailLogDeleteDialog.getDialogTitle()).to.eq('qeeperApp.mailLog.delete.question');
    await mailLogDeleteDialog.clickOnConfirmButton();

    expect(await mailLogComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
