import { browser, ExpectedConditions, element, by, ElementFinder } from 'protractor';

export class MailLogComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-mail-log div table .btn-danger'));
  title = element.all(by.css('jhi-mail-log div h2#page-heading span')).first();

  async clickOnCreateButton(timeout?: number) {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(timeout?: number) {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class MailLogUpdatePage {
  pageTitle = element(by.id('jhi-mail-log-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  subjectInput = element(by.id('field_subject'));
  contentInput = element(by.id('field_content'));
  mailDateInput = element(by.id('field_mailDate'));
  mailServerSelect = element(by.id('field_mailServer'));
  responseReceivedInput = element(by.id('field_responseReceived'));
  remarksInput = element(by.id('field_remarks'));
  toListInput = element(by.id('field_toList'));
  ccListInput = element(by.id('field_ccList'));
  processingTimeInput = element(by.id('field_processingTime'));

  async getPageTitle() {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setSubjectInput(subject) {
    await this.subjectInput.sendKeys(subject);
  }

  async getSubjectInput() {
    return await this.subjectInput.getAttribute('value');
  }

  async setContentInput(content) {
    await this.contentInput.sendKeys(content);
  }

  async getContentInput() {
    return await this.contentInput.getAttribute('value');
  }

  async setMailDateInput(mailDate) {
    await this.mailDateInput.sendKeys(mailDate);
  }

  async getMailDateInput() {
    return await this.mailDateInput.getAttribute('value');
  }

  async setMailServerSelect(mailServer) {
    await this.mailServerSelect.sendKeys(mailServer);
  }

  async getMailServerSelect() {
    return await this.mailServerSelect.element(by.css('option:checked')).getText();
  }

  async mailServerSelectLastOption(timeout?: number) {
    await this.mailServerSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async setResponseReceivedInput(responseReceived) {
    await this.responseReceivedInput.sendKeys(responseReceived);
  }

  async getResponseReceivedInput() {
    return await this.responseReceivedInput.getAttribute('value');
  }

  async setRemarksInput(remarks) {
    await this.remarksInput.sendKeys(remarks);
  }

  async getRemarksInput() {
    return await this.remarksInput.getAttribute('value');
  }

  async setToListInput(toList) {
    await this.toListInput.sendKeys(toList);
  }

  async getToListInput() {
    return await this.toListInput.getAttribute('value');
  }

  async setCcListInput(ccList) {
    await this.ccListInput.sendKeys(ccList);
  }

  async getCcListInput() {
    return await this.ccListInput.getAttribute('value');
  }

  async setProcessingTimeInput(processingTime) {
    await this.processingTimeInput.sendKeys(processingTime);
  }

  async getProcessingTimeInput() {
    return await this.processingTimeInput.getAttribute('value');
  }

  async save(timeout?: number) {
    await this.saveButton.click();
  }

  async cancel(timeout?: number) {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class MailLogDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-mailLog-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-mailLog'));

  async getDialogTitle() {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(timeout?: number) {
    await this.confirmButton.click();
  }
}
