package com.qk.qeeper.web.rest;

import com.qk.qeeper.QeeperApp;
import com.qk.qeeper.domain.SecretQuestion;
import com.qk.qeeper.domain.BankAccount;
import com.qk.qeeper.repository.SecretQuestionRepository;
import com.qk.qeeper.service.SecretQuestionService;
import com.qk.qeeper.service.dto.SecretQuestionDTO;
import com.qk.qeeper.service.mapper.SecretQuestionMapper;
import com.qk.qeeper.web.rest.errors.ExceptionTranslator;
import com.qk.qeeper.service.dto.SecretQuestionCriteria;
import com.qk.qeeper.service.SecretQuestionQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.qk.qeeper.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link SecretQuestionResource} REST controller.
 */
@SpringBootTest(classes = QeeperApp.class)
public class SecretQuestionResourceIT {

    private static final String DEFAULT_QUESTION = "AAAAAAAAAA";
    private static final String UPDATED_QUESTION = "BBBBBBBBBB";

    private static final String DEFAULT_ANSWER = "AAAAAAAAAA";
    private static final String UPDATED_ANSWER = "BBBBBBBBBB";

    @Autowired
    private SecretQuestionRepository secretQuestionRepository;

    @Autowired
    private SecretQuestionMapper secretQuestionMapper;

    @Autowired
    private SecretQuestionService secretQuestionService;

    @Autowired
    private SecretQuestionQueryService secretQuestionQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restSecretQuestionMockMvc;

    private SecretQuestion secretQuestion;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SecretQuestionResource secretQuestionResource = new SecretQuestionResource(secretQuestionService, secretQuestionQueryService);
        this.restSecretQuestionMockMvc = MockMvcBuilders.standaloneSetup(secretQuestionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SecretQuestion createEntity(EntityManager em) {
        SecretQuestion secretQuestion = new SecretQuestion()
            .question(DEFAULT_QUESTION)
            .answer(DEFAULT_ANSWER);
        // Add required entity
        BankAccount bankAccount;
        if (TestUtil.findAll(em, BankAccount.class).isEmpty()) {
            bankAccount = BankAccountResourceIT.createEntity(em);
            em.persist(bankAccount);
            em.flush();
        } else {
            bankAccount = TestUtil.findAll(em, BankAccount.class).get(0);
        }
        secretQuestion.setBankAccount(bankAccount);
        return secretQuestion;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SecretQuestion createUpdatedEntity(EntityManager em) {
        SecretQuestion secretQuestion = new SecretQuestion()
            .question(UPDATED_QUESTION)
            .answer(UPDATED_ANSWER);
        // Add required entity
        BankAccount bankAccount;
        if (TestUtil.findAll(em, BankAccount.class).isEmpty()) {
            bankAccount = BankAccountResourceIT.createUpdatedEntity(em);
            em.persist(bankAccount);
            em.flush();
        } else {
            bankAccount = TestUtil.findAll(em, BankAccount.class).get(0);
        }
        secretQuestion.setBankAccount(bankAccount);
        return secretQuestion;
    }

    @BeforeEach
    public void initTest() {
        secretQuestion = createEntity(em);
    }

    @Test
    @Transactional
    public void createSecretQuestion() throws Exception {
        int databaseSizeBeforeCreate = secretQuestionRepository.findAll().size();

        // Create the SecretQuestion
        SecretQuestionDTO secretQuestionDTO = secretQuestionMapper.toDto(secretQuestion);
        restSecretQuestionMockMvc.perform(post("/api/secret-questions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(secretQuestionDTO)))
            .andExpect(status().isCreated());

        // Validate the SecretQuestion in the database
        List<SecretQuestion> secretQuestionList = secretQuestionRepository.findAll();
        assertThat(secretQuestionList).hasSize(databaseSizeBeforeCreate + 1);
        SecretQuestion testSecretQuestion = secretQuestionList.get(secretQuestionList.size() - 1);
        assertThat(testSecretQuestion.getQuestion()).isEqualTo(DEFAULT_QUESTION);
        assertThat(testSecretQuestion.getAnswer()).isEqualTo(DEFAULT_ANSWER);
    }

    @Test
    @Transactional
    public void createSecretQuestionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = secretQuestionRepository.findAll().size();

        // Create the SecretQuestion with an existing ID
        secretQuestion.setId(1L);
        SecretQuestionDTO secretQuestionDTO = secretQuestionMapper.toDto(secretQuestion);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSecretQuestionMockMvc.perform(post("/api/secret-questions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(secretQuestionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SecretQuestion in the database
        List<SecretQuestion> secretQuestionList = secretQuestionRepository.findAll();
        assertThat(secretQuestionList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkQuestionIsRequired() throws Exception {
        int databaseSizeBeforeTest = secretQuestionRepository.findAll().size();
        // set the field null
        secretQuestion.setQuestion(null);

        // Create the SecretQuestion, which fails.
        SecretQuestionDTO secretQuestionDTO = secretQuestionMapper.toDto(secretQuestion);

        restSecretQuestionMockMvc.perform(post("/api/secret-questions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(secretQuestionDTO)))
            .andExpect(status().isBadRequest());

        List<SecretQuestion> secretQuestionList = secretQuestionRepository.findAll();
        assertThat(secretQuestionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAnswerIsRequired() throws Exception {
        int databaseSizeBeforeTest = secretQuestionRepository.findAll().size();
        // set the field null
        secretQuestion.setAnswer(null);

        // Create the SecretQuestion, which fails.
        SecretQuestionDTO secretQuestionDTO = secretQuestionMapper.toDto(secretQuestion);

        restSecretQuestionMockMvc.perform(post("/api/secret-questions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(secretQuestionDTO)))
            .andExpect(status().isBadRequest());

        List<SecretQuestion> secretQuestionList = secretQuestionRepository.findAll();
        assertThat(secretQuestionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSecretQuestions() throws Exception {
        // Initialize the database
        secretQuestionRepository.saveAndFlush(secretQuestion);

        // Get all the secretQuestionList
        restSecretQuestionMockMvc.perform(get("/api/secret-questions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(secretQuestion.getId().intValue())))
            .andExpect(jsonPath("$.[*].question").value(hasItem(DEFAULT_QUESTION.toString())))
            .andExpect(jsonPath("$.[*].answer").value(hasItem(DEFAULT_ANSWER.toString())));
    }
    
    @Test
    @Transactional
    public void getSecretQuestion() throws Exception {
        // Initialize the database
        secretQuestionRepository.saveAndFlush(secretQuestion);

        // Get the secretQuestion
        restSecretQuestionMockMvc.perform(get("/api/secret-questions/{id}", secretQuestion.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(secretQuestion.getId().intValue()))
            .andExpect(jsonPath("$.question").value(DEFAULT_QUESTION.toString()))
            .andExpect(jsonPath("$.answer").value(DEFAULT_ANSWER.toString()));
    }

    @Test
    @Transactional
    public void getAllSecretQuestionsByQuestionIsEqualToSomething() throws Exception {
        // Initialize the database
        secretQuestionRepository.saveAndFlush(secretQuestion);

        // Get all the secretQuestionList where question equals to DEFAULT_QUESTION
        defaultSecretQuestionShouldBeFound("question.equals=" + DEFAULT_QUESTION);

        // Get all the secretQuestionList where question equals to UPDATED_QUESTION
        defaultSecretQuestionShouldNotBeFound("question.equals=" + UPDATED_QUESTION);
    }

    @Test
    @Transactional
    public void getAllSecretQuestionsByQuestionIsInShouldWork() throws Exception {
        // Initialize the database
        secretQuestionRepository.saveAndFlush(secretQuestion);

        // Get all the secretQuestionList where question in DEFAULT_QUESTION or UPDATED_QUESTION
        defaultSecretQuestionShouldBeFound("question.in=" + DEFAULT_QUESTION + "," + UPDATED_QUESTION);

        // Get all the secretQuestionList where question equals to UPDATED_QUESTION
        defaultSecretQuestionShouldNotBeFound("question.in=" + UPDATED_QUESTION);
    }

    @Test
    @Transactional
    public void getAllSecretQuestionsByQuestionIsNullOrNotNull() throws Exception {
        // Initialize the database
        secretQuestionRepository.saveAndFlush(secretQuestion);

        // Get all the secretQuestionList where question is not null
        defaultSecretQuestionShouldBeFound("question.specified=true");

        // Get all the secretQuestionList where question is null
        defaultSecretQuestionShouldNotBeFound("question.specified=false");
    }

    @Test
    @Transactional
    public void getAllSecretQuestionsByAnswerIsEqualToSomething() throws Exception {
        // Initialize the database
        secretQuestionRepository.saveAndFlush(secretQuestion);

        // Get all the secretQuestionList where answer equals to DEFAULT_ANSWER
        defaultSecretQuestionShouldBeFound("answer.equals=" + DEFAULT_ANSWER);

        // Get all the secretQuestionList where answer equals to UPDATED_ANSWER
        defaultSecretQuestionShouldNotBeFound("answer.equals=" + UPDATED_ANSWER);
    }

    @Test
    @Transactional
    public void getAllSecretQuestionsByAnswerIsInShouldWork() throws Exception {
        // Initialize the database
        secretQuestionRepository.saveAndFlush(secretQuestion);

        // Get all the secretQuestionList where answer in DEFAULT_ANSWER or UPDATED_ANSWER
        defaultSecretQuestionShouldBeFound("answer.in=" + DEFAULT_ANSWER + "," + UPDATED_ANSWER);

        // Get all the secretQuestionList where answer equals to UPDATED_ANSWER
        defaultSecretQuestionShouldNotBeFound("answer.in=" + UPDATED_ANSWER);
    }

    @Test
    @Transactional
    public void getAllSecretQuestionsByAnswerIsNullOrNotNull() throws Exception {
        // Initialize the database
        secretQuestionRepository.saveAndFlush(secretQuestion);

        // Get all the secretQuestionList where answer is not null
        defaultSecretQuestionShouldBeFound("answer.specified=true");

        // Get all the secretQuestionList where answer is null
        defaultSecretQuestionShouldNotBeFound("answer.specified=false");
    }

    @Test
    @Transactional
    public void getAllSecretQuestionsByBankAccountIsEqualToSomething() throws Exception {
        // Get already existing entity
        BankAccount bankAccount = secretQuestion.getBankAccount();
        secretQuestionRepository.saveAndFlush(secretQuestion);
        Long bankAccountId = bankAccount.getId();

        // Get all the secretQuestionList where bankAccount equals to bankAccountId
        defaultSecretQuestionShouldBeFound("bankAccountId.equals=" + bankAccountId);

        // Get all the secretQuestionList where bankAccount equals to bankAccountId + 1
        defaultSecretQuestionShouldNotBeFound("bankAccountId.equals=" + (bankAccountId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultSecretQuestionShouldBeFound(String filter) throws Exception {
        restSecretQuestionMockMvc.perform(get("/api/secret-questions?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(secretQuestion.getId().intValue())))
            .andExpect(jsonPath("$.[*].question").value(hasItem(DEFAULT_QUESTION)))
            .andExpect(jsonPath("$.[*].answer").value(hasItem(DEFAULT_ANSWER)));

        // Check, that the count call also returns 1
        restSecretQuestionMockMvc.perform(get("/api/secret-questions/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultSecretQuestionShouldNotBeFound(String filter) throws Exception {
        restSecretQuestionMockMvc.perform(get("/api/secret-questions?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restSecretQuestionMockMvc.perform(get("/api/secret-questions/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingSecretQuestion() throws Exception {
        // Get the secretQuestion
        restSecretQuestionMockMvc.perform(get("/api/secret-questions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSecretQuestion() throws Exception {
        // Initialize the database
        secretQuestionRepository.saveAndFlush(secretQuestion);

        int databaseSizeBeforeUpdate = secretQuestionRepository.findAll().size();

        // Update the secretQuestion
        SecretQuestion updatedSecretQuestion = secretQuestionRepository.findById(secretQuestion.getId()).get();
        // Disconnect from session so that the updates on updatedSecretQuestion are not directly saved in db
        em.detach(updatedSecretQuestion);
        updatedSecretQuestion
            .question(UPDATED_QUESTION)
            .answer(UPDATED_ANSWER);
        SecretQuestionDTO secretQuestionDTO = secretQuestionMapper.toDto(updatedSecretQuestion);

        restSecretQuestionMockMvc.perform(put("/api/secret-questions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(secretQuestionDTO)))
            .andExpect(status().isOk());

        // Validate the SecretQuestion in the database
        List<SecretQuestion> secretQuestionList = secretQuestionRepository.findAll();
        assertThat(secretQuestionList).hasSize(databaseSizeBeforeUpdate);
        SecretQuestion testSecretQuestion = secretQuestionList.get(secretQuestionList.size() - 1);
        assertThat(testSecretQuestion.getQuestion()).isEqualTo(UPDATED_QUESTION);
        assertThat(testSecretQuestion.getAnswer()).isEqualTo(UPDATED_ANSWER);
    }

    @Test
    @Transactional
    public void updateNonExistingSecretQuestion() throws Exception {
        int databaseSizeBeforeUpdate = secretQuestionRepository.findAll().size();

        // Create the SecretQuestion
        SecretQuestionDTO secretQuestionDTO = secretQuestionMapper.toDto(secretQuestion);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSecretQuestionMockMvc.perform(put("/api/secret-questions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(secretQuestionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SecretQuestion in the database
        List<SecretQuestion> secretQuestionList = secretQuestionRepository.findAll();
        assertThat(secretQuestionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSecretQuestion() throws Exception {
        // Initialize the database
        secretQuestionRepository.saveAndFlush(secretQuestion);

        int databaseSizeBeforeDelete = secretQuestionRepository.findAll().size();

        // Delete the secretQuestion
        restSecretQuestionMockMvc.perform(delete("/api/secret-questions/{id}", secretQuestion.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<SecretQuestion> secretQuestionList = secretQuestionRepository.findAll();
        assertThat(secretQuestionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SecretQuestion.class);
        SecretQuestion secretQuestion1 = new SecretQuestion();
        secretQuestion1.setId(1L);
        SecretQuestion secretQuestion2 = new SecretQuestion();
        secretQuestion2.setId(secretQuestion1.getId());
        assertThat(secretQuestion1).isEqualTo(secretQuestion2);
        secretQuestion2.setId(2L);
        assertThat(secretQuestion1).isNotEqualTo(secretQuestion2);
        secretQuestion1.setId(null);
        assertThat(secretQuestion1).isNotEqualTo(secretQuestion2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SecretQuestionDTO.class);
        SecretQuestionDTO secretQuestionDTO1 = new SecretQuestionDTO();
        secretQuestionDTO1.setId(1L);
        SecretQuestionDTO secretQuestionDTO2 = new SecretQuestionDTO();
        assertThat(secretQuestionDTO1).isNotEqualTo(secretQuestionDTO2);
        secretQuestionDTO2.setId(secretQuestionDTO1.getId());
        assertThat(secretQuestionDTO1).isEqualTo(secretQuestionDTO2);
        secretQuestionDTO2.setId(2L);
        assertThat(secretQuestionDTO1).isNotEqualTo(secretQuestionDTO2);
        secretQuestionDTO1.setId(null);
        assertThat(secretQuestionDTO1).isNotEqualTo(secretQuestionDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(secretQuestionMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(secretQuestionMapper.fromId(null)).isNull();
    }
}
