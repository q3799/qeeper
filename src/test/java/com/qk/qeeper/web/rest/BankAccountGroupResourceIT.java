package com.qk.qeeper.web.rest;

import com.qk.qeeper.QeeperApp;
import com.qk.qeeper.domain.BankAccountGroup;
import com.qk.qeeper.domain.BankAccount;
import com.qk.qeeper.repository.BankAccountGroupRepository;
import com.qk.qeeper.service.BankAccountGroupService;
import com.qk.qeeper.service.dto.BankAccountGroupDTO;
import com.qk.qeeper.service.mapper.BankAccountGroupMapper;
import com.qk.qeeper.web.rest.errors.ExceptionTranslator;
import com.qk.qeeper.service.dto.BankAccountGroupCriteria;
import com.qk.qeeper.service.BankAccountGroupQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.qk.qeeper.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link BankAccountGroupResource} REST controller.
 */
@SpringBootTest(classes = QeeperApp.class)
public class BankAccountGroupResourceIT {

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private BankAccountGroupRepository bankAccountGroupRepository;

    @Autowired
    private BankAccountGroupMapper bankAccountGroupMapper;

    @Autowired
    private BankAccountGroupService bankAccountGroupService;

    @Autowired
    private BankAccountGroupQueryService bankAccountGroupQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restBankAccountGroupMockMvc;

    private BankAccountGroup bankAccountGroup;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final BankAccountGroupResource bankAccountGroupResource = new BankAccountGroupResource(bankAccountGroupService, bankAccountGroupQueryService);
        this.restBankAccountGroupMockMvc = MockMvcBuilders.standaloneSetup(bankAccountGroupResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BankAccountGroup createEntity(EntityManager em) {
        BankAccountGroup bankAccountGroup = new BankAccountGroup()
            .code(DEFAULT_CODE)
            .name(DEFAULT_NAME);
        return bankAccountGroup;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BankAccountGroup createUpdatedEntity(EntityManager em) {
        BankAccountGroup bankAccountGroup = new BankAccountGroup()
            .code(UPDATED_CODE)
            .name(UPDATED_NAME);
        return bankAccountGroup;
    }

    @BeforeEach
    public void initTest() {
        bankAccountGroup = createEntity(em);
    }

    @Test
    @Transactional
    public void createBankAccountGroup() throws Exception {
        int databaseSizeBeforeCreate = bankAccountGroupRepository.findAll().size();

        // Create the BankAccountGroup
        BankAccountGroupDTO bankAccountGroupDTO = bankAccountGroupMapper.toDto(bankAccountGroup);
        restBankAccountGroupMockMvc.perform(post("/api/bank-account-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bankAccountGroupDTO)))
            .andExpect(status().isCreated());

        // Validate the BankAccountGroup in the database
        List<BankAccountGroup> bankAccountGroupList = bankAccountGroupRepository.findAll();
        assertThat(bankAccountGroupList).hasSize(databaseSizeBeforeCreate + 1);
        BankAccountGroup testBankAccountGroup = bankAccountGroupList.get(bankAccountGroupList.size() - 1);
        assertThat(testBankAccountGroup.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testBankAccountGroup.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createBankAccountGroupWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = bankAccountGroupRepository.findAll().size();

        // Create the BankAccountGroup with an existing ID
        bankAccountGroup.setId(1L);
        BankAccountGroupDTO bankAccountGroupDTO = bankAccountGroupMapper.toDto(bankAccountGroup);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBankAccountGroupMockMvc.perform(post("/api/bank-account-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bankAccountGroupDTO)))
            .andExpect(status().isBadRequest());

        // Validate the BankAccountGroup in the database
        List<BankAccountGroup> bankAccountGroupList = bankAccountGroupRepository.findAll();
        assertThat(bankAccountGroupList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = bankAccountGroupRepository.findAll().size();
        // set the field null
        bankAccountGroup.setCode(null);

        // Create the BankAccountGroup, which fails.
        BankAccountGroupDTO bankAccountGroupDTO = bankAccountGroupMapper.toDto(bankAccountGroup);

        restBankAccountGroupMockMvc.perform(post("/api/bank-account-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bankAccountGroupDTO)))
            .andExpect(status().isBadRequest());

        List<BankAccountGroup> bankAccountGroupList = bankAccountGroupRepository.findAll();
        assertThat(bankAccountGroupList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = bankAccountGroupRepository.findAll().size();
        // set the field null
        bankAccountGroup.setName(null);

        // Create the BankAccountGroup, which fails.
        BankAccountGroupDTO bankAccountGroupDTO = bankAccountGroupMapper.toDto(bankAccountGroup);

        restBankAccountGroupMockMvc.perform(post("/api/bank-account-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bankAccountGroupDTO)))
            .andExpect(status().isBadRequest());

        List<BankAccountGroup> bankAccountGroupList = bankAccountGroupRepository.findAll();
        assertThat(bankAccountGroupList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllBankAccountGroups() throws Exception {
        // Initialize the database
        bankAccountGroupRepository.saveAndFlush(bankAccountGroup);

        // Get all the bankAccountGroupList
        restBankAccountGroupMockMvc.perform(get("/api/bank-account-groups?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bankAccountGroup.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }
    
    @Test
    @Transactional
    public void getBankAccountGroup() throws Exception {
        // Initialize the database
        bankAccountGroupRepository.saveAndFlush(bankAccountGroup);

        // Get the bankAccountGroup
        restBankAccountGroupMockMvc.perform(get("/api/bank-account-groups/{id}", bankAccountGroup.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(bankAccountGroup.getId().intValue()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getAllBankAccountGroupsByCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        bankAccountGroupRepository.saveAndFlush(bankAccountGroup);

        // Get all the bankAccountGroupList where code equals to DEFAULT_CODE
        defaultBankAccountGroupShouldBeFound("code.equals=" + DEFAULT_CODE);

        // Get all the bankAccountGroupList where code equals to UPDATED_CODE
        defaultBankAccountGroupShouldNotBeFound("code.equals=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    public void getAllBankAccountGroupsByCodeIsInShouldWork() throws Exception {
        // Initialize the database
        bankAccountGroupRepository.saveAndFlush(bankAccountGroup);

        // Get all the bankAccountGroupList where code in DEFAULT_CODE or UPDATED_CODE
        defaultBankAccountGroupShouldBeFound("code.in=" + DEFAULT_CODE + "," + UPDATED_CODE);

        // Get all the bankAccountGroupList where code equals to UPDATED_CODE
        defaultBankAccountGroupShouldNotBeFound("code.in=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    public void getAllBankAccountGroupsByCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        bankAccountGroupRepository.saveAndFlush(bankAccountGroup);

        // Get all the bankAccountGroupList where code is not null
        defaultBankAccountGroupShouldBeFound("code.specified=true");

        // Get all the bankAccountGroupList where code is null
        defaultBankAccountGroupShouldNotBeFound("code.specified=false");
    }

    @Test
    @Transactional
    public void getAllBankAccountGroupsByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        bankAccountGroupRepository.saveAndFlush(bankAccountGroup);

        // Get all the bankAccountGroupList where name equals to DEFAULT_NAME
        defaultBankAccountGroupShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the bankAccountGroupList where name equals to UPDATED_NAME
        defaultBankAccountGroupShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllBankAccountGroupsByNameIsInShouldWork() throws Exception {
        // Initialize the database
        bankAccountGroupRepository.saveAndFlush(bankAccountGroup);

        // Get all the bankAccountGroupList where name in DEFAULT_NAME or UPDATED_NAME
        defaultBankAccountGroupShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the bankAccountGroupList where name equals to UPDATED_NAME
        defaultBankAccountGroupShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllBankAccountGroupsByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        bankAccountGroupRepository.saveAndFlush(bankAccountGroup);

        // Get all the bankAccountGroupList where name is not null
        defaultBankAccountGroupShouldBeFound("name.specified=true");

        // Get all the bankAccountGroupList where name is null
        defaultBankAccountGroupShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    public void getAllBankAccountGroupsByBankAccountsIsEqualToSomething() throws Exception {
        // Initialize the database
        BankAccount bankAccounts = BankAccountResourceIT.createEntity(em);
        em.persist(bankAccounts);
        em.flush();
        bankAccountGroup.addBankAccounts(bankAccounts);
        bankAccountGroupRepository.saveAndFlush(bankAccountGroup);
        Long bankAccountsId = bankAccounts.getId();

        // Get all the bankAccountGroupList where bankAccounts equals to bankAccountsId
        defaultBankAccountGroupShouldBeFound("bankAccountsId.equals=" + bankAccountsId);

        // Get all the bankAccountGroupList where bankAccounts equals to bankAccountsId + 1
        defaultBankAccountGroupShouldNotBeFound("bankAccountsId.equals=" + (bankAccountsId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultBankAccountGroupShouldBeFound(String filter) throws Exception {
        restBankAccountGroupMockMvc.perform(get("/api/bank-account-groups?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bankAccountGroup.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));

        // Check, that the count call also returns 1
        restBankAccountGroupMockMvc.perform(get("/api/bank-account-groups/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultBankAccountGroupShouldNotBeFound(String filter) throws Exception {
        restBankAccountGroupMockMvc.perform(get("/api/bank-account-groups?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restBankAccountGroupMockMvc.perform(get("/api/bank-account-groups/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingBankAccountGroup() throws Exception {
        // Get the bankAccountGroup
        restBankAccountGroupMockMvc.perform(get("/api/bank-account-groups/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBankAccountGroup() throws Exception {
        // Initialize the database
        bankAccountGroupRepository.saveAndFlush(bankAccountGroup);

        int databaseSizeBeforeUpdate = bankAccountGroupRepository.findAll().size();

        // Update the bankAccountGroup
        BankAccountGroup updatedBankAccountGroup = bankAccountGroupRepository.findById(bankAccountGroup.getId()).get();
        // Disconnect from session so that the updates on updatedBankAccountGroup are not directly saved in db
        em.detach(updatedBankAccountGroup);
        updatedBankAccountGroup
            .code(UPDATED_CODE)
            .name(UPDATED_NAME);
        BankAccountGroupDTO bankAccountGroupDTO = bankAccountGroupMapper.toDto(updatedBankAccountGroup);

        restBankAccountGroupMockMvc.perform(put("/api/bank-account-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bankAccountGroupDTO)))
            .andExpect(status().isOk());

        // Validate the BankAccountGroup in the database
        List<BankAccountGroup> bankAccountGroupList = bankAccountGroupRepository.findAll();
        assertThat(bankAccountGroupList).hasSize(databaseSizeBeforeUpdate);
        BankAccountGroup testBankAccountGroup = bankAccountGroupList.get(bankAccountGroupList.size() - 1);
        assertThat(testBankAccountGroup.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testBankAccountGroup.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingBankAccountGroup() throws Exception {
        int databaseSizeBeforeUpdate = bankAccountGroupRepository.findAll().size();

        // Create the BankAccountGroup
        BankAccountGroupDTO bankAccountGroupDTO = bankAccountGroupMapper.toDto(bankAccountGroup);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBankAccountGroupMockMvc.perform(put("/api/bank-account-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bankAccountGroupDTO)))
            .andExpect(status().isBadRequest());

        // Validate the BankAccountGroup in the database
        List<BankAccountGroup> bankAccountGroupList = bankAccountGroupRepository.findAll();
        assertThat(bankAccountGroupList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBankAccountGroup() throws Exception {
        // Initialize the database
        bankAccountGroupRepository.saveAndFlush(bankAccountGroup);

        int databaseSizeBeforeDelete = bankAccountGroupRepository.findAll().size();

        // Delete the bankAccountGroup
        restBankAccountGroupMockMvc.perform(delete("/api/bank-account-groups/{id}", bankAccountGroup.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<BankAccountGroup> bankAccountGroupList = bankAccountGroupRepository.findAll();
        assertThat(bankAccountGroupList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BankAccountGroup.class);
        BankAccountGroup bankAccountGroup1 = new BankAccountGroup();
        bankAccountGroup1.setId(1L);
        BankAccountGroup bankAccountGroup2 = new BankAccountGroup();
        bankAccountGroup2.setId(bankAccountGroup1.getId());
        assertThat(bankAccountGroup1).isEqualTo(bankAccountGroup2);
        bankAccountGroup2.setId(2L);
        assertThat(bankAccountGroup1).isNotEqualTo(bankAccountGroup2);
        bankAccountGroup1.setId(null);
        assertThat(bankAccountGroup1).isNotEqualTo(bankAccountGroup2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(BankAccountGroupDTO.class);
        BankAccountGroupDTO bankAccountGroupDTO1 = new BankAccountGroupDTO();
        bankAccountGroupDTO1.setId(1L);
        BankAccountGroupDTO bankAccountGroupDTO2 = new BankAccountGroupDTO();
        assertThat(bankAccountGroupDTO1).isNotEqualTo(bankAccountGroupDTO2);
        bankAccountGroupDTO2.setId(bankAccountGroupDTO1.getId());
        assertThat(bankAccountGroupDTO1).isEqualTo(bankAccountGroupDTO2);
        bankAccountGroupDTO2.setId(2L);
        assertThat(bankAccountGroupDTO1).isNotEqualTo(bankAccountGroupDTO2);
        bankAccountGroupDTO1.setId(null);
        assertThat(bankAccountGroupDTO1).isNotEqualTo(bankAccountGroupDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(bankAccountGroupMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(bankAccountGroupMapper.fromId(null)).isNull();
    }
}
