package com.qk.qeeper.web.rest;

import com.qk.qeeper.QeeperApp;
import com.qk.qeeper.domain.NonBankAccount;
import com.qk.qeeper.domain.NonBankClient;
import com.qk.qeeper.domain.NonBankAccountGroup;
import com.qk.qeeper.repository.NonBankAccountRepository;
import com.qk.qeeper.service.NonBankAccountService;
import com.qk.qeeper.service.dto.NonBankAccountDTO;
import com.qk.qeeper.service.mapper.NonBankAccountMapper;
import com.qk.qeeper.web.rest.errors.ExceptionTranslator;
import com.qk.qeeper.service.dto.NonBankAccountCriteria;
import com.qk.qeeper.service.NonBankAccountQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.qk.qeeper.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.qk.qeeper.domain.enumeration.AccountStatus;
/**
 * Integration tests for the {@Link NonBankAccountResource} REST controller.
 */
@SpringBootTest(classes = QeeperApp.class)
public class NonBankAccountResourceIT {

    private static final String DEFAULT_LOGIN_ID = "AAAAAAAAAA";
    private static final String UPDATED_LOGIN_ID = "BBBBBBBBBB";

    private static final String DEFAULT_LOGIN_PASSWORD = "AAAAAAAAAA";
    private static final String UPDATED_LOGIN_PASSWORD = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "o@[471.378.769.3";
    private static final String UPDATED_EMAIL = "ng@At.m.sj]";

    private static final String DEFAULT_PHONE = "+661709413";
    private static final String UPDATED_PHONE = "+23367";

    private static final Boolean DEFAULT_PASSWORD_GETS_EXPIRED = false;
    private static final Boolean UPDATED_PASSWORD_GETS_EXPIRED = true;

    private static final Instant DEFAULT_PASSWORD_UPDATED_ON = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_PASSWORD_UPDATED_ON = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_EXPIRATION_PERIOD_DAYS = 0;
    private static final Integer UPDATED_EXPIRATION_PERIOD_DAYS = 1;

    private static final AccountStatus DEFAULT_ACCOUNT_STATUS = AccountStatus.ACTIVE;
    private static final AccountStatus UPDATED_ACCOUNT_STATUS = AccountStatus.IN_USE;

    private static final String DEFAULT_REMARKS = "AAAAAAAAAA";
    private static final String UPDATED_REMARKS = "BBBBBBBBBB";

    private static final Boolean DEFAULT_MULTIPLE_SESSION_ALLOWED = false;
    private static final Boolean UPDATED_MULTIPLE_SESSION_ALLOWED = true;

    private static final String DEFAULT_ACCOUNT_ID = "AAAAAAAAAA";
    private static final String UPDATED_ACCOUNT_ID = "BBBBBBBBBB";

    @Autowired
    private NonBankAccountRepository nonBankAccountRepository;

    @Autowired
    private NonBankAccountMapper nonBankAccountMapper;

    @Autowired
    private NonBankAccountService nonBankAccountService;

    @Autowired
    private NonBankAccountQueryService nonBankAccountQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restNonBankAccountMockMvc;

    private NonBankAccount nonBankAccount;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final NonBankAccountResource nonBankAccountResource = new NonBankAccountResource(nonBankAccountService, nonBankAccountQueryService);
        this.restNonBankAccountMockMvc = MockMvcBuilders.standaloneSetup(nonBankAccountResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NonBankAccount createEntity(EntityManager em) {
        NonBankAccount nonBankAccount = new NonBankAccount()
            .loginId(DEFAULT_LOGIN_ID)
            .loginPassword(DEFAULT_LOGIN_PASSWORD)
            .name(DEFAULT_NAME)
            .email(DEFAULT_EMAIL)
            .phone(DEFAULT_PHONE)
            .passwordGetsExpired(DEFAULT_PASSWORD_GETS_EXPIRED)
            .passwordUpdatedOn(DEFAULT_PASSWORD_UPDATED_ON)
            .expirationPeriodDays(DEFAULT_EXPIRATION_PERIOD_DAYS)
            .accountStatus(DEFAULT_ACCOUNT_STATUS)
            .remarks(DEFAULT_REMARKS)
            .multipleSessionAllowed(DEFAULT_MULTIPLE_SESSION_ALLOWED)
            .accountId(DEFAULT_ACCOUNT_ID);
        // Add required entity
        NonBankClient nonBankClient;
        if (TestUtil.findAll(em, NonBankClient.class).isEmpty()) {
            nonBankClient = NonBankClientResourceIT.createEntity(em);
            em.persist(nonBankClient);
            em.flush();
        } else {
            nonBankClient = TestUtil.findAll(em, NonBankClient.class).get(0);
        }
        nonBankAccount.setNonBankClient(nonBankClient);
        return nonBankAccount;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NonBankAccount createUpdatedEntity(EntityManager em) {
        NonBankAccount nonBankAccount = new NonBankAccount()
            .loginId(UPDATED_LOGIN_ID)
            .loginPassword(UPDATED_LOGIN_PASSWORD)
            .name(UPDATED_NAME)
            .email(UPDATED_EMAIL)
            .phone(UPDATED_PHONE)
            .passwordGetsExpired(UPDATED_PASSWORD_GETS_EXPIRED)
            .passwordUpdatedOn(UPDATED_PASSWORD_UPDATED_ON)
            .expirationPeriodDays(UPDATED_EXPIRATION_PERIOD_DAYS)
            .accountStatus(UPDATED_ACCOUNT_STATUS)
            .remarks(UPDATED_REMARKS)
            .multipleSessionAllowed(UPDATED_MULTIPLE_SESSION_ALLOWED)
            .accountId(UPDATED_ACCOUNT_ID);
        // Add required entity
        NonBankClient nonBankClient;
        if (TestUtil.findAll(em, NonBankClient.class).isEmpty()) {
            nonBankClient = NonBankClientResourceIT.createUpdatedEntity(em);
            em.persist(nonBankClient);
            em.flush();
        } else {
            nonBankClient = TestUtil.findAll(em, NonBankClient.class).get(0);
        }
        nonBankAccount.setNonBankClient(nonBankClient);
        return nonBankAccount;
    }

    @BeforeEach
    public void initTest() {
        nonBankAccount = createEntity(em);
    }

    @Test
    @Transactional
    public void createNonBankAccount() throws Exception {
        int databaseSizeBeforeCreate = nonBankAccountRepository.findAll().size();

        // Create the NonBankAccount
        NonBankAccountDTO nonBankAccountDTO = nonBankAccountMapper.toDto(nonBankAccount);
        restNonBankAccountMockMvc.perform(post("/api/non-bank-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(nonBankAccountDTO)))
            .andExpect(status().isCreated());

        // Validate the NonBankAccount in the database
        List<NonBankAccount> nonBankAccountList = nonBankAccountRepository.findAll();
        assertThat(nonBankAccountList).hasSize(databaseSizeBeforeCreate + 1);
        NonBankAccount testNonBankAccount = nonBankAccountList.get(nonBankAccountList.size() - 1);
        assertThat(testNonBankAccount.getLoginId()).isEqualTo(DEFAULT_LOGIN_ID);
        assertThat(testNonBankAccount.getLoginPassword()).isEqualTo(DEFAULT_LOGIN_PASSWORD);
        assertThat(testNonBankAccount.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testNonBankAccount.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testNonBankAccount.getPhone()).isEqualTo(DEFAULT_PHONE);
        assertThat(testNonBankAccount.isPasswordGetsExpired()).isEqualTo(DEFAULT_PASSWORD_GETS_EXPIRED);
        assertThat(testNonBankAccount.getPasswordUpdatedOn()).isEqualTo(DEFAULT_PASSWORD_UPDATED_ON);
        assertThat(testNonBankAccount.getExpirationPeriodDays()).isEqualTo(DEFAULT_EXPIRATION_PERIOD_DAYS);
        assertThat(testNonBankAccount.getAccountStatus()).isEqualTo(DEFAULT_ACCOUNT_STATUS);
        assertThat(testNonBankAccount.getRemarks()).isEqualTo(DEFAULT_REMARKS);
        assertThat(testNonBankAccount.isMultipleSessionAllowed()).isEqualTo(DEFAULT_MULTIPLE_SESSION_ALLOWED);
        assertThat(testNonBankAccount.getAccountId()).isEqualTo(DEFAULT_ACCOUNT_ID);
    }

    @Test
    @Transactional
    public void createNonBankAccountWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = nonBankAccountRepository.findAll().size();

        // Create the NonBankAccount with an existing ID
        nonBankAccount.setId(1L);
        NonBankAccountDTO nonBankAccountDTO = nonBankAccountMapper.toDto(nonBankAccount);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNonBankAccountMockMvc.perform(post("/api/non-bank-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(nonBankAccountDTO)))
            .andExpect(status().isBadRequest());

        // Validate the NonBankAccount in the database
        List<NonBankAccount> nonBankAccountList = nonBankAccountRepository.findAll();
        assertThat(nonBankAccountList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkLoginIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = nonBankAccountRepository.findAll().size();
        // set the field null
        nonBankAccount.setLoginId(null);

        // Create the NonBankAccount, which fails.
        NonBankAccountDTO nonBankAccountDTO = nonBankAccountMapper.toDto(nonBankAccount);

        restNonBankAccountMockMvc.perform(post("/api/non-bank-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(nonBankAccountDTO)))
            .andExpect(status().isBadRequest());

        List<NonBankAccount> nonBankAccountList = nonBankAccountRepository.findAll();
        assertThat(nonBankAccountList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLoginPasswordIsRequired() throws Exception {
        int databaseSizeBeforeTest = nonBankAccountRepository.findAll().size();
        // set the field null
        nonBankAccount.setLoginPassword(null);

        // Create the NonBankAccount, which fails.
        NonBankAccountDTO nonBankAccountDTO = nonBankAccountMapper.toDto(nonBankAccount);

        restNonBankAccountMockMvc.perform(post("/api/non-bank-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(nonBankAccountDTO)))
            .andExpect(status().isBadRequest());

        List<NonBankAccount> nonBankAccountList = nonBankAccountRepository.findAll();
        assertThat(nonBankAccountList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEmailIsRequired() throws Exception {
        int databaseSizeBeforeTest = nonBankAccountRepository.findAll().size();
        // set the field null
        nonBankAccount.setEmail(null);

        // Create the NonBankAccount, which fails.
        NonBankAccountDTO nonBankAccountDTO = nonBankAccountMapper.toDto(nonBankAccount);

        restNonBankAccountMockMvc.perform(post("/api/non-bank-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(nonBankAccountDTO)))
            .andExpect(status().isBadRequest());

        List<NonBankAccount> nonBankAccountList = nonBankAccountRepository.findAll();
        assertThat(nonBankAccountList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPasswordGetsExpiredIsRequired() throws Exception {
        int databaseSizeBeforeTest = nonBankAccountRepository.findAll().size();
        // set the field null
        nonBankAccount.setPasswordGetsExpired(null);

        // Create the NonBankAccount, which fails.
        NonBankAccountDTO nonBankAccountDTO = nonBankAccountMapper.toDto(nonBankAccount);

        restNonBankAccountMockMvc.perform(post("/api/non-bank-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(nonBankAccountDTO)))
            .andExpect(status().isBadRequest());

        List<NonBankAccount> nonBankAccountList = nonBankAccountRepository.findAll();
        assertThat(nonBankAccountList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAccountStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = nonBankAccountRepository.findAll().size();
        // set the field null
        nonBankAccount.setAccountStatus(null);

        // Create the NonBankAccount, which fails.
        NonBankAccountDTO nonBankAccountDTO = nonBankAccountMapper.toDto(nonBankAccount);

        restNonBankAccountMockMvc.perform(post("/api/non-bank-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(nonBankAccountDTO)))
            .andExpect(status().isBadRequest());

        List<NonBankAccount> nonBankAccountList = nonBankAccountRepository.findAll();
        assertThat(nonBankAccountList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkMultipleSessionAllowedIsRequired() throws Exception {
        int databaseSizeBeforeTest = nonBankAccountRepository.findAll().size();
        // set the field null
        nonBankAccount.setMultipleSessionAllowed(null);

        // Create the NonBankAccount, which fails.
        NonBankAccountDTO nonBankAccountDTO = nonBankAccountMapper.toDto(nonBankAccount);

        restNonBankAccountMockMvc.perform(post("/api/non-bank-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(nonBankAccountDTO)))
            .andExpect(status().isBadRequest());

        List<NonBankAccount> nonBankAccountList = nonBankAccountRepository.findAll();
        assertThat(nonBankAccountList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllNonBankAccounts() throws Exception {
        // Initialize the database
        nonBankAccountRepository.saveAndFlush(nonBankAccount);

        // Get all the nonBankAccountList
        restNonBankAccountMockMvc.perform(get("/api/non-bank-accounts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(nonBankAccount.getId().intValue())))
            .andExpect(jsonPath("$.[*].loginId").value(hasItem(DEFAULT_LOGIN_ID.toString())))
            .andExpect(jsonPath("$.[*].loginPassword").value(hasItem(DEFAULT_LOGIN_PASSWORD.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE.toString())))
            .andExpect(jsonPath("$.[*].passwordGetsExpired").value(hasItem(DEFAULT_PASSWORD_GETS_EXPIRED.booleanValue())))
            .andExpect(jsonPath("$.[*].passwordUpdatedOn").value(hasItem(DEFAULT_PASSWORD_UPDATED_ON.toString())))
            .andExpect(jsonPath("$.[*].expirationPeriodDays").value(hasItem(DEFAULT_EXPIRATION_PERIOD_DAYS)))
            .andExpect(jsonPath("$.[*].accountStatus").value(hasItem(DEFAULT_ACCOUNT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS.toString())))
            .andExpect(jsonPath("$.[*].multipleSessionAllowed").value(hasItem(DEFAULT_MULTIPLE_SESSION_ALLOWED.booleanValue())))
            .andExpect(jsonPath("$.[*].accountId").value(hasItem(DEFAULT_ACCOUNT_ID.toString())));
    }
    
    @Test
    @Transactional
    public void getNonBankAccount() throws Exception {
        // Initialize the database
        nonBankAccountRepository.saveAndFlush(nonBankAccount);

        // Get the nonBankAccount
        restNonBankAccountMockMvc.perform(get("/api/non-bank-accounts/{id}", nonBankAccount.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(nonBankAccount.getId().intValue()))
            .andExpect(jsonPath("$.loginId").value(DEFAULT_LOGIN_ID.toString()))
            .andExpect(jsonPath("$.loginPassword").value(DEFAULT_LOGIN_PASSWORD.toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE.toString()))
            .andExpect(jsonPath("$.passwordGetsExpired").value(DEFAULT_PASSWORD_GETS_EXPIRED.booleanValue()))
            .andExpect(jsonPath("$.passwordUpdatedOn").value(DEFAULT_PASSWORD_UPDATED_ON.toString()))
            .andExpect(jsonPath("$.expirationPeriodDays").value(DEFAULT_EXPIRATION_PERIOD_DAYS))
            .andExpect(jsonPath("$.accountStatus").value(DEFAULT_ACCOUNT_STATUS.toString()))
            .andExpect(jsonPath("$.remarks").value(DEFAULT_REMARKS.toString()))
            .andExpect(jsonPath("$.multipleSessionAllowed").value(DEFAULT_MULTIPLE_SESSION_ALLOWED.booleanValue()))
            .andExpect(jsonPath("$.accountId").value(DEFAULT_ACCOUNT_ID.toString()));
    }

    @Test
    @Transactional
    public void getAllNonBankAccountsByLoginIdIsEqualToSomething() throws Exception {
        // Initialize the database
        nonBankAccountRepository.saveAndFlush(nonBankAccount);

        // Get all the nonBankAccountList where loginId equals to DEFAULT_LOGIN_ID
        defaultNonBankAccountShouldBeFound("loginId.equals=" + DEFAULT_LOGIN_ID);

        // Get all the nonBankAccountList where loginId equals to UPDATED_LOGIN_ID
        defaultNonBankAccountShouldNotBeFound("loginId.equals=" + UPDATED_LOGIN_ID);
    }

    @Test
    @Transactional
    public void getAllNonBankAccountsByLoginIdIsInShouldWork() throws Exception {
        // Initialize the database
        nonBankAccountRepository.saveAndFlush(nonBankAccount);

        // Get all the nonBankAccountList where loginId in DEFAULT_LOGIN_ID or UPDATED_LOGIN_ID
        defaultNonBankAccountShouldBeFound("loginId.in=" + DEFAULT_LOGIN_ID + "," + UPDATED_LOGIN_ID);

        // Get all the nonBankAccountList where loginId equals to UPDATED_LOGIN_ID
        defaultNonBankAccountShouldNotBeFound("loginId.in=" + UPDATED_LOGIN_ID);
    }

    @Test
    @Transactional
    public void getAllNonBankAccountsByLoginIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        nonBankAccountRepository.saveAndFlush(nonBankAccount);

        // Get all the nonBankAccountList where loginId is not null
        defaultNonBankAccountShouldBeFound("loginId.specified=true");

        // Get all the nonBankAccountList where loginId is null
        defaultNonBankAccountShouldNotBeFound("loginId.specified=false");
    }

    @Test
    @Transactional
    public void getAllNonBankAccountsByLoginPasswordIsEqualToSomething() throws Exception {
        // Initialize the database
        nonBankAccountRepository.saveAndFlush(nonBankAccount);

        // Get all the nonBankAccountList where loginPassword equals to DEFAULT_LOGIN_PASSWORD
        defaultNonBankAccountShouldBeFound("loginPassword.equals=" + DEFAULT_LOGIN_PASSWORD);

        // Get all the nonBankAccountList where loginPassword equals to UPDATED_LOGIN_PASSWORD
        defaultNonBankAccountShouldNotBeFound("loginPassword.equals=" + UPDATED_LOGIN_PASSWORD);
    }

    @Test
    @Transactional
    public void getAllNonBankAccountsByLoginPasswordIsInShouldWork() throws Exception {
        // Initialize the database
        nonBankAccountRepository.saveAndFlush(nonBankAccount);

        // Get all the nonBankAccountList where loginPassword in DEFAULT_LOGIN_PASSWORD or UPDATED_LOGIN_PASSWORD
        defaultNonBankAccountShouldBeFound("loginPassword.in=" + DEFAULT_LOGIN_PASSWORD + "," + UPDATED_LOGIN_PASSWORD);

        // Get all the nonBankAccountList where loginPassword equals to UPDATED_LOGIN_PASSWORD
        defaultNonBankAccountShouldNotBeFound("loginPassword.in=" + UPDATED_LOGIN_PASSWORD);
    }

    @Test
    @Transactional
    public void getAllNonBankAccountsByLoginPasswordIsNullOrNotNull() throws Exception {
        // Initialize the database
        nonBankAccountRepository.saveAndFlush(nonBankAccount);

        // Get all the nonBankAccountList where loginPassword is not null
        defaultNonBankAccountShouldBeFound("loginPassword.specified=true");

        // Get all the nonBankAccountList where loginPassword is null
        defaultNonBankAccountShouldNotBeFound("loginPassword.specified=false");
    }

    @Test
    @Transactional
    public void getAllNonBankAccountsByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        nonBankAccountRepository.saveAndFlush(nonBankAccount);

        // Get all the nonBankAccountList where name equals to DEFAULT_NAME
        defaultNonBankAccountShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the nonBankAccountList where name equals to UPDATED_NAME
        defaultNonBankAccountShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllNonBankAccountsByNameIsInShouldWork() throws Exception {
        // Initialize the database
        nonBankAccountRepository.saveAndFlush(nonBankAccount);

        // Get all the nonBankAccountList where name in DEFAULT_NAME or UPDATED_NAME
        defaultNonBankAccountShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the nonBankAccountList where name equals to UPDATED_NAME
        defaultNonBankAccountShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllNonBankAccountsByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        nonBankAccountRepository.saveAndFlush(nonBankAccount);

        // Get all the nonBankAccountList where name is not null
        defaultNonBankAccountShouldBeFound("name.specified=true");

        // Get all the nonBankAccountList where name is null
        defaultNonBankAccountShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    public void getAllNonBankAccountsByEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        nonBankAccountRepository.saveAndFlush(nonBankAccount);

        // Get all the nonBankAccountList where email equals to DEFAULT_EMAIL
        defaultNonBankAccountShouldBeFound("email.equals=" + DEFAULT_EMAIL);

        // Get all the nonBankAccountList where email equals to UPDATED_EMAIL
        defaultNonBankAccountShouldNotBeFound("email.equals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllNonBankAccountsByEmailIsInShouldWork() throws Exception {
        // Initialize the database
        nonBankAccountRepository.saveAndFlush(nonBankAccount);

        // Get all the nonBankAccountList where email in DEFAULT_EMAIL or UPDATED_EMAIL
        defaultNonBankAccountShouldBeFound("email.in=" + DEFAULT_EMAIL + "," + UPDATED_EMAIL);

        // Get all the nonBankAccountList where email equals to UPDATED_EMAIL
        defaultNonBankAccountShouldNotBeFound("email.in=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllNonBankAccountsByEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        nonBankAccountRepository.saveAndFlush(nonBankAccount);

        // Get all the nonBankAccountList where email is not null
        defaultNonBankAccountShouldBeFound("email.specified=true");

        // Get all the nonBankAccountList where email is null
        defaultNonBankAccountShouldNotBeFound("email.specified=false");
    }

    @Test
    @Transactional
    public void getAllNonBankAccountsByPhoneIsEqualToSomething() throws Exception {
        // Initialize the database
        nonBankAccountRepository.saveAndFlush(nonBankAccount);

        // Get all the nonBankAccountList where phone equals to DEFAULT_PHONE
        defaultNonBankAccountShouldBeFound("phone.equals=" + DEFAULT_PHONE);

        // Get all the nonBankAccountList where phone equals to UPDATED_PHONE
        defaultNonBankAccountShouldNotBeFound("phone.equals=" + UPDATED_PHONE);
    }

    @Test
    @Transactional
    public void getAllNonBankAccountsByPhoneIsInShouldWork() throws Exception {
        // Initialize the database
        nonBankAccountRepository.saveAndFlush(nonBankAccount);

        // Get all the nonBankAccountList where phone in DEFAULT_PHONE or UPDATED_PHONE
        defaultNonBankAccountShouldBeFound("phone.in=" + DEFAULT_PHONE + "," + UPDATED_PHONE);

        // Get all the nonBankAccountList where phone equals to UPDATED_PHONE
        defaultNonBankAccountShouldNotBeFound("phone.in=" + UPDATED_PHONE);
    }

    @Test
    @Transactional
    public void getAllNonBankAccountsByPhoneIsNullOrNotNull() throws Exception {
        // Initialize the database
        nonBankAccountRepository.saveAndFlush(nonBankAccount);

        // Get all the nonBankAccountList where phone is not null
        defaultNonBankAccountShouldBeFound("phone.specified=true");

        // Get all the nonBankAccountList where phone is null
        defaultNonBankAccountShouldNotBeFound("phone.specified=false");
    }

    @Test
    @Transactional
    public void getAllNonBankAccountsByPasswordGetsExpiredIsEqualToSomething() throws Exception {
        // Initialize the database
        nonBankAccountRepository.saveAndFlush(nonBankAccount);

        // Get all the nonBankAccountList where passwordGetsExpired equals to DEFAULT_PASSWORD_GETS_EXPIRED
        defaultNonBankAccountShouldBeFound("passwordGetsExpired.equals=" + DEFAULT_PASSWORD_GETS_EXPIRED);

        // Get all the nonBankAccountList where passwordGetsExpired equals to UPDATED_PASSWORD_GETS_EXPIRED
        defaultNonBankAccountShouldNotBeFound("passwordGetsExpired.equals=" + UPDATED_PASSWORD_GETS_EXPIRED);
    }

    @Test
    @Transactional
    public void getAllNonBankAccountsByPasswordGetsExpiredIsInShouldWork() throws Exception {
        // Initialize the database
        nonBankAccountRepository.saveAndFlush(nonBankAccount);

        // Get all the nonBankAccountList where passwordGetsExpired in DEFAULT_PASSWORD_GETS_EXPIRED or UPDATED_PASSWORD_GETS_EXPIRED
        defaultNonBankAccountShouldBeFound("passwordGetsExpired.in=" + DEFAULT_PASSWORD_GETS_EXPIRED + "," + UPDATED_PASSWORD_GETS_EXPIRED);

        // Get all the nonBankAccountList where passwordGetsExpired equals to UPDATED_PASSWORD_GETS_EXPIRED
        defaultNonBankAccountShouldNotBeFound("passwordGetsExpired.in=" + UPDATED_PASSWORD_GETS_EXPIRED);
    }

    @Test
    @Transactional
    public void getAllNonBankAccountsByPasswordGetsExpiredIsNullOrNotNull() throws Exception {
        // Initialize the database
        nonBankAccountRepository.saveAndFlush(nonBankAccount);

        // Get all the nonBankAccountList where passwordGetsExpired is not null
        defaultNonBankAccountShouldBeFound("passwordGetsExpired.specified=true");

        // Get all the nonBankAccountList where passwordGetsExpired is null
        defaultNonBankAccountShouldNotBeFound("passwordGetsExpired.specified=false");
    }

    @Test
    @Transactional
    public void getAllNonBankAccountsByPasswordUpdatedOnIsEqualToSomething() throws Exception {
        // Initialize the database
        nonBankAccountRepository.saveAndFlush(nonBankAccount);

        // Get all the nonBankAccountList where passwordUpdatedOn equals to DEFAULT_PASSWORD_UPDATED_ON
        defaultNonBankAccountShouldBeFound("passwordUpdatedOn.equals=" + DEFAULT_PASSWORD_UPDATED_ON);

        // Get all the nonBankAccountList where passwordUpdatedOn equals to UPDATED_PASSWORD_UPDATED_ON
        defaultNonBankAccountShouldNotBeFound("passwordUpdatedOn.equals=" + UPDATED_PASSWORD_UPDATED_ON);
    }

    @Test
    @Transactional
    public void getAllNonBankAccountsByPasswordUpdatedOnIsInShouldWork() throws Exception {
        // Initialize the database
        nonBankAccountRepository.saveAndFlush(nonBankAccount);

        // Get all the nonBankAccountList where passwordUpdatedOn in DEFAULT_PASSWORD_UPDATED_ON or UPDATED_PASSWORD_UPDATED_ON
        defaultNonBankAccountShouldBeFound("passwordUpdatedOn.in=" + DEFAULT_PASSWORD_UPDATED_ON + "," + UPDATED_PASSWORD_UPDATED_ON);

        // Get all the nonBankAccountList where passwordUpdatedOn equals to UPDATED_PASSWORD_UPDATED_ON
        defaultNonBankAccountShouldNotBeFound("passwordUpdatedOn.in=" + UPDATED_PASSWORD_UPDATED_ON);
    }

    @Test
    @Transactional
    public void getAllNonBankAccountsByPasswordUpdatedOnIsNullOrNotNull() throws Exception {
        // Initialize the database
        nonBankAccountRepository.saveAndFlush(nonBankAccount);

        // Get all the nonBankAccountList where passwordUpdatedOn is not null
        defaultNonBankAccountShouldBeFound("passwordUpdatedOn.specified=true");

        // Get all the nonBankAccountList where passwordUpdatedOn is null
        defaultNonBankAccountShouldNotBeFound("passwordUpdatedOn.specified=false");
    }

    @Test
    @Transactional
    public void getAllNonBankAccountsByExpirationPeriodDaysIsEqualToSomething() throws Exception {
        // Initialize the database
        nonBankAccountRepository.saveAndFlush(nonBankAccount);

        // Get all the nonBankAccountList where expirationPeriodDays equals to DEFAULT_EXPIRATION_PERIOD_DAYS
        defaultNonBankAccountShouldBeFound("expirationPeriodDays.equals=" + DEFAULT_EXPIRATION_PERIOD_DAYS);

        // Get all the nonBankAccountList where expirationPeriodDays equals to UPDATED_EXPIRATION_PERIOD_DAYS
        defaultNonBankAccountShouldNotBeFound("expirationPeriodDays.equals=" + UPDATED_EXPIRATION_PERIOD_DAYS);
    }

    @Test
    @Transactional
    public void getAllNonBankAccountsByExpirationPeriodDaysIsInShouldWork() throws Exception {
        // Initialize the database
        nonBankAccountRepository.saveAndFlush(nonBankAccount);

        // Get all the nonBankAccountList where expirationPeriodDays in DEFAULT_EXPIRATION_PERIOD_DAYS or UPDATED_EXPIRATION_PERIOD_DAYS
        defaultNonBankAccountShouldBeFound("expirationPeriodDays.in=" + DEFAULT_EXPIRATION_PERIOD_DAYS + "," + UPDATED_EXPIRATION_PERIOD_DAYS);

        // Get all the nonBankAccountList where expirationPeriodDays equals to UPDATED_EXPIRATION_PERIOD_DAYS
        defaultNonBankAccountShouldNotBeFound("expirationPeriodDays.in=" + UPDATED_EXPIRATION_PERIOD_DAYS);
    }

    @Test
    @Transactional
    public void getAllNonBankAccountsByExpirationPeriodDaysIsNullOrNotNull() throws Exception {
        // Initialize the database
        nonBankAccountRepository.saveAndFlush(nonBankAccount);

        // Get all the nonBankAccountList where expirationPeriodDays is not null
        defaultNonBankAccountShouldBeFound("expirationPeriodDays.specified=true");

        // Get all the nonBankAccountList where expirationPeriodDays is null
        defaultNonBankAccountShouldNotBeFound("expirationPeriodDays.specified=false");
    }

    @Test
    @Transactional
    public void getAllNonBankAccountsByExpirationPeriodDaysIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        nonBankAccountRepository.saveAndFlush(nonBankAccount);

        // Get all the nonBankAccountList where expirationPeriodDays greater than or equals to DEFAULT_EXPIRATION_PERIOD_DAYS
        defaultNonBankAccountShouldBeFound("expirationPeriodDays.greaterOrEqualThan=" + DEFAULT_EXPIRATION_PERIOD_DAYS);

        // Get all the nonBankAccountList where expirationPeriodDays greater than or equals to UPDATED_EXPIRATION_PERIOD_DAYS
        defaultNonBankAccountShouldNotBeFound("expirationPeriodDays.greaterOrEqualThan=" + UPDATED_EXPIRATION_PERIOD_DAYS);
    }

    @Test
    @Transactional
    public void getAllNonBankAccountsByExpirationPeriodDaysIsLessThanSomething() throws Exception {
        // Initialize the database
        nonBankAccountRepository.saveAndFlush(nonBankAccount);

        // Get all the nonBankAccountList where expirationPeriodDays less than or equals to DEFAULT_EXPIRATION_PERIOD_DAYS
        defaultNonBankAccountShouldNotBeFound("expirationPeriodDays.lessThan=" + DEFAULT_EXPIRATION_PERIOD_DAYS);

        // Get all the nonBankAccountList where expirationPeriodDays less than or equals to UPDATED_EXPIRATION_PERIOD_DAYS
        defaultNonBankAccountShouldBeFound("expirationPeriodDays.lessThan=" + UPDATED_EXPIRATION_PERIOD_DAYS);
    }


    @Test
    @Transactional
    public void getAllNonBankAccountsByAccountStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        nonBankAccountRepository.saveAndFlush(nonBankAccount);

        // Get all the nonBankAccountList where accountStatus equals to DEFAULT_ACCOUNT_STATUS
        defaultNonBankAccountShouldBeFound("accountStatus.equals=" + DEFAULT_ACCOUNT_STATUS);

        // Get all the nonBankAccountList where accountStatus equals to UPDATED_ACCOUNT_STATUS
        defaultNonBankAccountShouldNotBeFound("accountStatus.equals=" + UPDATED_ACCOUNT_STATUS);
    }

    @Test
    @Transactional
    public void getAllNonBankAccountsByAccountStatusIsInShouldWork() throws Exception {
        // Initialize the database
        nonBankAccountRepository.saveAndFlush(nonBankAccount);

        // Get all the nonBankAccountList where accountStatus in DEFAULT_ACCOUNT_STATUS or UPDATED_ACCOUNT_STATUS
        defaultNonBankAccountShouldBeFound("accountStatus.in=" + DEFAULT_ACCOUNT_STATUS + "," + UPDATED_ACCOUNT_STATUS);

        // Get all the nonBankAccountList where accountStatus equals to UPDATED_ACCOUNT_STATUS
        defaultNonBankAccountShouldNotBeFound("accountStatus.in=" + UPDATED_ACCOUNT_STATUS);
    }

    @Test
    @Transactional
    public void getAllNonBankAccountsByAccountStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        nonBankAccountRepository.saveAndFlush(nonBankAccount);

        // Get all the nonBankAccountList where accountStatus is not null
        defaultNonBankAccountShouldBeFound("accountStatus.specified=true");

        // Get all the nonBankAccountList where accountStatus is null
        defaultNonBankAccountShouldNotBeFound("accountStatus.specified=false");
    }

    @Test
    @Transactional
    public void getAllNonBankAccountsByRemarksIsEqualToSomething() throws Exception {
        // Initialize the database
        nonBankAccountRepository.saveAndFlush(nonBankAccount);

        // Get all the nonBankAccountList where remarks equals to DEFAULT_REMARKS
        defaultNonBankAccountShouldBeFound("remarks.equals=" + DEFAULT_REMARKS);

        // Get all the nonBankAccountList where remarks equals to UPDATED_REMARKS
        defaultNonBankAccountShouldNotBeFound("remarks.equals=" + UPDATED_REMARKS);
    }

    @Test
    @Transactional
    public void getAllNonBankAccountsByRemarksIsInShouldWork() throws Exception {
        // Initialize the database
        nonBankAccountRepository.saveAndFlush(nonBankAccount);

        // Get all the nonBankAccountList where remarks in DEFAULT_REMARKS or UPDATED_REMARKS
        defaultNonBankAccountShouldBeFound("remarks.in=" + DEFAULT_REMARKS + "," + UPDATED_REMARKS);

        // Get all the nonBankAccountList where remarks equals to UPDATED_REMARKS
        defaultNonBankAccountShouldNotBeFound("remarks.in=" + UPDATED_REMARKS);
    }

    @Test
    @Transactional
    public void getAllNonBankAccountsByRemarksIsNullOrNotNull() throws Exception {
        // Initialize the database
        nonBankAccountRepository.saveAndFlush(nonBankAccount);

        // Get all the nonBankAccountList where remarks is not null
        defaultNonBankAccountShouldBeFound("remarks.specified=true");

        // Get all the nonBankAccountList where remarks is null
        defaultNonBankAccountShouldNotBeFound("remarks.specified=false");
    }

    @Test
    @Transactional
    public void getAllNonBankAccountsByMultipleSessionAllowedIsEqualToSomething() throws Exception {
        // Initialize the database
        nonBankAccountRepository.saveAndFlush(nonBankAccount);

        // Get all the nonBankAccountList where multipleSessionAllowed equals to DEFAULT_MULTIPLE_SESSION_ALLOWED
        defaultNonBankAccountShouldBeFound("multipleSessionAllowed.equals=" + DEFAULT_MULTIPLE_SESSION_ALLOWED);

        // Get all the nonBankAccountList where multipleSessionAllowed equals to UPDATED_MULTIPLE_SESSION_ALLOWED
        defaultNonBankAccountShouldNotBeFound("multipleSessionAllowed.equals=" + UPDATED_MULTIPLE_SESSION_ALLOWED);
    }

    @Test
    @Transactional
    public void getAllNonBankAccountsByMultipleSessionAllowedIsInShouldWork() throws Exception {
        // Initialize the database
        nonBankAccountRepository.saveAndFlush(nonBankAccount);

        // Get all the nonBankAccountList where multipleSessionAllowed in DEFAULT_MULTIPLE_SESSION_ALLOWED or UPDATED_MULTIPLE_SESSION_ALLOWED
        defaultNonBankAccountShouldBeFound("multipleSessionAllowed.in=" + DEFAULT_MULTIPLE_SESSION_ALLOWED + "," + UPDATED_MULTIPLE_SESSION_ALLOWED);

        // Get all the nonBankAccountList where multipleSessionAllowed equals to UPDATED_MULTIPLE_SESSION_ALLOWED
        defaultNonBankAccountShouldNotBeFound("multipleSessionAllowed.in=" + UPDATED_MULTIPLE_SESSION_ALLOWED);
    }

    @Test
    @Transactional
    public void getAllNonBankAccountsByMultipleSessionAllowedIsNullOrNotNull() throws Exception {
        // Initialize the database
        nonBankAccountRepository.saveAndFlush(nonBankAccount);

        // Get all the nonBankAccountList where multipleSessionAllowed is not null
        defaultNonBankAccountShouldBeFound("multipleSessionAllowed.specified=true");

        // Get all the nonBankAccountList where multipleSessionAllowed is null
        defaultNonBankAccountShouldNotBeFound("multipleSessionAllowed.specified=false");
    }

    @Test
    @Transactional
    public void getAllNonBankAccountsByAccountIdIsEqualToSomething() throws Exception {
        // Initialize the database
        nonBankAccountRepository.saveAndFlush(nonBankAccount);

        // Get all the nonBankAccountList where accountId equals to DEFAULT_ACCOUNT_ID
        defaultNonBankAccountShouldBeFound("accountId.equals=" + DEFAULT_ACCOUNT_ID);

        // Get all the nonBankAccountList where accountId equals to UPDATED_ACCOUNT_ID
        defaultNonBankAccountShouldNotBeFound("accountId.equals=" + UPDATED_ACCOUNT_ID);
    }

    @Test
    @Transactional
    public void getAllNonBankAccountsByAccountIdIsInShouldWork() throws Exception {
        // Initialize the database
        nonBankAccountRepository.saveAndFlush(nonBankAccount);

        // Get all the nonBankAccountList where accountId in DEFAULT_ACCOUNT_ID or UPDATED_ACCOUNT_ID
        defaultNonBankAccountShouldBeFound("accountId.in=" + DEFAULT_ACCOUNT_ID + "," + UPDATED_ACCOUNT_ID);

        // Get all the nonBankAccountList where accountId equals to UPDATED_ACCOUNT_ID
        defaultNonBankAccountShouldNotBeFound("accountId.in=" + UPDATED_ACCOUNT_ID);
    }

    @Test
    @Transactional
    public void getAllNonBankAccountsByAccountIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        nonBankAccountRepository.saveAndFlush(nonBankAccount);

        // Get all the nonBankAccountList where accountId is not null
        defaultNonBankAccountShouldBeFound("accountId.specified=true");

        // Get all the nonBankAccountList where accountId is null
        defaultNonBankAccountShouldNotBeFound("accountId.specified=false");
    }

    @Test
    @Transactional
    public void getAllNonBankAccountsByNonBankClientIsEqualToSomething() throws Exception {
        // Get already existing entity
        NonBankClient nonBankClient = nonBankAccount.getNonBankClient();
        nonBankAccountRepository.saveAndFlush(nonBankAccount);
        Long nonBankClientId = nonBankClient.getId();

        // Get all the nonBankAccountList where nonBankClient equals to nonBankClientId
        defaultNonBankAccountShouldBeFound("nonBankClientId.equals=" + nonBankClientId);

        // Get all the nonBankAccountList where nonBankClient equals to nonBankClientId + 1
        defaultNonBankAccountShouldNotBeFound("nonBankClientId.equals=" + (nonBankClientId + 1));
    }


    @Test
    @Transactional
    public void getAllNonBankAccountsByNonBankAccountGroupIsEqualToSomething() throws Exception {
        // Initialize the database
        NonBankAccountGroup nonBankAccountGroup = NonBankAccountGroupResourceIT.createEntity(em);
        em.persist(nonBankAccountGroup);
        em.flush();
        nonBankAccount.setNonBankAccountGroup(nonBankAccountGroup);
        nonBankAccountRepository.saveAndFlush(nonBankAccount);
        Long nonBankAccountGroupId = nonBankAccountGroup.getId();

        // Get all the nonBankAccountList where nonBankAccountGroup equals to nonBankAccountGroupId
        defaultNonBankAccountShouldBeFound("nonBankAccountGroupId.equals=" + nonBankAccountGroupId);

        // Get all the nonBankAccountList where nonBankAccountGroup equals to nonBankAccountGroupId + 1
        defaultNonBankAccountShouldNotBeFound("nonBankAccountGroupId.equals=" + (nonBankAccountGroupId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultNonBankAccountShouldBeFound(String filter) throws Exception {
        restNonBankAccountMockMvc.perform(get("/api/non-bank-accounts?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(nonBankAccount.getId().intValue())))
            .andExpect(jsonPath("$.[*].loginId").value(hasItem(DEFAULT_LOGIN_ID)))
            .andExpect(jsonPath("$.[*].loginPassword").value(hasItem(DEFAULT_LOGIN_PASSWORD)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE)))
            .andExpect(jsonPath("$.[*].passwordGetsExpired").value(hasItem(DEFAULT_PASSWORD_GETS_EXPIRED.booleanValue())))
            .andExpect(jsonPath("$.[*].passwordUpdatedOn").value(hasItem(DEFAULT_PASSWORD_UPDATED_ON.toString())))
            .andExpect(jsonPath("$.[*].expirationPeriodDays").value(hasItem(DEFAULT_EXPIRATION_PERIOD_DAYS)))
            .andExpect(jsonPath("$.[*].accountStatus").value(hasItem(DEFAULT_ACCOUNT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS)))
            .andExpect(jsonPath("$.[*].multipleSessionAllowed").value(hasItem(DEFAULT_MULTIPLE_SESSION_ALLOWED.booleanValue())))
            .andExpect(jsonPath("$.[*].accountId").value(hasItem(DEFAULT_ACCOUNT_ID)));

        // Check, that the count call also returns 1
        restNonBankAccountMockMvc.perform(get("/api/non-bank-accounts/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultNonBankAccountShouldNotBeFound(String filter) throws Exception {
        restNonBankAccountMockMvc.perform(get("/api/non-bank-accounts?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restNonBankAccountMockMvc.perform(get("/api/non-bank-accounts/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingNonBankAccount() throws Exception {
        // Get the nonBankAccount
        restNonBankAccountMockMvc.perform(get("/api/non-bank-accounts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNonBankAccount() throws Exception {
        // Initialize the database
        nonBankAccountRepository.saveAndFlush(nonBankAccount);

        int databaseSizeBeforeUpdate = nonBankAccountRepository.findAll().size();

        // Update the nonBankAccount
        NonBankAccount updatedNonBankAccount = nonBankAccountRepository.findById(nonBankAccount.getId()).get();
        // Disconnect from session so that the updates on updatedNonBankAccount are not directly saved in db
        em.detach(updatedNonBankAccount);
        updatedNonBankAccount
            .loginId(UPDATED_LOGIN_ID)
            .loginPassword(UPDATED_LOGIN_PASSWORD)
            .name(UPDATED_NAME)
            .email(UPDATED_EMAIL)
            .phone(UPDATED_PHONE)
            .passwordGetsExpired(UPDATED_PASSWORD_GETS_EXPIRED)
            .passwordUpdatedOn(UPDATED_PASSWORD_UPDATED_ON)
            .expirationPeriodDays(UPDATED_EXPIRATION_PERIOD_DAYS)
            .accountStatus(UPDATED_ACCOUNT_STATUS)
            .remarks(UPDATED_REMARKS)
            .multipleSessionAllowed(UPDATED_MULTIPLE_SESSION_ALLOWED)
            .accountId(UPDATED_ACCOUNT_ID);
        NonBankAccountDTO nonBankAccountDTO = nonBankAccountMapper.toDto(updatedNonBankAccount);

        restNonBankAccountMockMvc.perform(put("/api/non-bank-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(nonBankAccountDTO)))
            .andExpect(status().isOk());

        // Validate the NonBankAccount in the database
        List<NonBankAccount> nonBankAccountList = nonBankAccountRepository.findAll();
        assertThat(nonBankAccountList).hasSize(databaseSizeBeforeUpdate);
        NonBankAccount testNonBankAccount = nonBankAccountList.get(nonBankAccountList.size() - 1);
        assertThat(testNonBankAccount.getLoginId()).isEqualTo(UPDATED_LOGIN_ID);
        assertThat(testNonBankAccount.getLoginPassword()).isEqualTo(UPDATED_LOGIN_PASSWORD);
        assertThat(testNonBankAccount.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testNonBankAccount.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testNonBankAccount.getPhone()).isEqualTo(UPDATED_PHONE);
        assertThat(testNonBankAccount.isPasswordGetsExpired()).isEqualTo(UPDATED_PASSWORD_GETS_EXPIRED);
        assertThat(testNonBankAccount.getPasswordUpdatedOn()).isEqualTo(UPDATED_PASSWORD_UPDATED_ON);
        assertThat(testNonBankAccount.getExpirationPeriodDays()).isEqualTo(UPDATED_EXPIRATION_PERIOD_DAYS);
        assertThat(testNonBankAccount.getAccountStatus()).isEqualTo(UPDATED_ACCOUNT_STATUS);
        assertThat(testNonBankAccount.getRemarks()).isEqualTo(UPDATED_REMARKS);
        assertThat(testNonBankAccount.isMultipleSessionAllowed()).isEqualTo(UPDATED_MULTIPLE_SESSION_ALLOWED);
        assertThat(testNonBankAccount.getAccountId()).isEqualTo(UPDATED_ACCOUNT_ID);
    }

    @Test
    @Transactional
    public void updateNonExistingNonBankAccount() throws Exception {
        int databaseSizeBeforeUpdate = nonBankAccountRepository.findAll().size();

        // Create the NonBankAccount
        NonBankAccountDTO nonBankAccountDTO = nonBankAccountMapper.toDto(nonBankAccount);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restNonBankAccountMockMvc.perform(put("/api/non-bank-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(nonBankAccountDTO)))
            .andExpect(status().isBadRequest());

        // Validate the NonBankAccount in the database
        List<NonBankAccount> nonBankAccountList = nonBankAccountRepository.findAll();
        assertThat(nonBankAccountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteNonBankAccount() throws Exception {
        // Initialize the database
        nonBankAccountRepository.saveAndFlush(nonBankAccount);

        int databaseSizeBeforeDelete = nonBankAccountRepository.findAll().size();

        // Delete the nonBankAccount
        restNonBankAccountMockMvc.perform(delete("/api/non-bank-accounts/{id}", nonBankAccount.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<NonBankAccount> nonBankAccountList = nonBankAccountRepository.findAll();
        assertThat(nonBankAccountList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(NonBankAccount.class);
        NonBankAccount nonBankAccount1 = new NonBankAccount();
        nonBankAccount1.setId(1L);
        NonBankAccount nonBankAccount2 = new NonBankAccount();
        nonBankAccount2.setId(nonBankAccount1.getId());
        assertThat(nonBankAccount1).isEqualTo(nonBankAccount2);
        nonBankAccount2.setId(2L);
        assertThat(nonBankAccount1).isNotEqualTo(nonBankAccount2);
        nonBankAccount1.setId(null);
        assertThat(nonBankAccount1).isNotEqualTo(nonBankAccount2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(NonBankAccountDTO.class);
        NonBankAccountDTO nonBankAccountDTO1 = new NonBankAccountDTO();
        nonBankAccountDTO1.setId(1L);
        NonBankAccountDTO nonBankAccountDTO2 = new NonBankAccountDTO();
        assertThat(nonBankAccountDTO1).isNotEqualTo(nonBankAccountDTO2);
        nonBankAccountDTO2.setId(nonBankAccountDTO1.getId());
        assertThat(nonBankAccountDTO1).isEqualTo(nonBankAccountDTO2);
        nonBankAccountDTO2.setId(2L);
        assertThat(nonBankAccountDTO1).isNotEqualTo(nonBankAccountDTO2);
        nonBankAccountDTO1.setId(null);
        assertThat(nonBankAccountDTO1).isNotEqualTo(nonBankAccountDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(nonBankAccountMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(nonBankAccountMapper.fromId(null)).isNull();
    }
}
