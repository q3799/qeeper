package com.qk.qeeper.web.rest;

import com.qk.qeeper.QeeperApp;
import com.qk.qeeper.domain.BankAccount;
import com.qk.qeeper.domain.Employee;
import com.qk.qeeper.domain.BankBranch;
import com.qk.qeeper.domain.MobileNumber;
import com.qk.qeeper.domain.BankAccount;
import com.qk.qeeper.domain.Card;
import com.qk.qeeper.domain.BankAccountGroup;
import com.qk.qeeper.domain.SecretQuestion;
import com.qk.qeeper.repository.BankAccountRepository;
import com.qk.qeeper.service.BankAccountService;
import com.qk.qeeper.service.dto.BankAccountDTO;
import com.qk.qeeper.service.mapper.BankAccountMapper;
import com.qk.qeeper.web.rest.errors.ExceptionTranslator;
import com.qk.qeeper.service.dto.BankAccountCriteria;
import com.qk.qeeper.service.BankAccountQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.qk.qeeper.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.qk.qeeper.domain.enumeration.BankAccountType;
import com.qk.qeeper.domain.enumeration.AccountStatus;
/**
 * Integration tests for the {@Link BankAccountResource} REST controller.
 */
@SpringBootTest(classes = QeeperApp.class)
public class BankAccountResourceIT {

    private static final Instant DEFAULT_DATE_OF_OPENING = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_OF_OPENING = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_OPENED_BY = "AAAAAAAAAA";
    private static final String UPDATED_OPENED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_CUSTOMER_ID = "AAAAAAAAAA";
    private static final String UPDATED_CUSTOMER_ID = "BBBBBBBBBB";

    private static final String DEFAULT_ACCOUNT_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_ACCOUNT_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_LOGIN_ID = "AAAAAAAAAA";
    private static final String UPDATED_LOGIN_ID = "BBBBBBBBBB";

    private static final String DEFAULT_LOGIN_PASSWORD = "AAAAAAAAAA";
    private static final String UPDATED_LOGIN_PASSWORD = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "hi@[45.095.0.sRMT]";
    private static final String UPDATED_EMAIL = "t@q.OZM";

    private static final String DEFAULT_PHONE = "777103911653";
    private static final String UPDATED_PHONE = "+83";

    private static final BankAccountType DEFAULT_BANK_ACCOUNT_TYPE = BankAccountType.SAVING;
    private static final BankAccountType UPDATED_BANK_ACCOUNT_TYPE = BankAccountType.CURRENT;

    private static final Boolean DEFAULT_CHEQUE_BOOK_ISSUED = false;
    private static final Boolean UPDATED_CHEQUE_BOOK_ISSUED = true;

    private static final Double DEFAULT_ACCOUNT_BALANCE = 0D;
    private static final Double UPDATED_ACCOUNT_BALANCE = 1D;

    private static final Instant DEFAULT_BALANCE_CHECKED_ON = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_BALANCE_CHECKED_ON = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Double DEFAULT_MINIMUM_BALANCE = 1D;
    private static final Double UPDATED_MINIMUM_BALANCE = 2D;

    private static final Boolean DEFAULT_PASSWORD_GETS_EXPIRED = false;
    private static final Boolean UPDATED_PASSWORD_GETS_EXPIRED = true;

    private static final Integer DEFAULT_EXPIRATION_PERIOD_DAYS = 0;
    private static final Integer UPDATED_EXPIRATION_PERIOD_DAYS = 1;

    private static final Instant DEFAULT_PASSWORD_UPDATED_ON = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_PASSWORD_UPDATED_ON = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_REMARKS = "AAAAAAAAAA";
    private static final String UPDATED_REMARKS = "BBBBBBBBBB";

    private static final AccountStatus DEFAULT_ACCOUNT_STATUS = AccountStatus.ACTIVE;
    private static final AccountStatus UPDATED_ACCOUNT_STATUS = AccountStatus.IN_USE;

    private static final Boolean DEFAULT_MULTIPLE_SESSION_ALLOWED = false;
    private static final Boolean UPDATED_MULTIPLE_SESSION_ALLOWED = true;

    private static final String DEFAULT_PROJECT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_PROJECT_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_PROJECT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_PROJECT_CODE = "BBBBBBBBBB";

    private static final Instant DEFAULT_REQUESTED_ON = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_REQUESTED_ON = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_REQUESTED_BY = "AAAAAAAAAA";
    private static final String UPDATED_REQUESTED_BY = "BBBBBBBBBB";

    @Autowired
    private BankAccountRepository bankAccountRepository;

    @Autowired
    private BankAccountMapper bankAccountMapper;

    @Autowired
    private BankAccountService bankAccountService;

    @Autowired
    private BankAccountQueryService bankAccountQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restBankAccountMockMvc;

    private BankAccount bankAccount;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final BankAccountResource bankAccountResource = new BankAccountResource(bankAccountService, bankAccountQueryService);
        this.restBankAccountMockMvc = MockMvcBuilders.standaloneSetup(bankAccountResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BankAccount createEntity(EntityManager em) {
        BankAccount bankAccount = new BankAccount()
            .dateOfOpening(DEFAULT_DATE_OF_OPENING)
            .openedBy(DEFAULT_OPENED_BY)
            .customerId(DEFAULT_CUSTOMER_ID)
            .accountNumber(DEFAULT_ACCOUNT_NUMBER)
            .loginId(DEFAULT_LOGIN_ID)
            .loginPassword(DEFAULT_LOGIN_PASSWORD)
            .email(DEFAULT_EMAIL)
            .phone(DEFAULT_PHONE)
            .bankAccountType(DEFAULT_BANK_ACCOUNT_TYPE)
            .chequeBookIssued(DEFAULT_CHEQUE_BOOK_ISSUED)
            .accountBalance(DEFAULT_ACCOUNT_BALANCE)
            .balanceCheckedOn(DEFAULT_BALANCE_CHECKED_ON)
            .minimumBalance(DEFAULT_MINIMUM_BALANCE)
            .passwordGetsExpired(DEFAULT_PASSWORD_GETS_EXPIRED)
            .expirationPeriodDays(DEFAULT_EXPIRATION_PERIOD_DAYS)
            .passwordUpdatedOn(DEFAULT_PASSWORD_UPDATED_ON)
            .remarks(DEFAULT_REMARKS)
            .accountStatus(DEFAULT_ACCOUNT_STATUS)
            .multipleSessionAllowed(DEFAULT_MULTIPLE_SESSION_ALLOWED)
            .projectName(DEFAULT_PROJECT_NAME)
            .projectCode(DEFAULT_PROJECT_CODE)
            .requestedOn(DEFAULT_REQUESTED_ON)
            .requestedBy(DEFAULT_REQUESTED_BY);
        // Add required entity
        Employee employee;
        if (TestUtil.findAll(em, Employee.class).isEmpty()) {
            employee = EmployeeResourceIT.createEntity(em);
            em.persist(employee);
            em.flush();
        } else {
            employee = TestUtil.findAll(em, Employee.class).get(0);
        }
        bankAccount.setAccountHolder(employee);
        // Add required entity
        BankBranch bankBranch;
        if (TestUtil.findAll(em, BankBranch.class).isEmpty()) {
            bankBranch = BankBranchResourceIT.createEntity(em);
            em.persist(bankBranch);
            em.flush();
        } else {
            bankBranch = TestUtil.findAll(em, BankBranch.class).get(0);
        }
        bankAccount.setBankBranch(bankBranch);
        // Add required entity
        MobileNumber mobileNumber;
        if (TestUtil.findAll(em, MobileNumber.class).isEmpty()) {
            mobileNumber = MobileNumberResourceIT.createEntity(em);
            em.persist(mobileNumber);
            em.flush();
        } else {
            mobileNumber = TestUtil.findAll(em, MobileNumber.class).get(0);
        }
        bankAccount.setMobileNumberMapped(mobileNumber);
        return bankAccount;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BankAccount createUpdatedEntity(EntityManager em) {
        BankAccount bankAccount = new BankAccount()
            .dateOfOpening(UPDATED_DATE_OF_OPENING)
            .openedBy(UPDATED_OPENED_BY)
            .customerId(UPDATED_CUSTOMER_ID)
            .accountNumber(UPDATED_ACCOUNT_NUMBER)
            .loginId(UPDATED_LOGIN_ID)
            .loginPassword(UPDATED_LOGIN_PASSWORD)
            .email(UPDATED_EMAIL)
            .phone(UPDATED_PHONE)
            .bankAccountType(UPDATED_BANK_ACCOUNT_TYPE)
            .chequeBookIssued(UPDATED_CHEQUE_BOOK_ISSUED)
            .accountBalance(UPDATED_ACCOUNT_BALANCE)
            .balanceCheckedOn(UPDATED_BALANCE_CHECKED_ON)
            .minimumBalance(UPDATED_MINIMUM_BALANCE)
            .passwordGetsExpired(UPDATED_PASSWORD_GETS_EXPIRED)
            .expirationPeriodDays(UPDATED_EXPIRATION_PERIOD_DAYS)
            .passwordUpdatedOn(UPDATED_PASSWORD_UPDATED_ON)
            .remarks(UPDATED_REMARKS)
            .accountStatus(UPDATED_ACCOUNT_STATUS)
            .multipleSessionAllowed(UPDATED_MULTIPLE_SESSION_ALLOWED)
            .projectName(UPDATED_PROJECT_NAME)
            .projectCode(UPDATED_PROJECT_CODE)
            .requestedOn(UPDATED_REQUESTED_ON)
            .requestedBy(UPDATED_REQUESTED_BY);
        // Add required entity
        Employee employee;
        if (TestUtil.findAll(em, Employee.class).isEmpty()) {
            employee = EmployeeResourceIT.createUpdatedEntity(em);
            em.persist(employee);
            em.flush();
        } else {
            employee = TestUtil.findAll(em, Employee.class).get(0);
        }
        bankAccount.setAccountHolder(employee);
        // Add required entity
        BankBranch bankBranch;
        if (TestUtil.findAll(em, BankBranch.class).isEmpty()) {
            bankBranch = BankBranchResourceIT.createUpdatedEntity(em);
            em.persist(bankBranch);
            em.flush();
        } else {
            bankBranch = TestUtil.findAll(em, BankBranch.class).get(0);
        }
        bankAccount.setBankBranch(bankBranch);
        // Add required entity
        MobileNumber mobileNumber;
        if (TestUtil.findAll(em, MobileNumber.class).isEmpty()) {
            mobileNumber = MobileNumberResourceIT.createUpdatedEntity(em);
            em.persist(mobileNumber);
            em.flush();
        } else {
            mobileNumber = TestUtil.findAll(em, MobileNumber.class).get(0);
        }
        bankAccount.setMobileNumberMapped(mobileNumber);
        return bankAccount;
    }

    @BeforeEach
    public void initTest() {
        bankAccount = createEntity(em);
    }

    @Test
    @Transactional
    public void createBankAccount() throws Exception {
        int databaseSizeBeforeCreate = bankAccountRepository.findAll().size();

        // Create the BankAccount
        BankAccountDTO bankAccountDTO = bankAccountMapper.toDto(bankAccount);
        restBankAccountMockMvc.perform(post("/api/bank-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bankAccountDTO)))
            .andExpect(status().isCreated());

        // Validate the BankAccount in the database
        List<BankAccount> bankAccountList = bankAccountRepository.findAll();
        assertThat(bankAccountList).hasSize(databaseSizeBeforeCreate + 1);
        BankAccount testBankAccount = bankAccountList.get(bankAccountList.size() - 1);
        assertThat(testBankAccount.getDateOfOpening()).isEqualTo(DEFAULT_DATE_OF_OPENING);
        assertThat(testBankAccount.getOpenedBy()).isEqualTo(DEFAULT_OPENED_BY);
        assertThat(testBankAccount.getCustomerId()).isEqualTo(DEFAULT_CUSTOMER_ID);
        assertThat(testBankAccount.getAccountNumber()).isEqualTo(DEFAULT_ACCOUNT_NUMBER);
        assertThat(testBankAccount.getLoginId()).isEqualTo(DEFAULT_LOGIN_ID);
        assertThat(testBankAccount.getLoginPassword()).isEqualTo(DEFAULT_LOGIN_PASSWORD);
        assertThat(testBankAccount.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testBankAccount.getPhone()).isEqualTo(DEFAULT_PHONE);
        assertThat(testBankAccount.getBankAccountType()).isEqualTo(DEFAULT_BANK_ACCOUNT_TYPE);
        assertThat(testBankAccount.isChequeBookIssued()).isEqualTo(DEFAULT_CHEQUE_BOOK_ISSUED);
        assertThat(testBankAccount.getAccountBalance()).isEqualTo(DEFAULT_ACCOUNT_BALANCE);
        assertThat(testBankAccount.getBalanceCheckedOn()).isEqualTo(DEFAULT_BALANCE_CHECKED_ON);
        assertThat(testBankAccount.getMinimumBalance()).isEqualTo(DEFAULT_MINIMUM_BALANCE);
        assertThat(testBankAccount.isPasswordGetsExpired()).isEqualTo(DEFAULT_PASSWORD_GETS_EXPIRED);
        assertThat(testBankAccount.getExpirationPeriodDays()).isEqualTo(DEFAULT_EXPIRATION_PERIOD_DAYS);
        assertThat(testBankAccount.getPasswordUpdatedOn()).isEqualTo(DEFAULT_PASSWORD_UPDATED_ON);
        assertThat(testBankAccount.getRemarks()).isEqualTo(DEFAULT_REMARKS);
        assertThat(testBankAccount.getAccountStatus()).isEqualTo(DEFAULT_ACCOUNT_STATUS);
        assertThat(testBankAccount.isMultipleSessionAllowed()).isEqualTo(DEFAULT_MULTIPLE_SESSION_ALLOWED);
        assertThat(testBankAccount.getProjectName()).isEqualTo(DEFAULT_PROJECT_NAME);
        assertThat(testBankAccount.getProjectCode()).isEqualTo(DEFAULT_PROJECT_CODE);
        assertThat(testBankAccount.getRequestedOn()).isEqualTo(DEFAULT_REQUESTED_ON);
        assertThat(testBankAccount.getRequestedBy()).isEqualTo(DEFAULT_REQUESTED_BY);
    }

    @Test
    @Transactional
    public void createBankAccountWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = bankAccountRepository.findAll().size();

        // Create the BankAccount with an existing ID
        bankAccount.setId(1L);
        BankAccountDTO bankAccountDTO = bankAccountMapper.toDto(bankAccount);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBankAccountMockMvc.perform(post("/api/bank-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bankAccountDTO)))
            .andExpect(status().isBadRequest());

        // Validate the BankAccount in the database
        List<BankAccount> bankAccountList = bankAccountRepository.findAll();
        assertThat(bankAccountList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkCustomerIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = bankAccountRepository.findAll().size();
        // set the field null
        bankAccount.setCustomerId(null);

        // Create the BankAccount, which fails.
        BankAccountDTO bankAccountDTO = bankAccountMapper.toDto(bankAccount);

        restBankAccountMockMvc.perform(post("/api/bank-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bankAccountDTO)))
            .andExpect(status().isBadRequest());

        List<BankAccount> bankAccountList = bankAccountRepository.findAll();
        assertThat(bankAccountList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLoginIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = bankAccountRepository.findAll().size();
        // set the field null
        bankAccount.setLoginId(null);

        // Create the BankAccount, which fails.
        BankAccountDTO bankAccountDTO = bankAccountMapper.toDto(bankAccount);

        restBankAccountMockMvc.perform(post("/api/bank-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bankAccountDTO)))
            .andExpect(status().isBadRequest());

        List<BankAccount> bankAccountList = bankAccountRepository.findAll();
        assertThat(bankAccountList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLoginPasswordIsRequired() throws Exception {
        int databaseSizeBeforeTest = bankAccountRepository.findAll().size();
        // set the field null
        bankAccount.setLoginPassword(null);

        // Create the BankAccount, which fails.
        BankAccountDTO bankAccountDTO = bankAccountMapper.toDto(bankAccount);

        restBankAccountMockMvc.perform(post("/api/bank-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bankAccountDTO)))
            .andExpect(status().isBadRequest());

        List<BankAccount> bankAccountList = bankAccountRepository.findAll();
        assertThat(bankAccountList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEmailIsRequired() throws Exception {
        int databaseSizeBeforeTest = bankAccountRepository.findAll().size();
        // set the field null
        bankAccount.setEmail(null);

        // Create the BankAccount, which fails.
        BankAccountDTO bankAccountDTO = bankAccountMapper.toDto(bankAccount);

        restBankAccountMockMvc.perform(post("/api/bank-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bankAccountDTO)))
            .andExpect(status().isBadRequest());

        List<BankAccount> bankAccountList = bankAccountRepository.findAll();
        assertThat(bankAccountList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkBankAccountTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = bankAccountRepository.findAll().size();
        // set the field null
        bankAccount.setBankAccountType(null);

        // Create the BankAccount, which fails.
        BankAccountDTO bankAccountDTO = bankAccountMapper.toDto(bankAccount);

        restBankAccountMockMvc.perform(post("/api/bank-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bankAccountDTO)))
            .andExpect(status().isBadRequest());

        List<BankAccount> bankAccountList = bankAccountRepository.findAll();
        assertThat(bankAccountList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkChequeBookIssuedIsRequired() throws Exception {
        int databaseSizeBeforeTest = bankAccountRepository.findAll().size();
        // set the field null
        bankAccount.setChequeBookIssued(null);

        // Create the BankAccount, which fails.
        BankAccountDTO bankAccountDTO = bankAccountMapper.toDto(bankAccount);

        restBankAccountMockMvc.perform(post("/api/bank-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bankAccountDTO)))
            .andExpect(status().isBadRequest());

        List<BankAccount> bankAccountList = bankAccountRepository.findAll();
        assertThat(bankAccountList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPasswordGetsExpiredIsRequired() throws Exception {
        int databaseSizeBeforeTest = bankAccountRepository.findAll().size();
        // set the field null
        bankAccount.setPasswordGetsExpired(null);

        // Create the BankAccount, which fails.
        BankAccountDTO bankAccountDTO = bankAccountMapper.toDto(bankAccount);

        restBankAccountMockMvc.perform(post("/api/bank-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bankAccountDTO)))
            .andExpect(status().isBadRequest());

        List<BankAccount> bankAccountList = bankAccountRepository.findAll();
        assertThat(bankAccountList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAccountStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = bankAccountRepository.findAll().size();
        // set the field null
        bankAccount.setAccountStatus(null);

        // Create the BankAccount, which fails.
        BankAccountDTO bankAccountDTO = bankAccountMapper.toDto(bankAccount);

        restBankAccountMockMvc.perform(post("/api/bank-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bankAccountDTO)))
            .andExpect(status().isBadRequest());

        List<BankAccount> bankAccountList = bankAccountRepository.findAll();
        assertThat(bankAccountList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkMultipleSessionAllowedIsRequired() throws Exception {
        int databaseSizeBeforeTest = bankAccountRepository.findAll().size();
        // set the field null
        bankAccount.setMultipleSessionAllowed(null);

        // Create the BankAccount, which fails.
        BankAccountDTO bankAccountDTO = bankAccountMapper.toDto(bankAccount);

        restBankAccountMockMvc.perform(post("/api/bank-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bankAccountDTO)))
            .andExpect(status().isBadRequest());

        List<BankAccount> bankAccountList = bankAccountRepository.findAll();
        assertThat(bankAccountList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllBankAccounts() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList
        restBankAccountMockMvc.perform(get("/api/bank-accounts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bankAccount.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateOfOpening").value(hasItem(DEFAULT_DATE_OF_OPENING.toString())))
            .andExpect(jsonPath("$.[*].openedBy").value(hasItem(DEFAULT_OPENED_BY.toString())))
            .andExpect(jsonPath("$.[*].customerId").value(hasItem(DEFAULT_CUSTOMER_ID.toString())))
            .andExpect(jsonPath("$.[*].accountNumber").value(hasItem(DEFAULT_ACCOUNT_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].loginId").value(hasItem(DEFAULT_LOGIN_ID.toString())))
            .andExpect(jsonPath("$.[*].loginPassword").value(hasItem(DEFAULT_LOGIN_PASSWORD.toString())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE.toString())))
            .andExpect(jsonPath("$.[*].bankAccountType").value(hasItem(DEFAULT_BANK_ACCOUNT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].chequeBookIssued").value(hasItem(DEFAULT_CHEQUE_BOOK_ISSUED.booleanValue())))
            .andExpect(jsonPath("$.[*].accountBalance").value(hasItem(DEFAULT_ACCOUNT_BALANCE.doubleValue())))
            .andExpect(jsonPath("$.[*].balanceCheckedOn").value(hasItem(DEFAULT_BALANCE_CHECKED_ON.toString())))
            .andExpect(jsonPath("$.[*].minimumBalance").value(hasItem(DEFAULT_MINIMUM_BALANCE.doubleValue())))
            .andExpect(jsonPath("$.[*].passwordGetsExpired").value(hasItem(DEFAULT_PASSWORD_GETS_EXPIRED.booleanValue())))
            .andExpect(jsonPath("$.[*].expirationPeriodDays").value(hasItem(DEFAULT_EXPIRATION_PERIOD_DAYS)))
            .andExpect(jsonPath("$.[*].passwordUpdatedOn").value(hasItem(DEFAULT_PASSWORD_UPDATED_ON.toString())))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS.toString())))
            .andExpect(jsonPath("$.[*].accountStatus").value(hasItem(DEFAULT_ACCOUNT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].multipleSessionAllowed").value(hasItem(DEFAULT_MULTIPLE_SESSION_ALLOWED.booleanValue())))
            .andExpect(jsonPath("$.[*].projectName").value(hasItem(DEFAULT_PROJECT_NAME.toString())))
            .andExpect(jsonPath("$.[*].projectCode").value(hasItem(DEFAULT_PROJECT_CODE.toString())))
            .andExpect(jsonPath("$.[*].requestedOn").value(hasItem(DEFAULT_REQUESTED_ON.toString())))
            .andExpect(jsonPath("$.[*].requestedBy").value(hasItem(DEFAULT_REQUESTED_BY.toString())));
    }
    
    @Test
    @Transactional
    public void getBankAccount() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get the bankAccount
        restBankAccountMockMvc.perform(get("/api/bank-accounts/{id}", bankAccount.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(bankAccount.getId().intValue()))
            .andExpect(jsonPath("$.dateOfOpening").value(DEFAULT_DATE_OF_OPENING.toString()))
            .andExpect(jsonPath("$.openedBy").value(DEFAULT_OPENED_BY.toString()))
            .andExpect(jsonPath("$.customerId").value(DEFAULT_CUSTOMER_ID.toString()))
            .andExpect(jsonPath("$.accountNumber").value(DEFAULT_ACCOUNT_NUMBER.toString()))
            .andExpect(jsonPath("$.loginId").value(DEFAULT_LOGIN_ID.toString()))
            .andExpect(jsonPath("$.loginPassword").value(DEFAULT_LOGIN_PASSWORD.toString()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE.toString()))
            .andExpect(jsonPath("$.bankAccountType").value(DEFAULT_BANK_ACCOUNT_TYPE.toString()))
            .andExpect(jsonPath("$.chequeBookIssued").value(DEFAULT_CHEQUE_BOOK_ISSUED.booleanValue()))
            .andExpect(jsonPath("$.accountBalance").value(DEFAULT_ACCOUNT_BALANCE.doubleValue()))
            .andExpect(jsonPath("$.balanceCheckedOn").value(DEFAULT_BALANCE_CHECKED_ON.toString()))
            .andExpect(jsonPath("$.minimumBalance").value(DEFAULT_MINIMUM_BALANCE.doubleValue()))
            .andExpect(jsonPath("$.passwordGetsExpired").value(DEFAULT_PASSWORD_GETS_EXPIRED.booleanValue()))
            .andExpect(jsonPath("$.expirationPeriodDays").value(DEFAULT_EXPIRATION_PERIOD_DAYS))
            .andExpect(jsonPath("$.passwordUpdatedOn").value(DEFAULT_PASSWORD_UPDATED_ON.toString()))
            .andExpect(jsonPath("$.remarks").value(DEFAULT_REMARKS.toString()))
            .andExpect(jsonPath("$.accountStatus").value(DEFAULT_ACCOUNT_STATUS.toString()))
            .andExpect(jsonPath("$.multipleSessionAllowed").value(DEFAULT_MULTIPLE_SESSION_ALLOWED.booleanValue()))
            .andExpect(jsonPath("$.projectName").value(DEFAULT_PROJECT_NAME.toString()))
            .andExpect(jsonPath("$.projectCode").value(DEFAULT_PROJECT_CODE.toString()))
            .andExpect(jsonPath("$.requestedOn").value(DEFAULT_REQUESTED_ON.toString()))
            .andExpect(jsonPath("$.requestedBy").value(DEFAULT_REQUESTED_BY.toString()));
    }

    @Test
    @Transactional
    public void getAllBankAccountsByDateOfOpeningIsEqualToSomething() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where dateOfOpening equals to DEFAULT_DATE_OF_OPENING
        defaultBankAccountShouldBeFound("dateOfOpening.equals=" + DEFAULT_DATE_OF_OPENING);

        // Get all the bankAccountList where dateOfOpening equals to UPDATED_DATE_OF_OPENING
        defaultBankAccountShouldNotBeFound("dateOfOpening.equals=" + UPDATED_DATE_OF_OPENING);
    }

    @Test
    @Transactional
    public void getAllBankAccountsByDateOfOpeningIsInShouldWork() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where dateOfOpening in DEFAULT_DATE_OF_OPENING or UPDATED_DATE_OF_OPENING
        defaultBankAccountShouldBeFound("dateOfOpening.in=" + DEFAULT_DATE_OF_OPENING + "," + UPDATED_DATE_OF_OPENING);

        // Get all the bankAccountList where dateOfOpening equals to UPDATED_DATE_OF_OPENING
        defaultBankAccountShouldNotBeFound("dateOfOpening.in=" + UPDATED_DATE_OF_OPENING);
    }

    @Test
    @Transactional
    public void getAllBankAccountsByDateOfOpeningIsNullOrNotNull() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where dateOfOpening is not null
        defaultBankAccountShouldBeFound("dateOfOpening.specified=true");

        // Get all the bankAccountList where dateOfOpening is null
        defaultBankAccountShouldNotBeFound("dateOfOpening.specified=false");
    }

    @Test
    @Transactional
    public void getAllBankAccountsByOpenedByIsEqualToSomething() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where openedBy equals to DEFAULT_OPENED_BY
        defaultBankAccountShouldBeFound("openedBy.equals=" + DEFAULT_OPENED_BY);

        // Get all the bankAccountList where openedBy equals to UPDATED_OPENED_BY
        defaultBankAccountShouldNotBeFound("openedBy.equals=" + UPDATED_OPENED_BY);
    }

    @Test
    @Transactional
    public void getAllBankAccountsByOpenedByIsInShouldWork() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where openedBy in DEFAULT_OPENED_BY or UPDATED_OPENED_BY
        defaultBankAccountShouldBeFound("openedBy.in=" + DEFAULT_OPENED_BY + "," + UPDATED_OPENED_BY);

        // Get all the bankAccountList where openedBy equals to UPDATED_OPENED_BY
        defaultBankAccountShouldNotBeFound("openedBy.in=" + UPDATED_OPENED_BY);
    }

    @Test
    @Transactional
    public void getAllBankAccountsByOpenedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where openedBy is not null
        defaultBankAccountShouldBeFound("openedBy.specified=true");

        // Get all the bankAccountList where openedBy is null
        defaultBankAccountShouldNotBeFound("openedBy.specified=false");
    }

    @Test
    @Transactional
    public void getAllBankAccountsByCustomerIdIsEqualToSomething() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where customerId equals to DEFAULT_CUSTOMER_ID
        defaultBankAccountShouldBeFound("customerId.equals=" + DEFAULT_CUSTOMER_ID);

        // Get all the bankAccountList where customerId equals to UPDATED_CUSTOMER_ID
        defaultBankAccountShouldNotBeFound("customerId.equals=" + UPDATED_CUSTOMER_ID);
    }

    @Test
    @Transactional
    public void getAllBankAccountsByCustomerIdIsInShouldWork() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where customerId in DEFAULT_CUSTOMER_ID or UPDATED_CUSTOMER_ID
        defaultBankAccountShouldBeFound("customerId.in=" + DEFAULT_CUSTOMER_ID + "," + UPDATED_CUSTOMER_ID);

        // Get all the bankAccountList where customerId equals to UPDATED_CUSTOMER_ID
        defaultBankAccountShouldNotBeFound("customerId.in=" + UPDATED_CUSTOMER_ID);
    }

    @Test
    @Transactional
    public void getAllBankAccountsByCustomerIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where customerId is not null
        defaultBankAccountShouldBeFound("customerId.specified=true");

        // Get all the bankAccountList where customerId is null
        defaultBankAccountShouldNotBeFound("customerId.specified=false");
    }

    @Test
    @Transactional
    public void getAllBankAccountsByAccountNumberIsEqualToSomething() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where accountNumber equals to DEFAULT_ACCOUNT_NUMBER
        defaultBankAccountShouldBeFound("accountNumber.equals=" + DEFAULT_ACCOUNT_NUMBER);

        // Get all the bankAccountList where accountNumber equals to UPDATED_ACCOUNT_NUMBER
        defaultBankAccountShouldNotBeFound("accountNumber.equals=" + UPDATED_ACCOUNT_NUMBER);
    }

    @Test
    @Transactional
    public void getAllBankAccountsByAccountNumberIsInShouldWork() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where accountNumber in DEFAULT_ACCOUNT_NUMBER or UPDATED_ACCOUNT_NUMBER
        defaultBankAccountShouldBeFound("accountNumber.in=" + DEFAULT_ACCOUNT_NUMBER + "," + UPDATED_ACCOUNT_NUMBER);

        // Get all the bankAccountList where accountNumber equals to UPDATED_ACCOUNT_NUMBER
        defaultBankAccountShouldNotBeFound("accountNumber.in=" + UPDATED_ACCOUNT_NUMBER);
    }

    @Test
    @Transactional
    public void getAllBankAccountsByAccountNumberIsNullOrNotNull() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where accountNumber is not null
        defaultBankAccountShouldBeFound("accountNumber.specified=true");

        // Get all the bankAccountList where accountNumber is null
        defaultBankAccountShouldNotBeFound("accountNumber.specified=false");
    }

    @Test
    @Transactional
    public void getAllBankAccountsByLoginIdIsEqualToSomething() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where loginId equals to DEFAULT_LOGIN_ID
        defaultBankAccountShouldBeFound("loginId.equals=" + DEFAULT_LOGIN_ID);

        // Get all the bankAccountList where loginId equals to UPDATED_LOGIN_ID
        defaultBankAccountShouldNotBeFound("loginId.equals=" + UPDATED_LOGIN_ID);
    }

    @Test
    @Transactional
    public void getAllBankAccountsByLoginIdIsInShouldWork() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where loginId in DEFAULT_LOGIN_ID or UPDATED_LOGIN_ID
        defaultBankAccountShouldBeFound("loginId.in=" + DEFAULT_LOGIN_ID + "," + UPDATED_LOGIN_ID);

        // Get all the bankAccountList where loginId equals to UPDATED_LOGIN_ID
        defaultBankAccountShouldNotBeFound("loginId.in=" + UPDATED_LOGIN_ID);
    }

    @Test
    @Transactional
    public void getAllBankAccountsByLoginIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where loginId is not null
        defaultBankAccountShouldBeFound("loginId.specified=true");

        // Get all the bankAccountList where loginId is null
        defaultBankAccountShouldNotBeFound("loginId.specified=false");
    }

    @Test
    @Transactional
    public void getAllBankAccountsByLoginPasswordIsEqualToSomething() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where loginPassword equals to DEFAULT_LOGIN_PASSWORD
        defaultBankAccountShouldBeFound("loginPassword.equals=" + DEFAULT_LOGIN_PASSWORD);

        // Get all the bankAccountList where loginPassword equals to UPDATED_LOGIN_PASSWORD
        defaultBankAccountShouldNotBeFound("loginPassword.equals=" + UPDATED_LOGIN_PASSWORD);
    }

    @Test
    @Transactional
    public void getAllBankAccountsByLoginPasswordIsInShouldWork() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where loginPassword in DEFAULT_LOGIN_PASSWORD or UPDATED_LOGIN_PASSWORD
        defaultBankAccountShouldBeFound("loginPassword.in=" + DEFAULT_LOGIN_PASSWORD + "," + UPDATED_LOGIN_PASSWORD);

        // Get all the bankAccountList where loginPassword equals to UPDATED_LOGIN_PASSWORD
        defaultBankAccountShouldNotBeFound("loginPassword.in=" + UPDATED_LOGIN_PASSWORD);
    }

    @Test
    @Transactional
    public void getAllBankAccountsByLoginPasswordIsNullOrNotNull() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where loginPassword is not null
        defaultBankAccountShouldBeFound("loginPassword.specified=true");

        // Get all the bankAccountList where loginPassword is null
        defaultBankAccountShouldNotBeFound("loginPassword.specified=false");
    }

    @Test
    @Transactional
    public void getAllBankAccountsByEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where email equals to DEFAULT_EMAIL
        defaultBankAccountShouldBeFound("email.equals=" + DEFAULT_EMAIL);

        // Get all the bankAccountList where email equals to UPDATED_EMAIL
        defaultBankAccountShouldNotBeFound("email.equals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllBankAccountsByEmailIsInShouldWork() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where email in DEFAULT_EMAIL or UPDATED_EMAIL
        defaultBankAccountShouldBeFound("email.in=" + DEFAULT_EMAIL + "," + UPDATED_EMAIL);

        // Get all the bankAccountList where email equals to UPDATED_EMAIL
        defaultBankAccountShouldNotBeFound("email.in=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllBankAccountsByEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where email is not null
        defaultBankAccountShouldBeFound("email.specified=true");

        // Get all the bankAccountList where email is null
        defaultBankAccountShouldNotBeFound("email.specified=false");
    }

    @Test
    @Transactional
    public void getAllBankAccountsByPhoneIsEqualToSomething() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where phone equals to DEFAULT_PHONE
        defaultBankAccountShouldBeFound("phone.equals=" + DEFAULT_PHONE);

        // Get all the bankAccountList where phone equals to UPDATED_PHONE
        defaultBankAccountShouldNotBeFound("phone.equals=" + UPDATED_PHONE);
    }

    @Test
    @Transactional
    public void getAllBankAccountsByPhoneIsInShouldWork() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where phone in DEFAULT_PHONE or UPDATED_PHONE
        defaultBankAccountShouldBeFound("phone.in=" + DEFAULT_PHONE + "," + UPDATED_PHONE);

        // Get all the bankAccountList where phone equals to UPDATED_PHONE
        defaultBankAccountShouldNotBeFound("phone.in=" + UPDATED_PHONE);
    }

    @Test
    @Transactional
    public void getAllBankAccountsByPhoneIsNullOrNotNull() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where phone is not null
        defaultBankAccountShouldBeFound("phone.specified=true");

        // Get all the bankAccountList where phone is null
        defaultBankAccountShouldNotBeFound("phone.specified=false");
    }

    @Test
    @Transactional
    public void getAllBankAccountsByBankAccountTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where bankAccountType equals to DEFAULT_BANK_ACCOUNT_TYPE
        defaultBankAccountShouldBeFound("bankAccountType.equals=" + DEFAULT_BANK_ACCOUNT_TYPE);

        // Get all the bankAccountList where bankAccountType equals to UPDATED_BANK_ACCOUNT_TYPE
        defaultBankAccountShouldNotBeFound("bankAccountType.equals=" + UPDATED_BANK_ACCOUNT_TYPE);
    }

    @Test
    @Transactional
    public void getAllBankAccountsByBankAccountTypeIsInShouldWork() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where bankAccountType in DEFAULT_BANK_ACCOUNT_TYPE or UPDATED_BANK_ACCOUNT_TYPE
        defaultBankAccountShouldBeFound("bankAccountType.in=" + DEFAULT_BANK_ACCOUNT_TYPE + "," + UPDATED_BANK_ACCOUNT_TYPE);

        // Get all the bankAccountList where bankAccountType equals to UPDATED_BANK_ACCOUNT_TYPE
        defaultBankAccountShouldNotBeFound("bankAccountType.in=" + UPDATED_BANK_ACCOUNT_TYPE);
    }

    @Test
    @Transactional
    public void getAllBankAccountsByBankAccountTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where bankAccountType is not null
        defaultBankAccountShouldBeFound("bankAccountType.specified=true");

        // Get all the bankAccountList where bankAccountType is null
        defaultBankAccountShouldNotBeFound("bankAccountType.specified=false");
    }

    @Test
    @Transactional
    public void getAllBankAccountsByChequeBookIssuedIsEqualToSomething() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where chequeBookIssued equals to DEFAULT_CHEQUE_BOOK_ISSUED
        defaultBankAccountShouldBeFound("chequeBookIssued.equals=" + DEFAULT_CHEQUE_BOOK_ISSUED);

        // Get all the bankAccountList where chequeBookIssued equals to UPDATED_CHEQUE_BOOK_ISSUED
        defaultBankAccountShouldNotBeFound("chequeBookIssued.equals=" + UPDATED_CHEQUE_BOOK_ISSUED);
    }

    @Test
    @Transactional
    public void getAllBankAccountsByChequeBookIssuedIsInShouldWork() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where chequeBookIssued in DEFAULT_CHEQUE_BOOK_ISSUED or UPDATED_CHEQUE_BOOK_ISSUED
        defaultBankAccountShouldBeFound("chequeBookIssued.in=" + DEFAULT_CHEQUE_BOOK_ISSUED + "," + UPDATED_CHEQUE_BOOK_ISSUED);

        // Get all the bankAccountList where chequeBookIssued equals to UPDATED_CHEQUE_BOOK_ISSUED
        defaultBankAccountShouldNotBeFound("chequeBookIssued.in=" + UPDATED_CHEQUE_BOOK_ISSUED);
    }

    @Test
    @Transactional
    public void getAllBankAccountsByChequeBookIssuedIsNullOrNotNull() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where chequeBookIssued is not null
        defaultBankAccountShouldBeFound("chequeBookIssued.specified=true");

        // Get all the bankAccountList where chequeBookIssued is null
        defaultBankAccountShouldNotBeFound("chequeBookIssued.specified=false");
    }

    @Test
    @Transactional
    public void getAllBankAccountsByAccountBalanceIsEqualToSomething() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where accountBalance equals to DEFAULT_ACCOUNT_BALANCE
        defaultBankAccountShouldBeFound("accountBalance.equals=" + DEFAULT_ACCOUNT_BALANCE);

        // Get all the bankAccountList where accountBalance equals to UPDATED_ACCOUNT_BALANCE
        defaultBankAccountShouldNotBeFound("accountBalance.equals=" + UPDATED_ACCOUNT_BALANCE);
    }

    @Test
    @Transactional
    public void getAllBankAccountsByAccountBalanceIsInShouldWork() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where accountBalance in DEFAULT_ACCOUNT_BALANCE or UPDATED_ACCOUNT_BALANCE
        defaultBankAccountShouldBeFound("accountBalance.in=" + DEFAULT_ACCOUNT_BALANCE + "," + UPDATED_ACCOUNT_BALANCE);

        // Get all the bankAccountList where accountBalance equals to UPDATED_ACCOUNT_BALANCE
        defaultBankAccountShouldNotBeFound("accountBalance.in=" + UPDATED_ACCOUNT_BALANCE);
    }

    @Test
    @Transactional
    public void getAllBankAccountsByAccountBalanceIsNullOrNotNull() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where accountBalance is not null
        defaultBankAccountShouldBeFound("accountBalance.specified=true");

        // Get all the bankAccountList where accountBalance is null
        defaultBankAccountShouldNotBeFound("accountBalance.specified=false");
    }

    @Test
    @Transactional
    public void getAllBankAccountsByBalanceCheckedOnIsEqualToSomething() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where balanceCheckedOn equals to DEFAULT_BALANCE_CHECKED_ON
        defaultBankAccountShouldBeFound("balanceCheckedOn.equals=" + DEFAULT_BALANCE_CHECKED_ON);

        // Get all the bankAccountList where balanceCheckedOn equals to UPDATED_BALANCE_CHECKED_ON
        defaultBankAccountShouldNotBeFound("balanceCheckedOn.equals=" + UPDATED_BALANCE_CHECKED_ON);
    }

    @Test
    @Transactional
    public void getAllBankAccountsByBalanceCheckedOnIsInShouldWork() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where balanceCheckedOn in DEFAULT_BALANCE_CHECKED_ON or UPDATED_BALANCE_CHECKED_ON
        defaultBankAccountShouldBeFound("balanceCheckedOn.in=" + DEFAULT_BALANCE_CHECKED_ON + "," + UPDATED_BALANCE_CHECKED_ON);

        // Get all the bankAccountList where balanceCheckedOn equals to UPDATED_BALANCE_CHECKED_ON
        defaultBankAccountShouldNotBeFound("balanceCheckedOn.in=" + UPDATED_BALANCE_CHECKED_ON);
    }

    @Test
    @Transactional
    public void getAllBankAccountsByBalanceCheckedOnIsNullOrNotNull() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where balanceCheckedOn is not null
        defaultBankAccountShouldBeFound("balanceCheckedOn.specified=true");

        // Get all the bankAccountList where balanceCheckedOn is null
        defaultBankAccountShouldNotBeFound("balanceCheckedOn.specified=false");
    }

    @Test
    @Transactional
    public void getAllBankAccountsByMinimumBalanceIsEqualToSomething() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where minimumBalance equals to DEFAULT_MINIMUM_BALANCE
        defaultBankAccountShouldBeFound("minimumBalance.equals=" + DEFAULT_MINIMUM_BALANCE);

        // Get all the bankAccountList where minimumBalance equals to UPDATED_MINIMUM_BALANCE
        defaultBankAccountShouldNotBeFound("minimumBalance.equals=" + UPDATED_MINIMUM_BALANCE);
    }

    @Test
    @Transactional
    public void getAllBankAccountsByMinimumBalanceIsInShouldWork() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where minimumBalance in DEFAULT_MINIMUM_BALANCE or UPDATED_MINIMUM_BALANCE
        defaultBankAccountShouldBeFound("minimumBalance.in=" + DEFAULT_MINIMUM_BALANCE + "," + UPDATED_MINIMUM_BALANCE);

        // Get all the bankAccountList where minimumBalance equals to UPDATED_MINIMUM_BALANCE
        defaultBankAccountShouldNotBeFound("minimumBalance.in=" + UPDATED_MINIMUM_BALANCE);
    }

    @Test
    @Transactional
    public void getAllBankAccountsByMinimumBalanceIsNullOrNotNull() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where minimumBalance is not null
        defaultBankAccountShouldBeFound("minimumBalance.specified=true");

        // Get all the bankAccountList where minimumBalance is null
        defaultBankAccountShouldNotBeFound("minimumBalance.specified=false");
    }

    @Test
    @Transactional
    public void getAllBankAccountsByPasswordGetsExpiredIsEqualToSomething() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where passwordGetsExpired equals to DEFAULT_PASSWORD_GETS_EXPIRED
        defaultBankAccountShouldBeFound("passwordGetsExpired.equals=" + DEFAULT_PASSWORD_GETS_EXPIRED);

        // Get all the bankAccountList where passwordGetsExpired equals to UPDATED_PASSWORD_GETS_EXPIRED
        defaultBankAccountShouldNotBeFound("passwordGetsExpired.equals=" + UPDATED_PASSWORD_GETS_EXPIRED);
    }

    @Test
    @Transactional
    public void getAllBankAccountsByPasswordGetsExpiredIsInShouldWork() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where passwordGetsExpired in DEFAULT_PASSWORD_GETS_EXPIRED or UPDATED_PASSWORD_GETS_EXPIRED
        defaultBankAccountShouldBeFound("passwordGetsExpired.in=" + DEFAULT_PASSWORD_GETS_EXPIRED + "," + UPDATED_PASSWORD_GETS_EXPIRED);

        // Get all the bankAccountList where passwordGetsExpired equals to UPDATED_PASSWORD_GETS_EXPIRED
        defaultBankAccountShouldNotBeFound("passwordGetsExpired.in=" + UPDATED_PASSWORD_GETS_EXPIRED);
    }

    @Test
    @Transactional
    public void getAllBankAccountsByPasswordGetsExpiredIsNullOrNotNull() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where passwordGetsExpired is not null
        defaultBankAccountShouldBeFound("passwordGetsExpired.specified=true");

        // Get all the bankAccountList where passwordGetsExpired is null
        defaultBankAccountShouldNotBeFound("passwordGetsExpired.specified=false");
    }

    @Test
    @Transactional
    public void getAllBankAccountsByExpirationPeriodDaysIsEqualToSomething() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where expirationPeriodDays equals to DEFAULT_EXPIRATION_PERIOD_DAYS
        defaultBankAccountShouldBeFound("expirationPeriodDays.equals=" + DEFAULT_EXPIRATION_PERIOD_DAYS);

        // Get all the bankAccountList where expirationPeriodDays equals to UPDATED_EXPIRATION_PERIOD_DAYS
        defaultBankAccountShouldNotBeFound("expirationPeriodDays.equals=" + UPDATED_EXPIRATION_PERIOD_DAYS);
    }

    @Test
    @Transactional
    public void getAllBankAccountsByExpirationPeriodDaysIsInShouldWork() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where expirationPeriodDays in DEFAULT_EXPIRATION_PERIOD_DAYS or UPDATED_EXPIRATION_PERIOD_DAYS
        defaultBankAccountShouldBeFound("expirationPeriodDays.in=" + DEFAULT_EXPIRATION_PERIOD_DAYS + "," + UPDATED_EXPIRATION_PERIOD_DAYS);

        // Get all the bankAccountList where expirationPeriodDays equals to UPDATED_EXPIRATION_PERIOD_DAYS
        defaultBankAccountShouldNotBeFound("expirationPeriodDays.in=" + UPDATED_EXPIRATION_PERIOD_DAYS);
    }

    @Test
    @Transactional
    public void getAllBankAccountsByExpirationPeriodDaysIsNullOrNotNull() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where expirationPeriodDays is not null
        defaultBankAccountShouldBeFound("expirationPeriodDays.specified=true");

        // Get all the bankAccountList where expirationPeriodDays is null
        defaultBankAccountShouldNotBeFound("expirationPeriodDays.specified=false");
    }

    @Test
    @Transactional
    public void getAllBankAccountsByExpirationPeriodDaysIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where expirationPeriodDays greater than or equals to DEFAULT_EXPIRATION_PERIOD_DAYS
        defaultBankAccountShouldBeFound("expirationPeriodDays.greaterOrEqualThan=" + DEFAULT_EXPIRATION_PERIOD_DAYS);

        // Get all the bankAccountList where expirationPeriodDays greater than or equals to UPDATED_EXPIRATION_PERIOD_DAYS
        defaultBankAccountShouldNotBeFound("expirationPeriodDays.greaterOrEqualThan=" + UPDATED_EXPIRATION_PERIOD_DAYS);
    }

    @Test
    @Transactional
    public void getAllBankAccountsByExpirationPeriodDaysIsLessThanSomething() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where expirationPeriodDays less than or equals to DEFAULT_EXPIRATION_PERIOD_DAYS
        defaultBankAccountShouldNotBeFound("expirationPeriodDays.lessThan=" + DEFAULT_EXPIRATION_PERIOD_DAYS);

        // Get all the bankAccountList where expirationPeriodDays less than or equals to UPDATED_EXPIRATION_PERIOD_DAYS
        defaultBankAccountShouldBeFound("expirationPeriodDays.lessThan=" + UPDATED_EXPIRATION_PERIOD_DAYS);
    }


    @Test
    @Transactional
    public void getAllBankAccountsByPasswordUpdatedOnIsEqualToSomething() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where passwordUpdatedOn equals to DEFAULT_PASSWORD_UPDATED_ON
        defaultBankAccountShouldBeFound("passwordUpdatedOn.equals=" + DEFAULT_PASSWORD_UPDATED_ON);

        // Get all the bankAccountList where passwordUpdatedOn equals to UPDATED_PASSWORD_UPDATED_ON
        defaultBankAccountShouldNotBeFound("passwordUpdatedOn.equals=" + UPDATED_PASSWORD_UPDATED_ON);
    }

    @Test
    @Transactional
    public void getAllBankAccountsByPasswordUpdatedOnIsInShouldWork() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where passwordUpdatedOn in DEFAULT_PASSWORD_UPDATED_ON or UPDATED_PASSWORD_UPDATED_ON
        defaultBankAccountShouldBeFound("passwordUpdatedOn.in=" + DEFAULT_PASSWORD_UPDATED_ON + "," + UPDATED_PASSWORD_UPDATED_ON);

        // Get all the bankAccountList where passwordUpdatedOn equals to UPDATED_PASSWORD_UPDATED_ON
        defaultBankAccountShouldNotBeFound("passwordUpdatedOn.in=" + UPDATED_PASSWORD_UPDATED_ON);
    }

    @Test
    @Transactional
    public void getAllBankAccountsByPasswordUpdatedOnIsNullOrNotNull() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where passwordUpdatedOn is not null
        defaultBankAccountShouldBeFound("passwordUpdatedOn.specified=true");

        // Get all the bankAccountList where passwordUpdatedOn is null
        defaultBankAccountShouldNotBeFound("passwordUpdatedOn.specified=false");
    }

    @Test
    @Transactional
    public void getAllBankAccountsByRemarksIsEqualToSomething() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where remarks equals to DEFAULT_REMARKS
        defaultBankAccountShouldBeFound("remarks.equals=" + DEFAULT_REMARKS);

        // Get all the bankAccountList where remarks equals to UPDATED_REMARKS
        defaultBankAccountShouldNotBeFound("remarks.equals=" + UPDATED_REMARKS);
    }

    @Test
    @Transactional
    public void getAllBankAccountsByRemarksIsInShouldWork() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where remarks in DEFAULT_REMARKS or UPDATED_REMARKS
        defaultBankAccountShouldBeFound("remarks.in=" + DEFAULT_REMARKS + "," + UPDATED_REMARKS);

        // Get all the bankAccountList where remarks equals to UPDATED_REMARKS
        defaultBankAccountShouldNotBeFound("remarks.in=" + UPDATED_REMARKS);
    }

    @Test
    @Transactional
    public void getAllBankAccountsByRemarksIsNullOrNotNull() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where remarks is not null
        defaultBankAccountShouldBeFound("remarks.specified=true");

        // Get all the bankAccountList where remarks is null
        defaultBankAccountShouldNotBeFound("remarks.specified=false");
    }

    @Test
    @Transactional
    public void getAllBankAccountsByAccountStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where accountStatus equals to DEFAULT_ACCOUNT_STATUS
        defaultBankAccountShouldBeFound("accountStatus.equals=" + DEFAULT_ACCOUNT_STATUS);

        // Get all the bankAccountList where accountStatus equals to UPDATED_ACCOUNT_STATUS
        defaultBankAccountShouldNotBeFound("accountStatus.equals=" + UPDATED_ACCOUNT_STATUS);
    }

    @Test
    @Transactional
    public void getAllBankAccountsByAccountStatusIsInShouldWork() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where accountStatus in DEFAULT_ACCOUNT_STATUS or UPDATED_ACCOUNT_STATUS
        defaultBankAccountShouldBeFound("accountStatus.in=" + DEFAULT_ACCOUNT_STATUS + "," + UPDATED_ACCOUNT_STATUS);

        // Get all the bankAccountList where accountStatus equals to UPDATED_ACCOUNT_STATUS
        defaultBankAccountShouldNotBeFound("accountStatus.in=" + UPDATED_ACCOUNT_STATUS);
    }

    @Test
    @Transactional
    public void getAllBankAccountsByAccountStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where accountStatus is not null
        defaultBankAccountShouldBeFound("accountStatus.specified=true");

        // Get all the bankAccountList where accountStatus is null
        defaultBankAccountShouldNotBeFound("accountStatus.specified=false");
    }

    @Test
    @Transactional
    public void getAllBankAccountsByMultipleSessionAllowedIsEqualToSomething() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where multipleSessionAllowed equals to DEFAULT_MULTIPLE_SESSION_ALLOWED
        defaultBankAccountShouldBeFound("multipleSessionAllowed.equals=" + DEFAULT_MULTIPLE_SESSION_ALLOWED);

        // Get all the bankAccountList where multipleSessionAllowed equals to UPDATED_MULTIPLE_SESSION_ALLOWED
        defaultBankAccountShouldNotBeFound("multipleSessionAllowed.equals=" + UPDATED_MULTIPLE_SESSION_ALLOWED);
    }

    @Test
    @Transactional
    public void getAllBankAccountsByMultipleSessionAllowedIsInShouldWork() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where multipleSessionAllowed in DEFAULT_MULTIPLE_SESSION_ALLOWED or UPDATED_MULTIPLE_SESSION_ALLOWED
        defaultBankAccountShouldBeFound("multipleSessionAllowed.in=" + DEFAULT_MULTIPLE_SESSION_ALLOWED + "," + UPDATED_MULTIPLE_SESSION_ALLOWED);

        // Get all the bankAccountList where multipleSessionAllowed equals to UPDATED_MULTIPLE_SESSION_ALLOWED
        defaultBankAccountShouldNotBeFound("multipleSessionAllowed.in=" + UPDATED_MULTIPLE_SESSION_ALLOWED);
    }

    @Test
    @Transactional
    public void getAllBankAccountsByMultipleSessionAllowedIsNullOrNotNull() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where multipleSessionAllowed is not null
        defaultBankAccountShouldBeFound("multipleSessionAllowed.specified=true");

        // Get all the bankAccountList where multipleSessionAllowed is null
        defaultBankAccountShouldNotBeFound("multipleSessionAllowed.specified=false");
    }

    @Test
    @Transactional
    public void getAllBankAccountsByProjectNameIsEqualToSomething() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where projectName equals to DEFAULT_PROJECT_NAME
        defaultBankAccountShouldBeFound("projectName.equals=" + DEFAULT_PROJECT_NAME);

        // Get all the bankAccountList where projectName equals to UPDATED_PROJECT_NAME
        defaultBankAccountShouldNotBeFound("projectName.equals=" + UPDATED_PROJECT_NAME);
    }

    @Test
    @Transactional
    public void getAllBankAccountsByProjectNameIsInShouldWork() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where projectName in DEFAULT_PROJECT_NAME or UPDATED_PROJECT_NAME
        defaultBankAccountShouldBeFound("projectName.in=" + DEFAULT_PROJECT_NAME + "," + UPDATED_PROJECT_NAME);

        // Get all the bankAccountList where projectName equals to UPDATED_PROJECT_NAME
        defaultBankAccountShouldNotBeFound("projectName.in=" + UPDATED_PROJECT_NAME);
    }

    @Test
    @Transactional
    public void getAllBankAccountsByProjectNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where projectName is not null
        defaultBankAccountShouldBeFound("projectName.specified=true");

        // Get all the bankAccountList where projectName is null
        defaultBankAccountShouldNotBeFound("projectName.specified=false");
    }

    @Test
    @Transactional
    public void getAllBankAccountsByProjectCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where projectCode equals to DEFAULT_PROJECT_CODE
        defaultBankAccountShouldBeFound("projectCode.equals=" + DEFAULT_PROJECT_CODE);

        // Get all the bankAccountList where projectCode equals to UPDATED_PROJECT_CODE
        defaultBankAccountShouldNotBeFound("projectCode.equals=" + UPDATED_PROJECT_CODE);
    }

    @Test
    @Transactional
    public void getAllBankAccountsByProjectCodeIsInShouldWork() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where projectCode in DEFAULT_PROJECT_CODE or UPDATED_PROJECT_CODE
        defaultBankAccountShouldBeFound("projectCode.in=" + DEFAULT_PROJECT_CODE + "," + UPDATED_PROJECT_CODE);

        // Get all the bankAccountList where projectCode equals to UPDATED_PROJECT_CODE
        defaultBankAccountShouldNotBeFound("projectCode.in=" + UPDATED_PROJECT_CODE);
    }

    @Test
    @Transactional
    public void getAllBankAccountsByProjectCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where projectCode is not null
        defaultBankAccountShouldBeFound("projectCode.specified=true");

        // Get all the bankAccountList where projectCode is null
        defaultBankAccountShouldNotBeFound("projectCode.specified=false");
    }

    @Test
    @Transactional
    public void getAllBankAccountsByRequestedOnIsEqualToSomething() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where requestedOn equals to DEFAULT_REQUESTED_ON
        defaultBankAccountShouldBeFound("requestedOn.equals=" + DEFAULT_REQUESTED_ON);

        // Get all the bankAccountList where requestedOn equals to UPDATED_REQUESTED_ON
        defaultBankAccountShouldNotBeFound("requestedOn.equals=" + UPDATED_REQUESTED_ON);
    }

    @Test
    @Transactional
    public void getAllBankAccountsByRequestedOnIsInShouldWork() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where requestedOn in DEFAULT_REQUESTED_ON or UPDATED_REQUESTED_ON
        defaultBankAccountShouldBeFound("requestedOn.in=" + DEFAULT_REQUESTED_ON + "," + UPDATED_REQUESTED_ON);

        // Get all the bankAccountList where requestedOn equals to UPDATED_REQUESTED_ON
        defaultBankAccountShouldNotBeFound("requestedOn.in=" + UPDATED_REQUESTED_ON);
    }

    @Test
    @Transactional
    public void getAllBankAccountsByRequestedOnIsNullOrNotNull() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where requestedOn is not null
        defaultBankAccountShouldBeFound("requestedOn.specified=true");

        // Get all the bankAccountList where requestedOn is null
        defaultBankAccountShouldNotBeFound("requestedOn.specified=false");
    }

    @Test
    @Transactional
    public void getAllBankAccountsByRequestedByIsEqualToSomething() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where requestedBy equals to DEFAULT_REQUESTED_BY
        defaultBankAccountShouldBeFound("requestedBy.equals=" + DEFAULT_REQUESTED_BY);

        // Get all the bankAccountList where requestedBy equals to UPDATED_REQUESTED_BY
        defaultBankAccountShouldNotBeFound("requestedBy.equals=" + UPDATED_REQUESTED_BY);
    }

    @Test
    @Transactional
    public void getAllBankAccountsByRequestedByIsInShouldWork() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where requestedBy in DEFAULT_REQUESTED_BY or UPDATED_REQUESTED_BY
        defaultBankAccountShouldBeFound("requestedBy.in=" + DEFAULT_REQUESTED_BY + "," + UPDATED_REQUESTED_BY);

        // Get all the bankAccountList where requestedBy equals to UPDATED_REQUESTED_BY
        defaultBankAccountShouldNotBeFound("requestedBy.in=" + UPDATED_REQUESTED_BY);
    }

    @Test
    @Transactional
    public void getAllBankAccountsByRequestedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        // Get all the bankAccountList where requestedBy is not null
        defaultBankAccountShouldBeFound("requestedBy.specified=true");

        // Get all the bankAccountList where requestedBy is null
        defaultBankAccountShouldNotBeFound("requestedBy.specified=false");
    }

    @Test
    @Transactional
    public void getAllBankAccountsByAccountHolderIsEqualToSomething() throws Exception {
        // Get already existing entity
        Employee accountHolder = bankAccount.getAccountHolder();
        bankAccountRepository.saveAndFlush(bankAccount);
        Long accountHolderId = accountHolder.getId();

        // Get all the bankAccountList where accountHolder equals to accountHolderId
        defaultBankAccountShouldBeFound("accountHolderId.equals=" + accountHolderId);

        // Get all the bankAccountList where accountHolder equals to accountHolderId + 1
        defaultBankAccountShouldNotBeFound("accountHolderId.equals=" + (accountHolderId + 1));
    }


    @Test
    @Transactional
    public void getAllBankAccountsByBankBranchIsEqualToSomething() throws Exception {
        // Get already existing entity
        BankBranch bankBranch = bankAccount.getBankBranch();
        bankAccountRepository.saveAndFlush(bankAccount);
        Long bankBranchId = bankBranch.getId();

        // Get all the bankAccountList where bankBranch equals to bankBranchId
        defaultBankAccountShouldBeFound("bankBranchId.equals=" + bankBranchId);

        // Get all the bankAccountList where bankBranch equals to bankBranchId + 1
        defaultBankAccountShouldNotBeFound("bankBranchId.equals=" + (bankBranchId + 1));
    }


    @Test
    @Transactional
    public void getAllBankAccountsByMobileNumberMappedIsEqualToSomething() throws Exception {
        // Get already existing entity
        MobileNumber mobileNumberMapped = bankAccount.getMobileNumberMapped();
        bankAccountRepository.saveAndFlush(bankAccount);
        Long mobileNumberMappedId = mobileNumberMapped.getId();

        // Get all the bankAccountList where mobileNumberMapped equals to mobileNumberMappedId
        defaultBankAccountShouldBeFound("mobileNumberMappedId.equals=" + mobileNumberMappedId);

        // Get all the bankAccountList where mobileNumberMapped equals to mobileNumberMappedId + 1
        defaultBankAccountShouldNotBeFound("mobileNumberMappedId.equals=" + (mobileNumberMappedId + 1));
    }


    @Test
    @Transactional
    public void getAllBankAccountsByParentAccountIsEqualToSomething() throws Exception {
        // Initialize the database
        BankAccount parentAccount = BankAccountResourceIT.createEntity(em);
        em.persist(parentAccount);
        em.flush();
        bankAccount.setParentAccount(parentAccount);
        bankAccountRepository.saveAndFlush(bankAccount);
        Long parentAccountId = parentAccount.getId();

        // Get all the bankAccountList where parentAccount equals to parentAccountId
        defaultBankAccountShouldBeFound("parentAccountId.equals=" + parentAccountId);

        // Get all the bankAccountList where parentAccount equals to parentAccountId + 1
        defaultBankAccountShouldNotBeFound("parentAccountId.equals=" + (parentAccountId + 1));
    }


    @Test
    @Transactional
    public void getAllBankAccountsByCardsIsEqualToSomething() throws Exception {
        // Initialize the database
        Card cards = CardResourceIT.createEntity(em);
        em.persist(cards);
        em.flush();
        bankAccount.addCards(cards);
        bankAccountRepository.saveAndFlush(bankAccount);
        Long cardsId = cards.getId();

        // Get all the bankAccountList where cards equals to cardsId
        defaultBankAccountShouldBeFound("cardsId.equals=" + cardsId);

        // Get all the bankAccountList where cards equals to cardsId + 1
        defaultBankAccountShouldNotBeFound("cardsId.equals=" + (cardsId + 1));
    }


    @Test
    @Transactional
    public void getAllBankAccountsByBankAccountGroupIsEqualToSomething() throws Exception {
        // Initialize the database
        BankAccountGroup bankAccountGroup = BankAccountGroupResourceIT.createEntity(em);
        em.persist(bankAccountGroup);
        em.flush();
        bankAccount.setBankAccountGroup(bankAccountGroup);
        bankAccountRepository.saveAndFlush(bankAccount);
        Long bankAccountGroupId = bankAccountGroup.getId();

        // Get all the bankAccountList where bankAccountGroup equals to bankAccountGroupId
        defaultBankAccountShouldBeFound("bankAccountGroupId.equals=" + bankAccountGroupId);

        // Get all the bankAccountList where bankAccountGroup equals to bankAccountGroupId + 1
        defaultBankAccountShouldNotBeFound("bankAccountGroupId.equals=" + (bankAccountGroupId + 1));
    }


    @Test
    @Transactional
    public void getAllBankAccountsBySecretQuestionsIsEqualToSomething() throws Exception {
        // Initialize the database
        SecretQuestion secretQuestions = SecretQuestionResourceIT.createEntity(em);
        em.persist(secretQuestions);
        em.flush();
        bankAccount.addSecretQuestions(secretQuestions);
        bankAccountRepository.saveAndFlush(bankAccount);
        Long secretQuestionsId = secretQuestions.getId();

        // Get all the bankAccountList where secretQuestions equals to secretQuestionsId
        defaultBankAccountShouldBeFound("secretQuestionsId.equals=" + secretQuestionsId);

        // Get all the bankAccountList where secretQuestions equals to secretQuestionsId + 1
        defaultBankAccountShouldNotBeFound("secretQuestionsId.equals=" + (secretQuestionsId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultBankAccountShouldBeFound(String filter) throws Exception {
        restBankAccountMockMvc.perform(get("/api/bank-accounts?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bankAccount.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateOfOpening").value(hasItem(DEFAULT_DATE_OF_OPENING.toString())))
            .andExpect(jsonPath("$.[*].openedBy").value(hasItem(DEFAULT_OPENED_BY)))
            .andExpect(jsonPath("$.[*].customerId").value(hasItem(DEFAULT_CUSTOMER_ID)))
            .andExpect(jsonPath("$.[*].accountNumber").value(hasItem(DEFAULT_ACCOUNT_NUMBER)))
            .andExpect(jsonPath("$.[*].loginId").value(hasItem(DEFAULT_LOGIN_ID)))
            .andExpect(jsonPath("$.[*].loginPassword").value(hasItem(DEFAULT_LOGIN_PASSWORD)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE)))
            .andExpect(jsonPath("$.[*].bankAccountType").value(hasItem(DEFAULT_BANK_ACCOUNT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].chequeBookIssued").value(hasItem(DEFAULT_CHEQUE_BOOK_ISSUED.booleanValue())))
            .andExpect(jsonPath("$.[*].accountBalance").value(hasItem(DEFAULT_ACCOUNT_BALANCE.doubleValue())))
            .andExpect(jsonPath("$.[*].balanceCheckedOn").value(hasItem(DEFAULT_BALANCE_CHECKED_ON.toString())))
            .andExpect(jsonPath("$.[*].minimumBalance").value(hasItem(DEFAULT_MINIMUM_BALANCE.doubleValue())))
            .andExpect(jsonPath("$.[*].passwordGetsExpired").value(hasItem(DEFAULT_PASSWORD_GETS_EXPIRED.booleanValue())))
            .andExpect(jsonPath("$.[*].expirationPeriodDays").value(hasItem(DEFAULT_EXPIRATION_PERIOD_DAYS)))
            .andExpect(jsonPath("$.[*].passwordUpdatedOn").value(hasItem(DEFAULT_PASSWORD_UPDATED_ON.toString())))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS)))
            .andExpect(jsonPath("$.[*].accountStatus").value(hasItem(DEFAULT_ACCOUNT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].multipleSessionAllowed").value(hasItem(DEFAULT_MULTIPLE_SESSION_ALLOWED.booleanValue())))
            .andExpect(jsonPath("$.[*].projectName").value(hasItem(DEFAULT_PROJECT_NAME)))
            .andExpect(jsonPath("$.[*].projectCode").value(hasItem(DEFAULT_PROJECT_CODE)))
            .andExpect(jsonPath("$.[*].requestedOn").value(hasItem(DEFAULT_REQUESTED_ON.toString())))
            .andExpect(jsonPath("$.[*].requestedBy").value(hasItem(DEFAULT_REQUESTED_BY)));

        // Check, that the count call also returns 1
        restBankAccountMockMvc.perform(get("/api/bank-accounts/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultBankAccountShouldNotBeFound(String filter) throws Exception {
        restBankAccountMockMvc.perform(get("/api/bank-accounts?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restBankAccountMockMvc.perform(get("/api/bank-accounts/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingBankAccount() throws Exception {
        // Get the bankAccount
        restBankAccountMockMvc.perform(get("/api/bank-accounts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBankAccount() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        int databaseSizeBeforeUpdate = bankAccountRepository.findAll().size();

        // Update the bankAccount
        BankAccount updatedBankAccount = bankAccountRepository.findById(bankAccount.getId()).get();
        // Disconnect from session so that the updates on updatedBankAccount are not directly saved in db
        em.detach(updatedBankAccount);
        updatedBankAccount
            .dateOfOpening(UPDATED_DATE_OF_OPENING)
            .openedBy(UPDATED_OPENED_BY)
            .customerId(UPDATED_CUSTOMER_ID)
            .accountNumber(UPDATED_ACCOUNT_NUMBER)
            .loginId(UPDATED_LOGIN_ID)
            .loginPassword(UPDATED_LOGIN_PASSWORD)
            .email(UPDATED_EMAIL)
            .phone(UPDATED_PHONE)
            .bankAccountType(UPDATED_BANK_ACCOUNT_TYPE)
            .chequeBookIssued(UPDATED_CHEQUE_BOOK_ISSUED)
            .accountBalance(UPDATED_ACCOUNT_BALANCE)
            .balanceCheckedOn(UPDATED_BALANCE_CHECKED_ON)
            .minimumBalance(UPDATED_MINIMUM_BALANCE)
            .passwordGetsExpired(UPDATED_PASSWORD_GETS_EXPIRED)
            .expirationPeriodDays(UPDATED_EXPIRATION_PERIOD_DAYS)
            .passwordUpdatedOn(UPDATED_PASSWORD_UPDATED_ON)
            .remarks(UPDATED_REMARKS)
            .accountStatus(UPDATED_ACCOUNT_STATUS)
            .multipleSessionAllowed(UPDATED_MULTIPLE_SESSION_ALLOWED)
            .projectName(UPDATED_PROJECT_NAME)
            .projectCode(UPDATED_PROJECT_CODE)
            .requestedOn(UPDATED_REQUESTED_ON)
            .requestedBy(UPDATED_REQUESTED_BY);
        BankAccountDTO bankAccountDTO = bankAccountMapper.toDto(updatedBankAccount);

        restBankAccountMockMvc.perform(put("/api/bank-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bankAccountDTO)))
            .andExpect(status().isOk());

        // Validate the BankAccount in the database
        List<BankAccount> bankAccountList = bankAccountRepository.findAll();
        assertThat(bankAccountList).hasSize(databaseSizeBeforeUpdate);
        BankAccount testBankAccount = bankAccountList.get(bankAccountList.size() - 1);
        assertThat(testBankAccount.getDateOfOpening()).isEqualTo(UPDATED_DATE_OF_OPENING);
        assertThat(testBankAccount.getOpenedBy()).isEqualTo(UPDATED_OPENED_BY);
        assertThat(testBankAccount.getCustomerId()).isEqualTo(UPDATED_CUSTOMER_ID);
        assertThat(testBankAccount.getAccountNumber()).isEqualTo(UPDATED_ACCOUNT_NUMBER);
        assertThat(testBankAccount.getLoginId()).isEqualTo(UPDATED_LOGIN_ID);
        assertThat(testBankAccount.getLoginPassword()).isEqualTo(UPDATED_LOGIN_PASSWORD);
        assertThat(testBankAccount.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testBankAccount.getPhone()).isEqualTo(UPDATED_PHONE);
        assertThat(testBankAccount.getBankAccountType()).isEqualTo(UPDATED_BANK_ACCOUNT_TYPE);
        assertThat(testBankAccount.isChequeBookIssued()).isEqualTo(UPDATED_CHEQUE_BOOK_ISSUED);
        assertThat(testBankAccount.getAccountBalance()).isEqualTo(UPDATED_ACCOUNT_BALANCE);
        assertThat(testBankAccount.getBalanceCheckedOn()).isEqualTo(UPDATED_BALANCE_CHECKED_ON);
        assertThat(testBankAccount.getMinimumBalance()).isEqualTo(UPDATED_MINIMUM_BALANCE);
        assertThat(testBankAccount.isPasswordGetsExpired()).isEqualTo(UPDATED_PASSWORD_GETS_EXPIRED);
        assertThat(testBankAccount.getExpirationPeriodDays()).isEqualTo(UPDATED_EXPIRATION_PERIOD_DAYS);
        assertThat(testBankAccount.getPasswordUpdatedOn()).isEqualTo(UPDATED_PASSWORD_UPDATED_ON);
        assertThat(testBankAccount.getRemarks()).isEqualTo(UPDATED_REMARKS);
        assertThat(testBankAccount.getAccountStatus()).isEqualTo(UPDATED_ACCOUNT_STATUS);
        assertThat(testBankAccount.isMultipleSessionAllowed()).isEqualTo(UPDATED_MULTIPLE_SESSION_ALLOWED);
        assertThat(testBankAccount.getProjectName()).isEqualTo(UPDATED_PROJECT_NAME);
        assertThat(testBankAccount.getProjectCode()).isEqualTo(UPDATED_PROJECT_CODE);
        assertThat(testBankAccount.getRequestedOn()).isEqualTo(UPDATED_REQUESTED_ON);
        assertThat(testBankAccount.getRequestedBy()).isEqualTo(UPDATED_REQUESTED_BY);
    }

    @Test
    @Transactional
    public void updateNonExistingBankAccount() throws Exception {
        int databaseSizeBeforeUpdate = bankAccountRepository.findAll().size();

        // Create the BankAccount
        BankAccountDTO bankAccountDTO = bankAccountMapper.toDto(bankAccount);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBankAccountMockMvc.perform(put("/api/bank-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bankAccountDTO)))
            .andExpect(status().isBadRequest());

        // Validate the BankAccount in the database
        List<BankAccount> bankAccountList = bankAccountRepository.findAll();
        assertThat(bankAccountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBankAccount() throws Exception {
        // Initialize the database
        bankAccountRepository.saveAndFlush(bankAccount);

        int databaseSizeBeforeDelete = bankAccountRepository.findAll().size();

        // Delete the bankAccount
        restBankAccountMockMvc.perform(delete("/api/bank-accounts/{id}", bankAccount.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<BankAccount> bankAccountList = bankAccountRepository.findAll();
        assertThat(bankAccountList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BankAccount.class);
        BankAccount bankAccount1 = new BankAccount();
        bankAccount1.setId(1L);
        BankAccount bankAccount2 = new BankAccount();
        bankAccount2.setId(bankAccount1.getId());
        assertThat(bankAccount1).isEqualTo(bankAccount2);
        bankAccount2.setId(2L);
        assertThat(bankAccount1).isNotEqualTo(bankAccount2);
        bankAccount1.setId(null);
        assertThat(bankAccount1).isNotEqualTo(bankAccount2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(BankAccountDTO.class);
        BankAccountDTO bankAccountDTO1 = new BankAccountDTO();
        bankAccountDTO1.setId(1L);
        BankAccountDTO bankAccountDTO2 = new BankAccountDTO();
        assertThat(bankAccountDTO1).isNotEqualTo(bankAccountDTO2);
        bankAccountDTO2.setId(bankAccountDTO1.getId());
        assertThat(bankAccountDTO1).isEqualTo(bankAccountDTO2);
        bankAccountDTO2.setId(2L);
        assertThat(bankAccountDTO1).isNotEqualTo(bankAccountDTO2);
        bankAccountDTO1.setId(null);
        assertThat(bankAccountDTO1).isNotEqualTo(bankAccountDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(bankAccountMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(bankAccountMapper.fromId(null)).isNull();
    }
}
