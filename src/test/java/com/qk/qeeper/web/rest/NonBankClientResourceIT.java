package com.qk.qeeper.web.rest;

import com.qk.qeeper.QeeperApp;
import com.qk.qeeper.domain.NonBankClient;
import com.qk.qeeper.repository.NonBankClientRepository;
import com.qk.qeeper.service.NonBankClientService;
import com.qk.qeeper.service.dto.NonBankClientDTO;
import com.qk.qeeper.service.mapper.NonBankClientMapper;
import com.qk.qeeper.web.rest.errors.ExceptionTranslator;
import com.qk.qeeper.service.dto.NonBankClientCriteria;
import com.qk.qeeper.service.NonBankClientQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.qk.qeeper.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link NonBankClientResource} REST controller.
 */
@SpringBootTest(classes = QeeperApp.class)
public class NonBankClientResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_URL = "http://www.before.com";
    private static final String UPDATED_URL = "http://www.after.com";

    @Autowired
    private NonBankClientRepository nonBankClientRepository;

    @Autowired
    private NonBankClientMapper nonBankClientMapper;

    @Autowired
    private NonBankClientService nonBankClientService;

    @Autowired
    private NonBankClientQueryService nonBankClientQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restNonBankClientMockMvc;

    private NonBankClient nonBankClient;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final NonBankClientResource nonBankClientResource = new NonBankClientResource(nonBankClientService, nonBankClientQueryService);
        this.restNonBankClientMockMvc = MockMvcBuilders.standaloneSetup(nonBankClientResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NonBankClient createEntity(EntityManager em) {
        NonBankClient nonBankClient = new NonBankClient()
            .name(DEFAULT_NAME)
            .url(DEFAULT_URL);
        return nonBankClient;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NonBankClient createUpdatedEntity(EntityManager em) {
        NonBankClient nonBankClient = new NonBankClient()
            .name(UPDATED_NAME)
            .url(UPDATED_URL);
        return nonBankClient;
    }

    @BeforeEach
    public void initTest() {
        nonBankClient = createEntity(em);
    }

    @Test
    @Transactional
    public void createNonBankClient() throws Exception {
        int databaseSizeBeforeCreate = nonBankClientRepository.findAll().size();

        // Create the NonBankClient
        NonBankClientDTO nonBankClientDTO = nonBankClientMapper.toDto(nonBankClient);
        restNonBankClientMockMvc.perform(post("/api/non-bank-clients")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(nonBankClientDTO)))
            .andExpect(status().isCreated());

        // Validate the NonBankClient in the database
        List<NonBankClient> nonBankClientList = nonBankClientRepository.findAll();
        assertThat(nonBankClientList).hasSize(databaseSizeBeforeCreate + 1);
        NonBankClient testNonBankClient = nonBankClientList.get(nonBankClientList.size() - 1);
        assertThat(testNonBankClient.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testNonBankClient.getUrl()).isEqualTo(DEFAULT_URL);
    }

    @Test
    @Transactional
    public void createNonBankClientWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = nonBankClientRepository.findAll().size();

        // Create the NonBankClient with an existing ID
        nonBankClient.setId(1L);
        NonBankClientDTO nonBankClientDTO = nonBankClientMapper.toDto(nonBankClient);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNonBankClientMockMvc.perform(post("/api/non-bank-clients")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(nonBankClientDTO)))
            .andExpect(status().isBadRequest());

        // Validate the NonBankClient in the database
        List<NonBankClient> nonBankClientList = nonBankClientRepository.findAll();
        assertThat(nonBankClientList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = nonBankClientRepository.findAll().size();
        // set the field null
        nonBankClient.setName(null);

        // Create the NonBankClient, which fails.
        NonBankClientDTO nonBankClientDTO = nonBankClientMapper.toDto(nonBankClient);

        restNonBankClientMockMvc.perform(post("/api/non-bank-clients")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(nonBankClientDTO)))
            .andExpect(status().isBadRequest());

        List<NonBankClient> nonBankClientList = nonBankClientRepository.findAll();
        assertThat(nonBankClientList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllNonBankClients() throws Exception {
        // Initialize the database
        nonBankClientRepository.saveAndFlush(nonBankClient);

        // Get all the nonBankClientList
        restNonBankClientMockMvc.perform(get("/api/non-bank-clients?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(nonBankClient.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL.toString())));
    }
    
    @Test
    @Transactional
    public void getNonBankClient() throws Exception {
        // Initialize the database
        nonBankClientRepository.saveAndFlush(nonBankClient);

        // Get the nonBankClient
        restNonBankClientMockMvc.perform(get("/api/non-bank-clients/{id}", nonBankClient.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(nonBankClient.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.url").value(DEFAULT_URL.toString()));
    }

    @Test
    @Transactional
    public void getAllNonBankClientsByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        nonBankClientRepository.saveAndFlush(nonBankClient);

        // Get all the nonBankClientList where name equals to DEFAULT_NAME
        defaultNonBankClientShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the nonBankClientList where name equals to UPDATED_NAME
        defaultNonBankClientShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllNonBankClientsByNameIsInShouldWork() throws Exception {
        // Initialize the database
        nonBankClientRepository.saveAndFlush(nonBankClient);

        // Get all the nonBankClientList where name in DEFAULT_NAME or UPDATED_NAME
        defaultNonBankClientShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the nonBankClientList where name equals to UPDATED_NAME
        defaultNonBankClientShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllNonBankClientsByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        nonBankClientRepository.saveAndFlush(nonBankClient);

        // Get all the nonBankClientList where name is not null
        defaultNonBankClientShouldBeFound("name.specified=true");

        // Get all the nonBankClientList where name is null
        defaultNonBankClientShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    public void getAllNonBankClientsByUrlIsEqualToSomething() throws Exception {
        // Initialize the database
        nonBankClientRepository.saveAndFlush(nonBankClient);

        // Get all the nonBankClientList where url equals to DEFAULT_URL
        defaultNonBankClientShouldBeFound("url.equals=" + DEFAULT_URL);

        // Get all the nonBankClientList where url equals to UPDATED_URL
        defaultNonBankClientShouldNotBeFound("url.equals=" + UPDATED_URL);
    }

    @Test
    @Transactional
    public void getAllNonBankClientsByUrlIsInShouldWork() throws Exception {
        // Initialize the database
        nonBankClientRepository.saveAndFlush(nonBankClient);

        // Get all the nonBankClientList where url in DEFAULT_URL or UPDATED_URL
        defaultNonBankClientShouldBeFound("url.in=" + DEFAULT_URL + "," + UPDATED_URL);

        // Get all the nonBankClientList where url equals to UPDATED_URL
        defaultNonBankClientShouldNotBeFound("url.in=" + UPDATED_URL);
    }

    @Test
    @Transactional
    public void getAllNonBankClientsByUrlIsNullOrNotNull() throws Exception {
        // Initialize the database
        nonBankClientRepository.saveAndFlush(nonBankClient);

        // Get all the nonBankClientList where url is not null
        defaultNonBankClientShouldBeFound("url.specified=true");

        // Get all the nonBankClientList where url is null
        defaultNonBankClientShouldNotBeFound("url.specified=false");
    }
    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultNonBankClientShouldBeFound(String filter) throws Exception {
        restNonBankClientMockMvc.perform(get("/api/non-bank-clients?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(nonBankClient.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL)));

        // Check, that the count call also returns 1
        restNonBankClientMockMvc.perform(get("/api/non-bank-clients/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultNonBankClientShouldNotBeFound(String filter) throws Exception {
        restNonBankClientMockMvc.perform(get("/api/non-bank-clients?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restNonBankClientMockMvc.perform(get("/api/non-bank-clients/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingNonBankClient() throws Exception {
        // Get the nonBankClient
        restNonBankClientMockMvc.perform(get("/api/non-bank-clients/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNonBankClient() throws Exception {
        // Initialize the database
        nonBankClientRepository.saveAndFlush(nonBankClient);

        int databaseSizeBeforeUpdate = nonBankClientRepository.findAll().size();

        // Update the nonBankClient
        NonBankClient updatedNonBankClient = nonBankClientRepository.findById(nonBankClient.getId()).get();
        // Disconnect from session so that the updates on updatedNonBankClient are not directly saved in db
        em.detach(updatedNonBankClient);
        updatedNonBankClient
            .name(UPDATED_NAME)
            .url(UPDATED_URL);
        NonBankClientDTO nonBankClientDTO = nonBankClientMapper.toDto(updatedNonBankClient);

        restNonBankClientMockMvc.perform(put("/api/non-bank-clients")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(nonBankClientDTO)))
            .andExpect(status().isOk());

        // Validate the NonBankClient in the database
        List<NonBankClient> nonBankClientList = nonBankClientRepository.findAll();
        assertThat(nonBankClientList).hasSize(databaseSizeBeforeUpdate);
        NonBankClient testNonBankClient = nonBankClientList.get(nonBankClientList.size() - 1);
        assertThat(testNonBankClient.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testNonBankClient.getUrl()).isEqualTo(UPDATED_URL);
    }

    @Test
    @Transactional
    public void updateNonExistingNonBankClient() throws Exception {
        int databaseSizeBeforeUpdate = nonBankClientRepository.findAll().size();

        // Create the NonBankClient
        NonBankClientDTO nonBankClientDTO = nonBankClientMapper.toDto(nonBankClient);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restNonBankClientMockMvc.perform(put("/api/non-bank-clients")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(nonBankClientDTO)))
            .andExpect(status().isBadRequest());

        // Validate the NonBankClient in the database
        List<NonBankClient> nonBankClientList = nonBankClientRepository.findAll();
        assertThat(nonBankClientList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteNonBankClient() throws Exception {
        // Initialize the database
        nonBankClientRepository.saveAndFlush(nonBankClient);

        int databaseSizeBeforeDelete = nonBankClientRepository.findAll().size();

        // Delete the nonBankClient
        restNonBankClientMockMvc.perform(delete("/api/non-bank-clients/{id}", nonBankClient.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<NonBankClient> nonBankClientList = nonBankClientRepository.findAll();
        assertThat(nonBankClientList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(NonBankClient.class);
        NonBankClient nonBankClient1 = new NonBankClient();
        nonBankClient1.setId(1L);
        NonBankClient nonBankClient2 = new NonBankClient();
        nonBankClient2.setId(nonBankClient1.getId());
        assertThat(nonBankClient1).isEqualTo(nonBankClient2);
        nonBankClient2.setId(2L);
        assertThat(nonBankClient1).isNotEqualTo(nonBankClient2);
        nonBankClient1.setId(null);
        assertThat(nonBankClient1).isNotEqualTo(nonBankClient2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(NonBankClientDTO.class);
        NonBankClientDTO nonBankClientDTO1 = new NonBankClientDTO();
        nonBankClientDTO1.setId(1L);
        NonBankClientDTO nonBankClientDTO2 = new NonBankClientDTO();
        assertThat(nonBankClientDTO1).isNotEqualTo(nonBankClientDTO2);
        nonBankClientDTO2.setId(nonBankClientDTO1.getId());
        assertThat(nonBankClientDTO1).isEqualTo(nonBankClientDTO2);
        nonBankClientDTO2.setId(2L);
        assertThat(nonBankClientDTO1).isNotEqualTo(nonBankClientDTO2);
        nonBankClientDTO1.setId(null);
        assertThat(nonBankClientDTO1).isNotEqualTo(nonBankClientDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(nonBankClientMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(nonBankClientMapper.fromId(null)).isNull();
    }
}
