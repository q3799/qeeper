package com.qk.qeeper.web.rest;

import com.qk.qeeper.QeeperApp;
import com.qk.qeeper.domain.MobileNumber;
import com.qk.qeeper.domain.Employee;
import com.qk.qeeper.repository.MobileNumberRepository;
import com.qk.qeeper.service.MobileNumberService;
import com.qk.qeeper.service.dto.MobileNumberDTO;
import com.qk.qeeper.service.mapper.MobileNumberMapper;
import com.qk.qeeper.web.rest.errors.ExceptionTranslator;
import com.qk.qeeper.service.dto.MobileNumberCriteria;
import com.qk.qeeper.service.MobileNumberQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.qk.qeeper.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.qk.qeeper.domain.enumeration.Operator;
import com.qk.qeeper.domain.enumeration.MobileConnectionPlan;
/**
 * Integration tests for the {@Link MobileNumberResource} REST controller.
 */
@SpringBootTest(classes = QeeperApp.class)
public class MobileNumberResourceIT {

    private static final String DEFAULT_NUMBER = "19676631";
    private static final String UPDATED_NUMBER = "+38104526899847";

    private static final String DEFAULT_ASSET_TAG = "AAAAAAAAAA";
    private static final String UPDATED_ASSET_TAG = "BBBBBBBBBB";

    private static final Operator DEFAULT_OPERATOR = Operator.AIRTEL;
    private static final Operator UPDATED_OPERATOR = Operator.VODAFONE;

    private static final MobileConnectionPlan DEFAULT_MOBILE_CONNECTION_PLAN = MobileConnectionPlan.PREPAID;
    private static final MobileConnectionPlan UPDATED_MOBILE_CONNECTION_PLAN = MobileConnectionPlan.POSTPAID;

    private static final String DEFAULT_REMARKS = "AAAAAAAAAA";
    private static final String UPDATED_REMARKS = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ACTIVE = false;
    private static final Boolean UPDATED_ACTIVE = true;

    @Autowired
    private MobileNumberRepository mobileNumberRepository;

    @Autowired
    private MobileNumberMapper mobileNumberMapper;

    @Autowired
    private MobileNumberService mobileNumberService;

    @Autowired
    private MobileNumberQueryService mobileNumberQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restMobileNumberMockMvc;

    private MobileNumber mobileNumber;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MobileNumberResource mobileNumberResource = new MobileNumberResource(mobileNumberService, mobileNumberQueryService);
        this.restMobileNumberMockMvc = MockMvcBuilders.standaloneSetup(mobileNumberResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MobileNumber createEntity(EntityManager em) {
        MobileNumber mobileNumber = new MobileNumber()
            .number(DEFAULT_NUMBER)
            .assetTag(DEFAULT_ASSET_TAG)
            .operator(DEFAULT_OPERATOR)
            .mobileConnectionPlan(DEFAULT_MOBILE_CONNECTION_PLAN)
            .remarks(DEFAULT_REMARKS)
            .active(DEFAULT_ACTIVE);
        // Add required entity
        Employee employee;
        if (TestUtil.findAll(em, Employee.class).isEmpty()) {
            employee = EmployeeResourceIT.createEntity(em);
            em.persist(employee);
            em.flush();
        } else {
            employee = TestUtil.findAll(em, Employee.class).get(0);
        }
        mobileNumber.setRegisteredTo(employee);
        return mobileNumber;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MobileNumber createUpdatedEntity(EntityManager em) {
        MobileNumber mobileNumber = new MobileNumber()
            .number(UPDATED_NUMBER)
            .assetTag(UPDATED_ASSET_TAG)
            .operator(UPDATED_OPERATOR)
            .mobileConnectionPlan(UPDATED_MOBILE_CONNECTION_PLAN)
            .remarks(UPDATED_REMARKS)
            .active(UPDATED_ACTIVE);
        // Add required entity
        Employee employee;
        if (TestUtil.findAll(em, Employee.class).isEmpty()) {
            employee = EmployeeResourceIT.createUpdatedEntity(em);
            em.persist(employee);
            em.flush();
        } else {
            employee = TestUtil.findAll(em, Employee.class).get(0);
        }
        mobileNumber.setRegisteredTo(employee);
        return mobileNumber;
    }

    @BeforeEach
    public void initTest() {
        mobileNumber = createEntity(em);
    }

    @Test
    @Transactional
    public void createMobileNumber() throws Exception {
        int databaseSizeBeforeCreate = mobileNumberRepository.findAll().size();

        // Create the MobileNumber
        MobileNumberDTO mobileNumberDTO = mobileNumberMapper.toDto(mobileNumber);
        restMobileNumberMockMvc.perform(post("/api/mobile-numbers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(mobileNumberDTO)))
            .andExpect(status().isCreated());

        // Validate the MobileNumber in the database
        List<MobileNumber> mobileNumberList = mobileNumberRepository.findAll();
        assertThat(mobileNumberList).hasSize(databaseSizeBeforeCreate + 1);
        MobileNumber testMobileNumber = mobileNumberList.get(mobileNumberList.size() - 1);
        assertThat(testMobileNumber.getNumber()).isEqualTo(DEFAULT_NUMBER);
        assertThat(testMobileNumber.getAssetTag()).isEqualTo(DEFAULT_ASSET_TAG);
        assertThat(testMobileNumber.getOperator()).isEqualTo(DEFAULT_OPERATOR);
        assertThat(testMobileNumber.getMobileConnectionPlan()).isEqualTo(DEFAULT_MOBILE_CONNECTION_PLAN);
        assertThat(testMobileNumber.getRemarks()).isEqualTo(DEFAULT_REMARKS);
        assertThat(testMobileNumber.isActive()).isEqualTo(DEFAULT_ACTIVE);
    }

    @Test
    @Transactional
    public void createMobileNumberWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = mobileNumberRepository.findAll().size();

        // Create the MobileNumber with an existing ID
        mobileNumber.setId(1L);
        MobileNumberDTO mobileNumberDTO = mobileNumberMapper.toDto(mobileNumber);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMobileNumberMockMvc.perform(post("/api/mobile-numbers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(mobileNumberDTO)))
            .andExpect(status().isBadRequest());

        // Validate the MobileNumber in the database
        List<MobileNumber> mobileNumberList = mobileNumberRepository.findAll();
        assertThat(mobileNumberList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkOperatorIsRequired() throws Exception {
        int databaseSizeBeforeTest = mobileNumberRepository.findAll().size();
        // set the field null
        mobileNumber.setOperator(null);

        // Create the MobileNumber, which fails.
        MobileNumberDTO mobileNumberDTO = mobileNumberMapper.toDto(mobileNumber);

        restMobileNumberMockMvc.perform(post("/api/mobile-numbers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(mobileNumberDTO)))
            .andExpect(status().isBadRequest());

        List<MobileNumber> mobileNumberList = mobileNumberRepository.findAll();
        assertThat(mobileNumberList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkActiveIsRequired() throws Exception {
        int databaseSizeBeforeTest = mobileNumberRepository.findAll().size();
        // set the field null
        mobileNumber.setActive(null);

        // Create the MobileNumber, which fails.
        MobileNumberDTO mobileNumberDTO = mobileNumberMapper.toDto(mobileNumber);

        restMobileNumberMockMvc.perform(post("/api/mobile-numbers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(mobileNumberDTO)))
            .andExpect(status().isBadRequest());

        List<MobileNumber> mobileNumberList = mobileNumberRepository.findAll();
        assertThat(mobileNumberList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllMobileNumbers() throws Exception {
        // Initialize the database
        mobileNumberRepository.saveAndFlush(mobileNumber);

        // Get all the mobileNumberList
        restMobileNumberMockMvc.perform(get("/api/mobile-numbers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(mobileNumber.getId().intValue())))
            .andExpect(jsonPath("$.[*].number").value(hasItem(DEFAULT_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].assetTag").value(hasItem(DEFAULT_ASSET_TAG.toString())))
            .andExpect(jsonPath("$.[*].operator").value(hasItem(DEFAULT_OPERATOR.toString())))
            .andExpect(jsonPath("$.[*].mobileConnectionPlan").value(hasItem(DEFAULT_MOBILE_CONNECTION_PLAN.toString())))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS.toString())))
            .andExpect(jsonPath("$.[*].active").value(hasItem(DEFAULT_ACTIVE.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getMobileNumber() throws Exception {
        // Initialize the database
        mobileNumberRepository.saveAndFlush(mobileNumber);

        // Get the mobileNumber
        restMobileNumberMockMvc.perform(get("/api/mobile-numbers/{id}", mobileNumber.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(mobileNumber.getId().intValue()))
            .andExpect(jsonPath("$.number").value(DEFAULT_NUMBER.toString()))
            .andExpect(jsonPath("$.assetTag").value(DEFAULT_ASSET_TAG.toString()))
            .andExpect(jsonPath("$.operator").value(DEFAULT_OPERATOR.toString()))
            .andExpect(jsonPath("$.mobileConnectionPlan").value(DEFAULT_MOBILE_CONNECTION_PLAN.toString()))
            .andExpect(jsonPath("$.remarks").value(DEFAULT_REMARKS.toString()))
            .andExpect(jsonPath("$.active").value(DEFAULT_ACTIVE.booleanValue()));
    }

    @Test
    @Transactional
    public void getAllMobileNumbersByNumberIsEqualToSomething() throws Exception {
        // Initialize the database
        mobileNumberRepository.saveAndFlush(mobileNumber);

        // Get all the mobileNumberList where number equals to DEFAULT_NUMBER
        defaultMobileNumberShouldBeFound("number.equals=" + DEFAULT_NUMBER);

        // Get all the mobileNumberList where number equals to UPDATED_NUMBER
        defaultMobileNumberShouldNotBeFound("number.equals=" + UPDATED_NUMBER);
    }

    @Test
    @Transactional
    public void getAllMobileNumbersByNumberIsInShouldWork() throws Exception {
        // Initialize the database
        mobileNumberRepository.saveAndFlush(mobileNumber);

        // Get all the mobileNumberList where number in DEFAULT_NUMBER or UPDATED_NUMBER
        defaultMobileNumberShouldBeFound("number.in=" + DEFAULT_NUMBER + "," + UPDATED_NUMBER);

        // Get all the mobileNumberList where number equals to UPDATED_NUMBER
        defaultMobileNumberShouldNotBeFound("number.in=" + UPDATED_NUMBER);
    }

    @Test
    @Transactional
    public void getAllMobileNumbersByNumberIsNullOrNotNull() throws Exception {
        // Initialize the database
        mobileNumberRepository.saveAndFlush(mobileNumber);

        // Get all the mobileNumberList where number is not null
        defaultMobileNumberShouldBeFound("number.specified=true");

        // Get all the mobileNumberList where number is null
        defaultMobileNumberShouldNotBeFound("number.specified=false");
    }

    @Test
    @Transactional
    public void getAllMobileNumbersByAssetTagIsEqualToSomething() throws Exception {
        // Initialize the database
        mobileNumberRepository.saveAndFlush(mobileNumber);

        // Get all the mobileNumberList where assetTag equals to DEFAULT_ASSET_TAG
        defaultMobileNumberShouldBeFound("assetTag.equals=" + DEFAULT_ASSET_TAG);

        // Get all the mobileNumberList where assetTag equals to UPDATED_ASSET_TAG
        defaultMobileNumberShouldNotBeFound("assetTag.equals=" + UPDATED_ASSET_TAG);
    }

    @Test
    @Transactional
    public void getAllMobileNumbersByAssetTagIsInShouldWork() throws Exception {
        // Initialize the database
        mobileNumberRepository.saveAndFlush(mobileNumber);

        // Get all the mobileNumberList where assetTag in DEFAULT_ASSET_TAG or UPDATED_ASSET_TAG
        defaultMobileNumberShouldBeFound("assetTag.in=" + DEFAULT_ASSET_TAG + "," + UPDATED_ASSET_TAG);

        // Get all the mobileNumberList where assetTag equals to UPDATED_ASSET_TAG
        defaultMobileNumberShouldNotBeFound("assetTag.in=" + UPDATED_ASSET_TAG);
    }

    @Test
    @Transactional
    public void getAllMobileNumbersByAssetTagIsNullOrNotNull() throws Exception {
        // Initialize the database
        mobileNumberRepository.saveAndFlush(mobileNumber);

        // Get all the mobileNumberList where assetTag is not null
        defaultMobileNumberShouldBeFound("assetTag.specified=true");

        // Get all the mobileNumberList where assetTag is null
        defaultMobileNumberShouldNotBeFound("assetTag.specified=false");
    }

    @Test
    @Transactional
    public void getAllMobileNumbersByOperatorIsEqualToSomething() throws Exception {
        // Initialize the database
        mobileNumberRepository.saveAndFlush(mobileNumber);

        // Get all the mobileNumberList where operator equals to DEFAULT_OPERATOR
        defaultMobileNumberShouldBeFound("operator.equals=" + DEFAULT_OPERATOR);

        // Get all the mobileNumberList where operator equals to UPDATED_OPERATOR
        defaultMobileNumberShouldNotBeFound("operator.equals=" + UPDATED_OPERATOR);
    }

    @Test
    @Transactional
    public void getAllMobileNumbersByOperatorIsInShouldWork() throws Exception {
        // Initialize the database
        mobileNumberRepository.saveAndFlush(mobileNumber);

        // Get all the mobileNumberList where operator in DEFAULT_OPERATOR or UPDATED_OPERATOR
        defaultMobileNumberShouldBeFound("operator.in=" + DEFAULT_OPERATOR + "," + UPDATED_OPERATOR);

        // Get all the mobileNumberList where operator equals to UPDATED_OPERATOR
        defaultMobileNumberShouldNotBeFound("operator.in=" + UPDATED_OPERATOR);
    }

    @Test
    @Transactional
    public void getAllMobileNumbersByOperatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        mobileNumberRepository.saveAndFlush(mobileNumber);

        // Get all the mobileNumberList where operator is not null
        defaultMobileNumberShouldBeFound("operator.specified=true");

        // Get all the mobileNumberList where operator is null
        defaultMobileNumberShouldNotBeFound("operator.specified=false");
    }

    @Test
    @Transactional
    public void getAllMobileNumbersByMobileConnectionPlanIsEqualToSomething() throws Exception {
        // Initialize the database
        mobileNumberRepository.saveAndFlush(mobileNumber);

        // Get all the mobileNumberList where mobileConnectionPlan equals to DEFAULT_MOBILE_CONNECTION_PLAN
        defaultMobileNumberShouldBeFound("mobileConnectionPlan.equals=" + DEFAULT_MOBILE_CONNECTION_PLAN);

        // Get all the mobileNumberList where mobileConnectionPlan equals to UPDATED_MOBILE_CONNECTION_PLAN
        defaultMobileNumberShouldNotBeFound("mobileConnectionPlan.equals=" + UPDATED_MOBILE_CONNECTION_PLAN);
    }

    @Test
    @Transactional
    public void getAllMobileNumbersByMobileConnectionPlanIsInShouldWork() throws Exception {
        // Initialize the database
        mobileNumberRepository.saveAndFlush(mobileNumber);

        // Get all the mobileNumberList where mobileConnectionPlan in DEFAULT_MOBILE_CONNECTION_PLAN or UPDATED_MOBILE_CONNECTION_PLAN
        defaultMobileNumberShouldBeFound("mobileConnectionPlan.in=" + DEFAULT_MOBILE_CONNECTION_PLAN + "," + UPDATED_MOBILE_CONNECTION_PLAN);

        // Get all the mobileNumberList where mobileConnectionPlan equals to UPDATED_MOBILE_CONNECTION_PLAN
        defaultMobileNumberShouldNotBeFound("mobileConnectionPlan.in=" + UPDATED_MOBILE_CONNECTION_PLAN);
    }

    @Test
    @Transactional
    public void getAllMobileNumbersByMobileConnectionPlanIsNullOrNotNull() throws Exception {
        // Initialize the database
        mobileNumberRepository.saveAndFlush(mobileNumber);

        // Get all the mobileNumberList where mobileConnectionPlan is not null
        defaultMobileNumberShouldBeFound("mobileConnectionPlan.specified=true");

        // Get all the mobileNumberList where mobileConnectionPlan is null
        defaultMobileNumberShouldNotBeFound("mobileConnectionPlan.specified=false");
    }

    @Test
    @Transactional
    public void getAllMobileNumbersByRemarksIsEqualToSomething() throws Exception {
        // Initialize the database
        mobileNumberRepository.saveAndFlush(mobileNumber);

        // Get all the mobileNumberList where remarks equals to DEFAULT_REMARKS
        defaultMobileNumberShouldBeFound("remarks.equals=" + DEFAULT_REMARKS);

        // Get all the mobileNumberList where remarks equals to UPDATED_REMARKS
        defaultMobileNumberShouldNotBeFound("remarks.equals=" + UPDATED_REMARKS);
    }

    @Test
    @Transactional
    public void getAllMobileNumbersByRemarksIsInShouldWork() throws Exception {
        // Initialize the database
        mobileNumberRepository.saveAndFlush(mobileNumber);

        // Get all the mobileNumberList where remarks in DEFAULT_REMARKS or UPDATED_REMARKS
        defaultMobileNumberShouldBeFound("remarks.in=" + DEFAULT_REMARKS + "," + UPDATED_REMARKS);

        // Get all the mobileNumberList where remarks equals to UPDATED_REMARKS
        defaultMobileNumberShouldNotBeFound("remarks.in=" + UPDATED_REMARKS);
    }

    @Test
    @Transactional
    public void getAllMobileNumbersByRemarksIsNullOrNotNull() throws Exception {
        // Initialize the database
        mobileNumberRepository.saveAndFlush(mobileNumber);

        // Get all the mobileNumberList where remarks is not null
        defaultMobileNumberShouldBeFound("remarks.specified=true");

        // Get all the mobileNumberList where remarks is null
        defaultMobileNumberShouldNotBeFound("remarks.specified=false");
    }

    @Test
    @Transactional
    public void getAllMobileNumbersByActiveIsEqualToSomething() throws Exception {
        // Initialize the database
        mobileNumberRepository.saveAndFlush(mobileNumber);

        // Get all the mobileNumberList where active equals to DEFAULT_ACTIVE
        defaultMobileNumberShouldBeFound("active.equals=" + DEFAULT_ACTIVE);

        // Get all the mobileNumberList where active equals to UPDATED_ACTIVE
        defaultMobileNumberShouldNotBeFound("active.equals=" + UPDATED_ACTIVE);
    }

    @Test
    @Transactional
    public void getAllMobileNumbersByActiveIsInShouldWork() throws Exception {
        // Initialize the database
        mobileNumberRepository.saveAndFlush(mobileNumber);

        // Get all the mobileNumberList where active in DEFAULT_ACTIVE or UPDATED_ACTIVE
        defaultMobileNumberShouldBeFound("active.in=" + DEFAULT_ACTIVE + "," + UPDATED_ACTIVE);

        // Get all the mobileNumberList where active equals to UPDATED_ACTIVE
        defaultMobileNumberShouldNotBeFound("active.in=" + UPDATED_ACTIVE);
    }

    @Test
    @Transactional
    public void getAllMobileNumbersByActiveIsNullOrNotNull() throws Exception {
        // Initialize the database
        mobileNumberRepository.saveAndFlush(mobileNumber);

        // Get all the mobileNumberList where active is not null
        defaultMobileNumberShouldBeFound("active.specified=true");

        // Get all the mobileNumberList where active is null
        defaultMobileNumberShouldNotBeFound("active.specified=false");
    }

    @Test
    @Transactional
    public void getAllMobileNumbersByRegisteredToIsEqualToSomething() throws Exception {
        // Get already existing entity
        Employee registeredTo = mobileNumber.getRegisteredTo();
        mobileNumberRepository.saveAndFlush(mobileNumber);
        Long registeredToId = registeredTo.getId();

        // Get all the mobileNumberList where registeredTo equals to registeredToId
        defaultMobileNumberShouldBeFound("registeredToId.equals=" + registeredToId);

        // Get all the mobileNumberList where registeredTo equals to registeredToId + 1
        defaultMobileNumberShouldNotBeFound("registeredToId.equals=" + (registeredToId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultMobileNumberShouldBeFound(String filter) throws Exception {
        restMobileNumberMockMvc.perform(get("/api/mobile-numbers?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(mobileNumber.getId().intValue())))
            .andExpect(jsonPath("$.[*].number").value(hasItem(DEFAULT_NUMBER)))
            .andExpect(jsonPath("$.[*].assetTag").value(hasItem(DEFAULT_ASSET_TAG)))
            .andExpect(jsonPath("$.[*].operator").value(hasItem(DEFAULT_OPERATOR.toString())))
            .andExpect(jsonPath("$.[*].mobileConnectionPlan").value(hasItem(DEFAULT_MOBILE_CONNECTION_PLAN.toString())))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS)))
            .andExpect(jsonPath("$.[*].active").value(hasItem(DEFAULT_ACTIVE.booleanValue())));

        // Check, that the count call also returns 1
        restMobileNumberMockMvc.perform(get("/api/mobile-numbers/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultMobileNumberShouldNotBeFound(String filter) throws Exception {
        restMobileNumberMockMvc.perform(get("/api/mobile-numbers?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restMobileNumberMockMvc.perform(get("/api/mobile-numbers/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingMobileNumber() throws Exception {
        // Get the mobileNumber
        restMobileNumberMockMvc.perform(get("/api/mobile-numbers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMobileNumber() throws Exception {
        // Initialize the database
        mobileNumberRepository.saveAndFlush(mobileNumber);

        int databaseSizeBeforeUpdate = mobileNumberRepository.findAll().size();

        // Update the mobileNumber
        MobileNumber updatedMobileNumber = mobileNumberRepository.findById(mobileNumber.getId()).get();
        // Disconnect from session so that the updates on updatedMobileNumber are not directly saved in db
        em.detach(updatedMobileNumber);
        updatedMobileNumber
            .number(UPDATED_NUMBER)
            .assetTag(UPDATED_ASSET_TAG)
            .operator(UPDATED_OPERATOR)
            .mobileConnectionPlan(UPDATED_MOBILE_CONNECTION_PLAN)
            .remarks(UPDATED_REMARKS)
            .active(UPDATED_ACTIVE);
        MobileNumberDTO mobileNumberDTO = mobileNumberMapper.toDto(updatedMobileNumber);

        restMobileNumberMockMvc.perform(put("/api/mobile-numbers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(mobileNumberDTO)))
            .andExpect(status().isOk());

        // Validate the MobileNumber in the database
        List<MobileNumber> mobileNumberList = mobileNumberRepository.findAll();
        assertThat(mobileNumberList).hasSize(databaseSizeBeforeUpdate);
        MobileNumber testMobileNumber = mobileNumberList.get(mobileNumberList.size() - 1);
        assertThat(testMobileNumber.getNumber()).isEqualTo(UPDATED_NUMBER);
        assertThat(testMobileNumber.getAssetTag()).isEqualTo(UPDATED_ASSET_TAG);
        assertThat(testMobileNumber.getOperator()).isEqualTo(UPDATED_OPERATOR);
        assertThat(testMobileNumber.getMobileConnectionPlan()).isEqualTo(UPDATED_MOBILE_CONNECTION_PLAN);
        assertThat(testMobileNumber.getRemarks()).isEqualTo(UPDATED_REMARKS);
        assertThat(testMobileNumber.isActive()).isEqualTo(UPDATED_ACTIVE);
    }

    @Test
    @Transactional
    public void updateNonExistingMobileNumber() throws Exception {
        int databaseSizeBeforeUpdate = mobileNumberRepository.findAll().size();

        // Create the MobileNumber
        MobileNumberDTO mobileNumberDTO = mobileNumberMapper.toDto(mobileNumber);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMobileNumberMockMvc.perform(put("/api/mobile-numbers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(mobileNumberDTO)))
            .andExpect(status().isBadRequest());

        // Validate the MobileNumber in the database
        List<MobileNumber> mobileNumberList = mobileNumberRepository.findAll();
        assertThat(mobileNumberList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteMobileNumber() throws Exception {
        // Initialize the database
        mobileNumberRepository.saveAndFlush(mobileNumber);

        int databaseSizeBeforeDelete = mobileNumberRepository.findAll().size();

        // Delete the mobileNumber
        restMobileNumberMockMvc.perform(delete("/api/mobile-numbers/{id}", mobileNumber.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<MobileNumber> mobileNumberList = mobileNumberRepository.findAll();
        assertThat(mobileNumberList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MobileNumber.class);
        MobileNumber mobileNumber1 = new MobileNumber();
        mobileNumber1.setId(1L);
        MobileNumber mobileNumber2 = new MobileNumber();
        mobileNumber2.setId(mobileNumber1.getId());
        assertThat(mobileNumber1).isEqualTo(mobileNumber2);
        mobileNumber2.setId(2L);
        assertThat(mobileNumber1).isNotEqualTo(mobileNumber2);
        mobileNumber1.setId(null);
        assertThat(mobileNumber1).isNotEqualTo(mobileNumber2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(MobileNumberDTO.class);
        MobileNumberDTO mobileNumberDTO1 = new MobileNumberDTO();
        mobileNumberDTO1.setId(1L);
        MobileNumberDTO mobileNumberDTO2 = new MobileNumberDTO();
        assertThat(mobileNumberDTO1).isNotEqualTo(mobileNumberDTO2);
        mobileNumberDTO2.setId(mobileNumberDTO1.getId());
        assertThat(mobileNumberDTO1).isEqualTo(mobileNumberDTO2);
        mobileNumberDTO2.setId(2L);
        assertThat(mobileNumberDTO1).isNotEqualTo(mobileNumberDTO2);
        mobileNumberDTO1.setId(null);
        assertThat(mobileNumberDTO1).isNotEqualTo(mobileNumberDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(mobileNumberMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(mobileNumberMapper.fromId(null)).isNull();
    }
}
