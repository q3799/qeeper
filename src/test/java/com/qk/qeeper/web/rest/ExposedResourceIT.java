package com.qk.qeeper.web.rest;

import com.qk.qeeper.QeeperApp;
import com.qk.qeeper.domain.enumeration.ResourceType;
import com.qk.qeeper.service.BankAccountService;
import com.qk.qeeper.service.CardService;
import com.qk.qeeper.service.NonBankAccountService;
import com.qk.qeeper.service.dto.CredentialRequestDTO;
import com.qk.qeeper.service.dto.CredentialResponseDTO;
import io.github.jhipster.config.JHipsterProperties;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.head;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Test class for the ExposedResource REST controller.
 *
 * @see ExposedResource
 */
@SpringBootTest(classes = QeeperApp.class)
public class ExposedResourceIT {

    private static final String GROUP_CODE = "AAAAAAAAAA";

    private static final ResourceType RESOURCE_TYPE_BANK_ACCOUNT = ResourceType.BANK_ACCOUNT;
    private static final ResourceType RESOURCE_TYPE_CARD = ResourceType.CARD;
    private static final ResourceType RESOURCE_TYPE_NON_BANK_ACCOUNT = ResourceType.NON_BANK_ACCOUNT;

    private static final String ACCESS_KEY = "645b51183e1f56b323f3417f66a6b9a6ee380b4e09f84ef71afaf80e3d846648df4967acd59b95eca7afb63f4ee056b9140202de543679ec1b72b40f609acd1c";

    private MockMvc restExposedResourceMockMvc;

    @Autowired
    private JHipsterProperties jHipsterProperties;

    @Autowired
    private BankAccountService bankAccountService;

    @Autowired
    private CardService cardService;

    @Autowired
    private NonBankAccountService nonBankAccountService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        ExposedResource exposedResource = new ExposedResource(jHipsterProperties, bankAccountService, cardService, nonBankAccountService);
        restExposedResourceMockMvc = MockMvcBuilders
            .standaloneSetup(exposedResource)
            .build();
    }

    /**
     * Test getCredentials
     */
    @Test
    public void testGetCredentials() throws Exception {
        CredentialRequestDTO credentialRequestDTO = new CredentialRequestDTO()
            .groupCode(GROUP_CODE)
            .resourceType(RESOURCE_TYPE_BANK_ACCOUNT);

        HttpHeaders headers = new HttpHeaders();
        headers.add("accessKey", ACCESS_KEY);

        restExposedResourceMockMvc.perform(post("/api/exposed/credentials")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .headers(headers)
            .content(TestUtil.convertObjectToJsonBytes(credentialRequestDTO)))
            .andExpect(status().isOk())
            .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8));
    }
}
