package com.qk.qeeper.web.rest;

import com.qk.qeeper.QeeperApp;
import com.qk.qeeper.domain.EscalationMatrix;
import com.qk.qeeper.domain.ContactPerson;
import com.qk.qeeper.repository.EscalationMatrixRepository;
import com.qk.qeeper.service.EscalationMatrixService;
import com.qk.qeeper.service.dto.EscalationMatrixDTO;
import com.qk.qeeper.service.mapper.EscalationMatrixMapper;
import com.qk.qeeper.web.rest.errors.ExceptionTranslator;
import com.qk.qeeper.service.dto.EscalationMatrixCriteria;
import com.qk.qeeper.service.EscalationMatrixQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.qk.qeeper.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.qk.qeeper.domain.enumeration.EscalationType;
/**
 * Integration tests for the {@Link EscalationMatrixResource} REST controller.
 */
@SpringBootTest(classes = QeeperApp.class)
public class EscalationMatrixResourceIT {

    private static final EscalationType DEFAULT_ESCALATION_TYPE = EscalationType.PASSWORD_EXPIRE;
    private static final EscalationType UPDATED_ESCALATION_TYPE = EscalationType.ACCOUNT_NOT_WORKING;

    private static final Integer DEFAULT_LEVEL = 1;
    private static final Integer UPDATED_LEVEL = 2;

    private static final Boolean DEFAULT_RECEIVE_SMS = false;
    private static final Boolean UPDATED_RECEIVE_SMS = true;

    private static final Boolean DEFAULT_RECEIVE_EMAIL = false;
    private static final Boolean UPDATED_RECEIVE_EMAIL = true;

    @Autowired
    private EscalationMatrixRepository escalationMatrixRepository;

    @Autowired
    private EscalationMatrixMapper escalationMatrixMapper;

    @Autowired
    private EscalationMatrixService escalationMatrixService;

    @Autowired
    private EscalationMatrixQueryService escalationMatrixQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restEscalationMatrixMockMvc;

    private EscalationMatrix escalationMatrix;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final EscalationMatrixResource escalationMatrixResource = new EscalationMatrixResource(escalationMatrixService, escalationMatrixQueryService);
        this.restEscalationMatrixMockMvc = MockMvcBuilders.standaloneSetup(escalationMatrixResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EscalationMatrix createEntity(EntityManager em) {
        EscalationMatrix escalationMatrix = new EscalationMatrix()
            .escalationType(DEFAULT_ESCALATION_TYPE)
            .level(DEFAULT_LEVEL)
            .receiveSms(DEFAULT_RECEIVE_SMS)
            .receiveEmail(DEFAULT_RECEIVE_EMAIL);
        // Add required entity
        ContactPerson contactPerson;
        if (TestUtil.findAll(em, ContactPerson.class).isEmpty()) {
            contactPerson = ContactPersonResourceIT.createEntity(em);
            em.persist(contactPerson);
            em.flush();
        } else {
            contactPerson = TestUtil.findAll(em, ContactPerson.class).get(0);
        }
        escalationMatrix.setContactPerson(contactPerson);
        return escalationMatrix;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EscalationMatrix createUpdatedEntity(EntityManager em) {
        EscalationMatrix escalationMatrix = new EscalationMatrix()
            .escalationType(UPDATED_ESCALATION_TYPE)
            .level(UPDATED_LEVEL)
            .receiveSms(UPDATED_RECEIVE_SMS)
            .receiveEmail(UPDATED_RECEIVE_EMAIL);
        // Add required entity
        ContactPerson contactPerson;
        if (TestUtil.findAll(em, ContactPerson.class).isEmpty()) {
            contactPerson = ContactPersonResourceIT.createUpdatedEntity(em);
            em.persist(contactPerson);
            em.flush();
        } else {
            contactPerson = TestUtil.findAll(em, ContactPerson.class).get(0);
        }
        escalationMatrix.setContactPerson(contactPerson);
        return escalationMatrix;
    }

    @BeforeEach
    public void initTest() {
        escalationMatrix = createEntity(em);
    }

    @Test
    @Transactional
    public void createEscalationMatrix() throws Exception {
        int databaseSizeBeforeCreate = escalationMatrixRepository.findAll().size();

        // Create the EscalationMatrix
        EscalationMatrixDTO escalationMatrixDTO = escalationMatrixMapper.toDto(escalationMatrix);
        restEscalationMatrixMockMvc.perform(post("/api/escalation-matrices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(escalationMatrixDTO)))
            .andExpect(status().isCreated());

        // Validate the EscalationMatrix in the database
        List<EscalationMatrix> escalationMatrixList = escalationMatrixRepository.findAll();
        assertThat(escalationMatrixList).hasSize(databaseSizeBeforeCreate + 1);
        EscalationMatrix testEscalationMatrix = escalationMatrixList.get(escalationMatrixList.size() - 1);
        assertThat(testEscalationMatrix.getEscalationType()).isEqualTo(DEFAULT_ESCALATION_TYPE);
        assertThat(testEscalationMatrix.getLevel()).isEqualTo(DEFAULT_LEVEL);
        assertThat(testEscalationMatrix.isReceiveSms()).isEqualTo(DEFAULT_RECEIVE_SMS);
        assertThat(testEscalationMatrix.isReceiveEmail()).isEqualTo(DEFAULT_RECEIVE_EMAIL);
    }

    @Test
    @Transactional
    public void createEscalationMatrixWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = escalationMatrixRepository.findAll().size();

        // Create the EscalationMatrix with an existing ID
        escalationMatrix.setId(1L);
        EscalationMatrixDTO escalationMatrixDTO = escalationMatrixMapper.toDto(escalationMatrix);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEscalationMatrixMockMvc.perform(post("/api/escalation-matrices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(escalationMatrixDTO)))
            .andExpect(status().isBadRequest());

        // Validate the EscalationMatrix in the database
        List<EscalationMatrix> escalationMatrixList = escalationMatrixRepository.findAll();
        assertThat(escalationMatrixList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkEscalationTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = escalationMatrixRepository.findAll().size();
        // set the field null
        escalationMatrix.setEscalationType(null);

        // Create the EscalationMatrix, which fails.
        EscalationMatrixDTO escalationMatrixDTO = escalationMatrixMapper.toDto(escalationMatrix);

        restEscalationMatrixMockMvc.perform(post("/api/escalation-matrices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(escalationMatrixDTO)))
            .andExpect(status().isBadRequest());

        List<EscalationMatrix> escalationMatrixList = escalationMatrixRepository.findAll();
        assertThat(escalationMatrixList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLevelIsRequired() throws Exception {
        int databaseSizeBeforeTest = escalationMatrixRepository.findAll().size();
        // set the field null
        escalationMatrix.setLevel(null);

        // Create the EscalationMatrix, which fails.
        EscalationMatrixDTO escalationMatrixDTO = escalationMatrixMapper.toDto(escalationMatrix);

        restEscalationMatrixMockMvc.perform(post("/api/escalation-matrices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(escalationMatrixDTO)))
            .andExpect(status().isBadRequest());

        List<EscalationMatrix> escalationMatrixList = escalationMatrixRepository.findAll();
        assertThat(escalationMatrixList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkReceiveSmsIsRequired() throws Exception {
        int databaseSizeBeforeTest = escalationMatrixRepository.findAll().size();
        // set the field null
        escalationMatrix.setReceiveSms(null);

        // Create the EscalationMatrix, which fails.
        EscalationMatrixDTO escalationMatrixDTO = escalationMatrixMapper.toDto(escalationMatrix);

        restEscalationMatrixMockMvc.perform(post("/api/escalation-matrices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(escalationMatrixDTO)))
            .andExpect(status().isBadRequest());

        List<EscalationMatrix> escalationMatrixList = escalationMatrixRepository.findAll();
        assertThat(escalationMatrixList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkReceiveEmailIsRequired() throws Exception {
        int databaseSizeBeforeTest = escalationMatrixRepository.findAll().size();
        // set the field null
        escalationMatrix.setReceiveEmail(null);

        // Create the EscalationMatrix, which fails.
        EscalationMatrixDTO escalationMatrixDTO = escalationMatrixMapper.toDto(escalationMatrix);

        restEscalationMatrixMockMvc.perform(post("/api/escalation-matrices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(escalationMatrixDTO)))
            .andExpect(status().isBadRequest());

        List<EscalationMatrix> escalationMatrixList = escalationMatrixRepository.findAll();
        assertThat(escalationMatrixList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllEscalationMatrices() throws Exception {
        // Initialize the database
        escalationMatrixRepository.saveAndFlush(escalationMatrix);

        // Get all the escalationMatrixList
        restEscalationMatrixMockMvc.perform(get("/api/escalation-matrices?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(escalationMatrix.getId().intValue())))
            .andExpect(jsonPath("$.[*].escalationType").value(hasItem(DEFAULT_ESCALATION_TYPE.toString())))
            .andExpect(jsonPath("$.[*].level").value(hasItem(DEFAULT_LEVEL)))
            .andExpect(jsonPath("$.[*].receiveSms").value(hasItem(DEFAULT_RECEIVE_SMS.booleanValue())))
            .andExpect(jsonPath("$.[*].receiveEmail").value(hasItem(DEFAULT_RECEIVE_EMAIL.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getEscalationMatrix() throws Exception {
        // Initialize the database
        escalationMatrixRepository.saveAndFlush(escalationMatrix);

        // Get the escalationMatrix
        restEscalationMatrixMockMvc.perform(get("/api/escalation-matrices/{id}", escalationMatrix.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(escalationMatrix.getId().intValue()))
            .andExpect(jsonPath("$.escalationType").value(DEFAULT_ESCALATION_TYPE.toString()))
            .andExpect(jsonPath("$.level").value(DEFAULT_LEVEL))
            .andExpect(jsonPath("$.receiveSms").value(DEFAULT_RECEIVE_SMS.booleanValue()))
            .andExpect(jsonPath("$.receiveEmail").value(DEFAULT_RECEIVE_EMAIL.booleanValue()));
    }

    @Test
    @Transactional
    public void getAllEscalationMatricesByEscalationTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        escalationMatrixRepository.saveAndFlush(escalationMatrix);

        // Get all the escalationMatrixList where escalationType equals to DEFAULT_ESCALATION_TYPE
        defaultEscalationMatrixShouldBeFound("escalationType.equals=" + DEFAULT_ESCALATION_TYPE);

        // Get all the escalationMatrixList where escalationType equals to UPDATED_ESCALATION_TYPE
        defaultEscalationMatrixShouldNotBeFound("escalationType.equals=" + UPDATED_ESCALATION_TYPE);
    }

    @Test
    @Transactional
    public void getAllEscalationMatricesByEscalationTypeIsInShouldWork() throws Exception {
        // Initialize the database
        escalationMatrixRepository.saveAndFlush(escalationMatrix);

        // Get all the escalationMatrixList where escalationType in DEFAULT_ESCALATION_TYPE or UPDATED_ESCALATION_TYPE
        defaultEscalationMatrixShouldBeFound("escalationType.in=" + DEFAULT_ESCALATION_TYPE + "," + UPDATED_ESCALATION_TYPE);

        // Get all the escalationMatrixList where escalationType equals to UPDATED_ESCALATION_TYPE
        defaultEscalationMatrixShouldNotBeFound("escalationType.in=" + UPDATED_ESCALATION_TYPE);
    }

    @Test
    @Transactional
    public void getAllEscalationMatricesByEscalationTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        escalationMatrixRepository.saveAndFlush(escalationMatrix);

        // Get all the escalationMatrixList where escalationType is not null
        defaultEscalationMatrixShouldBeFound("escalationType.specified=true");

        // Get all the escalationMatrixList where escalationType is null
        defaultEscalationMatrixShouldNotBeFound("escalationType.specified=false");
    }

    @Test
    @Transactional
    public void getAllEscalationMatricesByLevelIsEqualToSomething() throws Exception {
        // Initialize the database
        escalationMatrixRepository.saveAndFlush(escalationMatrix);

        // Get all the escalationMatrixList where level equals to DEFAULT_LEVEL
        defaultEscalationMatrixShouldBeFound("level.equals=" + DEFAULT_LEVEL);

        // Get all the escalationMatrixList where level equals to UPDATED_LEVEL
        defaultEscalationMatrixShouldNotBeFound("level.equals=" + UPDATED_LEVEL);
    }

    @Test
    @Transactional
    public void getAllEscalationMatricesByLevelIsInShouldWork() throws Exception {
        // Initialize the database
        escalationMatrixRepository.saveAndFlush(escalationMatrix);

        // Get all the escalationMatrixList where level in DEFAULT_LEVEL or UPDATED_LEVEL
        defaultEscalationMatrixShouldBeFound("level.in=" + DEFAULT_LEVEL + "," + UPDATED_LEVEL);

        // Get all the escalationMatrixList where level equals to UPDATED_LEVEL
        defaultEscalationMatrixShouldNotBeFound("level.in=" + UPDATED_LEVEL);
    }

    @Test
    @Transactional
    public void getAllEscalationMatricesByLevelIsNullOrNotNull() throws Exception {
        // Initialize the database
        escalationMatrixRepository.saveAndFlush(escalationMatrix);

        // Get all the escalationMatrixList where level is not null
        defaultEscalationMatrixShouldBeFound("level.specified=true");

        // Get all the escalationMatrixList where level is null
        defaultEscalationMatrixShouldNotBeFound("level.specified=false");
    }

    @Test
    @Transactional
    public void getAllEscalationMatricesByLevelIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        escalationMatrixRepository.saveAndFlush(escalationMatrix);

        // Get all the escalationMatrixList where level greater than or equals to DEFAULT_LEVEL
        defaultEscalationMatrixShouldBeFound("level.greaterOrEqualThan=" + DEFAULT_LEVEL);

        // Get all the escalationMatrixList where level greater than or equals to (DEFAULT_LEVEL + 1)
        defaultEscalationMatrixShouldNotBeFound("level.greaterOrEqualThan=" + (DEFAULT_LEVEL + 1));
    }

    @Test
    @Transactional
    public void getAllEscalationMatricesByLevelIsLessThanSomething() throws Exception {
        // Initialize the database
        escalationMatrixRepository.saveAndFlush(escalationMatrix);

        // Get all the escalationMatrixList where level less than or equals to DEFAULT_LEVEL
        defaultEscalationMatrixShouldNotBeFound("level.lessThan=" + DEFAULT_LEVEL);

        // Get all the escalationMatrixList where level less than or equals to (DEFAULT_LEVEL + 1)
        defaultEscalationMatrixShouldBeFound("level.lessThan=" + (DEFAULT_LEVEL + 1));
    }


    @Test
    @Transactional
    public void getAllEscalationMatricesByReceiveSmsIsEqualToSomething() throws Exception {
        // Initialize the database
        escalationMatrixRepository.saveAndFlush(escalationMatrix);

        // Get all the escalationMatrixList where receiveSms equals to DEFAULT_RECEIVE_SMS
        defaultEscalationMatrixShouldBeFound("receiveSms.equals=" + DEFAULT_RECEIVE_SMS);

        // Get all the escalationMatrixList where receiveSms equals to UPDATED_RECEIVE_SMS
        defaultEscalationMatrixShouldNotBeFound("receiveSms.equals=" + UPDATED_RECEIVE_SMS);
    }

    @Test
    @Transactional
    public void getAllEscalationMatricesByReceiveSmsIsInShouldWork() throws Exception {
        // Initialize the database
        escalationMatrixRepository.saveAndFlush(escalationMatrix);

        // Get all the escalationMatrixList where receiveSms in DEFAULT_RECEIVE_SMS or UPDATED_RECEIVE_SMS
        defaultEscalationMatrixShouldBeFound("receiveSms.in=" + DEFAULT_RECEIVE_SMS + "," + UPDATED_RECEIVE_SMS);

        // Get all the escalationMatrixList where receiveSms equals to UPDATED_RECEIVE_SMS
        defaultEscalationMatrixShouldNotBeFound("receiveSms.in=" + UPDATED_RECEIVE_SMS);
    }

    @Test
    @Transactional
    public void getAllEscalationMatricesByReceiveSmsIsNullOrNotNull() throws Exception {
        // Initialize the database
        escalationMatrixRepository.saveAndFlush(escalationMatrix);

        // Get all the escalationMatrixList where receiveSms is not null
        defaultEscalationMatrixShouldBeFound("receiveSms.specified=true");

        // Get all the escalationMatrixList where receiveSms is null
        defaultEscalationMatrixShouldNotBeFound("receiveSms.specified=false");
    }

    @Test
    @Transactional
    public void getAllEscalationMatricesByReceiveEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        escalationMatrixRepository.saveAndFlush(escalationMatrix);

        // Get all the escalationMatrixList where receiveEmail equals to DEFAULT_RECEIVE_EMAIL
        defaultEscalationMatrixShouldBeFound("receiveEmail.equals=" + DEFAULT_RECEIVE_EMAIL);

        // Get all the escalationMatrixList where receiveEmail equals to UPDATED_RECEIVE_EMAIL
        defaultEscalationMatrixShouldNotBeFound("receiveEmail.equals=" + UPDATED_RECEIVE_EMAIL);
    }

    @Test
    @Transactional
    public void getAllEscalationMatricesByReceiveEmailIsInShouldWork() throws Exception {
        // Initialize the database
        escalationMatrixRepository.saveAndFlush(escalationMatrix);

        // Get all the escalationMatrixList where receiveEmail in DEFAULT_RECEIVE_EMAIL or UPDATED_RECEIVE_EMAIL
        defaultEscalationMatrixShouldBeFound("receiveEmail.in=" + DEFAULT_RECEIVE_EMAIL + "," + UPDATED_RECEIVE_EMAIL);

        // Get all the escalationMatrixList where receiveEmail equals to UPDATED_RECEIVE_EMAIL
        defaultEscalationMatrixShouldNotBeFound("receiveEmail.in=" + UPDATED_RECEIVE_EMAIL);
    }

    @Test
    @Transactional
    public void getAllEscalationMatricesByReceiveEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        escalationMatrixRepository.saveAndFlush(escalationMatrix);

        // Get all the escalationMatrixList where receiveEmail is not null
        defaultEscalationMatrixShouldBeFound("receiveEmail.specified=true");

        // Get all the escalationMatrixList where receiveEmail is null
        defaultEscalationMatrixShouldNotBeFound("receiveEmail.specified=false");
    }

    @Test
    @Transactional
    public void getAllEscalationMatricesByContactPersonIsEqualToSomething() throws Exception {
        // Get already existing entity
        ContactPerson contactPerson = escalationMatrix.getContactPerson();
        escalationMatrixRepository.saveAndFlush(escalationMatrix);
        Long contactPersonId = contactPerson.getId();

        // Get all the escalationMatrixList where contactPerson equals to contactPersonId
        defaultEscalationMatrixShouldBeFound("contactPersonId.equals=" + contactPersonId);

        // Get all the escalationMatrixList where contactPerson equals to contactPersonId + 1
        defaultEscalationMatrixShouldNotBeFound("contactPersonId.equals=" + (contactPersonId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultEscalationMatrixShouldBeFound(String filter) throws Exception {
        restEscalationMatrixMockMvc.perform(get("/api/escalation-matrices?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(escalationMatrix.getId().intValue())))
            .andExpect(jsonPath("$.[*].escalationType").value(hasItem(DEFAULT_ESCALATION_TYPE.toString())))
            .andExpect(jsonPath("$.[*].level").value(hasItem(DEFAULT_LEVEL)))
            .andExpect(jsonPath("$.[*].receiveSms").value(hasItem(DEFAULT_RECEIVE_SMS.booleanValue())))
            .andExpect(jsonPath("$.[*].receiveEmail").value(hasItem(DEFAULT_RECEIVE_EMAIL.booleanValue())));

        // Check, that the count call also returns 1
        restEscalationMatrixMockMvc.perform(get("/api/escalation-matrices/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultEscalationMatrixShouldNotBeFound(String filter) throws Exception {
        restEscalationMatrixMockMvc.perform(get("/api/escalation-matrices?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restEscalationMatrixMockMvc.perform(get("/api/escalation-matrices/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingEscalationMatrix() throws Exception {
        // Get the escalationMatrix
        restEscalationMatrixMockMvc.perform(get("/api/escalation-matrices/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEscalationMatrix() throws Exception {
        // Initialize the database
        escalationMatrixRepository.saveAndFlush(escalationMatrix);

        int databaseSizeBeforeUpdate = escalationMatrixRepository.findAll().size();

        // Update the escalationMatrix
        EscalationMatrix updatedEscalationMatrix = escalationMatrixRepository.findById(escalationMatrix.getId()).get();
        // Disconnect from session so that the updates on updatedEscalationMatrix are not directly saved in db
        em.detach(updatedEscalationMatrix);
        updatedEscalationMatrix
            .escalationType(UPDATED_ESCALATION_TYPE)
            .level(UPDATED_LEVEL)
            .receiveSms(UPDATED_RECEIVE_SMS)
            .receiveEmail(UPDATED_RECEIVE_EMAIL);
        EscalationMatrixDTO escalationMatrixDTO = escalationMatrixMapper.toDto(updatedEscalationMatrix);

        restEscalationMatrixMockMvc.perform(put("/api/escalation-matrices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(escalationMatrixDTO)))
            .andExpect(status().isOk());

        // Validate the EscalationMatrix in the database
        List<EscalationMatrix> escalationMatrixList = escalationMatrixRepository.findAll();
        assertThat(escalationMatrixList).hasSize(databaseSizeBeforeUpdate);
        EscalationMatrix testEscalationMatrix = escalationMatrixList.get(escalationMatrixList.size() - 1);
        assertThat(testEscalationMatrix.getEscalationType()).isEqualTo(UPDATED_ESCALATION_TYPE);
        assertThat(testEscalationMatrix.getLevel()).isEqualTo(UPDATED_LEVEL);
        assertThat(testEscalationMatrix.isReceiveSms()).isEqualTo(UPDATED_RECEIVE_SMS);
        assertThat(testEscalationMatrix.isReceiveEmail()).isEqualTo(UPDATED_RECEIVE_EMAIL);
    }

    @Test
    @Transactional
    public void updateNonExistingEscalationMatrix() throws Exception {
        int databaseSizeBeforeUpdate = escalationMatrixRepository.findAll().size();

        // Create the EscalationMatrix
        EscalationMatrixDTO escalationMatrixDTO = escalationMatrixMapper.toDto(escalationMatrix);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEscalationMatrixMockMvc.perform(put("/api/escalation-matrices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(escalationMatrixDTO)))
            .andExpect(status().isBadRequest());

        // Validate the EscalationMatrix in the database
        List<EscalationMatrix> escalationMatrixList = escalationMatrixRepository.findAll();
        assertThat(escalationMatrixList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteEscalationMatrix() throws Exception {
        // Initialize the database
        escalationMatrixRepository.saveAndFlush(escalationMatrix);

        int databaseSizeBeforeDelete = escalationMatrixRepository.findAll().size();

        // Delete the escalationMatrix
        restEscalationMatrixMockMvc.perform(delete("/api/escalation-matrices/{id}", escalationMatrix.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<EscalationMatrix> escalationMatrixList = escalationMatrixRepository.findAll();
        assertThat(escalationMatrixList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(EscalationMatrix.class);
        EscalationMatrix escalationMatrix1 = new EscalationMatrix();
        escalationMatrix1.setId(1L);
        EscalationMatrix escalationMatrix2 = new EscalationMatrix();
        escalationMatrix2.setId(escalationMatrix1.getId());
        assertThat(escalationMatrix1).isEqualTo(escalationMatrix2);
        escalationMatrix2.setId(2L);
        assertThat(escalationMatrix1).isNotEqualTo(escalationMatrix2);
        escalationMatrix1.setId(null);
        assertThat(escalationMatrix1).isNotEqualTo(escalationMatrix2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(EscalationMatrixDTO.class);
        EscalationMatrixDTO escalationMatrixDTO1 = new EscalationMatrixDTO();
        escalationMatrixDTO1.setId(1L);
        EscalationMatrixDTO escalationMatrixDTO2 = new EscalationMatrixDTO();
        assertThat(escalationMatrixDTO1).isNotEqualTo(escalationMatrixDTO2);
        escalationMatrixDTO2.setId(escalationMatrixDTO1.getId());
        assertThat(escalationMatrixDTO1).isEqualTo(escalationMatrixDTO2);
        escalationMatrixDTO2.setId(2L);
        assertThat(escalationMatrixDTO1).isNotEqualTo(escalationMatrixDTO2);
        escalationMatrixDTO1.setId(null);
        assertThat(escalationMatrixDTO1).isNotEqualTo(escalationMatrixDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(escalationMatrixMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(escalationMatrixMapper.fromId(null)).isNull();
    }
}
