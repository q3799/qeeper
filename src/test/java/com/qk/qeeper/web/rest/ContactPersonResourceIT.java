package com.qk.qeeper.web.rest;

import com.qk.qeeper.QeeperApp;
import com.qk.qeeper.domain.ContactPerson;
import com.qk.qeeper.repository.ContactPersonRepository;
import com.qk.qeeper.service.ContactPersonService;
import com.qk.qeeper.service.dto.ContactPersonDTO;
import com.qk.qeeper.service.mapper.ContactPersonMapper;
import com.qk.qeeper.web.rest.errors.ExceptionTranslator;
import com.qk.qeeper.service.dto.ContactPersonCriteria;
import com.qk.qeeper.service.ContactPersonQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.qk.qeeper.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link ContactPersonResource} REST controller.
 */
@SpringBootTest(classes = QeeperApp.class)
public class ContactPersonResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_PRIMARY_EMAIL = ".@n.LlX";
    private static final String UPDATED_PRIMARY_EMAIL = "3r@b.IaNA";

    private static final String DEFAULT_SECONDARY_EMAIL = "x@[8.792.041.4]";
    private static final String UPDATED_SECONDARY_EMAIL = "X@T.xs.vtJH]";

    private static final String DEFAULT_PRIMARY_PHONE = "880727500";
    private static final String UPDATED_PRIMARY_PHONE = "760220864771";

    private static final String DEFAULT_SECONDARY_PHONE = "535251564528519";
    private static final String UPDATED_SECONDARY_PHONE = "+449023793182";

    private static final Boolean DEFAULT_ACTIVE = false;
    private static final Boolean UPDATED_ACTIVE = true;

    @Autowired
    private ContactPersonRepository contactPersonRepository;

    @Autowired
    private ContactPersonMapper contactPersonMapper;

    @Autowired
    private ContactPersonService contactPersonService;

    @Autowired
    private ContactPersonQueryService contactPersonQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restContactPersonMockMvc;

    private ContactPerson contactPerson;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ContactPersonResource contactPersonResource = new ContactPersonResource(contactPersonService, contactPersonQueryService);
        this.restContactPersonMockMvc = MockMvcBuilders.standaloneSetup(contactPersonResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ContactPerson createEntity(EntityManager em) {
        ContactPerson contactPerson = new ContactPerson()
            .name(DEFAULT_NAME)
            .primaryEmail(DEFAULT_PRIMARY_EMAIL)
            .secondaryEmail(DEFAULT_SECONDARY_EMAIL)
            .primaryPhone(DEFAULT_PRIMARY_PHONE)
            .secondaryPhone(DEFAULT_SECONDARY_PHONE)
            .active(DEFAULT_ACTIVE);
        return contactPerson;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ContactPerson createUpdatedEntity(EntityManager em) {
        ContactPerson contactPerson = new ContactPerson()
            .name(UPDATED_NAME)
            .primaryEmail(UPDATED_PRIMARY_EMAIL)
            .secondaryEmail(UPDATED_SECONDARY_EMAIL)
            .primaryPhone(UPDATED_PRIMARY_PHONE)
            .secondaryPhone(UPDATED_SECONDARY_PHONE)
            .active(UPDATED_ACTIVE);
        return contactPerson;
    }

    @BeforeEach
    public void initTest() {
        contactPerson = createEntity(em);
    }

    @Test
    @Transactional
    public void createContactPerson() throws Exception {
        int databaseSizeBeforeCreate = contactPersonRepository.findAll().size();

        // Create the ContactPerson
        ContactPersonDTO contactPersonDTO = contactPersonMapper.toDto(contactPerson);
        restContactPersonMockMvc.perform(post("/api/contact-people")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(contactPersonDTO)))
            .andExpect(status().isCreated());

        // Validate the ContactPerson in the database
        List<ContactPerson> contactPersonList = contactPersonRepository.findAll();
        assertThat(contactPersonList).hasSize(databaseSizeBeforeCreate + 1);
        ContactPerson testContactPerson = contactPersonList.get(contactPersonList.size() - 1);
        assertThat(testContactPerson.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testContactPerson.getPrimaryEmail()).isEqualTo(DEFAULT_PRIMARY_EMAIL);
        assertThat(testContactPerson.getSecondaryEmail()).isEqualTo(DEFAULT_SECONDARY_EMAIL);
        assertThat(testContactPerson.getPrimaryPhone()).isEqualTo(DEFAULT_PRIMARY_PHONE);
        assertThat(testContactPerson.getSecondaryPhone()).isEqualTo(DEFAULT_SECONDARY_PHONE);
        assertThat(testContactPerson.isActive()).isEqualTo(DEFAULT_ACTIVE);
    }

    @Test
    @Transactional
    public void createContactPersonWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = contactPersonRepository.findAll().size();

        // Create the ContactPerson with an existing ID
        contactPerson.setId(1L);
        ContactPersonDTO contactPersonDTO = contactPersonMapper.toDto(contactPerson);

        // An entity with an existing ID cannot be created, so this API call must fail
        restContactPersonMockMvc.perform(post("/api/contact-people")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(contactPersonDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ContactPerson in the database
        List<ContactPerson> contactPersonList = contactPersonRepository.findAll();
        assertThat(contactPersonList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = contactPersonRepository.findAll().size();
        // set the field null
        contactPerson.setName(null);

        // Create the ContactPerson, which fails.
        ContactPersonDTO contactPersonDTO = contactPersonMapper.toDto(contactPerson);

        restContactPersonMockMvc.perform(post("/api/contact-people")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(contactPersonDTO)))
            .andExpect(status().isBadRequest());

        List<ContactPerson> contactPersonList = contactPersonRepository.findAll();
        assertThat(contactPersonList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPrimaryEmailIsRequired() throws Exception {
        int databaseSizeBeforeTest = contactPersonRepository.findAll().size();
        // set the field null
        contactPerson.setPrimaryEmail(null);

        // Create the ContactPerson, which fails.
        ContactPersonDTO contactPersonDTO = contactPersonMapper.toDto(contactPerson);

        restContactPersonMockMvc.perform(post("/api/contact-people")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(contactPersonDTO)))
            .andExpect(status().isBadRequest());

        List<ContactPerson> contactPersonList = contactPersonRepository.findAll();
        assertThat(contactPersonList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPrimaryPhoneIsRequired() throws Exception {
        int databaseSizeBeforeTest = contactPersonRepository.findAll().size();
        // set the field null
        contactPerson.setPrimaryPhone(null);

        // Create the ContactPerson, which fails.
        ContactPersonDTO contactPersonDTO = contactPersonMapper.toDto(contactPerson);

        restContactPersonMockMvc.perform(post("/api/contact-people")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(contactPersonDTO)))
            .andExpect(status().isBadRequest());

        List<ContactPerson> contactPersonList = contactPersonRepository.findAll();
        assertThat(contactPersonList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkActiveIsRequired() throws Exception {
        int databaseSizeBeforeTest = contactPersonRepository.findAll().size();
        // set the field null
        contactPerson.setActive(null);

        // Create the ContactPerson, which fails.
        ContactPersonDTO contactPersonDTO = contactPersonMapper.toDto(contactPerson);

        restContactPersonMockMvc.perform(post("/api/contact-people")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(contactPersonDTO)))
            .andExpect(status().isBadRequest());

        List<ContactPerson> contactPersonList = contactPersonRepository.findAll();
        assertThat(contactPersonList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllContactPeople() throws Exception {
        // Initialize the database
        contactPersonRepository.saveAndFlush(contactPerson);

        // Get all the contactPersonList
        restContactPersonMockMvc.perform(get("/api/contact-people?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(contactPerson.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].primaryEmail").value(hasItem(DEFAULT_PRIMARY_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].secondaryEmail").value(hasItem(DEFAULT_SECONDARY_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].primaryPhone").value(hasItem(DEFAULT_PRIMARY_PHONE.toString())))
            .andExpect(jsonPath("$.[*].secondaryPhone").value(hasItem(DEFAULT_SECONDARY_PHONE.toString())))
            .andExpect(jsonPath("$.[*].active").value(hasItem(DEFAULT_ACTIVE.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getContactPerson() throws Exception {
        // Initialize the database
        contactPersonRepository.saveAndFlush(contactPerson);

        // Get the contactPerson
        restContactPersonMockMvc.perform(get("/api/contact-people/{id}", contactPerson.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(contactPerson.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.primaryEmail").value(DEFAULT_PRIMARY_EMAIL.toString()))
            .andExpect(jsonPath("$.secondaryEmail").value(DEFAULT_SECONDARY_EMAIL.toString()))
            .andExpect(jsonPath("$.primaryPhone").value(DEFAULT_PRIMARY_PHONE.toString()))
            .andExpect(jsonPath("$.secondaryPhone").value(DEFAULT_SECONDARY_PHONE.toString()))
            .andExpect(jsonPath("$.active").value(DEFAULT_ACTIVE.booleanValue()));
    }

    @Test
    @Transactional
    public void getAllContactPeopleByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        contactPersonRepository.saveAndFlush(contactPerson);

        // Get all the contactPersonList where name equals to DEFAULT_NAME
        defaultContactPersonShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the contactPersonList where name equals to UPDATED_NAME
        defaultContactPersonShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllContactPeopleByNameIsInShouldWork() throws Exception {
        // Initialize the database
        contactPersonRepository.saveAndFlush(contactPerson);

        // Get all the contactPersonList where name in DEFAULT_NAME or UPDATED_NAME
        defaultContactPersonShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the contactPersonList where name equals to UPDATED_NAME
        defaultContactPersonShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllContactPeopleByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactPersonRepository.saveAndFlush(contactPerson);

        // Get all the contactPersonList where name is not null
        defaultContactPersonShouldBeFound("name.specified=true");

        // Get all the contactPersonList where name is null
        defaultContactPersonShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    public void getAllContactPeopleByPrimaryEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        contactPersonRepository.saveAndFlush(contactPerson);

        // Get all the contactPersonList where primaryEmail equals to DEFAULT_PRIMARY_EMAIL
        defaultContactPersonShouldBeFound("primaryEmail.equals=" + DEFAULT_PRIMARY_EMAIL);

        // Get all the contactPersonList where primaryEmail equals to UPDATED_PRIMARY_EMAIL
        defaultContactPersonShouldNotBeFound("primaryEmail.equals=" + UPDATED_PRIMARY_EMAIL);
    }

    @Test
    @Transactional
    public void getAllContactPeopleByPrimaryEmailIsInShouldWork() throws Exception {
        // Initialize the database
        contactPersonRepository.saveAndFlush(contactPerson);

        // Get all the contactPersonList where primaryEmail in DEFAULT_PRIMARY_EMAIL or UPDATED_PRIMARY_EMAIL
        defaultContactPersonShouldBeFound("primaryEmail.in=" + DEFAULT_PRIMARY_EMAIL + "," + UPDATED_PRIMARY_EMAIL);

        // Get all the contactPersonList where primaryEmail equals to UPDATED_PRIMARY_EMAIL
        defaultContactPersonShouldNotBeFound("primaryEmail.in=" + UPDATED_PRIMARY_EMAIL);
    }

    @Test
    @Transactional
    public void getAllContactPeopleByPrimaryEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactPersonRepository.saveAndFlush(contactPerson);

        // Get all the contactPersonList where primaryEmail is not null
        defaultContactPersonShouldBeFound("primaryEmail.specified=true");

        // Get all the contactPersonList where primaryEmail is null
        defaultContactPersonShouldNotBeFound("primaryEmail.specified=false");
    }

    @Test
    @Transactional
    public void getAllContactPeopleBySecondaryEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        contactPersonRepository.saveAndFlush(contactPerson);

        // Get all the contactPersonList where secondaryEmail equals to DEFAULT_SECONDARY_EMAIL
        defaultContactPersonShouldBeFound("secondaryEmail.equals=" + DEFAULT_SECONDARY_EMAIL);

        // Get all the contactPersonList where secondaryEmail equals to UPDATED_SECONDARY_EMAIL
        defaultContactPersonShouldNotBeFound("secondaryEmail.equals=" + UPDATED_SECONDARY_EMAIL);
    }

    @Test
    @Transactional
    public void getAllContactPeopleBySecondaryEmailIsInShouldWork() throws Exception {
        // Initialize the database
        contactPersonRepository.saveAndFlush(contactPerson);

        // Get all the contactPersonList where secondaryEmail in DEFAULT_SECONDARY_EMAIL or UPDATED_SECONDARY_EMAIL
        defaultContactPersonShouldBeFound("secondaryEmail.in=" + DEFAULT_SECONDARY_EMAIL + "," + UPDATED_SECONDARY_EMAIL);

        // Get all the contactPersonList where secondaryEmail equals to UPDATED_SECONDARY_EMAIL
        defaultContactPersonShouldNotBeFound("secondaryEmail.in=" + UPDATED_SECONDARY_EMAIL);
    }

    @Test
    @Transactional
    public void getAllContactPeopleBySecondaryEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactPersonRepository.saveAndFlush(contactPerson);

        // Get all the contactPersonList where secondaryEmail is not null
        defaultContactPersonShouldBeFound("secondaryEmail.specified=true");

        // Get all the contactPersonList where secondaryEmail is null
        defaultContactPersonShouldNotBeFound("secondaryEmail.specified=false");
    }

    @Test
    @Transactional
    public void getAllContactPeopleByPrimaryPhoneIsEqualToSomething() throws Exception {
        // Initialize the database
        contactPersonRepository.saveAndFlush(contactPerson);

        // Get all the contactPersonList where primaryPhone equals to DEFAULT_PRIMARY_PHONE
        defaultContactPersonShouldBeFound("primaryPhone.equals=" + DEFAULT_PRIMARY_PHONE);

        // Get all the contactPersonList where primaryPhone equals to UPDATED_PRIMARY_PHONE
        defaultContactPersonShouldNotBeFound("primaryPhone.equals=" + UPDATED_PRIMARY_PHONE);
    }

    @Test
    @Transactional
    public void getAllContactPeopleByPrimaryPhoneIsInShouldWork() throws Exception {
        // Initialize the database
        contactPersonRepository.saveAndFlush(contactPerson);

        // Get all the contactPersonList where primaryPhone in DEFAULT_PRIMARY_PHONE or UPDATED_PRIMARY_PHONE
        defaultContactPersonShouldBeFound("primaryPhone.in=" + DEFAULT_PRIMARY_PHONE + "," + UPDATED_PRIMARY_PHONE);

        // Get all the contactPersonList where primaryPhone equals to UPDATED_PRIMARY_PHONE
        defaultContactPersonShouldNotBeFound("primaryPhone.in=" + UPDATED_PRIMARY_PHONE);
    }

    @Test
    @Transactional
    public void getAllContactPeopleByPrimaryPhoneIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactPersonRepository.saveAndFlush(contactPerson);

        // Get all the contactPersonList where primaryPhone is not null
        defaultContactPersonShouldBeFound("primaryPhone.specified=true");

        // Get all the contactPersonList where primaryPhone is null
        defaultContactPersonShouldNotBeFound("primaryPhone.specified=false");
    }

    @Test
    @Transactional
    public void getAllContactPeopleBySecondaryPhoneIsEqualToSomething() throws Exception {
        // Initialize the database
        contactPersonRepository.saveAndFlush(contactPerson);

        // Get all the contactPersonList where secondaryPhone equals to DEFAULT_SECONDARY_PHONE
        defaultContactPersonShouldBeFound("secondaryPhone.equals=" + DEFAULT_SECONDARY_PHONE);

        // Get all the contactPersonList where secondaryPhone equals to UPDATED_SECONDARY_PHONE
        defaultContactPersonShouldNotBeFound("secondaryPhone.equals=" + UPDATED_SECONDARY_PHONE);
    }

    @Test
    @Transactional
    public void getAllContactPeopleBySecondaryPhoneIsInShouldWork() throws Exception {
        // Initialize the database
        contactPersonRepository.saveAndFlush(contactPerson);

        // Get all the contactPersonList where secondaryPhone in DEFAULT_SECONDARY_PHONE or UPDATED_SECONDARY_PHONE
        defaultContactPersonShouldBeFound("secondaryPhone.in=" + DEFAULT_SECONDARY_PHONE + "," + UPDATED_SECONDARY_PHONE);

        // Get all the contactPersonList where secondaryPhone equals to UPDATED_SECONDARY_PHONE
        defaultContactPersonShouldNotBeFound("secondaryPhone.in=" + UPDATED_SECONDARY_PHONE);
    }

    @Test
    @Transactional
    public void getAllContactPeopleBySecondaryPhoneIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactPersonRepository.saveAndFlush(contactPerson);

        // Get all the contactPersonList where secondaryPhone is not null
        defaultContactPersonShouldBeFound("secondaryPhone.specified=true");

        // Get all the contactPersonList where secondaryPhone is null
        defaultContactPersonShouldNotBeFound("secondaryPhone.specified=false");
    }

    @Test
    @Transactional
    public void getAllContactPeopleByActiveIsEqualToSomething() throws Exception {
        // Initialize the database
        contactPersonRepository.saveAndFlush(contactPerson);

        // Get all the contactPersonList where active equals to DEFAULT_ACTIVE
        defaultContactPersonShouldBeFound("active.equals=" + DEFAULT_ACTIVE);

        // Get all the contactPersonList where active equals to UPDATED_ACTIVE
        defaultContactPersonShouldNotBeFound("active.equals=" + UPDATED_ACTIVE);
    }

    @Test
    @Transactional
    public void getAllContactPeopleByActiveIsInShouldWork() throws Exception {
        // Initialize the database
        contactPersonRepository.saveAndFlush(contactPerson);

        // Get all the contactPersonList where active in DEFAULT_ACTIVE or UPDATED_ACTIVE
        defaultContactPersonShouldBeFound("active.in=" + DEFAULT_ACTIVE + "," + UPDATED_ACTIVE);

        // Get all the contactPersonList where active equals to UPDATED_ACTIVE
        defaultContactPersonShouldNotBeFound("active.in=" + UPDATED_ACTIVE);
    }

    @Test
    @Transactional
    public void getAllContactPeopleByActiveIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactPersonRepository.saveAndFlush(contactPerson);

        // Get all the contactPersonList where active is not null
        defaultContactPersonShouldBeFound("active.specified=true");

        // Get all the contactPersonList where active is null
        defaultContactPersonShouldNotBeFound("active.specified=false");
    }
    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultContactPersonShouldBeFound(String filter) throws Exception {
        restContactPersonMockMvc.perform(get("/api/contact-people?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(contactPerson.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].primaryEmail").value(hasItem(DEFAULT_PRIMARY_EMAIL)))
            .andExpect(jsonPath("$.[*].secondaryEmail").value(hasItem(DEFAULT_SECONDARY_EMAIL)))
            .andExpect(jsonPath("$.[*].primaryPhone").value(hasItem(DEFAULT_PRIMARY_PHONE)))
            .andExpect(jsonPath("$.[*].secondaryPhone").value(hasItem(DEFAULT_SECONDARY_PHONE)))
            .andExpect(jsonPath("$.[*].active").value(hasItem(DEFAULT_ACTIVE.booleanValue())));

        // Check, that the count call also returns 1
        restContactPersonMockMvc.perform(get("/api/contact-people/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultContactPersonShouldNotBeFound(String filter) throws Exception {
        restContactPersonMockMvc.perform(get("/api/contact-people?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restContactPersonMockMvc.perform(get("/api/contact-people/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingContactPerson() throws Exception {
        // Get the contactPerson
        restContactPersonMockMvc.perform(get("/api/contact-people/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateContactPerson() throws Exception {
        // Initialize the database
        contactPersonRepository.saveAndFlush(contactPerson);

        int databaseSizeBeforeUpdate = contactPersonRepository.findAll().size();

        // Update the contactPerson
        ContactPerson updatedContactPerson = contactPersonRepository.findById(contactPerson.getId()).get();
        // Disconnect from session so that the updates on updatedContactPerson are not directly saved in db
        em.detach(updatedContactPerson);
        updatedContactPerson
            .name(UPDATED_NAME)
            .primaryEmail(UPDATED_PRIMARY_EMAIL)
            .secondaryEmail(UPDATED_SECONDARY_EMAIL)
            .primaryPhone(UPDATED_PRIMARY_PHONE)
            .secondaryPhone(UPDATED_SECONDARY_PHONE)
            .active(UPDATED_ACTIVE);
        ContactPersonDTO contactPersonDTO = contactPersonMapper.toDto(updatedContactPerson);

        restContactPersonMockMvc.perform(put("/api/contact-people")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(contactPersonDTO)))
            .andExpect(status().isOk());

        // Validate the ContactPerson in the database
        List<ContactPerson> contactPersonList = contactPersonRepository.findAll();
        assertThat(contactPersonList).hasSize(databaseSizeBeforeUpdate);
        ContactPerson testContactPerson = contactPersonList.get(contactPersonList.size() - 1);
        assertThat(testContactPerson.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testContactPerson.getPrimaryEmail()).isEqualTo(UPDATED_PRIMARY_EMAIL);
        assertThat(testContactPerson.getSecondaryEmail()).isEqualTo(UPDATED_SECONDARY_EMAIL);
        assertThat(testContactPerson.getPrimaryPhone()).isEqualTo(UPDATED_PRIMARY_PHONE);
        assertThat(testContactPerson.getSecondaryPhone()).isEqualTo(UPDATED_SECONDARY_PHONE);
        assertThat(testContactPerson.isActive()).isEqualTo(UPDATED_ACTIVE);
    }

    @Test
    @Transactional
    public void updateNonExistingContactPerson() throws Exception {
        int databaseSizeBeforeUpdate = contactPersonRepository.findAll().size();

        // Create the ContactPerson
        ContactPersonDTO contactPersonDTO = contactPersonMapper.toDto(contactPerson);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restContactPersonMockMvc.perform(put("/api/contact-people")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(contactPersonDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ContactPerson in the database
        List<ContactPerson> contactPersonList = contactPersonRepository.findAll();
        assertThat(contactPersonList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteContactPerson() throws Exception {
        // Initialize the database
        contactPersonRepository.saveAndFlush(contactPerson);

        int databaseSizeBeforeDelete = contactPersonRepository.findAll().size();

        // Delete the contactPerson
        restContactPersonMockMvc.perform(delete("/api/contact-people/{id}", contactPerson.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ContactPerson> contactPersonList = contactPersonRepository.findAll();
        assertThat(contactPersonList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ContactPerson.class);
        ContactPerson contactPerson1 = new ContactPerson();
        contactPerson1.setId(1L);
        ContactPerson contactPerson2 = new ContactPerson();
        contactPerson2.setId(contactPerson1.getId());
        assertThat(contactPerson1).isEqualTo(contactPerson2);
        contactPerson2.setId(2L);
        assertThat(contactPerson1).isNotEqualTo(contactPerson2);
        contactPerson1.setId(null);
        assertThat(contactPerson1).isNotEqualTo(contactPerson2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ContactPersonDTO.class);
        ContactPersonDTO contactPersonDTO1 = new ContactPersonDTO();
        contactPersonDTO1.setId(1L);
        ContactPersonDTO contactPersonDTO2 = new ContactPersonDTO();
        assertThat(contactPersonDTO1).isNotEqualTo(contactPersonDTO2);
        contactPersonDTO2.setId(contactPersonDTO1.getId());
        assertThat(contactPersonDTO1).isEqualTo(contactPersonDTO2);
        contactPersonDTO2.setId(2L);
        assertThat(contactPersonDTO1).isNotEqualTo(contactPersonDTO2);
        contactPersonDTO1.setId(null);
        assertThat(contactPersonDTO1).isNotEqualTo(contactPersonDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(contactPersonMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(contactPersonMapper.fromId(null)).isNull();
    }
}
