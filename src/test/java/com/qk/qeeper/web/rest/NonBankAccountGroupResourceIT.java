package com.qk.qeeper.web.rest;

import com.qk.qeeper.QeeperApp;
import com.qk.qeeper.domain.NonBankAccountGroup;
import com.qk.qeeper.domain.NonBankAccount;
import com.qk.qeeper.repository.NonBankAccountGroupRepository;
import com.qk.qeeper.service.NonBankAccountGroupService;
import com.qk.qeeper.service.dto.NonBankAccountGroupDTO;
import com.qk.qeeper.service.mapper.NonBankAccountGroupMapper;
import com.qk.qeeper.web.rest.errors.ExceptionTranslator;
import com.qk.qeeper.service.dto.NonBankAccountGroupCriteria;
import com.qk.qeeper.service.NonBankAccountGroupQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.qk.qeeper.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link NonBankAccountGroupResource} REST controller.
 */
@SpringBootTest(classes = QeeperApp.class)
public class NonBankAccountGroupResourceIT {

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private NonBankAccountGroupRepository nonBankAccountGroupRepository;

    @Autowired
    private NonBankAccountGroupMapper nonBankAccountGroupMapper;

    @Autowired
    private NonBankAccountGroupService nonBankAccountGroupService;

    @Autowired
    private NonBankAccountGroupQueryService nonBankAccountGroupQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restNonBankAccountGroupMockMvc;

    private NonBankAccountGroup nonBankAccountGroup;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final NonBankAccountGroupResource nonBankAccountGroupResource = new NonBankAccountGroupResource(nonBankAccountGroupService, nonBankAccountGroupQueryService);
        this.restNonBankAccountGroupMockMvc = MockMvcBuilders.standaloneSetup(nonBankAccountGroupResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NonBankAccountGroup createEntity(EntityManager em) {
        NonBankAccountGroup nonBankAccountGroup = new NonBankAccountGroup()
            .code(DEFAULT_CODE)
            .name(DEFAULT_NAME);
        return nonBankAccountGroup;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NonBankAccountGroup createUpdatedEntity(EntityManager em) {
        NonBankAccountGroup nonBankAccountGroup = new NonBankAccountGroup()
            .code(UPDATED_CODE)
            .name(UPDATED_NAME);
        return nonBankAccountGroup;
    }

    @BeforeEach
    public void initTest() {
        nonBankAccountGroup = createEntity(em);
    }

    @Test
    @Transactional
    public void createNonBankAccountGroup() throws Exception {
        int databaseSizeBeforeCreate = nonBankAccountGroupRepository.findAll().size();

        // Create the NonBankAccountGroup
        NonBankAccountGroupDTO nonBankAccountGroupDTO = nonBankAccountGroupMapper.toDto(nonBankAccountGroup);
        restNonBankAccountGroupMockMvc.perform(post("/api/non-bank-account-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(nonBankAccountGroupDTO)))
            .andExpect(status().isCreated());

        // Validate the NonBankAccountGroup in the database
        List<NonBankAccountGroup> nonBankAccountGroupList = nonBankAccountGroupRepository.findAll();
        assertThat(nonBankAccountGroupList).hasSize(databaseSizeBeforeCreate + 1);
        NonBankAccountGroup testNonBankAccountGroup = nonBankAccountGroupList.get(nonBankAccountGroupList.size() - 1);
        assertThat(testNonBankAccountGroup.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testNonBankAccountGroup.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createNonBankAccountGroupWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = nonBankAccountGroupRepository.findAll().size();

        // Create the NonBankAccountGroup with an existing ID
        nonBankAccountGroup.setId(1L);
        NonBankAccountGroupDTO nonBankAccountGroupDTO = nonBankAccountGroupMapper.toDto(nonBankAccountGroup);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNonBankAccountGroupMockMvc.perform(post("/api/non-bank-account-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(nonBankAccountGroupDTO)))
            .andExpect(status().isBadRequest());

        // Validate the NonBankAccountGroup in the database
        List<NonBankAccountGroup> nonBankAccountGroupList = nonBankAccountGroupRepository.findAll();
        assertThat(nonBankAccountGroupList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = nonBankAccountGroupRepository.findAll().size();
        // set the field null
        nonBankAccountGroup.setCode(null);

        // Create the NonBankAccountGroup, which fails.
        NonBankAccountGroupDTO nonBankAccountGroupDTO = nonBankAccountGroupMapper.toDto(nonBankAccountGroup);

        restNonBankAccountGroupMockMvc.perform(post("/api/non-bank-account-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(nonBankAccountGroupDTO)))
            .andExpect(status().isBadRequest());

        List<NonBankAccountGroup> nonBankAccountGroupList = nonBankAccountGroupRepository.findAll();
        assertThat(nonBankAccountGroupList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = nonBankAccountGroupRepository.findAll().size();
        // set the field null
        nonBankAccountGroup.setName(null);

        // Create the NonBankAccountGroup, which fails.
        NonBankAccountGroupDTO nonBankAccountGroupDTO = nonBankAccountGroupMapper.toDto(nonBankAccountGroup);

        restNonBankAccountGroupMockMvc.perform(post("/api/non-bank-account-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(nonBankAccountGroupDTO)))
            .andExpect(status().isBadRequest());

        List<NonBankAccountGroup> nonBankAccountGroupList = nonBankAccountGroupRepository.findAll();
        assertThat(nonBankAccountGroupList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllNonBankAccountGroups() throws Exception {
        // Initialize the database
        nonBankAccountGroupRepository.saveAndFlush(nonBankAccountGroup);

        // Get all the nonBankAccountGroupList
        restNonBankAccountGroupMockMvc.perform(get("/api/non-bank-account-groups?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(nonBankAccountGroup.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }
    
    @Test
    @Transactional
    public void getNonBankAccountGroup() throws Exception {
        // Initialize the database
        nonBankAccountGroupRepository.saveAndFlush(nonBankAccountGroup);

        // Get the nonBankAccountGroup
        restNonBankAccountGroupMockMvc.perform(get("/api/non-bank-account-groups/{id}", nonBankAccountGroup.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(nonBankAccountGroup.getId().intValue()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getAllNonBankAccountGroupsByCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        nonBankAccountGroupRepository.saveAndFlush(nonBankAccountGroup);

        // Get all the nonBankAccountGroupList where code equals to DEFAULT_CODE
        defaultNonBankAccountGroupShouldBeFound("code.equals=" + DEFAULT_CODE);

        // Get all the nonBankAccountGroupList where code equals to UPDATED_CODE
        defaultNonBankAccountGroupShouldNotBeFound("code.equals=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    public void getAllNonBankAccountGroupsByCodeIsInShouldWork() throws Exception {
        // Initialize the database
        nonBankAccountGroupRepository.saveAndFlush(nonBankAccountGroup);

        // Get all the nonBankAccountGroupList where code in DEFAULT_CODE or UPDATED_CODE
        defaultNonBankAccountGroupShouldBeFound("code.in=" + DEFAULT_CODE + "," + UPDATED_CODE);

        // Get all the nonBankAccountGroupList where code equals to UPDATED_CODE
        defaultNonBankAccountGroupShouldNotBeFound("code.in=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    public void getAllNonBankAccountGroupsByCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        nonBankAccountGroupRepository.saveAndFlush(nonBankAccountGroup);

        // Get all the nonBankAccountGroupList where code is not null
        defaultNonBankAccountGroupShouldBeFound("code.specified=true");

        // Get all the nonBankAccountGroupList where code is null
        defaultNonBankAccountGroupShouldNotBeFound("code.specified=false");
    }

    @Test
    @Transactional
    public void getAllNonBankAccountGroupsByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        nonBankAccountGroupRepository.saveAndFlush(nonBankAccountGroup);

        // Get all the nonBankAccountGroupList where name equals to DEFAULT_NAME
        defaultNonBankAccountGroupShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the nonBankAccountGroupList where name equals to UPDATED_NAME
        defaultNonBankAccountGroupShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllNonBankAccountGroupsByNameIsInShouldWork() throws Exception {
        // Initialize the database
        nonBankAccountGroupRepository.saveAndFlush(nonBankAccountGroup);

        // Get all the nonBankAccountGroupList where name in DEFAULT_NAME or UPDATED_NAME
        defaultNonBankAccountGroupShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the nonBankAccountGroupList where name equals to UPDATED_NAME
        defaultNonBankAccountGroupShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllNonBankAccountGroupsByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        nonBankAccountGroupRepository.saveAndFlush(nonBankAccountGroup);

        // Get all the nonBankAccountGroupList where name is not null
        defaultNonBankAccountGroupShouldBeFound("name.specified=true");

        // Get all the nonBankAccountGroupList where name is null
        defaultNonBankAccountGroupShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    public void getAllNonBankAccountGroupsByNonBankAccountsIsEqualToSomething() throws Exception {
        // Initialize the database
        NonBankAccount nonBankAccounts = NonBankAccountResourceIT.createEntity(em);
        em.persist(nonBankAccounts);
        em.flush();
        nonBankAccountGroup.addNonBankAccounts(nonBankAccounts);
        nonBankAccountGroupRepository.saveAndFlush(nonBankAccountGroup);
        Long nonBankAccountsId = nonBankAccounts.getId();

        // Get all the nonBankAccountGroupList where nonBankAccounts equals to nonBankAccountsId
        defaultNonBankAccountGroupShouldBeFound("nonBankAccountsId.equals=" + nonBankAccountsId);

        // Get all the nonBankAccountGroupList where nonBankAccounts equals to nonBankAccountsId + 1
        defaultNonBankAccountGroupShouldNotBeFound("nonBankAccountsId.equals=" + (nonBankAccountsId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultNonBankAccountGroupShouldBeFound(String filter) throws Exception {
        restNonBankAccountGroupMockMvc.perform(get("/api/non-bank-account-groups?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(nonBankAccountGroup.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));

        // Check, that the count call also returns 1
        restNonBankAccountGroupMockMvc.perform(get("/api/non-bank-account-groups/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultNonBankAccountGroupShouldNotBeFound(String filter) throws Exception {
        restNonBankAccountGroupMockMvc.perform(get("/api/non-bank-account-groups?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restNonBankAccountGroupMockMvc.perform(get("/api/non-bank-account-groups/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingNonBankAccountGroup() throws Exception {
        // Get the nonBankAccountGroup
        restNonBankAccountGroupMockMvc.perform(get("/api/non-bank-account-groups/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNonBankAccountGroup() throws Exception {
        // Initialize the database
        nonBankAccountGroupRepository.saveAndFlush(nonBankAccountGroup);

        int databaseSizeBeforeUpdate = nonBankAccountGroupRepository.findAll().size();

        // Update the nonBankAccountGroup
        NonBankAccountGroup updatedNonBankAccountGroup = nonBankAccountGroupRepository.findById(nonBankAccountGroup.getId()).get();
        // Disconnect from session so that the updates on updatedNonBankAccountGroup are not directly saved in db
        em.detach(updatedNonBankAccountGroup);
        updatedNonBankAccountGroup
            .code(UPDATED_CODE)
            .name(UPDATED_NAME);
        NonBankAccountGroupDTO nonBankAccountGroupDTO = nonBankAccountGroupMapper.toDto(updatedNonBankAccountGroup);

        restNonBankAccountGroupMockMvc.perform(put("/api/non-bank-account-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(nonBankAccountGroupDTO)))
            .andExpect(status().isOk());

        // Validate the NonBankAccountGroup in the database
        List<NonBankAccountGroup> nonBankAccountGroupList = nonBankAccountGroupRepository.findAll();
        assertThat(nonBankAccountGroupList).hasSize(databaseSizeBeforeUpdate);
        NonBankAccountGroup testNonBankAccountGroup = nonBankAccountGroupList.get(nonBankAccountGroupList.size() - 1);
        assertThat(testNonBankAccountGroup.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testNonBankAccountGroup.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingNonBankAccountGroup() throws Exception {
        int databaseSizeBeforeUpdate = nonBankAccountGroupRepository.findAll().size();

        // Create the NonBankAccountGroup
        NonBankAccountGroupDTO nonBankAccountGroupDTO = nonBankAccountGroupMapper.toDto(nonBankAccountGroup);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restNonBankAccountGroupMockMvc.perform(put("/api/non-bank-account-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(nonBankAccountGroupDTO)))
            .andExpect(status().isBadRequest());

        // Validate the NonBankAccountGroup in the database
        List<NonBankAccountGroup> nonBankAccountGroupList = nonBankAccountGroupRepository.findAll();
        assertThat(nonBankAccountGroupList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteNonBankAccountGroup() throws Exception {
        // Initialize the database
        nonBankAccountGroupRepository.saveAndFlush(nonBankAccountGroup);

        int databaseSizeBeforeDelete = nonBankAccountGroupRepository.findAll().size();

        // Delete the nonBankAccountGroup
        restNonBankAccountGroupMockMvc.perform(delete("/api/non-bank-account-groups/{id}", nonBankAccountGroup.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<NonBankAccountGroup> nonBankAccountGroupList = nonBankAccountGroupRepository.findAll();
        assertThat(nonBankAccountGroupList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(NonBankAccountGroup.class);
        NonBankAccountGroup nonBankAccountGroup1 = new NonBankAccountGroup();
        nonBankAccountGroup1.setId(1L);
        NonBankAccountGroup nonBankAccountGroup2 = new NonBankAccountGroup();
        nonBankAccountGroup2.setId(nonBankAccountGroup1.getId());
        assertThat(nonBankAccountGroup1).isEqualTo(nonBankAccountGroup2);
        nonBankAccountGroup2.setId(2L);
        assertThat(nonBankAccountGroup1).isNotEqualTo(nonBankAccountGroup2);
        nonBankAccountGroup1.setId(null);
        assertThat(nonBankAccountGroup1).isNotEqualTo(nonBankAccountGroup2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(NonBankAccountGroupDTO.class);
        NonBankAccountGroupDTO nonBankAccountGroupDTO1 = new NonBankAccountGroupDTO();
        nonBankAccountGroupDTO1.setId(1L);
        NonBankAccountGroupDTO nonBankAccountGroupDTO2 = new NonBankAccountGroupDTO();
        assertThat(nonBankAccountGroupDTO1).isNotEqualTo(nonBankAccountGroupDTO2);
        nonBankAccountGroupDTO2.setId(nonBankAccountGroupDTO1.getId());
        assertThat(nonBankAccountGroupDTO1).isEqualTo(nonBankAccountGroupDTO2);
        nonBankAccountGroupDTO2.setId(2L);
        assertThat(nonBankAccountGroupDTO1).isNotEqualTo(nonBankAccountGroupDTO2);
        nonBankAccountGroupDTO1.setId(null);
        assertThat(nonBankAccountGroupDTO1).isNotEqualTo(nonBankAccountGroupDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(nonBankAccountGroupMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(nonBankAccountGroupMapper.fromId(null)).isNull();
    }
}
