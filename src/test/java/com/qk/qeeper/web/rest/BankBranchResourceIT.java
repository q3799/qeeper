package com.qk.qeeper.web.rest;

import com.qk.qeeper.QeeperApp;
import com.qk.qeeper.domain.BankBranch;
import com.qk.qeeper.domain.Bank;
import com.qk.qeeper.repository.BankBranchRepository;
import com.qk.qeeper.service.BankBranchService;
import com.qk.qeeper.service.dto.BankBranchDTO;
import com.qk.qeeper.service.mapper.BankBranchMapper;
import com.qk.qeeper.web.rest.errors.ExceptionTranslator;
import com.qk.qeeper.service.dto.BankBranchCriteria;
import com.qk.qeeper.service.BankBranchQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.qk.qeeper.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link BankBranchResource} REST controller.
 */
@SpringBootTest(classes = QeeperApp.class)
public class BankBranchResourceIT {

    private static final String DEFAULT_LOCATION = "AAAAAAAAAA";
    private static final String UPDATED_LOCATION = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_IFSC_CODE = "gmRz8fbEwbq";
    private static final String UPDATED_IFSC_CODE = "cYocRubA3zE";

    private static final String DEFAULT_MICR_CODE = "AAAAAAAAAA";
    private static final String UPDATED_MICR_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_REMARKS = "AAAAAAAAAA";
    private static final String UPDATED_REMARKS = "BBBBBBBBBB";

    @Autowired
    private BankBranchRepository bankBranchRepository;

    @Autowired
    private BankBranchMapper bankBranchMapper;

    @Autowired
    private BankBranchService bankBranchService;

    @Autowired
    private BankBranchQueryService bankBranchQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restBankBranchMockMvc;

    private BankBranch bankBranch;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final BankBranchResource bankBranchResource = new BankBranchResource(bankBranchService, bankBranchQueryService);
        this.restBankBranchMockMvc = MockMvcBuilders.standaloneSetup(bankBranchResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BankBranch createEntity(EntityManager em) {
        BankBranch bankBranch = new BankBranch()
            .location(DEFAULT_LOCATION)
            .address(DEFAULT_ADDRESS)
            .ifscCode(DEFAULT_IFSC_CODE)
            .micrCode(DEFAULT_MICR_CODE)
            .remarks(DEFAULT_REMARKS);
        // Add required entity
        Bank bank;
        if (TestUtil.findAll(em, Bank.class).isEmpty()) {
            bank = BankResourceIT.createEntity(em);
            em.persist(bank);
            em.flush();
        } else {
            bank = TestUtil.findAll(em, Bank.class).get(0);
        }
        bankBranch.setBank(bank);
        return bankBranch;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BankBranch createUpdatedEntity(EntityManager em) {
        BankBranch bankBranch = new BankBranch()
            .location(UPDATED_LOCATION)
            .address(UPDATED_ADDRESS)
            .ifscCode(UPDATED_IFSC_CODE)
            .micrCode(UPDATED_MICR_CODE)
            .remarks(UPDATED_REMARKS);
        // Add required entity
        Bank bank;
        if (TestUtil.findAll(em, Bank.class).isEmpty()) {
            bank = BankResourceIT.createUpdatedEntity(em);
            em.persist(bank);
            em.flush();
        } else {
            bank = TestUtil.findAll(em, Bank.class).get(0);
        }
        bankBranch.setBank(bank);
        return bankBranch;
    }

    @BeforeEach
    public void initTest() {
        bankBranch = createEntity(em);
    }

    @Test
    @Transactional
    public void createBankBranch() throws Exception {
        int databaseSizeBeforeCreate = bankBranchRepository.findAll().size();

        // Create the BankBranch
        BankBranchDTO bankBranchDTO = bankBranchMapper.toDto(bankBranch);
        restBankBranchMockMvc.perform(post("/api/bank-branches")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bankBranchDTO)))
            .andExpect(status().isCreated());

        // Validate the BankBranch in the database
        List<BankBranch> bankBranchList = bankBranchRepository.findAll();
        assertThat(bankBranchList).hasSize(databaseSizeBeforeCreate + 1);
        BankBranch testBankBranch = bankBranchList.get(bankBranchList.size() - 1);
        assertThat(testBankBranch.getLocation()).isEqualTo(DEFAULT_LOCATION);
        assertThat(testBankBranch.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testBankBranch.getIfscCode()).isEqualTo(DEFAULT_IFSC_CODE);
        assertThat(testBankBranch.getMicrCode()).isEqualTo(DEFAULT_MICR_CODE);
        assertThat(testBankBranch.getRemarks()).isEqualTo(DEFAULT_REMARKS);
    }

    @Test
    @Transactional
    public void createBankBranchWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = bankBranchRepository.findAll().size();

        // Create the BankBranch with an existing ID
        bankBranch.setId(1L);
        BankBranchDTO bankBranchDTO = bankBranchMapper.toDto(bankBranch);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBankBranchMockMvc.perform(post("/api/bank-branches")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bankBranchDTO)))
            .andExpect(status().isBadRequest());

        // Validate the BankBranch in the database
        List<BankBranch> bankBranchList = bankBranchRepository.findAll();
        assertThat(bankBranchList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkLocationIsRequired() throws Exception {
        int databaseSizeBeforeTest = bankBranchRepository.findAll().size();
        // set the field null
        bankBranch.setLocation(null);

        // Create the BankBranch, which fails.
        BankBranchDTO bankBranchDTO = bankBranchMapper.toDto(bankBranch);

        restBankBranchMockMvc.perform(post("/api/bank-branches")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bankBranchDTO)))
            .andExpect(status().isBadRequest());

        List<BankBranch> bankBranchList = bankBranchRepository.findAll();
        assertThat(bankBranchList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllBankBranches() throws Exception {
        // Initialize the database
        bankBranchRepository.saveAndFlush(bankBranch);

        // Get all the bankBranchList
        restBankBranchMockMvc.perform(get("/api/bank-branches?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bankBranch.getId().intValue())))
            .andExpect(jsonPath("$.[*].location").value(hasItem(DEFAULT_LOCATION.toString())))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].ifscCode").value(hasItem(DEFAULT_IFSC_CODE.toString())))
            .andExpect(jsonPath("$.[*].micrCode").value(hasItem(DEFAULT_MICR_CODE.toString())))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS.toString())));
    }
    
    @Test
    @Transactional
    public void getBankBranch() throws Exception {
        // Initialize the database
        bankBranchRepository.saveAndFlush(bankBranch);

        // Get the bankBranch
        restBankBranchMockMvc.perform(get("/api/bank-branches/{id}", bankBranch.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(bankBranch.getId().intValue()))
            .andExpect(jsonPath("$.location").value(DEFAULT_LOCATION.toString()))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS.toString()))
            .andExpect(jsonPath("$.ifscCode").value(DEFAULT_IFSC_CODE.toString()))
            .andExpect(jsonPath("$.micrCode").value(DEFAULT_MICR_CODE.toString()))
            .andExpect(jsonPath("$.remarks").value(DEFAULT_REMARKS.toString()));
    }

    @Test
    @Transactional
    public void getAllBankBranchesByLocationIsEqualToSomething() throws Exception {
        // Initialize the database
        bankBranchRepository.saveAndFlush(bankBranch);

        // Get all the bankBranchList where location equals to DEFAULT_LOCATION
        defaultBankBranchShouldBeFound("location.equals=" + DEFAULT_LOCATION);

        // Get all the bankBranchList where location equals to UPDATED_LOCATION
        defaultBankBranchShouldNotBeFound("location.equals=" + UPDATED_LOCATION);
    }

    @Test
    @Transactional
    public void getAllBankBranchesByLocationIsInShouldWork() throws Exception {
        // Initialize the database
        bankBranchRepository.saveAndFlush(bankBranch);

        // Get all the bankBranchList where location in DEFAULT_LOCATION or UPDATED_LOCATION
        defaultBankBranchShouldBeFound("location.in=" + DEFAULT_LOCATION + "," + UPDATED_LOCATION);

        // Get all the bankBranchList where location equals to UPDATED_LOCATION
        defaultBankBranchShouldNotBeFound("location.in=" + UPDATED_LOCATION);
    }

    @Test
    @Transactional
    public void getAllBankBranchesByLocationIsNullOrNotNull() throws Exception {
        // Initialize the database
        bankBranchRepository.saveAndFlush(bankBranch);

        // Get all the bankBranchList where location is not null
        defaultBankBranchShouldBeFound("location.specified=true");

        // Get all the bankBranchList where location is null
        defaultBankBranchShouldNotBeFound("location.specified=false");
    }

    @Test
    @Transactional
    public void getAllBankBranchesByAddressIsEqualToSomething() throws Exception {
        // Initialize the database
        bankBranchRepository.saveAndFlush(bankBranch);

        // Get all the bankBranchList where address equals to DEFAULT_ADDRESS
        defaultBankBranchShouldBeFound("address.equals=" + DEFAULT_ADDRESS);

        // Get all the bankBranchList where address equals to UPDATED_ADDRESS
        defaultBankBranchShouldNotBeFound("address.equals=" + UPDATED_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllBankBranchesByAddressIsInShouldWork() throws Exception {
        // Initialize the database
        bankBranchRepository.saveAndFlush(bankBranch);

        // Get all the bankBranchList where address in DEFAULT_ADDRESS or UPDATED_ADDRESS
        defaultBankBranchShouldBeFound("address.in=" + DEFAULT_ADDRESS + "," + UPDATED_ADDRESS);

        // Get all the bankBranchList where address equals to UPDATED_ADDRESS
        defaultBankBranchShouldNotBeFound("address.in=" + UPDATED_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllBankBranchesByAddressIsNullOrNotNull() throws Exception {
        // Initialize the database
        bankBranchRepository.saveAndFlush(bankBranch);

        // Get all the bankBranchList where address is not null
        defaultBankBranchShouldBeFound("address.specified=true");

        // Get all the bankBranchList where address is null
        defaultBankBranchShouldNotBeFound("address.specified=false");
    }

    @Test
    @Transactional
    public void getAllBankBranchesByIfscCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        bankBranchRepository.saveAndFlush(bankBranch);

        // Get all the bankBranchList where ifscCode equals to DEFAULT_IFSC_CODE
        defaultBankBranchShouldBeFound("ifscCode.equals=" + DEFAULT_IFSC_CODE);

        // Get all the bankBranchList where ifscCode equals to UPDATED_IFSC_CODE
        defaultBankBranchShouldNotBeFound("ifscCode.equals=" + UPDATED_IFSC_CODE);
    }

    @Test
    @Transactional
    public void getAllBankBranchesByIfscCodeIsInShouldWork() throws Exception {
        // Initialize the database
        bankBranchRepository.saveAndFlush(bankBranch);

        // Get all the bankBranchList where ifscCode in DEFAULT_IFSC_CODE or UPDATED_IFSC_CODE
        defaultBankBranchShouldBeFound("ifscCode.in=" + DEFAULT_IFSC_CODE + "," + UPDATED_IFSC_CODE);

        // Get all the bankBranchList where ifscCode equals to UPDATED_IFSC_CODE
        defaultBankBranchShouldNotBeFound("ifscCode.in=" + UPDATED_IFSC_CODE);
    }

    @Test
    @Transactional
    public void getAllBankBranchesByIfscCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        bankBranchRepository.saveAndFlush(bankBranch);

        // Get all the bankBranchList where ifscCode is not null
        defaultBankBranchShouldBeFound("ifscCode.specified=true");

        // Get all the bankBranchList where ifscCode is null
        defaultBankBranchShouldNotBeFound("ifscCode.specified=false");
    }

    @Test
    @Transactional
    public void getAllBankBranchesByMicrCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        bankBranchRepository.saveAndFlush(bankBranch);

        // Get all the bankBranchList where micrCode equals to DEFAULT_MICR_CODE
        defaultBankBranchShouldBeFound("micrCode.equals=" + DEFAULT_MICR_CODE);

        // Get all the bankBranchList where micrCode equals to UPDATED_MICR_CODE
        defaultBankBranchShouldNotBeFound("micrCode.equals=" + UPDATED_MICR_CODE);
    }

    @Test
    @Transactional
    public void getAllBankBranchesByMicrCodeIsInShouldWork() throws Exception {
        // Initialize the database
        bankBranchRepository.saveAndFlush(bankBranch);

        // Get all the bankBranchList where micrCode in DEFAULT_MICR_CODE or UPDATED_MICR_CODE
        defaultBankBranchShouldBeFound("micrCode.in=" + DEFAULT_MICR_CODE + "," + UPDATED_MICR_CODE);

        // Get all the bankBranchList where micrCode equals to UPDATED_MICR_CODE
        defaultBankBranchShouldNotBeFound("micrCode.in=" + UPDATED_MICR_CODE);
    }

    @Test
    @Transactional
    public void getAllBankBranchesByMicrCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        bankBranchRepository.saveAndFlush(bankBranch);

        // Get all the bankBranchList where micrCode is not null
        defaultBankBranchShouldBeFound("micrCode.specified=true");

        // Get all the bankBranchList where micrCode is null
        defaultBankBranchShouldNotBeFound("micrCode.specified=false");
    }

    @Test
    @Transactional
    public void getAllBankBranchesByRemarksIsEqualToSomething() throws Exception {
        // Initialize the database
        bankBranchRepository.saveAndFlush(bankBranch);

        // Get all the bankBranchList where remarks equals to DEFAULT_REMARKS
        defaultBankBranchShouldBeFound("remarks.equals=" + DEFAULT_REMARKS);

        // Get all the bankBranchList where remarks equals to UPDATED_REMARKS
        defaultBankBranchShouldNotBeFound("remarks.equals=" + UPDATED_REMARKS);
    }

    @Test
    @Transactional
    public void getAllBankBranchesByRemarksIsInShouldWork() throws Exception {
        // Initialize the database
        bankBranchRepository.saveAndFlush(bankBranch);

        // Get all the bankBranchList where remarks in DEFAULT_REMARKS or UPDATED_REMARKS
        defaultBankBranchShouldBeFound("remarks.in=" + DEFAULT_REMARKS + "," + UPDATED_REMARKS);

        // Get all the bankBranchList where remarks equals to UPDATED_REMARKS
        defaultBankBranchShouldNotBeFound("remarks.in=" + UPDATED_REMARKS);
    }

    @Test
    @Transactional
    public void getAllBankBranchesByRemarksIsNullOrNotNull() throws Exception {
        // Initialize the database
        bankBranchRepository.saveAndFlush(bankBranch);

        // Get all the bankBranchList where remarks is not null
        defaultBankBranchShouldBeFound("remarks.specified=true");

        // Get all the bankBranchList where remarks is null
        defaultBankBranchShouldNotBeFound("remarks.specified=false");
    }

    @Test
    @Transactional
    public void getAllBankBranchesByBankIsEqualToSomething() throws Exception {
        // Get already existing entity
        Bank bank = bankBranch.getBank();
        bankBranchRepository.saveAndFlush(bankBranch);
        Long bankId = bank.getId();

        // Get all the bankBranchList where bank equals to bankId
        defaultBankBranchShouldBeFound("bankId.equals=" + bankId);

        // Get all the bankBranchList where bank equals to bankId + 1
        defaultBankBranchShouldNotBeFound("bankId.equals=" + (bankId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultBankBranchShouldBeFound(String filter) throws Exception {
        restBankBranchMockMvc.perform(get("/api/bank-branches?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bankBranch.getId().intValue())))
            .andExpect(jsonPath("$.[*].location").value(hasItem(DEFAULT_LOCATION)))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS)))
            .andExpect(jsonPath("$.[*].ifscCode").value(hasItem(DEFAULT_IFSC_CODE)))
            .andExpect(jsonPath("$.[*].micrCode").value(hasItem(DEFAULT_MICR_CODE)))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS)));

        // Check, that the count call also returns 1
        restBankBranchMockMvc.perform(get("/api/bank-branches/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultBankBranchShouldNotBeFound(String filter) throws Exception {
        restBankBranchMockMvc.perform(get("/api/bank-branches?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restBankBranchMockMvc.perform(get("/api/bank-branches/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingBankBranch() throws Exception {
        // Get the bankBranch
        restBankBranchMockMvc.perform(get("/api/bank-branches/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBankBranch() throws Exception {
        // Initialize the database
        bankBranchRepository.saveAndFlush(bankBranch);

        int databaseSizeBeforeUpdate = bankBranchRepository.findAll().size();

        // Update the bankBranch
        BankBranch updatedBankBranch = bankBranchRepository.findById(bankBranch.getId()).get();
        // Disconnect from session so that the updates on updatedBankBranch are not directly saved in db
        em.detach(updatedBankBranch);
        updatedBankBranch
            .location(UPDATED_LOCATION)
            .address(UPDATED_ADDRESS)
            .ifscCode(UPDATED_IFSC_CODE)
            .micrCode(UPDATED_MICR_CODE)
            .remarks(UPDATED_REMARKS);
        BankBranchDTO bankBranchDTO = bankBranchMapper.toDto(updatedBankBranch);

        restBankBranchMockMvc.perform(put("/api/bank-branches")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bankBranchDTO)))
            .andExpect(status().isOk());

        // Validate the BankBranch in the database
        List<BankBranch> bankBranchList = bankBranchRepository.findAll();
        assertThat(bankBranchList).hasSize(databaseSizeBeforeUpdate);
        BankBranch testBankBranch = bankBranchList.get(bankBranchList.size() - 1);
        assertThat(testBankBranch.getLocation()).isEqualTo(UPDATED_LOCATION);
        assertThat(testBankBranch.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testBankBranch.getIfscCode()).isEqualTo(UPDATED_IFSC_CODE);
        assertThat(testBankBranch.getMicrCode()).isEqualTo(UPDATED_MICR_CODE);
        assertThat(testBankBranch.getRemarks()).isEqualTo(UPDATED_REMARKS);
    }

    @Test
    @Transactional
    public void updateNonExistingBankBranch() throws Exception {
        int databaseSizeBeforeUpdate = bankBranchRepository.findAll().size();

        // Create the BankBranch
        BankBranchDTO bankBranchDTO = bankBranchMapper.toDto(bankBranch);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBankBranchMockMvc.perform(put("/api/bank-branches")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bankBranchDTO)))
            .andExpect(status().isBadRequest());

        // Validate the BankBranch in the database
        List<BankBranch> bankBranchList = bankBranchRepository.findAll();
        assertThat(bankBranchList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBankBranch() throws Exception {
        // Initialize the database
        bankBranchRepository.saveAndFlush(bankBranch);

        int databaseSizeBeforeDelete = bankBranchRepository.findAll().size();

        // Delete the bankBranch
        restBankBranchMockMvc.perform(delete("/api/bank-branches/{id}", bankBranch.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<BankBranch> bankBranchList = bankBranchRepository.findAll();
        assertThat(bankBranchList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BankBranch.class);
        BankBranch bankBranch1 = new BankBranch();
        bankBranch1.setId(1L);
        BankBranch bankBranch2 = new BankBranch();
        bankBranch2.setId(bankBranch1.getId());
        assertThat(bankBranch1).isEqualTo(bankBranch2);
        bankBranch2.setId(2L);
        assertThat(bankBranch1).isNotEqualTo(bankBranch2);
        bankBranch1.setId(null);
        assertThat(bankBranch1).isNotEqualTo(bankBranch2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(BankBranchDTO.class);
        BankBranchDTO bankBranchDTO1 = new BankBranchDTO();
        bankBranchDTO1.setId(1L);
        BankBranchDTO bankBranchDTO2 = new BankBranchDTO();
        assertThat(bankBranchDTO1).isNotEqualTo(bankBranchDTO2);
        bankBranchDTO2.setId(bankBranchDTO1.getId());
        assertThat(bankBranchDTO1).isEqualTo(bankBranchDTO2);
        bankBranchDTO2.setId(2L);
        assertThat(bankBranchDTO1).isNotEqualTo(bankBranchDTO2);
        bankBranchDTO1.setId(null);
        assertThat(bankBranchDTO1).isNotEqualTo(bankBranchDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(bankBranchMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(bankBranchMapper.fromId(null)).isNull();
    }
}
