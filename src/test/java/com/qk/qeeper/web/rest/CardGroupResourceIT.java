package com.qk.qeeper.web.rest;

import com.qk.qeeper.QeeperApp;
import com.qk.qeeper.domain.CardGroup;
import com.qk.qeeper.domain.Card;
import com.qk.qeeper.repository.CardGroupRepository;
import com.qk.qeeper.service.CardGroupService;
import com.qk.qeeper.service.dto.CardGroupDTO;
import com.qk.qeeper.service.mapper.CardGroupMapper;
import com.qk.qeeper.web.rest.errors.ExceptionTranslator;
import com.qk.qeeper.service.dto.CardGroupCriteria;
import com.qk.qeeper.service.CardGroupQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.qk.qeeper.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link CardGroupResource} REST controller.
 */
@SpringBootTest(classes = QeeperApp.class)
public class CardGroupResourceIT {

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private CardGroupRepository cardGroupRepository;

    @Autowired
    private CardGroupMapper cardGroupMapper;

    @Autowired
    private CardGroupService cardGroupService;

    @Autowired
    private CardGroupQueryService cardGroupQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restCardGroupMockMvc;

    private CardGroup cardGroup;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CardGroupResource cardGroupResource = new CardGroupResource(cardGroupService, cardGroupQueryService);
        this.restCardGroupMockMvc = MockMvcBuilders.standaloneSetup(cardGroupResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CardGroup createEntity(EntityManager em) {
        CardGroup cardGroup = new CardGroup()
            .code(DEFAULT_CODE)
            .name(DEFAULT_NAME);
        return cardGroup;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CardGroup createUpdatedEntity(EntityManager em) {
        CardGroup cardGroup = new CardGroup()
            .code(UPDATED_CODE)
            .name(UPDATED_NAME);
        return cardGroup;
    }

    @BeforeEach
    public void initTest() {
        cardGroup = createEntity(em);
    }

    @Test
    @Transactional
    public void createCardGroup() throws Exception {
        int databaseSizeBeforeCreate = cardGroupRepository.findAll().size();

        // Create the CardGroup
        CardGroupDTO cardGroupDTO = cardGroupMapper.toDto(cardGroup);
        restCardGroupMockMvc.perform(post("/api/card-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cardGroupDTO)))
            .andExpect(status().isCreated());

        // Validate the CardGroup in the database
        List<CardGroup> cardGroupList = cardGroupRepository.findAll();
        assertThat(cardGroupList).hasSize(databaseSizeBeforeCreate + 1);
        CardGroup testCardGroup = cardGroupList.get(cardGroupList.size() - 1);
        assertThat(testCardGroup.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testCardGroup.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createCardGroupWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = cardGroupRepository.findAll().size();

        // Create the CardGroup with an existing ID
        cardGroup.setId(1L);
        CardGroupDTO cardGroupDTO = cardGroupMapper.toDto(cardGroup);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCardGroupMockMvc.perform(post("/api/card-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cardGroupDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CardGroup in the database
        List<CardGroup> cardGroupList = cardGroupRepository.findAll();
        assertThat(cardGroupList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = cardGroupRepository.findAll().size();
        // set the field null
        cardGroup.setCode(null);

        // Create the CardGroup, which fails.
        CardGroupDTO cardGroupDTO = cardGroupMapper.toDto(cardGroup);

        restCardGroupMockMvc.perform(post("/api/card-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cardGroupDTO)))
            .andExpect(status().isBadRequest());

        List<CardGroup> cardGroupList = cardGroupRepository.findAll();
        assertThat(cardGroupList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = cardGroupRepository.findAll().size();
        // set the field null
        cardGroup.setName(null);

        // Create the CardGroup, which fails.
        CardGroupDTO cardGroupDTO = cardGroupMapper.toDto(cardGroup);

        restCardGroupMockMvc.perform(post("/api/card-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cardGroupDTO)))
            .andExpect(status().isBadRequest());

        List<CardGroup> cardGroupList = cardGroupRepository.findAll();
        assertThat(cardGroupList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCardGroups() throws Exception {
        // Initialize the database
        cardGroupRepository.saveAndFlush(cardGroup);

        // Get all the cardGroupList
        restCardGroupMockMvc.perform(get("/api/card-groups?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(cardGroup.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }
    
    @Test
    @Transactional
    public void getCardGroup() throws Exception {
        // Initialize the database
        cardGroupRepository.saveAndFlush(cardGroup);

        // Get the cardGroup
        restCardGroupMockMvc.perform(get("/api/card-groups/{id}", cardGroup.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(cardGroup.getId().intValue()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getAllCardGroupsByCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        cardGroupRepository.saveAndFlush(cardGroup);

        // Get all the cardGroupList where code equals to DEFAULT_CODE
        defaultCardGroupShouldBeFound("code.equals=" + DEFAULT_CODE);

        // Get all the cardGroupList where code equals to UPDATED_CODE
        defaultCardGroupShouldNotBeFound("code.equals=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    public void getAllCardGroupsByCodeIsInShouldWork() throws Exception {
        // Initialize the database
        cardGroupRepository.saveAndFlush(cardGroup);

        // Get all the cardGroupList where code in DEFAULT_CODE or UPDATED_CODE
        defaultCardGroupShouldBeFound("code.in=" + DEFAULT_CODE + "," + UPDATED_CODE);

        // Get all the cardGroupList where code equals to UPDATED_CODE
        defaultCardGroupShouldNotBeFound("code.in=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    public void getAllCardGroupsByCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        cardGroupRepository.saveAndFlush(cardGroup);

        // Get all the cardGroupList where code is not null
        defaultCardGroupShouldBeFound("code.specified=true");

        // Get all the cardGroupList where code is null
        defaultCardGroupShouldNotBeFound("code.specified=false");
    }

    @Test
    @Transactional
    public void getAllCardGroupsByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        cardGroupRepository.saveAndFlush(cardGroup);

        // Get all the cardGroupList where name equals to DEFAULT_NAME
        defaultCardGroupShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the cardGroupList where name equals to UPDATED_NAME
        defaultCardGroupShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllCardGroupsByNameIsInShouldWork() throws Exception {
        // Initialize the database
        cardGroupRepository.saveAndFlush(cardGroup);

        // Get all the cardGroupList where name in DEFAULT_NAME or UPDATED_NAME
        defaultCardGroupShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the cardGroupList where name equals to UPDATED_NAME
        defaultCardGroupShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllCardGroupsByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        cardGroupRepository.saveAndFlush(cardGroup);

        // Get all the cardGroupList where name is not null
        defaultCardGroupShouldBeFound("name.specified=true");

        // Get all the cardGroupList where name is null
        defaultCardGroupShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    public void getAllCardGroupsByCardsIsEqualToSomething() throws Exception {
        // Initialize the database
        Card cards = CardResourceIT.createEntity(em);
        em.persist(cards);
        em.flush();
        cardGroup.addCards(cards);
        cardGroupRepository.saveAndFlush(cardGroup);
        Long cardsId = cards.getId();

        // Get all the cardGroupList where cards equals to cardsId
        defaultCardGroupShouldBeFound("cardsId.equals=" + cardsId);

        // Get all the cardGroupList where cards equals to cardsId + 1
        defaultCardGroupShouldNotBeFound("cardsId.equals=" + (cardsId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultCardGroupShouldBeFound(String filter) throws Exception {
        restCardGroupMockMvc.perform(get("/api/card-groups?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(cardGroup.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));

        // Check, that the count call also returns 1
        restCardGroupMockMvc.perform(get("/api/card-groups/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultCardGroupShouldNotBeFound(String filter) throws Exception {
        restCardGroupMockMvc.perform(get("/api/card-groups?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restCardGroupMockMvc.perform(get("/api/card-groups/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingCardGroup() throws Exception {
        // Get the cardGroup
        restCardGroupMockMvc.perform(get("/api/card-groups/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCardGroup() throws Exception {
        // Initialize the database
        cardGroupRepository.saveAndFlush(cardGroup);

        int databaseSizeBeforeUpdate = cardGroupRepository.findAll().size();

        // Update the cardGroup
        CardGroup updatedCardGroup = cardGroupRepository.findById(cardGroup.getId()).get();
        // Disconnect from session so that the updates on updatedCardGroup are not directly saved in db
        em.detach(updatedCardGroup);
        updatedCardGroup
            .code(UPDATED_CODE)
            .name(UPDATED_NAME);
        CardGroupDTO cardGroupDTO = cardGroupMapper.toDto(updatedCardGroup);

        restCardGroupMockMvc.perform(put("/api/card-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cardGroupDTO)))
            .andExpect(status().isOk());

        // Validate the CardGroup in the database
        List<CardGroup> cardGroupList = cardGroupRepository.findAll();
        assertThat(cardGroupList).hasSize(databaseSizeBeforeUpdate);
        CardGroup testCardGroup = cardGroupList.get(cardGroupList.size() - 1);
        assertThat(testCardGroup.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testCardGroup.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingCardGroup() throws Exception {
        int databaseSizeBeforeUpdate = cardGroupRepository.findAll().size();

        // Create the CardGroup
        CardGroupDTO cardGroupDTO = cardGroupMapper.toDto(cardGroup);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCardGroupMockMvc.perform(put("/api/card-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cardGroupDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CardGroup in the database
        List<CardGroup> cardGroupList = cardGroupRepository.findAll();
        assertThat(cardGroupList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCardGroup() throws Exception {
        // Initialize the database
        cardGroupRepository.saveAndFlush(cardGroup);

        int databaseSizeBeforeDelete = cardGroupRepository.findAll().size();

        // Delete the cardGroup
        restCardGroupMockMvc.perform(delete("/api/card-groups/{id}", cardGroup.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CardGroup> cardGroupList = cardGroupRepository.findAll();
        assertThat(cardGroupList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CardGroup.class);
        CardGroup cardGroup1 = new CardGroup();
        cardGroup1.setId(1L);
        CardGroup cardGroup2 = new CardGroup();
        cardGroup2.setId(cardGroup1.getId());
        assertThat(cardGroup1).isEqualTo(cardGroup2);
        cardGroup2.setId(2L);
        assertThat(cardGroup1).isNotEqualTo(cardGroup2);
        cardGroup1.setId(null);
        assertThat(cardGroup1).isNotEqualTo(cardGroup2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CardGroupDTO.class);
        CardGroupDTO cardGroupDTO1 = new CardGroupDTO();
        cardGroupDTO1.setId(1L);
        CardGroupDTO cardGroupDTO2 = new CardGroupDTO();
        assertThat(cardGroupDTO1).isNotEqualTo(cardGroupDTO2);
        cardGroupDTO2.setId(cardGroupDTO1.getId());
        assertThat(cardGroupDTO1).isEqualTo(cardGroupDTO2);
        cardGroupDTO2.setId(2L);
        assertThat(cardGroupDTO1).isNotEqualTo(cardGroupDTO2);
        cardGroupDTO1.setId(null);
        assertThat(cardGroupDTO1).isNotEqualTo(cardGroupDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(cardGroupMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(cardGroupMapper.fromId(null)).isNull();
    }
}
