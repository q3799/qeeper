package com.qk.qeeper.web.rest;

import com.qk.qeeper.QeeperApp;
import com.qk.qeeper.domain.Card;
import com.qk.qeeper.domain.MobileNumber;
import com.qk.qeeper.domain.Employee;
import com.qk.qeeper.domain.Bank;
import com.qk.qeeper.domain.BankAccount;
import com.qk.qeeper.domain.CardGroup;
import com.qk.qeeper.repository.CardRepository;
import com.qk.qeeper.service.CardService;
import com.qk.qeeper.service.dto.CardDTO;
import com.qk.qeeper.service.mapper.CardMapper;
import com.qk.qeeper.web.rest.errors.ExceptionTranslator;
import com.qk.qeeper.service.dto.CardCriteria;
import com.qk.qeeper.service.CardQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.qk.qeeper.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.qk.qeeper.domain.enumeration.CardType;
import com.qk.qeeper.domain.enumeration.PaymentNetwork;
/**
 * Integration tests for the {@Link CardResource} REST controller.
 */
@SpringBootTest(classes = QeeperApp.class)
public class CardResourceIT {

    private static final CardType DEFAULT_CARD_TYPE = CardType.DEBIT;
    private static final CardType UPDATED_CARD_TYPE = CardType.CREDIT;

    private static final PaymentNetwork DEFAULT_PAYMENT_NETWORK = PaymentNetwork.VISA;
    private static final PaymentNetwork UPDATED_PAYMENT_NETWORK = PaymentNetwork.MASTERCARD;

    private static final String DEFAULT_CARD_NUMBER = "6011634776454970";
    private static final String UPDATED_CARD_NUMBER = "213161836511898";

    private static final String DEFAULT_VALID_FROM = "06/20";
    private static final String UPDATED_VALID_FROM = "10/57";

    private static final String DEFAULT_VALID_TILL = "12/5129";
    private static final String UPDATED_VALID_TILL = "09/6845";

    private static final String DEFAULT_PAYMENT_NETWORK_PIN = "722";
    private static final String UPDATED_PAYMENT_NETWORK_PIN = "093900";

    private static final String DEFAULT_CVV = "79125";
    private static final String UPDATED_CVV = "611";

    private static final String DEFAULT_NAME_ON_CARD = "AAAAAAAAAA";
    private static final String UPDATED_NAME_ON_CARD = "BBBBBBBBBB";

    private static final String DEFAULT_REMARKS = "AAAAAAAAAA";
    private static final String UPDATED_REMARKS = "BBBBBBBBBB";

    private static final String DEFAULT_PIN = "96380";
    private static final String UPDATED_PIN = "545800";

    private static final String DEFAULT_TPIN = "725";
    private static final String UPDATED_TPIN = "077150";

    @Autowired
    private CardRepository cardRepository;

    @Autowired
    private CardMapper cardMapper;

    @Autowired
    private CardService cardService;

    @Autowired
    private CardQueryService cardQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restCardMockMvc;

    private Card card;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CardResource cardResource = new CardResource(cardService, cardQueryService);
        this.restCardMockMvc = MockMvcBuilders.standaloneSetup(cardResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Card createEntity(EntityManager em) {
        Card card = new Card()
            .cardType(DEFAULT_CARD_TYPE)
            .paymentNetwork(DEFAULT_PAYMENT_NETWORK)
            .cardNumber(DEFAULT_CARD_NUMBER)
            .validFrom(DEFAULT_VALID_FROM)
            .validTill(DEFAULT_VALID_TILL)
            .paymentNetworkPin(DEFAULT_PAYMENT_NETWORK_PIN)
            .cvv(DEFAULT_CVV)
            .nameOnCard(DEFAULT_NAME_ON_CARD)
            .remarks(DEFAULT_REMARKS)
            .pin(DEFAULT_PIN)
            .tpin(DEFAULT_TPIN);
        // Add required entity
        MobileNumber mobileNumber;
        if (TestUtil.findAll(em, MobileNumber.class).isEmpty()) {
            mobileNumber = MobileNumberResourceIT.createEntity(em);
            em.persist(mobileNumber);
            em.flush();
        } else {
            mobileNumber = TestUtil.findAll(em, MobileNumber.class).get(0);
        }
        card.setMobileNumberMapped(mobileNumber);
        // Add required entity
        Employee employee;
        if (TestUtil.findAll(em, Employee.class).isEmpty()) {
            employee = EmployeeResourceIT.createEntity(em);
            em.persist(employee);
            em.flush();
        } else {
            employee = TestUtil.findAll(em, Employee.class).get(0);
        }
        card.setRegisteredTo(employee);
        // Add required entity
        Bank bank;
        if (TestUtil.findAll(em, Bank.class).isEmpty()) {
            bank = BankResourceIT.createEntity(em);
            em.persist(bank);
            em.flush();
        } else {
            bank = TestUtil.findAll(em, Bank.class).get(0);
        }
        card.setBank(bank);
        return card;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Card createUpdatedEntity(EntityManager em) {
        Card card = new Card()
            .cardType(UPDATED_CARD_TYPE)
            .paymentNetwork(UPDATED_PAYMENT_NETWORK)
            .cardNumber(UPDATED_CARD_NUMBER)
            .validFrom(UPDATED_VALID_FROM)
            .validTill(UPDATED_VALID_TILL)
            .paymentNetworkPin(UPDATED_PAYMENT_NETWORK_PIN)
            .cvv(UPDATED_CVV)
            .nameOnCard(UPDATED_NAME_ON_CARD)
            .remarks(UPDATED_REMARKS)
            .pin(UPDATED_PIN)
            .tpin(UPDATED_TPIN);
        // Add required entity
        MobileNumber mobileNumber;
        if (TestUtil.findAll(em, MobileNumber.class).isEmpty()) {
            mobileNumber = MobileNumberResourceIT.createUpdatedEntity(em);
            em.persist(mobileNumber);
            em.flush();
        } else {
            mobileNumber = TestUtil.findAll(em, MobileNumber.class).get(0);
        }
        card.setMobileNumberMapped(mobileNumber);
        // Add required entity
        Employee employee;
        if (TestUtil.findAll(em, Employee.class).isEmpty()) {
            employee = EmployeeResourceIT.createUpdatedEntity(em);
            em.persist(employee);
            em.flush();
        } else {
            employee = TestUtil.findAll(em, Employee.class).get(0);
        }
        card.setRegisteredTo(employee);
        // Add required entity
        Bank bank;
        if (TestUtil.findAll(em, Bank.class).isEmpty()) {
            bank = BankResourceIT.createUpdatedEntity(em);
            em.persist(bank);
            em.flush();
        } else {
            bank = TestUtil.findAll(em, Bank.class).get(0);
        }
        card.setBank(bank);
        return card;
    }

    @BeforeEach
    public void initTest() {
        card = createEntity(em);
    }

    @Test
    @Transactional
    public void createCard() throws Exception {
        int databaseSizeBeforeCreate = cardRepository.findAll().size();

        // Create the Card
        CardDTO cardDTO = cardMapper.toDto(card);
        restCardMockMvc.perform(post("/api/cards")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cardDTO)))
            .andExpect(status().isCreated());

        // Validate the Card in the database
        List<Card> cardList = cardRepository.findAll();
        assertThat(cardList).hasSize(databaseSizeBeforeCreate + 1);
        Card testCard = cardList.get(cardList.size() - 1);
        assertThat(testCard.getCardType()).isEqualTo(DEFAULT_CARD_TYPE);
        assertThat(testCard.getPaymentNetwork()).isEqualTo(DEFAULT_PAYMENT_NETWORK);
        assertThat(testCard.getCardNumber()).isEqualTo(DEFAULT_CARD_NUMBER);
        assertThat(testCard.getValidFrom()).isEqualTo(DEFAULT_VALID_FROM);
        assertThat(testCard.getValidTill()).isEqualTo(DEFAULT_VALID_TILL);
        assertThat(testCard.getPaymentNetworkPin()).isEqualTo(DEFAULT_PAYMENT_NETWORK_PIN);
        assertThat(testCard.getCvv()).isEqualTo(DEFAULT_CVV);
        assertThat(testCard.getNameOnCard()).isEqualTo(DEFAULT_NAME_ON_CARD);
        assertThat(testCard.getRemarks()).isEqualTo(DEFAULT_REMARKS);
        assertThat(testCard.getPin()).isEqualTo(DEFAULT_PIN);
        assertThat(testCard.getTpin()).isEqualTo(DEFAULT_TPIN);
    }

    @Test
    @Transactional
    public void createCardWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = cardRepository.findAll().size();

        // Create the Card with an existing ID
        card.setId(1L);
        CardDTO cardDTO = cardMapper.toDto(card);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCardMockMvc.perform(post("/api/cards")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cardDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Card in the database
        List<Card> cardList = cardRepository.findAll();
        assertThat(cardList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkCardTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = cardRepository.findAll().size();
        // set the field null
        card.setCardType(null);

        // Create the Card, which fails.
        CardDTO cardDTO = cardMapper.toDto(card);

        restCardMockMvc.perform(post("/api/cards")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cardDTO)))
            .andExpect(status().isBadRequest());

        List<Card> cardList = cardRepository.findAll();
        assertThat(cardList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPaymentNetworkIsRequired() throws Exception {
        int databaseSizeBeforeTest = cardRepository.findAll().size();
        // set the field null
        card.setPaymentNetwork(null);

        // Create the Card, which fails.
        CardDTO cardDTO = cardMapper.toDto(card);

        restCardMockMvc.perform(post("/api/cards")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cardDTO)))
            .andExpect(status().isBadRequest());

        List<Card> cardList = cardRepository.findAll();
        assertThat(cardList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCardNumberIsRequired() throws Exception {
        int databaseSizeBeforeTest = cardRepository.findAll().size();
        // set the field null
        card.setCardNumber(null);

        // Create the Card, which fails.
        CardDTO cardDTO = cardMapper.toDto(card);

        restCardMockMvc.perform(post("/api/cards")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cardDTO)))
            .andExpect(status().isBadRequest());

        List<Card> cardList = cardRepository.findAll();
        assertThat(cardList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkValidFromIsRequired() throws Exception {
        int databaseSizeBeforeTest = cardRepository.findAll().size();
        // set the field null
        card.setValidFrom(null);

        // Create the Card, which fails.
        CardDTO cardDTO = cardMapper.toDto(card);

        restCardMockMvc.perform(post("/api/cards")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cardDTO)))
            .andExpect(status().isBadRequest());

        List<Card> cardList = cardRepository.findAll();
        assertThat(cardList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkValidTillIsRequired() throws Exception {
        int databaseSizeBeforeTest = cardRepository.findAll().size();
        // set the field null
        card.setValidTill(null);

        // Create the Card, which fails.
        CardDTO cardDTO = cardMapper.toDto(card);

        restCardMockMvc.perform(post("/api/cards")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cardDTO)))
            .andExpect(status().isBadRequest());

        List<Card> cardList = cardRepository.findAll();
        assertThat(cardList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCvvIsRequired() throws Exception {
        int databaseSizeBeforeTest = cardRepository.findAll().size();
        // set the field null
        card.setCvv(null);

        // Create the Card, which fails.
        CardDTO cardDTO = cardMapper.toDto(card);

        restCardMockMvc.perform(post("/api/cards")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cardDTO)))
            .andExpect(status().isBadRequest());

        List<Card> cardList = cardRepository.findAll();
        assertThat(cardList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNameOnCardIsRequired() throws Exception {
        int databaseSizeBeforeTest = cardRepository.findAll().size();
        // set the field null
        card.setNameOnCard(null);

        // Create the Card, which fails.
        CardDTO cardDTO = cardMapper.toDto(card);

        restCardMockMvc.perform(post("/api/cards")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cardDTO)))
            .andExpect(status().isBadRequest());

        List<Card> cardList = cardRepository.findAll();
        assertThat(cardList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCards() throws Exception {
        // Initialize the database
        cardRepository.saveAndFlush(card);

        // Get all the cardList
        restCardMockMvc.perform(get("/api/cards?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(card.getId().intValue())))
            .andExpect(jsonPath("$.[*].cardType").value(hasItem(DEFAULT_CARD_TYPE.toString())))
            .andExpect(jsonPath("$.[*].paymentNetwork").value(hasItem(DEFAULT_PAYMENT_NETWORK.toString())))
            .andExpect(jsonPath("$.[*].cardNumber").value(hasItem(DEFAULT_CARD_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].validFrom").value(hasItem(DEFAULT_VALID_FROM.toString())))
            .andExpect(jsonPath("$.[*].validTill").value(hasItem(DEFAULT_VALID_TILL.toString())))
            .andExpect(jsonPath("$.[*].paymentNetworkPin").value(hasItem(DEFAULT_PAYMENT_NETWORK_PIN.toString())))
            .andExpect(jsonPath("$.[*].cvv").value(hasItem(DEFAULT_CVV.toString())))
            .andExpect(jsonPath("$.[*].nameOnCard").value(hasItem(DEFAULT_NAME_ON_CARD.toString())))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS.toString())))
            .andExpect(jsonPath("$.[*].pin").value(hasItem(DEFAULT_PIN.toString())))
            .andExpect(jsonPath("$.[*].tpin").value(hasItem(DEFAULT_TPIN.toString())));
    }
    
    @Test
    @Transactional
    public void getCard() throws Exception {
        // Initialize the database
        cardRepository.saveAndFlush(card);

        // Get the card
        restCardMockMvc.perform(get("/api/cards/{id}", card.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(card.getId().intValue()))
            .andExpect(jsonPath("$.cardType").value(DEFAULT_CARD_TYPE.toString()))
            .andExpect(jsonPath("$.paymentNetwork").value(DEFAULT_PAYMENT_NETWORK.toString()))
            .andExpect(jsonPath("$.cardNumber").value(DEFAULT_CARD_NUMBER.toString()))
            .andExpect(jsonPath("$.validFrom").value(DEFAULT_VALID_FROM.toString()))
            .andExpect(jsonPath("$.validTill").value(DEFAULT_VALID_TILL.toString()))
            .andExpect(jsonPath("$.paymentNetworkPin").value(DEFAULT_PAYMENT_NETWORK_PIN.toString()))
            .andExpect(jsonPath("$.cvv").value(DEFAULT_CVV.toString()))
            .andExpect(jsonPath("$.nameOnCard").value(DEFAULT_NAME_ON_CARD.toString()))
            .andExpect(jsonPath("$.remarks").value(DEFAULT_REMARKS.toString()))
            .andExpect(jsonPath("$.pin").value(DEFAULT_PIN.toString()))
            .andExpect(jsonPath("$.tpin").value(DEFAULT_TPIN.toString()));
    }

    @Test
    @Transactional
    public void getAllCardsByCardTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        cardRepository.saveAndFlush(card);

        // Get all the cardList where cardType equals to DEFAULT_CARD_TYPE
        defaultCardShouldBeFound("cardType.equals=" + DEFAULT_CARD_TYPE);

        // Get all the cardList where cardType equals to UPDATED_CARD_TYPE
        defaultCardShouldNotBeFound("cardType.equals=" + UPDATED_CARD_TYPE);
    }

    @Test
    @Transactional
    public void getAllCardsByCardTypeIsInShouldWork() throws Exception {
        // Initialize the database
        cardRepository.saveAndFlush(card);

        // Get all the cardList where cardType in DEFAULT_CARD_TYPE or UPDATED_CARD_TYPE
        defaultCardShouldBeFound("cardType.in=" + DEFAULT_CARD_TYPE + "," + UPDATED_CARD_TYPE);

        // Get all the cardList where cardType equals to UPDATED_CARD_TYPE
        defaultCardShouldNotBeFound("cardType.in=" + UPDATED_CARD_TYPE);
    }

    @Test
    @Transactional
    public void getAllCardsByCardTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        cardRepository.saveAndFlush(card);

        // Get all the cardList where cardType is not null
        defaultCardShouldBeFound("cardType.specified=true");

        // Get all the cardList where cardType is null
        defaultCardShouldNotBeFound("cardType.specified=false");
    }

    @Test
    @Transactional
    public void getAllCardsByPaymentNetworkIsEqualToSomething() throws Exception {
        // Initialize the database
        cardRepository.saveAndFlush(card);

        // Get all the cardList where paymentNetwork equals to DEFAULT_PAYMENT_NETWORK
        defaultCardShouldBeFound("paymentNetwork.equals=" + DEFAULT_PAYMENT_NETWORK);

        // Get all the cardList where paymentNetwork equals to UPDATED_PAYMENT_NETWORK
        defaultCardShouldNotBeFound("paymentNetwork.equals=" + UPDATED_PAYMENT_NETWORK);
    }

    @Test
    @Transactional
    public void getAllCardsByPaymentNetworkIsInShouldWork() throws Exception {
        // Initialize the database
        cardRepository.saveAndFlush(card);

        // Get all the cardList where paymentNetwork in DEFAULT_PAYMENT_NETWORK or UPDATED_PAYMENT_NETWORK
        defaultCardShouldBeFound("paymentNetwork.in=" + DEFAULT_PAYMENT_NETWORK + "," + UPDATED_PAYMENT_NETWORK);

        // Get all the cardList where paymentNetwork equals to UPDATED_PAYMENT_NETWORK
        defaultCardShouldNotBeFound("paymentNetwork.in=" + UPDATED_PAYMENT_NETWORK);
    }

    @Test
    @Transactional
    public void getAllCardsByPaymentNetworkIsNullOrNotNull() throws Exception {
        // Initialize the database
        cardRepository.saveAndFlush(card);

        // Get all the cardList where paymentNetwork is not null
        defaultCardShouldBeFound("paymentNetwork.specified=true");

        // Get all the cardList where paymentNetwork is null
        defaultCardShouldNotBeFound("paymentNetwork.specified=false");
    }

    @Test
    @Transactional
    public void getAllCardsByCardNumberIsEqualToSomething() throws Exception {
        // Initialize the database
        cardRepository.saveAndFlush(card);

        // Get all the cardList where cardNumber equals to DEFAULT_CARD_NUMBER
        defaultCardShouldBeFound("cardNumber.equals=" + DEFAULT_CARD_NUMBER);

        // Get all the cardList where cardNumber equals to UPDATED_CARD_NUMBER
        defaultCardShouldNotBeFound("cardNumber.equals=" + UPDATED_CARD_NUMBER);
    }

    @Test
    @Transactional
    public void getAllCardsByCardNumberIsInShouldWork() throws Exception {
        // Initialize the database
        cardRepository.saveAndFlush(card);

        // Get all the cardList where cardNumber in DEFAULT_CARD_NUMBER or UPDATED_CARD_NUMBER
        defaultCardShouldBeFound("cardNumber.in=" + DEFAULT_CARD_NUMBER + "," + UPDATED_CARD_NUMBER);

        // Get all the cardList where cardNumber equals to UPDATED_CARD_NUMBER
        defaultCardShouldNotBeFound("cardNumber.in=" + UPDATED_CARD_NUMBER);
    }

    @Test
    @Transactional
    public void getAllCardsByCardNumberIsNullOrNotNull() throws Exception {
        // Initialize the database
        cardRepository.saveAndFlush(card);

        // Get all the cardList where cardNumber is not null
        defaultCardShouldBeFound("cardNumber.specified=true");

        // Get all the cardList where cardNumber is null
        defaultCardShouldNotBeFound("cardNumber.specified=false");
    }

    @Test
    @Transactional
    public void getAllCardsByValidFromIsEqualToSomething() throws Exception {
        // Initialize the database
        cardRepository.saveAndFlush(card);

        // Get all the cardList where validFrom equals to DEFAULT_VALID_FROM
        defaultCardShouldBeFound("validFrom.equals=" + DEFAULT_VALID_FROM);

        // Get all the cardList where validFrom equals to UPDATED_VALID_FROM
        defaultCardShouldNotBeFound("validFrom.equals=" + UPDATED_VALID_FROM);
    }

    @Test
    @Transactional
    public void getAllCardsByValidFromIsInShouldWork() throws Exception {
        // Initialize the database
        cardRepository.saveAndFlush(card);

        // Get all the cardList where validFrom in DEFAULT_VALID_FROM or UPDATED_VALID_FROM
        defaultCardShouldBeFound("validFrom.in=" + DEFAULT_VALID_FROM + "," + UPDATED_VALID_FROM);

        // Get all the cardList where validFrom equals to UPDATED_VALID_FROM
        defaultCardShouldNotBeFound("validFrom.in=" + UPDATED_VALID_FROM);
    }

    @Test
    @Transactional
    public void getAllCardsByValidFromIsNullOrNotNull() throws Exception {
        // Initialize the database
        cardRepository.saveAndFlush(card);

        // Get all the cardList where validFrom is not null
        defaultCardShouldBeFound("validFrom.specified=true");

        // Get all the cardList where validFrom is null
        defaultCardShouldNotBeFound("validFrom.specified=false");
    }

    @Test
    @Transactional
    public void getAllCardsByValidTillIsEqualToSomething() throws Exception {
        // Initialize the database
        cardRepository.saveAndFlush(card);

        // Get all the cardList where validTill equals to DEFAULT_VALID_TILL
        defaultCardShouldBeFound("validTill.equals=" + DEFAULT_VALID_TILL);

        // Get all the cardList where validTill equals to UPDATED_VALID_TILL
        defaultCardShouldNotBeFound("validTill.equals=" + UPDATED_VALID_TILL);
    }

    @Test
    @Transactional
    public void getAllCardsByValidTillIsInShouldWork() throws Exception {
        // Initialize the database
        cardRepository.saveAndFlush(card);

        // Get all the cardList where validTill in DEFAULT_VALID_TILL or UPDATED_VALID_TILL
        defaultCardShouldBeFound("validTill.in=" + DEFAULT_VALID_TILL + "," + UPDATED_VALID_TILL);

        // Get all the cardList where validTill equals to UPDATED_VALID_TILL
        defaultCardShouldNotBeFound("validTill.in=" + UPDATED_VALID_TILL);
    }

    @Test
    @Transactional
    public void getAllCardsByValidTillIsNullOrNotNull() throws Exception {
        // Initialize the database
        cardRepository.saveAndFlush(card);

        // Get all the cardList where validTill is not null
        defaultCardShouldBeFound("validTill.specified=true");

        // Get all the cardList where validTill is null
        defaultCardShouldNotBeFound("validTill.specified=false");
    }

    @Test
    @Transactional
    public void getAllCardsByPaymentNetworkPinIsEqualToSomething() throws Exception {
        // Initialize the database
        cardRepository.saveAndFlush(card);

        // Get all the cardList where paymentNetworkPin equals to DEFAULT_PAYMENT_NETWORK_PIN
        defaultCardShouldBeFound("paymentNetworkPin.equals=" + DEFAULT_PAYMENT_NETWORK_PIN);

        // Get all the cardList where paymentNetworkPin equals to UPDATED_PAYMENT_NETWORK_PIN
        defaultCardShouldNotBeFound("paymentNetworkPin.equals=" + UPDATED_PAYMENT_NETWORK_PIN);
    }

    @Test
    @Transactional
    public void getAllCardsByPaymentNetworkPinIsInShouldWork() throws Exception {
        // Initialize the database
        cardRepository.saveAndFlush(card);

        // Get all the cardList where paymentNetworkPin in DEFAULT_PAYMENT_NETWORK_PIN or UPDATED_PAYMENT_NETWORK_PIN
        defaultCardShouldBeFound("paymentNetworkPin.in=" + DEFAULT_PAYMENT_NETWORK_PIN + "," + UPDATED_PAYMENT_NETWORK_PIN);

        // Get all the cardList where paymentNetworkPin equals to UPDATED_PAYMENT_NETWORK_PIN
        defaultCardShouldNotBeFound("paymentNetworkPin.in=" + UPDATED_PAYMENT_NETWORK_PIN);
    }

    @Test
    @Transactional
    public void getAllCardsByPaymentNetworkPinIsNullOrNotNull() throws Exception {
        // Initialize the database
        cardRepository.saveAndFlush(card);

        // Get all the cardList where paymentNetworkPin is not null
        defaultCardShouldBeFound("paymentNetworkPin.specified=true");

        // Get all the cardList where paymentNetworkPin is null
        defaultCardShouldNotBeFound("paymentNetworkPin.specified=false");
    }

    @Test
    @Transactional
    public void getAllCardsByCvvIsEqualToSomething() throws Exception {
        // Initialize the database
        cardRepository.saveAndFlush(card);

        // Get all the cardList where cvv equals to DEFAULT_CVV
        defaultCardShouldBeFound("cvv.equals=" + DEFAULT_CVV);

        // Get all the cardList where cvv equals to UPDATED_CVV
        defaultCardShouldNotBeFound("cvv.equals=" + UPDATED_CVV);
    }

    @Test
    @Transactional
    public void getAllCardsByCvvIsInShouldWork() throws Exception {
        // Initialize the database
        cardRepository.saveAndFlush(card);

        // Get all the cardList where cvv in DEFAULT_CVV or UPDATED_CVV
        defaultCardShouldBeFound("cvv.in=" + DEFAULT_CVV + "," + UPDATED_CVV);

        // Get all the cardList where cvv equals to UPDATED_CVV
        defaultCardShouldNotBeFound("cvv.in=" + UPDATED_CVV);
    }

    @Test
    @Transactional
    public void getAllCardsByCvvIsNullOrNotNull() throws Exception {
        // Initialize the database
        cardRepository.saveAndFlush(card);

        // Get all the cardList where cvv is not null
        defaultCardShouldBeFound("cvv.specified=true");

        // Get all the cardList where cvv is null
        defaultCardShouldNotBeFound("cvv.specified=false");
    }

    @Test
    @Transactional
    public void getAllCardsByNameOnCardIsEqualToSomething() throws Exception {
        // Initialize the database
        cardRepository.saveAndFlush(card);

        // Get all the cardList where nameOnCard equals to DEFAULT_NAME_ON_CARD
        defaultCardShouldBeFound("nameOnCard.equals=" + DEFAULT_NAME_ON_CARD);

        // Get all the cardList where nameOnCard equals to UPDATED_NAME_ON_CARD
        defaultCardShouldNotBeFound("nameOnCard.equals=" + UPDATED_NAME_ON_CARD);
    }

    @Test
    @Transactional
    public void getAllCardsByNameOnCardIsInShouldWork() throws Exception {
        // Initialize the database
        cardRepository.saveAndFlush(card);

        // Get all the cardList where nameOnCard in DEFAULT_NAME_ON_CARD or UPDATED_NAME_ON_CARD
        defaultCardShouldBeFound("nameOnCard.in=" + DEFAULT_NAME_ON_CARD + "," + UPDATED_NAME_ON_CARD);

        // Get all the cardList where nameOnCard equals to UPDATED_NAME_ON_CARD
        defaultCardShouldNotBeFound("nameOnCard.in=" + UPDATED_NAME_ON_CARD);
    }

    @Test
    @Transactional
    public void getAllCardsByNameOnCardIsNullOrNotNull() throws Exception {
        // Initialize the database
        cardRepository.saveAndFlush(card);

        // Get all the cardList where nameOnCard is not null
        defaultCardShouldBeFound("nameOnCard.specified=true");

        // Get all the cardList where nameOnCard is null
        defaultCardShouldNotBeFound("nameOnCard.specified=false");
    }

    @Test
    @Transactional
    public void getAllCardsByRemarksIsEqualToSomething() throws Exception {
        // Initialize the database
        cardRepository.saveAndFlush(card);

        // Get all the cardList where remarks equals to DEFAULT_REMARKS
        defaultCardShouldBeFound("remarks.equals=" + DEFAULT_REMARKS);

        // Get all the cardList where remarks equals to UPDATED_REMARKS
        defaultCardShouldNotBeFound("remarks.equals=" + UPDATED_REMARKS);
    }

    @Test
    @Transactional
    public void getAllCardsByRemarksIsInShouldWork() throws Exception {
        // Initialize the database
        cardRepository.saveAndFlush(card);

        // Get all the cardList where remarks in DEFAULT_REMARKS or UPDATED_REMARKS
        defaultCardShouldBeFound("remarks.in=" + DEFAULT_REMARKS + "," + UPDATED_REMARKS);

        // Get all the cardList where remarks equals to UPDATED_REMARKS
        defaultCardShouldNotBeFound("remarks.in=" + UPDATED_REMARKS);
    }

    @Test
    @Transactional
    public void getAllCardsByRemarksIsNullOrNotNull() throws Exception {
        // Initialize the database
        cardRepository.saveAndFlush(card);

        // Get all the cardList where remarks is not null
        defaultCardShouldBeFound("remarks.specified=true");

        // Get all the cardList where remarks is null
        defaultCardShouldNotBeFound("remarks.specified=false");
    }

    @Test
    @Transactional
    public void getAllCardsByPinIsEqualToSomething() throws Exception {
        // Initialize the database
        cardRepository.saveAndFlush(card);

        // Get all the cardList where pin equals to DEFAULT_PIN
        defaultCardShouldBeFound("pin.equals=" + DEFAULT_PIN);

        // Get all the cardList where pin equals to UPDATED_PIN
        defaultCardShouldNotBeFound("pin.equals=" + UPDATED_PIN);
    }

    @Test
    @Transactional
    public void getAllCardsByPinIsInShouldWork() throws Exception {
        // Initialize the database
        cardRepository.saveAndFlush(card);

        // Get all the cardList where pin in DEFAULT_PIN or UPDATED_PIN
        defaultCardShouldBeFound("pin.in=" + DEFAULT_PIN + "," + UPDATED_PIN);

        // Get all the cardList where pin equals to UPDATED_PIN
        defaultCardShouldNotBeFound("pin.in=" + UPDATED_PIN);
    }

    @Test
    @Transactional
    public void getAllCardsByPinIsNullOrNotNull() throws Exception {
        // Initialize the database
        cardRepository.saveAndFlush(card);

        // Get all the cardList where pin is not null
        defaultCardShouldBeFound("pin.specified=true");

        // Get all the cardList where pin is null
        defaultCardShouldNotBeFound("pin.specified=false");
    }

    @Test
    @Transactional
    public void getAllCardsByTpinIsEqualToSomething() throws Exception {
        // Initialize the database
        cardRepository.saveAndFlush(card);

        // Get all the cardList where tpin equals to DEFAULT_TPIN
        defaultCardShouldBeFound("tpin.equals=" + DEFAULT_TPIN);

        // Get all the cardList where tpin equals to UPDATED_TPIN
        defaultCardShouldNotBeFound("tpin.equals=" + UPDATED_TPIN);
    }

    @Test
    @Transactional
    public void getAllCardsByTpinIsInShouldWork() throws Exception {
        // Initialize the database
        cardRepository.saveAndFlush(card);

        // Get all the cardList where tpin in DEFAULT_TPIN or UPDATED_TPIN
        defaultCardShouldBeFound("tpin.in=" + DEFAULT_TPIN + "," + UPDATED_TPIN);

        // Get all the cardList where tpin equals to UPDATED_TPIN
        defaultCardShouldNotBeFound("tpin.in=" + UPDATED_TPIN);
    }

    @Test
    @Transactional
    public void getAllCardsByTpinIsNullOrNotNull() throws Exception {
        // Initialize the database
        cardRepository.saveAndFlush(card);

        // Get all the cardList where tpin is not null
        defaultCardShouldBeFound("tpin.specified=true");

        // Get all the cardList where tpin is null
        defaultCardShouldNotBeFound("tpin.specified=false");
    }

    @Test
    @Transactional
    public void getAllCardsByMobileNumberMappedIsEqualToSomething() throws Exception {
        // Get already existing entity
        MobileNumber mobileNumberMapped = card.getMobileNumberMapped();
        cardRepository.saveAndFlush(card);
        Long mobileNumberMappedId = mobileNumberMapped.getId();

        // Get all the cardList where mobileNumberMapped equals to mobileNumberMappedId
        defaultCardShouldBeFound("mobileNumberMappedId.equals=" + mobileNumberMappedId);

        // Get all the cardList where mobileNumberMapped equals to mobileNumberMappedId + 1
        defaultCardShouldNotBeFound("mobileNumberMappedId.equals=" + (mobileNumberMappedId + 1));
    }


    @Test
    @Transactional
    public void getAllCardsByRegisteredToIsEqualToSomething() throws Exception {
        // Get already existing entity
        Employee registeredTo = card.getRegisteredTo();
        cardRepository.saveAndFlush(card);
        Long registeredToId = registeredTo.getId();

        // Get all the cardList where registeredTo equals to registeredToId
        defaultCardShouldBeFound("registeredToId.equals=" + registeredToId);

        // Get all the cardList where registeredTo equals to registeredToId + 1
        defaultCardShouldNotBeFound("registeredToId.equals=" + (registeredToId + 1));
    }


    @Test
    @Transactional
    public void getAllCardsByBankIsEqualToSomething() throws Exception {
        // Get already existing entity
        Bank bank = card.getBank();
        cardRepository.saveAndFlush(card);
        Long bankId = bank.getId();

        // Get all the cardList where bank equals to bankId
        defaultCardShouldBeFound("bankId.equals=" + bankId);

        // Get all the cardList where bank equals to bankId + 1
        defaultCardShouldNotBeFound("bankId.equals=" + (bankId + 1));
    }


    @Test
    @Transactional
    public void getAllCardsByBankAccountIsEqualToSomething() throws Exception {
        // Initialize the database
        BankAccount bankAccount = BankAccountResourceIT.createEntity(em);
        em.persist(bankAccount);
        em.flush();
        card.setBankAccount(bankAccount);
        cardRepository.saveAndFlush(card);
        Long bankAccountId = bankAccount.getId();

        // Get all the cardList where bankAccount equals to bankAccountId
        defaultCardShouldBeFound("bankAccountId.equals=" + bankAccountId);

        // Get all the cardList where bankAccount equals to bankAccountId + 1
        defaultCardShouldNotBeFound("bankAccountId.equals=" + (bankAccountId + 1));
    }


    @Test
    @Transactional
    public void getAllCardsByCardGroupIsEqualToSomething() throws Exception {
        // Initialize the database
        CardGroup cardGroup = CardGroupResourceIT.createEntity(em);
        em.persist(cardGroup);
        em.flush();
        card.setCardGroup(cardGroup);
        cardRepository.saveAndFlush(card);
        Long cardGroupId = cardGroup.getId();

        // Get all the cardList where cardGroup equals to cardGroupId
        defaultCardShouldBeFound("cardGroupId.equals=" + cardGroupId);

        // Get all the cardList where cardGroup equals to cardGroupId + 1
        defaultCardShouldNotBeFound("cardGroupId.equals=" + (cardGroupId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultCardShouldBeFound(String filter) throws Exception {
        restCardMockMvc.perform(get("/api/cards?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(card.getId().intValue())))
            .andExpect(jsonPath("$.[*].cardType").value(hasItem(DEFAULT_CARD_TYPE.toString())))
            .andExpect(jsonPath("$.[*].paymentNetwork").value(hasItem(DEFAULT_PAYMENT_NETWORK.toString())))
            .andExpect(jsonPath("$.[*].cardNumber").value(hasItem(DEFAULT_CARD_NUMBER)))
            .andExpect(jsonPath("$.[*].validFrom").value(hasItem(DEFAULT_VALID_FROM)))
            .andExpect(jsonPath("$.[*].validTill").value(hasItem(DEFAULT_VALID_TILL)))
            .andExpect(jsonPath("$.[*].paymentNetworkPin").value(hasItem(DEFAULT_PAYMENT_NETWORK_PIN)))
            .andExpect(jsonPath("$.[*].cvv").value(hasItem(DEFAULT_CVV)))
            .andExpect(jsonPath("$.[*].nameOnCard").value(hasItem(DEFAULT_NAME_ON_CARD)))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS)))
            .andExpect(jsonPath("$.[*].pin").value(hasItem(DEFAULT_PIN)))
            .andExpect(jsonPath("$.[*].tpin").value(hasItem(DEFAULT_TPIN)));

        // Check, that the count call also returns 1
        restCardMockMvc.perform(get("/api/cards/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultCardShouldNotBeFound(String filter) throws Exception {
        restCardMockMvc.perform(get("/api/cards?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restCardMockMvc.perform(get("/api/cards/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingCard() throws Exception {
        // Get the card
        restCardMockMvc.perform(get("/api/cards/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCard() throws Exception {
        // Initialize the database
        cardRepository.saveAndFlush(card);

        int databaseSizeBeforeUpdate = cardRepository.findAll().size();

        // Update the card
        Card updatedCard = cardRepository.findById(card.getId()).get();
        // Disconnect from session so that the updates on updatedCard are not directly saved in db
        em.detach(updatedCard);
        updatedCard
            .cardType(UPDATED_CARD_TYPE)
            .paymentNetwork(UPDATED_PAYMENT_NETWORK)
            .cardNumber(UPDATED_CARD_NUMBER)
            .validFrom(UPDATED_VALID_FROM)
            .validTill(UPDATED_VALID_TILL)
            .paymentNetworkPin(UPDATED_PAYMENT_NETWORK_PIN)
            .cvv(UPDATED_CVV)
            .nameOnCard(UPDATED_NAME_ON_CARD)
            .remarks(UPDATED_REMARKS)
            .pin(UPDATED_PIN)
            .tpin(UPDATED_TPIN);
        CardDTO cardDTO = cardMapper.toDto(updatedCard);

        restCardMockMvc.perform(put("/api/cards")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cardDTO)))
            .andExpect(status().isOk());

        // Validate the Card in the database
        List<Card> cardList = cardRepository.findAll();
        assertThat(cardList).hasSize(databaseSizeBeforeUpdate);
        Card testCard = cardList.get(cardList.size() - 1);
        assertThat(testCard.getCardType()).isEqualTo(UPDATED_CARD_TYPE);
        assertThat(testCard.getPaymentNetwork()).isEqualTo(UPDATED_PAYMENT_NETWORK);
        assertThat(testCard.getCardNumber()).isEqualTo(UPDATED_CARD_NUMBER);
        assertThat(testCard.getValidFrom()).isEqualTo(UPDATED_VALID_FROM);
        assertThat(testCard.getValidTill()).isEqualTo(UPDATED_VALID_TILL);
        assertThat(testCard.getPaymentNetworkPin()).isEqualTo(UPDATED_PAYMENT_NETWORK_PIN);
        assertThat(testCard.getCvv()).isEqualTo(UPDATED_CVV);
        assertThat(testCard.getNameOnCard()).isEqualTo(UPDATED_NAME_ON_CARD);
        assertThat(testCard.getRemarks()).isEqualTo(UPDATED_REMARKS);
        assertThat(testCard.getPin()).isEqualTo(UPDATED_PIN);
        assertThat(testCard.getTpin()).isEqualTo(UPDATED_TPIN);
    }

    @Test
    @Transactional
    public void updateNonExistingCard() throws Exception {
        int databaseSizeBeforeUpdate = cardRepository.findAll().size();

        // Create the Card
        CardDTO cardDTO = cardMapper.toDto(card);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCardMockMvc.perform(put("/api/cards")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cardDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Card in the database
        List<Card> cardList = cardRepository.findAll();
        assertThat(cardList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCard() throws Exception {
        // Initialize the database
        cardRepository.saveAndFlush(card);

        int databaseSizeBeforeDelete = cardRepository.findAll().size();

        // Delete the card
        restCardMockMvc.perform(delete("/api/cards/{id}", card.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Card> cardList = cardRepository.findAll();
        assertThat(cardList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Card.class);
        Card card1 = new Card();
        card1.setId(1L);
        Card card2 = new Card();
        card2.setId(card1.getId());
        assertThat(card1).isEqualTo(card2);
        card2.setId(2L);
        assertThat(card1).isNotEqualTo(card2);
        card1.setId(null);
        assertThat(card1).isNotEqualTo(card2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CardDTO.class);
        CardDTO cardDTO1 = new CardDTO();
        cardDTO1.setId(1L);
        CardDTO cardDTO2 = new CardDTO();
        assertThat(cardDTO1).isNotEqualTo(cardDTO2);
        cardDTO2.setId(cardDTO1.getId());
        assertThat(cardDTO1).isEqualTo(cardDTO2);
        cardDTO2.setId(2L);
        assertThat(cardDTO1).isNotEqualTo(cardDTO2);
        cardDTO1.setId(null);
        assertThat(cardDTO1).isNotEqualTo(cardDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(cardMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(cardMapper.fromId(null)).isNull();
    }
}
