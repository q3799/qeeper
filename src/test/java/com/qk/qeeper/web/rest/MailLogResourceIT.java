package com.qk.qeeper.web.rest;

import com.qk.qeeper.QeeperApp;
import com.qk.qeeper.domain.MailLog;
import com.qk.qeeper.repository.MailLogRepository;
import com.qk.qeeper.service.MailLogService;
import com.qk.qeeper.service.dto.MailLogDTO;
import com.qk.qeeper.service.mapper.MailLogMapper;
import com.qk.qeeper.web.rest.errors.ExceptionTranslator;
import com.qk.qeeper.service.dto.MailLogCriteria;
import com.qk.qeeper.service.MailLogQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.qk.qeeper.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.qk.qeeper.domain.enumeration.MailServer;
/**
 * Integration tests for the {@Link MailLogResource} REST controller.
 */
@SpringBootTest(classes = QeeperApp.class)
public class MailLogResourceIT {

    private static final String DEFAULT_SUBJECT = "AAAAAAAAAA";
    private static final String UPDATED_SUBJECT = "BBBBBBBBBB";

    private static final String DEFAULT_CONTENT = "AAAAAAAAAA";
    private static final String UPDATED_CONTENT = "BBBBBBBBBB";

    private static final Instant DEFAULT_MAIL_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_MAIL_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final MailServer DEFAULT_MAIL_SERVER = MailServer.PRIMARY;
    private static final MailServer UPDATED_MAIL_SERVER = MailServer.SECONDARY;

    private static final String DEFAULT_RESPONSE_RECEIVED = "AAAAAAAAAA";
    private static final String UPDATED_RESPONSE_RECEIVED = "BBBBBBBBBB";

    private static final String DEFAULT_REMARKS = "AAAAAAAAAA";
    private static final String UPDATED_REMARKS = "BBBBBBBBBB";

    private static final String DEFAULT_TO_LIST = "AAAAAAAAAA";
    private static final String UPDATED_TO_LIST = "BBBBBBBBBB";

    private static final String DEFAULT_CC_LIST = "AAAAAAAAAA";
    private static final String UPDATED_CC_LIST = "BBBBBBBBBB";

    private static final Long DEFAULT_PROCESSING_TIME = 0L;
    private static final Long UPDATED_PROCESSING_TIME = 1L;

    @Autowired
    private MailLogRepository mailLogRepository;

    @Autowired
    private MailLogMapper mailLogMapper;

    @Autowired
    private MailLogService mailLogService;

    @Autowired
    private MailLogQueryService mailLogQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restMailLogMockMvc;

    private MailLog mailLog;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MailLogResource mailLogResource = new MailLogResource(mailLogService, mailLogQueryService);
        this.restMailLogMockMvc = MockMvcBuilders.standaloneSetup(mailLogResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MailLog createEntity(EntityManager em) {
        MailLog mailLog = new MailLog()
            .subject(DEFAULT_SUBJECT)
            .content(DEFAULT_CONTENT)
            .mailDate(DEFAULT_MAIL_DATE)
            .mailServer(DEFAULT_MAIL_SERVER)
            .responseReceived(DEFAULT_RESPONSE_RECEIVED)
            .remarks(DEFAULT_REMARKS)
            .toList(DEFAULT_TO_LIST)
            .ccList(DEFAULT_CC_LIST)
            .processingTime(DEFAULT_PROCESSING_TIME);
        return mailLog;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MailLog createUpdatedEntity(EntityManager em) {
        MailLog mailLog = new MailLog()
            .subject(UPDATED_SUBJECT)
            .content(UPDATED_CONTENT)
            .mailDate(UPDATED_MAIL_DATE)
            .mailServer(UPDATED_MAIL_SERVER)
            .responseReceived(UPDATED_RESPONSE_RECEIVED)
            .remarks(UPDATED_REMARKS)
            .toList(UPDATED_TO_LIST)
            .ccList(UPDATED_CC_LIST)
            .processingTime(UPDATED_PROCESSING_TIME);
        return mailLog;
    }

    @BeforeEach
    public void initTest() {
        mailLog = createEntity(em);
    }

    @Test
    @Transactional
    public void createMailLog() throws Exception {
        int databaseSizeBeforeCreate = mailLogRepository.findAll().size();

        // Create the MailLog
        MailLogDTO mailLogDTO = mailLogMapper.toDto(mailLog);
        restMailLogMockMvc.perform(post("/api/mail-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(mailLogDTO)))
            .andExpect(status().isCreated());

        // Validate the MailLog in the database
        List<MailLog> mailLogList = mailLogRepository.findAll();
        assertThat(mailLogList).hasSize(databaseSizeBeforeCreate + 1);
        MailLog testMailLog = mailLogList.get(mailLogList.size() - 1);
        assertThat(testMailLog.getSubject()).isEqualTo(DEFAULT_SUBJECT);
        assertThat(testMailLog.getContent()).isEqualTo(DEFAULT_CONTENT);
        assertThat(testMailLog.getMailDate()).isEqualTo(DEFAULT_MAIL_DATE);
        assertThat(testMailLog.getMailServer()).isEqualTo(DEFAULT_MAIL_SERVER);
        assertThat(testMailLog.getResponseReceived()).isEqualTo(DEFAULT_RESPONSE_RECEIVED);
        assertThat(testMailLog.getRemarks()).isEqualTo(DEFAULT_REMARKS);
        assertThat(testMailLog.getToList()).isEqualTo(DEFAULT_TO_LIST);
        assertThat(testMailLog.getCcList()).isEqualTo(DEFAULT_CC_LIST);
        assertThat(testMailLog.getProcessingTime()).isEqualTo(DEFAULT_PROCESSING_TIME);
    }

    @Test
    @Transactional
    public void createMailLogWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = mailLogRepository.findAll().size();

        // Create the MailLog with an existing ID
        mailLog.setId(1L);
        MailLogDTO mailLogDTO = mailLogMapper.toDto(mailLog);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMailLogMockMvc.perform(post("/api/mail-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(mailLogDTO)))
            .andExpect(status().isBadRequest());

        // Validate the MailLog in the database
        List<MailLog> mailLogList = mailLogRepository.findAll();
        assertThat(mailLogList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkSubjectIsRequired() throws Exception {
        int databaseSizeBeforeTest = mailLogRepository.findAll().size();
        // set the field null
        mailLog.setSubject(null);

        // Create the MailLog, which fails.
        MailLogDTO mailLogDTO = mailLogMapper.toDto(mailLog);

        restMailLogMockMvc.perform(post("/api/mail-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(mailLogDTO)))
            .andExpect(status().isBadRequest());

        List<MailLog> mailLogList = mailLogRepository.findAll();
        assertThat(mailLogList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllMailLogs() throws Exception {
        // Initialize the database
        mailLogRepository.saveAndFlush(mailLog);

        // Get all the mailLogList
        restMailLogMockMvc.perform(get("/api/mail-logs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(mailLog.getId().intValue())))
            .andExpect(jsonPath("$.[*].subject").value(hasItem(DEFAULT_SUBJECT.toString())))
            .andExpect(jsonPath("$.[*].content").value(hasItem(DEFAULT_CONTENT.toString())))
            .andExpect(jsonPath("$.[*].mailDate").value(hasItem(DEFAULT_MAIL_DATE.toString())))
            .andExpect(jsonPath("$.[*].mailServer").value(hasItem(DEFAULT_MAIL_SERVER.toString())))
            .andExpect(jsonPath("$.[*].responseReceived").value(hasItem(DEFAULT_RESPONSE_RECEIVED.toString())))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS.toString())))
            .andExpect(jsonPath("$.[*].toList").value(hasItem(DEFAULT_TO_LIST.toString())))
            .andExpect(jsonPath("$.[*].ccList").value(hasItem(DEFAULT_CC_LIST.toString())))
            .andExpect(jsonPath("$.[*].processingTime").value(hasItem(DEFAULT_PROCESSING_TIME.intValue())));
    }
    
    @Test
    @Transactional
    public void getMailLog() throws Exception {
        // Initialize the database
        mailLogRepository.saveAndFlush(mailLog);

        // Get the mailLog
        restMailLogMockMvc.perform(get("/api/mail-logs/{id}", mailLog.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(mailLog.getId().intValue()))
            .andExpect(jsonPath("$.subject").value(DEFAULT_SUBJECT.toString()))
            .andExpect(jsonPath("$.content").value(DEFAULT_CONTENT.toString()))
            .andExpect(jsonPath("$.mailDate").value(DEFAULT_MAIL_DATE.toString()))
            .andExpect(jsonPath("$.mailServer").value(DEFAULT_MAIL_SERVER.toString()))
            .andExpect(jsonPath("$.responseReceived").value(DEFAULT_RESPONSE_RECEIVED.toString()))
            .andExpect(jsonPath("$.remarks").value(DEFAULT_REMARKS.toString()))
            .andExpect(jsonPath("$.toList").value(DEFAULT_TO_LIST.toString()))
            .andExpect(jsonPath("$.ccList").value(DEFAULT_CC_LIST.toString()))
            .andExpect(jsonPath("$.processingTime").value(DEFAULT_PROCESSING_TIME.intValue()));
    }

    @Test
    @Transactional
    public void getAllMailLogsBySubjectIsEqualToSomething() throws Exception {
        // Initialize the database
        mailLogRepository.saveAndFlush(mailLog);

        // Get all the mailLogList where subject equals to DEFAULT_SUBJECT
        defaultMailLogShouldBeFound("subject.equals=" + DEFAULT_SUBJECT);

        // Get all the mailLogList where subject equals to UPDATED_SUBJECT
        defaultMailLogShouldNotBeFound("subject.equals=" + UPDATED_SUBJECT);
    }

    @Test
    @Transactional
    public void getAllMailLogsBySubjectIsInShouldWork() throws Exception {
        // Initialize the database
        mailLogRepository.saveAndFlush(mailLog);

        // Get all the mailLogList where subject in DEFAULT_SUBJECT or UPDATED_SUBJECT
        defaultMailLogShouldBeFound("subject.in=" + DEFAULT_SUBJECT + "," + UPDATED_SUBJECT);

        // Get all the mailLogList where subject equals to UPDATED_SUBJECT
        defaultMailLogShouldNotBeFound("subject.in=" + UPDATED_SUBJECT);
    }

    @Test
    @Transactional
    public void getAllMailLogsBySubjectIsNullOrNotNull() throws Exception {
        // Initialize the database
        mailLogRepository.saveAndFlush(mailLog);

        // Get all the mailLogList where subject is not null
        defaultMailLogShouldBeFound("subject.specified=true");

        // Get all the mailLogList where subject is null
        defaultMailLogShouldNotBeFound("subject.specified=false");
    }

    @Test
    @Transactional
    public void getAllMailLogsByContentIsEqualToSomething() throws Exception {
        // Initialize the database
        mailLogRepository.saveAndFlush(mailLog);

        // Get all the mailLogList where content equals to DEFAULT_CONTENT
        defaultMailLogShouldBeFound("content.equals=" + DEFAULT_CONTENT);

        // Get all the mailLogList where content equals to UPDATED_CONTENT
        defaultMailLogShouldNotBeFound("content.equals=" + UPDATED_CONTENT);
    }

    @Test
    @Transactional
    public void getAllMailLogsByContentIsInShouldWork() throws Exception {
        // Initialize the database
        mailLogRepository.saveAndFlush(mailLog);

        // Get all the mailLogList where content in DEFAULT_CONTENT or UPDATED_CONTENT
        defaultMailLogShouldBeFound("content.in=" + DEFAULT_CONTENT + "," + UPDATED_CONTENT);

        // Get all the mailLogList where content equals to UPDATED_CONTENT
        defaultMailLogShouldNotBeFound("content.in=" + UPDATED_CONTENT);
    }

    @Test
    @Transactional
    public void getAllMailLogsByContentIsNullOrNotNull() throws Exception {
        // Initialize the database
        mailLogRepository.saveAndFlush(mailLog);

        // Get all the mailLogList where content is not null
        defaultMailLogShouldBeFound("content.specified=true");

        // Get all the mailLogList where content is null
        defaultMailLogShouldNotBeFound("content.specified=false");
    }

    @Test
    @Transactional
    public void getAllMailLogsByMailDateIsEqualToSomething() throws Exception {
        // Initialize the database
        mailLogRepository.saveAndFlush(mailLog);

        // Get all the mailLogList where mailDate equals to DEFAULT_MAIL_DATE
        defaultMailLogShouldBeFound("mailDate.equals=" + DEFAULT_MAIL_DATE);

        // Get all the mailLogList where mailDate equals to UPDATED_MAIL_DATE
        defaultMailLogShouldNotBeFound("mailDate.equals=" + UPDATED_MAIL_DATE);
    }

    @Test
    @Transactional
    public void getAllMailLogsByMailDateIsInShouldWork() throws Exception {
        // Initialize the database
        mailLogRepository.saveAndFlush(mailLog);

        // Get all the mailLogList where mailDate in DEFAULT_MAIL_DATE or UPDATED_MAIL_DATE
        defaultMailLogShouldBeFound("mailDate.in=" + DEFAULT_MAIL_DATE + "," + UPDATED_MAIL_DATE);

        // Get all the mailLogList where mailDate equals to UPDATED_MAIL_DATE
        defaultMailLogShouldNotBeFound("mailDate.in=" + UPDATED_MAIL_DATE);
    }

    @Test
    @Transactional
    public void getAllMailLogsByMailDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        mailLogRepository.saveAndFlush(mailLog);

        // Get all the mailLogList where mailDate is not null
        defaultMailLogShouldBeFound("mailDate.specified=true");

        // Get all the mailLogList where mailDate is null
        defaultMailLogShouldNotBeFound("mailDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllMailLogsByMailServerIsEqualToSomething() throws Exception {
        // Initialize the database
        mailLogRepository.saveAndFlush(mailLog);

        // Get all the mailLogList where mailServer equals to DEFAULT_MAIL_SERVER
        defaultMailLogShouldBeFound("mailServer.equals=" + DEFAULT_MAIL_SERVER);

        // Get all the mailLogList where mailServer equals to UPDATED_MAIL_SERVER
        defaultMailLogShouldNotBeFound("mailServer.equals=" + UPDATED_MAIL_SERVER);
    }

    @Test
    @Transactional
    public void getAllMailLogsByMailServerIsInShouldWork() throws Exception {
        // Initialize the database
        mailLogRepository.saveAndFlush(mailLog);

        // Get all the mailLogList where mailServer in DEFAULT_MAIL_SERVER or UPDATED_MAIL_SERVER
        defaultMailLogShouldBeFound("mailServer.in=" + DEFAULT_MAIL_SERVER + "," + UPDATED_MAIL_SERVER);

        // Get all the mailLogList where mailServer equals to UPDATED_MAIL_SERVER
        defaultMailLogShouldNotBeFound("mailServer.in=" + UPDATED_MAIL_SERVER);
    }

    @Test
    @Transactional
    public void getAllMailLogsByMailServerIsNullOrNotNull() throws Exception {
        // Initialize the database
        mailLogRepository.saveAndFlush(mailLog);

        // Get all the mailLogList where mailServer is not null
        defaultMailLogShouldBeFound("mailServer.specified=true");

        // Get all the mailLogList where mailServer is null
        defaultMailLogShouldNotBeFound("mailServer.specified=false");
    }

    @Test
    @Transactional
    public void getAllMailLogsByResponseReceivedIsEqualToSomething() throws Exception {
        // Initialize the database
        mailLogRepository.saveAndFlush(mailLog);

        // Get all the mailLogList where responseReceived equals to DEFAULT_RESPONSE_RECEIVED
        defaultMailLogShouldBeFound("responseReceived.equals=" + DEFAULT_RESPONSE_RECEIVED);

        // Get all the mailLogList where responseReceived equals to UPDATED_RESPONSE_RECEIVED
        defaultMailLogShouldNotBeFound("responseReceived.equals=" + UPDATED_RESPONSE_RECEIVED);
    }

    @Test
    @Transactional
    public void getAllMailLogsByResponseReceivedIsInShouldWork() throws Exception {
        // Initialize the database
        mailLogRepository.saveAndFlush(mailLog);

        // Get all the mailLogList where responseReceived in DEFAULT_RESPONSE_RECEIVED or UPDATED_RESPONSE_RECEIVED
        defaultMailLogShouldBeFound("responseReceived.in=" + DEFAULT_RESPONSE_RECEIVED + "," + UPDATED_RESPONSE_RECEIVED);

        // Get all the mailLogList where responseReceived equals to UPDATED_RESPONSE_RECEIVED
        defaultMailLogShouldNotBeFound("responseReceived.in=" + UPDATED_RESPONSE_RECEIVED);
    }

    @Test
    @Transactional
    public void getAllMailLogsByResponseReceivedIsNullOrNotNull() throws Exception {
        // Initialize the database
        mailLogRepository.saveAndFlush(mailLog);

        // Get all the mailLogList where responseReceived is not null
        defaultMailLogShouldBeFound("responseReceived.specified=true");

        // Get all the mailLogList where responseReceived is null
        defaultMailLogShouldNotBeFound("responseReceived.specified=false");
    }

    @Test
    @Transactional
    public void getAllMailLogsByRemarksIsEqualToSomething() throws Exception {
        // Initialize the database
        mailLogRepository.saveAndFlush(mailLog);

        // Get all the mailLogList where remarks equals to DEFAULT_REMARKS
        defaultMailLogShouldBeFound("remarks.equals=" + DEFAULT_REMARKS);

        // Get all the mailLogList where remarks equals to UPDATED_REMARKS
        defaultMailLogShouldNotBeFound("remarks.equals=" + UPDATED_REMARKS);
    }

    @Test
    @Transactional
    public void getAllMailLogsByRemarksIsInShouldWork() throws Exception {
        // Initialize the database
        mailLogRepository.saveAndFlush(mailLog);

        // Get all the mailLogList where remarks in DEFAULT_REMARKS or UPDATED_REMARKS
        defaultMailLogShouldBeFound("remarks.in=" + DEFAULT_REMARKS + "," + UPDATED_REMARKS);

        // Get all the mailLogList where remarks equals to UPDATED_REMARKS
        defaultMailLogShouldNotBeFound("remarks.in=" + UPDATED_REMARKS);
    }

    @Test
    @Transactional
    public void getAllMailLogsByRemarksIsNullOrNotNull() throws Exception {
        // Initialize the database
        mailLogRepository.saveAndFlush(mailLog);

        // Get all the mailLogList where remarks is not null
        defaultMailLogShouldBeFound("remarks.specified=true");

        // Get all the mailLogList where remarks is null
        defaultMailLogShouldNotBeFound("remarks.specified=false");
    }

    @Test
    @Transactional
    public void getAllMailLogsByToListIsEqualToSomething() throws Exception {
        // Initialize the database
        mailLogRepository.saveAndFlush(mailLog);

        // Get all the mailLogList where toList equals to DEFAULT_TO_LIST
        defaultMailLogShouldBeFound("toList.equals=" + DEFAULT_TO_LIST);

        // Get all the mailLogList where toList equals to UPDATED_TO_LIST
        defaultMailLogShouldNotBeFound("toList.equals=" + UPDATED_TO_LIST);
    }

    @Test
    @Transactional
    public void getAllMailLogsByToListIsInShouldWork() throws Exception {
        // Initialize the database
        mailLogRepository.saveAndFlush(mailLog);

        // Get all the mailLogList where toList in DEFAULT_TO_LIST or UPDATED_TO_LIST
        defaultMailLogShouldBeFound("toList.in=" + DEFAULT_TO_LIST + "," + UPDATED_TO_LIST);

        // Get all the mailLogList where toList equals to UPDATED_TO_LIST
        defaultMailLogShouldNotBeFound("toList.in=" + UPDATED_TO_LIST);
    }

    @Test
    @Transactional
    public void getAllMailLogsByToListIsNullOrNotNull() throws Exception {
        // Initialize the database
        mailLogRepository.saveAndFlush(mailLog);

        // Get all the mailLogList where toList is not null
        defaultMailLogShouldBeFound("toList.specified=true");

        // Get all the mailLogList where toList is null
        defaultMailLogShouldNotBeFound("toList.specified=false");
    }

    @Test
    @Transactional
    public void getAllMailLogsByCcListIsEqualToSomething() throws Exception {
        // Initialize the database
        mailLogRepository.saveAndFlush(mailLog);

        // Get all the mailLogList where ccList equals to DEFAULT_CC_LIST
        defaultMailLogShouldBeFound("ccList.equals=" + DEFAULT_CC_LIST);

        // Get all the mailLogList where ccList equals to UPDATED_CC_LIST
        defaultMailLogShouldNotBeFound("ccList.equals=" + UPDATED_CC_LIST);
    }

    @Test
    @Transactional
    public void getAllMailLogsByCcListIsInShouldWork() throws Exception {
        // Initialize the database
        mailLogRepository.saveAndFlush(mailLog);

        // Get all the mailLogList where ccList in DEFAULT_CC_LIST or UPDATED_CC_LIST
        defaultMailLogShouldBeFound("ccList.in=" + DEFAULT_CC_LIST + "," + UPDATED_CC_LIST);

        // Get all the mailLogList where ccList equals to UPDATED_CC_LIST
        defaultMailLogShouldNotBeFound("ccList.in=" + UPDATED_CC_LIST);
    }

    @Test
    @Transactional
    public void getAllMailLogsByCcListIsNullOrNotNull() throws Exception {
        // Initialize the database
        mailLogRepository.saveAndFlush(mailLog);

        // Get all the mailLogList where ccList is not null
        defaultMailLogShouldBeFound("ccList.specified=true");

        // Get all the mailLogList where ccList is null
        defaultMailLogShouldNotBeFound("ccList.specified=false");
    }

    @Test
    @Transactional
    public void getAllMailLogsByProcessingTimeIsEqualToSomething() throws Exception {
        // Initialize the database
        mailLogRepository.saveAndFlush(mailLog);

        // Get all the mailLogList where processingTime equals to DEFAULT_PROCESSING_TIME
        defaultMailLogShouldBeFound("processingTime.equals=" + DEFAULT_PROCESSING_TIME);

        // Get all the mailLogList where processingTime equals to UPDATED_PROCESSING_TIME
        defaultMailLogShouldNotBeFound("processingTime.equals=" + UPDATED_PROCESSING_TIME);
    }

    @Test
    @Transactional
    public void getAllMailLogsByProcessingTimeIsInShouldWork() throws Exception {
        // Initialize the database
        mailLogRepository.saveAndFlush(mailLog);

        // Get all the mailLogList where processingTime in DEFAULT_PROCESSING_TIME or UPDATED_PROCESSING_TIME
        defaultMailLogShouldBeFound("processingTime.in=" + DEFAULT_PROCESSING_TIME + "," + UPDATED_PROCESSING_TIME);

        // Get all the mailLogList where processingTime equals to UPDATED_PROCESSING_TIME
        defaultMailLogShouldNotBeFound("processingTime.in=" + UPDATED_PROCESSING_TIME);
    }

    @Test
    @Transactional
    public void getAllMailLogsByProcessingTimeIsNullOrNotNull() throws Exception {
        // Initialize the database
        mailLogRepository.saveAndFlush(mailLog);

        // Get all the mailLogList where processingTime is not null
        defaultMailLogShouldBeFound("processingTime.specified=true");

        // Get all the mailLogList where processingTime is null
        defaultMailLogShouldNotBeFound("processingTime.specified=false");
    }

    @Test
    @Transactional
    public void getAllMailLogsByProcessingTimeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mailLogRepository.saveAndFlush(mailLog);

        // Get all the mailLogList where processingTime greater than or equals to DEFAULT_PROCESSING_TIME
        defaultMailLogShouldBeFound("processingTime.greaterOrEqualThan=" + DEFAULT_PROCESSING_TIME);

        // Get all the mailLogList where processingTime greater than or equals to UPDATED_PROCESSING_TIME
        defaultMailLogShouldNotBeFound("processingTime.greaterOrEqualThan=" + UPDATED_PROCESSING_TIME);
    }

    @Test
    @Transactional
    public void getAllMailLogsByProcessingTimeIsLessThanSomething() throws Exception {
        // Initialize the database
        mailLogRepository.saveAndFlush(mailLog);

        // Get all the mailLogList where processingTime less than or equals to DEFAULT_PROCESSING_TIME
        defaultMailLogShouldNotBeFound("processingTime.lessThan=" + DEFAULT_PROCESSING_TIME);

        // Get all the mailLogList where processingTime less than or equals to UPDATED_PROCESSING_TIME
        defaultMailLogShouldBeFound("processingTime.lessThan=" + UPDATED_PROCESSING_TIME);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultMailLogShouldBeFound(String filter) throws Exception {
        restMailLogMockMvc.perform(get("/api/mail-logs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(mailLog.getId().intValue())))
            .andExpect(jsonPath("$.[*].subject").value(hasItem(DEFAULT_SUBJECT)))
            .andExpect(jsonPath("$.[*].content").value(hasItem(DEFAULT_CONTENT)))
            .andExpect(jsonPath("$.[*].mailDate").value(hasItem(DEFAULT_MAIL_DATE.toString())))
            .andExpect(jsonPath("$.[*].mailServer").value(hasItem(DEFAULT_MAIL_SERVER.toString())))
            .andExpect(jsonPath("$.[*].responseReceived").value(hasItem(DEFAULT_RESPONSE_RECEIVED)))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS)))
            .andExpect(jsonPath("$.[*].toList").value(hasItem(DEFAULT_TO_LIST)))
            .andExpect(jsonPath("$.[*].ccList").value(hasItem(DEFAULT_CC_LIST)))
            .andExpect(jsonPath("$.[*].processingTime").value(hasItem(DEFAULT_PROCESSING_TIME.intValue())));

        // Check, that the count call also returns 1
        restMailLogMockMvc.perform(get("/api/mail-logs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultMailLogShouldNotBeFound(String filter) throws Exception {
        restMailLogMockMvc.perform(get("/api/mail-logs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restMailLogMockMvc.perform(get("/api/mail-logs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingMailLog() throws Exception {
        // Get the mailLog
        restMailLogMockMvc.perform(get("/api/mail-logs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMailLog() throws Exception {
        // Initialize the database
        mailLogRepository.saveAndFlush(mailLog);

        int databaseSizeBeforeUpdate = mailLogRepository.findAll().size();

        // Update the mailLog
        MailLog updatedMailLog = mailLogRepository.findById(mailLog.getId()).get();
        // Disconnect from session so that the updates on updatedMailLog are not directly saved in db
        em.detach(updatedMailLog);
        updatedMailLog
            .subject(UPDATED_SUBJECT)
            .content(UPDATED_CONTENT)
            .mailDate(UPDATED_MAIL_DATE)
            .mailServer(UPDATED_MAIL_SERVER)
            .responseReceived(UPDATED_RESPONSE_RECEIVED)
            .remarks(UPDATED_REMARKS)
            .toList(UPDATED_TO_LIST)
            .ccList(UPDATED_CC_LIST)
            .processingTime(UPDATED_PROCESSING_TIME);
        MailLogDTO mailLogDTO = mailLogMapper.toDto(updatedMailLog);

        restMailLogMockMvc.perform(put("/api/mail-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(mailLogDTO)))
            .andExpect(status().isOk());

        // Validate the MailLog in the database
        List<MailLog> mailLogList = mailLogRepository.findAll();
        assertThat(mailLogList).hasSize(databaseSizeBeforeUpdate);
        MailLog testMailLog = mailLogList.get(mailLogList.size() - 1);
        assertThat(testMailLog.getSubject()).isEqualTo(UPDATED_SUBJECT);
        assertThat(testMailLog.getContent()).isEqualTo(UPDATED_CONTENT);
        assertThat(testMailLog.getMailDate()).isEqualTo(UPDATED_MAIL_DATE);
        assertThat(testMailLog.getMailServer()).isEqualTo(UPDATED_MAIL_SERVER);
        assertThat(testMailLog.getResponseReceived()).isEqualTo(UPDATED_RESPONSE_RECEIVED);
        assertThat(testMailLog.getRemarks()).isEqualTo(UPDATED_REMARKS);
        assertThat(testMailLog.getToList()).isEqualTo(UPDATED_TO_LIST);
        assertThat(testMailLog.getCcList()).isEqualTo(UPDATED_CC_LIST);
        assertThat(testMailLog.getProcessingTime()).isEqualTo(UPDATED_PROCESSING_TIME);
    }

    @Test
    @Transactional
    public void updateNonExistingMailLog() throws Exception {
        int databaseSizeBeforeUpdate = mailLogRepository.findAll().size();

        // Create the MailLog
        MailLogDTO mailLogDTO = mailLogMapper.toDto(mailLog);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMailLogMockMvc.perform(put("/api/mail-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(mailLogDTO)))
            .andExpect(status().isBadRequest());

        // Validate the MailLog in the database
        List<MailLog> mailLogList = mailLogRepository.findAll();
        assertThat(mailLogList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteMailLog() throws Exception {
        // Initialize the database
        mailLogRepository.saveAndFlush(mailLog);

        int databaseSizeBeforeDelete = mailLogRepository.findAll().size();

        // Delete the mailLog
        restMailLogMockMvc.perform(delete("/api/mail-logs/{id}", mailLog.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<MailLog> mailLogList = mailLogRepository.findAll();
        assertThat(mailLogList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MailLog.class);
        MailLog mailLog1 = new MailLog();
        mailLog1.setId(1L);
        MailLog mailLog2 = new MailLog();
        mailLog2.setId(mailLog1.getId());
        assertThat(mailLog1).isEqualTo(mailLog2);
        mailLog2.setId(2L);
        assertThat(mailLog1).isNotEqualTo(mailLog2);
        mailLog1.setId(null);
        assertThat(mailLog1).isNotEqualTo(mailLog2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(MailLogDTO.class);
        MailLogDTO mailLogDTO1 = new MailLogDTO();
        mailLogDTO1.setId(1L);
        MailLogDTO mailLogDTO2 = new MailLogDTO();
        assertThat(mailLogDTO1).isNotEqualTo(mailLogDTO2);
        mailLogDTO2.setId(mailLogDTO1.getId());
        assertThat(mailLogDTO1).isEqualTo(mailLogDTO2);
        mailLogDTO2.setId(2L);
        assertThat(mailLogDTO1).isNotEqualTo(mailLogDTO2);
        mailLogDTO1.setId(null);
        assertThat(mailLogDTO1).isNotEqualTo(mailLogDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(mailLogMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(mailLogMapper.fromId(null)).isNull();
    }
}
